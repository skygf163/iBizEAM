/**
 * 钢丝绳位置
 *
 * @export
 * @interface EMEQLCTGSS
 */
export interface EMEQLCTGSS {

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMEQLCTGSS
     */
    description?: any;

    /**
     * 项目
     *
     * @returns {*}
     * @memberof EMEQLCTGSS
     */
    gssxm?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMEQLCTGSS
     */
    updatedate?: any;

    /**
     * 型号
     *
     * @returns {*}
     * @memberof EMEQLCTGSS
     */
    eqmodelcode?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMEQLCTGSS
     */
    updateman?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMEQLCTGSS
     */
    createdate?: any;

    /**
     * 预警期限(天)
     *
     * @returns {*}
     * @memberof EMEQLCTGSS
     */
    valve?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMEQLCTGSS
     */
    enable?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMEQLCTGSS
     */
    createman?: any;

    /**
     * 长度
     *
     * @returns {*}
     * @memberof EMEQLCTGSS
     */
    len?: any;

    /**
     * 直径
     *
     * @returns {*}
     * @memberof EMEQLCTGSS
     */
    zj?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMEQLCTGSS
     */
    orgid?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEQLCTGSS
     */
    equipname?: any;

    /**
     * 钢丝绳信息
     *
     * @returns {*}
     * @memberof EMEQLCTGSS
     */
    eqlocationinfo?: any;

    /**
     * 位置标识
     *
     * @returns {*}
     * @memberof EMEQLCTGSS
     */
    emeqlocationid?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEQLCTGSS
     */
    equipid?: any;
}