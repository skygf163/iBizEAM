/**
 * 触发记录
 *
 * @export
 * @interface EMPLANRECORD
 */
export interface EMPLANRECORD {

    /**
     * 触发记录标识
     *
     * @returns {*}
     * @memberof EMPLANRECORD
     */
    emplanrecordid?: any;

    /**
     * 触发记录名称
     *
     * @returns {*}
     * @memberof EMPLANRECORD
     */
    emplanrecordname?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMPLANRECORD
     */
    createman?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMPLANRECORD
     */
    createdate?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMPLANRECORD
     */
    updatedate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMPLANRECORD
     */
    updateman?: any;

    /**
     * 是否触发
     *
     * @returns {*}
     * @memberof EMPLANRECORD
     */
    istrigger?: any;

    /**
     * 计划编号
     *
     * @returns {*}
     * @memberof EMPLANRECORD
     */
    emplanid?: any;

    /**
     * 计划名称
     *
     * @returns {*}
     * @memberof EMPLANRECORD
     */
    emplanname?: any;

    /**
     * 原因
     *
     * @returns {*}
     * @memberof EMPLANRECORD
     */
    triggerca?: any;

    /**
     * 结果
     *
     * @returns {*}
     * @memberof EMPLANRECORD
     */
    triggerre?: any;

    /**
     * 备注
     *
     * @returns {*}
     * @memberof EMPLANRECORD
     */
    remark?: any;

    /**
     * 触发时间
     *
     * @returns {*}
     * @memberof EMPLANRECORD
     */
    triggerdate?: any;
}