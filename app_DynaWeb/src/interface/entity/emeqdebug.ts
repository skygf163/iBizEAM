/**
 * 事故记录
 *
 * @export
 * @interface EMEQDebug
 */
export interface EMEQDebug {

    /**
     * 事故结果
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    activeadesc?: any;

    /**
     * 预算(￥)
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    prefee?: any;

    /**
     * 事故记录标识
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    emeqdebugid?: any;

    /**
     * 图片
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    pic2?: any;

    /**
     * 图片
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    pic?: any;

    /**
     * 材料费(￥)
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    mfee?: any;

    /**
     * 事故记录
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    activedesc?: any;

    /**
     * 图片
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    pic3?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    createman?: any;

    /**
     * 事故日期
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    activedate?: any;

    /**
     * 责任部门
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    rdeptname?: any;

    /**
     * 图片
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    pic4?: any;

    /**
     * 起始时间
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    regionbegindate?: any;

    /**
     * 详细内容
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    content?: any;

    /**
     * 人工费(￥)
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    pfee?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    description?: any;

    /**
     * 持续时间(H)
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    activelengths?: any;

    /**
     * 事故记录名称
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    emeqdebugname?: any;

    /**
     * 责任人
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    rempname?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    updatedate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    updateman?: any;

    /**
     * 责任人
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    rempid?: any;

    /**
     * 停运时间(分)
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    eqstoplength?: any;

    /**
     * 结束时间
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    regionenddate?: any;

    /**
     * 事故原因
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    activebdesc?: any;

    /**
     * 责任部门
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    rdeptid?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    enable?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    orgid?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    createdate?: any;

    /**
     * 服务费(￥)
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    sfee?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    acclassname?: any;

    /**
     * 工单
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    woname?: any;

    /**
     * 原因
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    rfocaname?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    rservicename?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    objname?: any;

    /**
     * 方案
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    rfoacname?: any;

    /**
     * 模式
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    rfomoname?: any;

    /**
     * 责任班组
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    rteamname?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    rfodename?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    equipname?: any;

    /**
     * 原因
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    rfocaid?: any;

    /**
     * 方案
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    rfoacid?: any;

    /**
     * 工单
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    woid?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    rfodeid?: any;

    /**
     * 责任班组
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    rteamid?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    rserviceid?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    equipid?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    objid?: any;

    /**
     * 模式
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    rfomoid?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMEQDebug
     */
    acclassid?: any;
}