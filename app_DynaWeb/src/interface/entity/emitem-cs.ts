/**
 * 库间调整单
 *
 * @export
 * @interface EMItemCS
 */
export interface EMItemCS {

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMItemCS
     */
    updatedate?: any;

    /**
     * 调整单信息
     *
     * @returns {*}
     * @memberof EMItemCS
     */
    itemroutinfo?: any;

    /**
     * 单价
     *
     * @returns {*}
     * @memberof EMItemCS
     */
    price?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMItemCS
     */
    createdate?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMItemCS
     */
    createman?: any;

    /**
     * 调整日期
     *
     * @returns {*}
     * @memberof EMItemCS
     */
    sdate?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMItemCS
     */
    enable?: any;

    /**
     * 流程步骤
     *
     * @returns {*}
     * @memberof EMItemCS
     */
    wfstep?: any;

    /**
     * 调整单名称
     *
     * @returns {*}
     * @memberof EMItemCS
     */
    emitemcsname?: any;

    /**
     * 调整单号
     *
     * @returns {*}
     * @memberof EMItemCS
     */
    emitemcsid?: any;

    /**
     * 批次
     *
     * @returns {*}
     * @memberof EMItemCS
     */
    batcode?: any;

    /**
     * 工作流实例
     *
     * @returns {*}
     * @memberof EMItemCS
     */
    wfinstanceid?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMItemCS
     */
    description?: any;

    /**
     * 总金额
     *
     * @returns {*}
     * @memberof EMItemCS
     */
    amount?: any;

    /**
     * 调整状态
     *
     * @returns {*}
     * @memberof EMItemCS
     */
    tradestate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMItemCS
     */
    updateman?: any;

    /**
     * 工作流状态
     *
     * @returns {*}
     * @memberof EMItemCS
     */
    wfstate?: any;

    /**
     * 数量
     *
     * @returns {*}
     * @memberof EMItemCS
     */
    psum?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMItemCS
     */
    orgid?: any;

    /**
     * 库位
     *
     * @returns {*}
     * @memberof EMItemCS
     */
    storepartname?: any;

    /**
     * 入库单
     *
     * @returns {*}
     * @memberof EMItemCS
     */
    rname?: any;

    /**
     * 仓库
     *
     * @returns {*}
     * @memberof EMItemCS
     */
    storename?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMItemCS
     */
    itemname?: any;

    /**
     * 库存
     *
     * @returns {*}
     * @memberof EMItemCS
     */
    stockname?: any;

    /**
     * 仓库
     *
     * @returns {*}
     * @memberof EMItemCS
     */
    storeid?: any;

    /**
     * 入库单
     *
     * @returns {*}
     * @memberof EMItemCS
     */
    rid?: any;

    /**
     * 库位
     *
     * @returns {*}
     * @memberof EMItemCS
     */
    storepartid?: any;

    /**
     * 库存
     *
     * @returns {*}
     * @memberof EMItemCS
     */
    stockid?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMItemCS
     */
    itemid?: any;

    /**
     * 调整人
     *
     * @returns {*}
     * @memberof EMItemCS
     */
    sempid?: any;

    /**
     * 调整人
     *
     * @returns {*}
     * @memberof EMItemCS
     */
    sempname?: any;
}