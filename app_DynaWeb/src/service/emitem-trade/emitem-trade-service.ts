import { Http } from '@/utils';
import { Util } from '@/utils';
import EMItemTradeServiceBase from './emitem-trade-service-base';


/**
 * 物品交易服务对象
 *
 * @export
 * @class EMItemTradeService
 * @extends {EMItemTradeServiceBase}
 */
export default class EMItemTradeService extends EMItemTradeServiceBase {

    /**
     * Creates an instance of  EMItemTradeService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemTradeService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}