import { Http } from '@/utils';
import { Util } from '@/utils';
import EMEQLCTMapServiceBase from './emeqlctmap-service-base';


/**
 * 位置关系服务对象
 *
 * @export
 * @class EMEQLCTMapService
 * @extends {EMEQLCTMapServiceBase}
 */
export default class EMEQLCTMapService extends EMEQLCTMapServiceBase {

    /**
     * Creates an instance of  EMEQLCTMapService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQLCTMapService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}