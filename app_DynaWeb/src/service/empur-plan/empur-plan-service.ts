import { Http } from '@/utils';
import { Util } from '@/utils';
import EMPurPlanServiceBase from './empur-plan-service-base';


/**
 * 计划修理服务对象
 *
 * @export
 * @class EMPurPlanService
 * @extends {EMPurPlanServiceBase}
 */
export default class EMPurPlanService extends EMPurPlanServiceBase {

    /**
     * Creates an instance of  EMPurPlanService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPurPlanService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}