import { Http } from '@/utils';
import { Util } from '@/utils';
import EMBerthServiceBase from './emberth-service-base';


/**
 * 泊位服务对象
 *
 * @export
 * @class EMBerthService
 * @extends {EMBerthServiceBase}
 */
export default class EMBerthService extends EMBerthServiceBase {

    /**
     * Creates an instance of  EMBerthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMBerthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}