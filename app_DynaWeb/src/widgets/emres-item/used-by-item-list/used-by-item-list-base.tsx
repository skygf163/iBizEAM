import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, ListControlBase } from '@/studio-core';
import EMResItemService from '@/service/emres-item/emres-item-service';
import UsedByItemService from './used-by-item-list-service';
import EMResItemUIService from '@/uiservice/emres-item/emres-item-ui-service';

/**
 * dashboard_sysportlet4_list部件基类
 *
 * @export
 * @class ListControlBase
 * @extends {UsedByItemListBase}
 */
export class UsedByItemListBase extends ListControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof UsedByItemListBase
     */
    protected controlType: string = 'LIST';

    /**
     * 建构部件服务对象
     *
     * @type {UsedByItemService}
     * @memberof UsedByItemListBase
     */
    public service: UsedByItemService = new UsedByItemService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMResItemService}
     * @memberof UsedByItemListBase
     */
    public appEntityService: EMResItemService = new EMResItemService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof UsedByItemListBase
     */
    protected appDeName: string = 'emresitem';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof UsedByItemListBase
     */
    protected appDeLogicName: string = '物品资源';

    /**
     * 界面UI服务对象
     *
     * @type {EMResItemUIService}
     * @memberof UsedByItemBase
     */  
    public appUIService: EMResItemUIService = new EMResItemUIService(this.$store);


    /**
     * 分页条数
     *
     * @type {number}
     * @memberof UsedByItemListBase
     */
    public limit: number = 10;

    /**
     * 排序方向
     *
     * @type {string}
     * @memberof UsedByItemListBase
     */
    public minorSortDir: string = 'DESC';

    /**
     * 排序字段
     *
     * @type {string}
     * @memberof UsedByItemListBase
     */
    public minorSortPSDEF: string = 'bdate';




}