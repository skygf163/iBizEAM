/**
 * Info 部件模型
 *
 * @export
 * @class InfoModel
 */
export default class InfoModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof InfoModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'updateman',
      },
      {
        name: 'emeqspare',
        prop: 'emeqspareid',
      },
      {
        name: 'createdate',
      },
      {
        name: 'enable',
      },
      {
        name: 'emeqsparename',
      },
      {
        name: 'createman',
      },
      {
        name: 'orgid',
      },
      {
        name: 'eqsparecode',
      },
      {
        name: 'eqspareinfo',
      },
      {
        name: 'description',
      },
      {
        name: 'updatedate',
      },
    ]
  }


}
