/**
 * Main3 部件模型
 *
 * @export
 * @class Main3Model
 */
export default class Main3Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main3Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emeqtypeid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emeqtypename',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'eqtypecode',
        prop: 'eqtypecode',
        dataType: 'TEXT',
      },
      {
        name: 'emeqtypename',
        prop: 'emeqtypename',
        dataType: 'TEXT',
      },
      {
        name: 'eqtypepname',
        prop: 'eqtypepname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'eqtypegroup',
        prop: 'eqtypegroup',
        dataType: 'SSCODELIST',
      },
      {
        name: 'stype',
        prop: 'stype',
        dataType: 'TEXT',
      },
      {
        name: 'sname',
        prop: 'sname',
        dataType: 'TEXT',
      },
      {
        name: 'eqtypepid',
        prop: 'eqtypepid',
        dataType: 'PICKUP',
      },
      {
        name: 'emeqtypeid',
        prop: 'emeqtypeid',
        dataType: 'GUID',
      },
      {
        name: 'emeqtype',
        prop: 'emeqtypeid',
        dataType: 'FONTKEY',
      },
    ]
  }

}