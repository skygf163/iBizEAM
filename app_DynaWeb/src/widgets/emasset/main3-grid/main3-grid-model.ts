/**
 * Main3 部件模型
 *
 * @export
 * @class Main3Model
 */
export default class Main3Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main3GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main3GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'assetsort',
          prop: 'assetsort',
          dataType: 'TEXT',
        },
        {
          name: 'assetcode',
          prop: 'assetcode',
          dataType: 'TEXT',
        },
        {
          name: 'emassetname',
          prop: 'emassetname',
          dataType: 'TEXT',
        },
        {
          name: 'num',
          prop: 'num',
          dataType: 'TEXT',
        },
        {
          name: 'assettype',
          prop: 'assettype',
          dataType: 'SSCODELIST',
        },
        {
          name: 'assetlct',
          prop: 'assetlct',
          dataType: 'TEXT',
        },
        {
          name: 'eqmodelcode',
          prop: 'eqmodelcode',
          dataType: 'TEXT',
        },
        {
          name: 'assetclassname',
          prop: 'assetclassname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'assetstate',
          prop: 'assetstate',
          dataType: 'SSCODELIST',
        },
        {
          name: 'eqlife',
          prop: 'eqlife',
          dataType: 'FLOAT',
        },
        {
          name: 'eqstartdate',
          prop: 'eqstartdate',
          dataType: 'DATE',
        },
        {
          name: 'originalcost',
          prop: 'originalcost',
          dataType: 'CURRENCY',
        },
        {
          name: 'deptname',
          prop: 'deptname',
          dataType: 'TEXT',
        },
        {
          name: 'purchdate',
          prop: 'purchdate',
          dataType: 'DATE',
        },
        {
          name: 'empname',
          prop: 'empname',
          dataType: 'TEXT',
        },
        {
          name: 'unitname',
          prop: 'unitname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'ytzj',
          prop: 'ytzj',
          dataType: 'FLOAT',
        },
        {
          name: 'replacerate',
          prop: 'replacerate',
          dataType: 'FLOAT',
        },
        {
          name: 'mgrdeptname',
          prop: 'mgrdeptname',
          dataType: 'TEXT',
        },
        {
          name: 'rempname',
          prop: 'rempname',
          dataType: 'TEXT',
        },
        {
          name: 'eqserialcode',
          prop: 'eqserialcode',
          dataType: 'TEXT',
        },
        {
          name: 'blsystemdesc',
          prop: 'blsystemdesc',
          dataType: 'TEXT',
        },
        {
          name: 'keyattparam',
          prop: 'keyattparam',
          dataType: 'TEXT',
        },
        {
          name: 'now',
          prop: 'now',
          dataType: 'FLOAT',
        },
        {
          name: 'replacecost',
          prop: 'replacecost',
          dataType: 'FLOAT',
        },
        {
          name: 'lastzjdate',
          prop: 'lastzjdate',
          dataType: 'DATE',
        },
        {
          name: 'syqx',
          prop: 'syqx',
          dataType: 'TEXT',
        },
        {
          name: 'jtsb',
          prop: 'jtsb',
          dataType: 'TEXT',
        },
        {
          name: 'acclassid',
          prop: 'acclassid',
          dataType: 'PICKUP',
        },
        {
          name: 'assetclassid',
          prop: 'assetclassid',
          dataType: 'PICKUP',
        },
        {
          name: 'contractid',
          prop: 'contractid',
          dataType: 'PICKUP',
        },
        {
          name: 'labserviceid',
          prop: 'labserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'emassetname',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'emassetid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'emassetid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'unitid',
          prop: 'unitid',
          dataType: 'PICKUP',
        },
        {
          name: 'rserviceid',
          prop: 'rserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'mserviceid',
          prop: 'mserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'eqlocationid',
          prop: 'eqlocationid',
          dataType: 'PICKUP',
        },
        {
          name: 'emasset',
          prop: 'emassetid',
        },
      {
        name: 'n_assetclassname_like',
        prop: 'n_assetclassname_like',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_assettype_eq',
        prop: 'n_assettype_eq',
        dataType: 'SSCODELIST',
      },
      {
        name: 'n_assetstate_eq',
        prop: 'n_assetstate_eq',
        dataType: 'SSCODELIST',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}