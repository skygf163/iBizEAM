/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'updatedate',
      },
      {
        name: 'attention',
      },
      {
        name: 'contractobjgroup',
      },
      {
        name: 'orgid',
      },
      {
        name: 'enable',
      },
      {
        name: 'payamount',
      },
      {
        name: 'certdesc',
      },
      {
        name: 'att',
      },
      {
        name: 'pfcontractname',
      },
      {
        name: 'performrecord',
      },
      {
        name: 'contractdesc',
      },
      {
        name: 'checkstandard',
      },
      {
        name: 'updateman',
      },
      {
        name: 'apprstate',
      },
      {
        name: 'peramount',
      },
      {
        name: 'pfcontract',
        prop: 'pfcontractid',
      },
      {
        name: 'paydesc',
      },
      {
        name: 'paycnt',
      },
      {
        name: 'performline',
      },
      {
        name: 'contractnum',
      },
      {
        name: 'attdesc',
      },
      {
        name: 'description',
      },
      {
        name: 'contractdoc',
      },
      {
        name: 'changedesc',
      },
      {
        name: 'contractno',
      },
      {
        name: 'createman',
      },
      {
        name: 'rorgid',
      },
      {
        name: 'performstate',
      },
      {
        name: 'declaration',
      },
      {
        name: 'content',
      },
      {
        name: 'payreason',
      },
      {
        name: 'licdesc',
      },
      {
        name: 'sdate',
      },
      {
        name: 'apprdesc',
      },
      {
        name: 'contractprice',
      },
      {
        name: 'contracttypeid',
      },
      {
        name: 'amount',
      },
      {
        name: 'contractgroup',
      },
      {
        name: 'docdesc',
      },
      {
        name: 'contractqa',
      },
      {
        name: 'contractdate',
      },
      {
        name: 'recvdoc',
      },
      {
        name: 'apprdate',
      },
      {
        name: 'performplace',
      },
      {
        name: 'contractinfo',
      },
      {
        name: 'contractobjid',
      },
      {
        name: 'performway',
      },
      {
        name: 'changecontent',
      },
      {
        name: 'contractcode',
      },
      {
        name: 'contracttender',
      },
      {
        name: 'createdate',
      },
      {
        name: 'emservicename',
      },
      {
        name: 'emserviceid',
      },
    ]
  }


}