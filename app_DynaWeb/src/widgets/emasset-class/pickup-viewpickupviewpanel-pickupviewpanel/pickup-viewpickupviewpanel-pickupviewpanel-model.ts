/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'assetclasscode',
      },
      {
        name: 'orgid',
      },
      {
        name: 'createdate',
      },
      {
        name: 'reserver',
      },
      {
        name: 'updateman',
      },
      {
        name: 'description',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'reserver2',
      },
      {
        name: 'emassetclassname',
      },
      {
        name: 'assetclassgroup',
      },
      {
        name: 'createman',
      },
      {
        name: 'emassetclass',
        prop: 'emassetclassid',
      },
      {
        name: 'life',
      },
      {
        name: 'enable',
      },
      {
        name: 'assetclassinfo',
      },
      {
        name: 'assetclasspname',
      },
      {
        name: 'assetclasspcode',
      },
      {
        name: 'assetclasspid',
      },
    ]
  }


}