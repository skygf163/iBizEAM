/**
 * Location 部件模型
 *
 * @export
 * @class LocationModel
 */
export default class LocationModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof LocationModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'emobjmap',
        prop: 'emobjmapid',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'updateman',
      },
      {
        name: 'equipid',
      },
      {
        name: 'emobjmaptype',
      },
      {
        name: 'createdate',
      },
      {
        name: 'emobjmapname',
      },
      {
        name: 'createman',
      },
      {
        name: 'description',
      },
      {
        name: 'enable',
      },
      {
        name: 'orgid',
      },
      {
        name: 'objptype',
      },
      {
        name: 'majorequipname',
      },
      {
        name: 'majorequipid',
      },
      {
        name: 'objpname',
      },
      {
        name: 'objtype',
      },
      {
        name: 'objname',
      },
      {
        name: 'objid',
      },
      {
        name: 'objpid',
      },
    ]
  }


}