/**
 * Main3 部件模型
 *
 * @export
 * @class Main3Model
 */
export default class Main3Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main3Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emitemcsid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emitemcsname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'emitemcsid',
        prop: 'emitemcsid',
        dataType: 'GUID',
      },
      {
        name: 'itemid',
        prop: 'itemid',
        dataType: 'PICKUP',
      },
      {
        name: 'itemname',
        prop: 'itemname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'sdate',
        prop: 'sdate',
        dataType: 'DATETIME',
      },
      {
        name: 'sempid',
        prop: 'sempid',
        dataType: 'PICKUP',
      },
      {
        name: 'sempname',
        prop: 'sempname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'stockid',
        prop: 'stockid',
        dataType: 'PICKUP',
      },
      {
        name: 'stockname',
        prop: 'stockname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'psum',
        prop: 'psum',
        dataType: 'FLOAT',
      },
      {
        name: 'price',
        prop: 'price',
        dataType: 'FLOAT',
      },
      {
        name: 'amount',
        prop: 'amount',
        dataType: 'FLOAT',
      },
      {
        name: 'batcode',
        prop: 'batcode',
        dataType: 'TEXT',
      },
      {
        name: 'storename',
        prop: 'storename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'storepartname',
        prop: 'storepartname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'orgid',
        prop: 'orgid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'createman',
        prop: 'createman',
        dataType: 'TEXT',
      },
      {
        name: 'createdate',
        prop: 'createdate',
        dataType: 'DATETIME',
      },
      {
        name: 'updateman',
        prop: 'updateman',
        dataType: 'TEXT',
      },
      {
        name: 'updatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'storepartid',
        prop: 'storepartid',
        dataType: 'PICKUP',
      },
      {
        name: 'storeid',
        prop: 'storeid',
        dataType: 'PICKUP',
      },
      {
        name: 'emitemcs',
        prop: 'emitemcsid',
        dataType: 'FONTKEY',
      },
    ]
  }

}