/**
 * Main3 部件模型
 *
 * @export
 * @class Main3Model
 */
export default class Main3Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main3Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emenconsumid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emenconsumname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'enname',
        prop: 'enname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'woname',
        prop: 'woname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'equipname',
        prop: 'equipname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'objname',
        prop: 'objname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'edate',
        prop: 'edate',
        dataType: 'DATETIME',
      },
      {
        name: 'bdate',
        prop: 'bdate',
        dataType: 'DATETIME',
      },
      {
        name: 'curval',
        prop: 'curval',
        dataType: 'FLOAT',
      },
      {
        name: 'lastval',
        prop: 'lastval',
        dataType: 'FLOAT',
      },
      {
        name: 'vrate',
        prop: 'vrate',
        dataType: 'FLOAT',
      },
      {
        name: 'nval',
        prop: 'nval',
        dataType: 'FLOAT',
      },
      {
        name: 'price',
        prop: 'price',
        dataType: 'FLOAT',
      },
      {
        name: 'amount',
        prop: 'amount',
        dataType: 'FLOAT',
      },
      {
        name: 'objid',
        prop: 'objid',
        dataType: 'PICKUP',
      },
      {
        name: 'emenconsumid',
        prop: 'emenconsumid',
        dataType: 'GUID',
      },
      {
        name: 'woid',
        prop: 'woid',
        dataType: 'PICKUP',
      },
      {
        name: 'enid',
        prop: 'enid',
        dataType: 'PICKUP',
      },
      {
        name: 'equipid',
        prop: 'equipid',
        dataType: 'PICKUP',
      },
      {
        name: 'emenconsum',
        prop: 'emenconsumid',
        dataType: 'FONTKEY',
      },
    ]
  }

}