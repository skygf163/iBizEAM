/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'enable',
      },
      {
        name: 'objid',
      },
      {
        name: 'description',
      },
      {
        name: 'createman',
      },
      {
        name: 'emrfoacname',
      },
      {
        name: 'rfoaccode',
      },
      {
        name: 'orgid',
      },
      {
        name: 'createdate',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'updateman',
      },
      {
        name: 'rfoacinfo',
      },
      {
        name: 'emrfoac',
        prop: 'emrfoacid',
      },
      {
        name: 'rfodenane',
      },
      {
        name: 'rfomoname',
      },
      {
        name: 'rfodeid',
      },
      {
        name: 'rfomoid',
      },
    ]
  }


}