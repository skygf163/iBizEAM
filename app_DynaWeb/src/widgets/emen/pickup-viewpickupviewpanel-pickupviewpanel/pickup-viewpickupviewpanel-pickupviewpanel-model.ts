/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'createdate',
      },
      {
        name: 'energydesc',
      },
      {
        name: 'emen',
        prop: 'emenid',
      },
      {
        name: 'description',
      },
      {
        name: 'energycode',
      },
      {
        name: 'energytypeid',
      },
      {
        name: 'updateman',
      },
      {
        name: 'createman',
      },
      {
        name: 'vrate',
      },
      {
        name: 'enable',
      },
      {
        name: 'orgid',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'price',
      },
      {
        name: 'emenname',
      },
      {
        name: 'energyinfo',
      },
      {
        name: 'unitname',
      },
      {
        name: 'itemmtypeid',
      },
      {
        name: 'itemname',
      },
      {
        name: 'itemprice',
      },
      {
        name: 'itemmtypename',
      },
      {
        name: 'itemtypeid',
      },
      {
        name: 'itembtypeid',
      },
      {
        name: 'itembtypename',
      },
      {
        name: 'itemid',
      },
    ]
  }


}