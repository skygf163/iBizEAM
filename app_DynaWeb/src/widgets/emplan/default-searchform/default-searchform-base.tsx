import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, SearchFormControlBase } from '@/studio-core';
import EMPlanService from '@/service/emplan/emplan-service';
import DefaultService from './default-searchform-service';
import EMPlanUIService from '@/uiservice/emplan/emplan-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

/**
 * searchform部件基类
 *
 * @export
 * @class SearchFormControlBase
 * @extends {DefaultSearchFormBase}
 */
export class DefaultSearchFormBase extends SearchFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected controlType: string = 'SEARCHFORM';

    /**
     * 建构部件服务对象
     *
     * @type {DefaultService}
     * @memberof DefaultSearchFormBase
     */
    public service: DefaultService = new DefaultService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMPlanService}
     * @memberof DefaultSearchFormBase
     */
    public appEntityService: EMPlanService = new EMPlanService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeName: string = 'emplan';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeLogicName: string = '计划';

    /**
     * 界面UI服务对象
     *
     * @type {EMPlanUIService}
     * @memberof DefaultBase
     */  
    public appUIService: EMPlanUIService = new EMPlanUIService(this.$store);


    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public data: any = {
        n_emplanname_like: null,
        n_plantype_eq: null,
        n_planstate_eq: null,
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public detailsModel: any = {
        formpage1: new FormPageModel({ caption: '常规条件', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this })
, 
        n_emplanname_like: new FormItemModel({ caption: '计划名称(%)', detailType: 'FORMITEM', name: 'n_emplanname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_plantype_eq: new FormItemModel({ caption: '计划类型(=)', detailType: 'FORMITEM', name: 'n_plantype_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_planstate_eq: new FormItemModel({ caption: '计划状态(=)', detailType: 'FORMITEM', name: 'n_planstate_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
    };

    /**
     * 新建默认值
     * @memberof DefaultBase
     */
    public createDefault(){                    
    }
}