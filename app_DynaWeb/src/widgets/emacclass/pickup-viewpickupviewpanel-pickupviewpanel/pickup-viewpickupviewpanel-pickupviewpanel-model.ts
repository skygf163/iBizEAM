/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'emacclass',
        prop: 'emacclassid',
      },
      {
        name: 'code',
      },
      {
        name: 'createdate',
      },
      {
        name: 'createman',
      },
      {
        name: 'enable',
      },
      {
        name: 'description',
      },
      {
        name: 'emacclassname',
      },
      {
        name: 'updateman',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'orgid',
      },
    ]
  }


}