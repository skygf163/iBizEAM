import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, MainControlBase } from '@/studio-core';
import EMPODetailService from '@/service/empodetail/empodetail-service';
import PeDetailLineService from './pe-detail-line-portlet-service';
import EMPODetailUIService from '@/uiservice/empodetail/empodetail-ui-service';
import { Environment } from '@/environments/environment';
import UIService from '@/uiservice/ui-service';

/**
 * db_sysportlet2部件基类
 *
 * @export
 * @class MainControlBase
 * @extends {PeDetailLinePortletBase}
 */
export class PeDetailLinePortletBase extends MainControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof PeDetailLinePortletBase
     */
    protected controlType: string = 'PORTLET';

    /**
     * 建构部件服务对象
     *
     * @type {PeDetailLineService}
     * @memberof PeDetailLinePortletBase
     */
    public service: PeDetailLineService = new PeDetailLineService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMPODetailService}
     * @memberof PeDetailLinePortletBase
     */
    public appEntityService: EMPODetailService = new EMPODetailService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof PeDetailLinePortletBase
     */
    protected appDeName: string = 'empodetail';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof PeDetailLinePortletBase
     */
    protected appDeLogicName: string = '订单条目';

    /**
     * 界面UI服务对象
     *
     * @type {EMPODetailUIService}
     * @memberof PeDetailLineBase
     */  
    public appUIService: EMPODetailUIService = new EMPODetailUIService(this.$store);


    /**
     * 长度
     *
     * @type {number}
     * @memberof PeDetailLine
     */
    @Prop() public height?: number;

    /**
     * 宽度
     *
     * @type {number}
     * @memberof PeDetailLine
     */
    @Prop() public width?: number;

    /**
     * 门户部件类型
     *
     * @type {number}
     * @memberof PeDetailLineBase
     */
    public portletType: string = 'chart';

    /**
     * 界面行为模型数据
     *
     * @memberof PeDetailLineBase
     */
    public uiactionModel: any = {
    }



    /**
     * 是否自适应大小
     *
     * @returns {boolean}
     * @memberof PeDetailLineBase
     */
    @Prop({default: false})public isAdaptiveSize!: boolean;

    /**
     * 获取多项数据
     *
     * @returns {any[]}
     * @memberof PeDetailLineBase
     */
    public getDatas(): any[] {
        return [];
    }

    /**
     * 获取单项树
     *
     * @returns {*}
     * @memberof PeDetailLineBase
     */
    public getData(): any {
        return {};
    }

    /**
     * 获取高度
     *
     * @returns {any[]}
     * @memberof PeDetailLineBase
     */
    get getHeight(): any{
        if(!this.$util.isEmpty(this.height) && !this.$util.isNumberNaN(this.height)){
            if(this.height == 0){
                return 'auto';
            } else {
                return this.height+'px';
            }
        } else {
            return '300px';
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof PeDetailLineBase
     */
    public created() {
        this.afterCreated();
    }

    /**
     * 执行created后的逻辑
     *
     *  @memberof PeDetailLineBase
     */    
    public afterCreated(){
        if (this.viewState) {
            this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }) => {
                if(Object.is(tag, "all-portlet") && Object.is(action,'loadmodel')){
                   this.calcUIActionAuthState(data);
                }
                if (!Object.is(tag, this.name)) {
                    return;
                }
                const refs: any = this.$refs;
                Object.keys(refs).forEach((_name: string) => {
                    this.viewState.next({ tag: _name, action: action, data: data });
                });
            });
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof PeDetailLineBase
     */
    public destroyed() {
        this.afterDestroy();
    }

    /**
     * 执行destroyed后的逻辑
     *
     * @memberof PeDetailLineBase
     */
    public afterDestroy() {
        if (this.viewStateEvent) {
            this.viewStateEvent.unsubscribe();
        }
    }

    /**
     * 计算界面行为权限
     *
     * @memberof PeDetailLineBase
     */
    public calcUIActionAuthState(data:any = {}) {
        //  如果是操作栏，不计算权限
        if(this.portletType && Object.is('actionbar', this.portletType)) {
            return;
        }
        let _this: any = this;
        let uiservice: any = _this.appUIService ? _this.appUIService : new UIService(_this.$store);
        if(_this.uiactionModel){
            ViewTool.calcActionItemAuthState(data,_this.uiactionModel,uiservice);
        }
    }


    /**
     * 刷新
     *
     * @memberof PeDetailLineBase
     */
    public refresh(args?: any) {
      this.viewState.next({ tag: 'db_sysportlet2_chart', action: 'refresh', data: args });
    }

}
