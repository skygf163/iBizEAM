import EMAssetClear_en_US_Base from './emasset-clear_en_US_base';

function getLocaleResource(){
    const EMAssetClear_en_US_OwnData = {};
    const targetData = Object.assign(EMAssetClear_en_US_Base(), EMAssetClear_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
