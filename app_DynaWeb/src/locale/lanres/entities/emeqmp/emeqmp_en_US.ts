import EMEQMP_en_US_Base from './emeqmp_en_US_base';

function getLocaleResource(){
    const EMEQMP_en_US_OwnData = {};
    const targetData = Object.assign(EMEQMP_en_US_Base(), EMEQMP_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
