import EMItemROut_en_US_Base from './emitem-rout_en_US_base';

function getLocaleResource(){
    const EMItemROut_en_US_OwnData = {};
    const targetData = Object.assign(EMItemROut_en_US_Base(), EMItemROut_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
