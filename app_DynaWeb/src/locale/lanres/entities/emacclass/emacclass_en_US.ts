import EMACClass_en_US_Base from './emacclass_en_US_base';

function getLocaleResource(){
    const EMACClass_en_US_OwnData = {};
    const targetData = Object.assign(EMACClass_en_US_Base(), EMACClass_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
