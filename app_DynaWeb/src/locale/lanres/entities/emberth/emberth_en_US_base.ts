import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			createman: commonLogic.appcommonhandle("建立人",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			berthcode: commonLogic.appcommonhandle("泊位编码",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			emberthname: commonLogic.appcommonhandle("泊位名称",null),
			emberthid: commonLogic.appcommonhandle("流水号",null),
		},
			views: {
				pickupview: {
					caption: commonLogic.appcommonhandle("泊位",null),
					title: commonLogic.appcommonhandle("泊位数据选择视图",null),
				},
				pickupgridview: {
					caption: commonLogic.appcommonhandle("泊位",null),
					title: commonLogic.appcommonhandle("泊位选择表格视图",null),
				},
			},
			main_grid: {
				columns: {
					emberthname: commonLogic.appcommonhandle("泊位名称",null),
					updateman: commonLogic.appcommonhandle("更新人",null),
					updatedate: commonLogic.appcommonhandle("更新时间",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
				},
				uiactions: {
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;