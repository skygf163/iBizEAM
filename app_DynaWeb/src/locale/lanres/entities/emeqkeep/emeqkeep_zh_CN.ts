import EMEQKeep_zh_CN_Base from './emeqkeep_zh_CN_base';

function getLocaleResource(){
    const EMEQKeep_zh_CN_OwnData = {};
    const targetData = Object.assign(EMEQKeep_zh_CN_Base(), EMEQKeep_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;