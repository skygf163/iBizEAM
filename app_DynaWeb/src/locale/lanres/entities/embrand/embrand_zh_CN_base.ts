import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("品牌", null),
		fields: {
			embrandid: commonLogic.appcommonhandle("流水号",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			embrandname: commonLogic.appcommonhandle("品牌名称",null),
			embrandcode: commonLogic.appcommonhandle("品牌编码",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			machtypecode: commonLogic.appcommonhandle("机种编码",null),
		},
			views: {
				pickupview: {
					caption: commonLogic.appcommonhandle("品牌",null),
					title: commonLogic.appcommonhandle("品牌数据选择视图",null),
				},
				pickupgridview: {
					caption: commonLogic.appcommonhandle("品牌",null),
					title: commonLogic.appcommonhandle("品牌选择表格视图",null),
				},
			},
			main_grid: {
				columns: {
					embrandname: commonLogic.appcommonhandle("品牌名称",null),
					updateman: commonLogic.appcommonhandle("更新人",null),
					updatedate: commonLogic.appcommonhandle("更新时间",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
				},
				uiactions: {
				},
			},
		};
		return data;
}
export default getLocaleResourceBase;