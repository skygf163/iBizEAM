import EMPlanDetail_en_US_Base from './emplan-detail_en_US_base';

function getLocaleResource(){
    const EMPlanDetail_en_US_OwnData = {};
    const targetData = Object.assign(EMPlanDetail_en_US_Base(), EMPlanDetail_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
