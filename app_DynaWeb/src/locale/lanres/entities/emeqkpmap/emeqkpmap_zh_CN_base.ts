import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("设备关键点关系", null),
		fields: {
			emeqkpmapname: commonLogic.appcommonhandle("设备关键点关系",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			description: commonLogic.appcommonhandle("描述",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			emeqkpmapid: commonLogic.appcommonhandle("设备关键点关系标识",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			refobjname: commonLogic.appcommonhandle("设备关键点归属",null),
			kpname: commonLogic.appcommonhandle("设备关键点",null),
			kpid: commonLogic.appcommonhandle("设备关键点",null),
			refobjid: commonLogic.appcommonhandle("设备关键点归属",null),
		},
		};
		return data;
}
export default getLocaleResourceBase;