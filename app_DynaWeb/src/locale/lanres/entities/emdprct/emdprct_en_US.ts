import EMDPRCT_en_US_Base from './emdprct_en_US_base';

function getLocaleResource(){
    const EMDPRCT_en_US_OwnData = {};
    const targetData = Object.assign(EMDPRCT_en_US_Base(), EMDPRCT_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
