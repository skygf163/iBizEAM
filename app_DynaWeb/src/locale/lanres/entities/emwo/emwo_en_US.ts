import EMWO_en_US_Base from './emwo_en_US_base';

function getLocaleResource(){
    const EMWO_en_US_OwnData = {};
    const targetData = Object.assign(EMWO_en_US_Base(), EMWO_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
