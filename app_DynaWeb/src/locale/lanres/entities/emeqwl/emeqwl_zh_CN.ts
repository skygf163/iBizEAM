import EMEQWL_zh_CN_Base from './emeqwl_zh_CN_base';

function getLocaleResource(){
    const EMEQWL_zh_CN_OwnData = {};
    const targetData = Object.assign(EMEQWL_zh_CN_Base(), EMEQWL_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;