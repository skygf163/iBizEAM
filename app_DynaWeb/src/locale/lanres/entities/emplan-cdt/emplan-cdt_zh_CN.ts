import EMPlanCDT_zh_CN_Base from './emplan-cdt_zh_CN_base';

function getLocaleResource(){
    const EMPlanCDT_zh_CN_OwnData = {};
    const targetData = Object.assign(EMPlanCDT_zh_CN_Base(), EMPlanCDT_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;