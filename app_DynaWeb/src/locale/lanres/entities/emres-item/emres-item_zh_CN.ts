import EMResItem_zh_CN_Base from './emres-item_zh_CN_base';

function getLocaleResource(){
    const EMResItem_zh_CN_OwnData = {};
    const targetData = Object.assign(EMResItem_zh_CN_Base(), EMResItem_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;