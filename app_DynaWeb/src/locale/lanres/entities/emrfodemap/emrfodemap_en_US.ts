import EMRFODEMap_en_US_Base from './emrfodemap_en_US_base';

function getLocaleResource(){
    const EMRFODEMap_en_US_OwnData = {};
    const targetData = Object.assign(EMRFODEMap_en_US_Base(), EMRFODEMap_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
