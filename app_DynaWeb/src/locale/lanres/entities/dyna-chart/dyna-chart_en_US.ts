import DynaChart_en_US_Base from './dyna-chart_en_US_base';

function getLocaleResource(){
    const DynaChart_en_US_OwnData = {};
    const targetData = Object.assign(DynaChart_en_US_Base(), DynaChart_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
