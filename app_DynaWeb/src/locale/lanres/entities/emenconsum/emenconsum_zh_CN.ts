import EMENConsum_zh_CN_Base from './emenconsum_zh_CN_base';

function getLocaleResource(){
    const EMENConsum_zh_CN_OwnData = {};
    const targetData = Object.assign(EMENConsum_zh_CN_Base(), EMENConsum_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;