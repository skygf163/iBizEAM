import EMEQLCTRHY_zh_CN_Base from './emeqlctrhy_zh_CN_base';

function getLocaleResource(){
    const EMEQLCTRHY_zh_CN_OwnData = {};
    const targetData = Object.assign(EMEQLCTRHY_zh_CN_Base(), EMEQLCTRHY_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;