import PFContract_zh_CN_Base from './pfcontract_zh_CN_base';

function getLocaleResource(){
    const PFContract_zh_CN_OwnData = {};
    const targetData = Object.assign(PFContract_zh_CN_Base(), PFContract_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;