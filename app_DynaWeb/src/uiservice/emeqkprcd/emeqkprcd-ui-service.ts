import EMEQKPRCDUIServiceBase from './emeqkprcd-ui-service-base';

/**
 * 关键点记录UI服务对象
 *
 * @export
 * @class EMEQKPRCDUIService
 */
export default class EMEQKPRCDUIService extends EMEQKPRCDUIServiceBase {

    /**
     * Creates an instance of  EMEQKPRCDUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQKPRCDUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}