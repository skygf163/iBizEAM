import PLANSCHEDULE_MUIServiceBase from './planschedule-m-ui-service-base';

/**
 * 计划_按月UI服务对象
 *
 * @export
 * @class PLANSCHEDULE_MUIService
 */
export default class PLANSCHEDULE_MUIService extends PLANSCHEDULE_MUIServiceBase {

    /**
     * Creates an instance of  PLANSCHEDULE_MUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  PLANSCHEDULE_MUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}