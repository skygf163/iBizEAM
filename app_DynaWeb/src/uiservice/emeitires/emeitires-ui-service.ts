import EMEITIResUIServiceBase from './emeitires-ui-service-base';

/**
 * 轮胎清单UI服务对象
 *
 * @export
 * @class EMEITIResUIService
 */
export default class EMEITIResUIService extends EMEITIResUIServiceBase {

    /**
     * Creates an instance of  EMEITIResUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEITIResUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}