import EMBrandUIServiceBase from './embrand-ui-service-base';

/**
 * 品牌UI服务对象
 *
 * @export
 * @class EMBrandUIService
 */
export default class EMBrandUIService extends EMBrandUIServiceBase {

    /**
     * Creates an instance of  EMBrandUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMBrandUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}