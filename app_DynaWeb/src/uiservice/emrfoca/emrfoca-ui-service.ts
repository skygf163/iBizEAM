import EMRFOCAUIServiceBase from './emrfoca-ui-service-base';

/**
 * 原因UI服务对象
 *
 * @export
 * @class EMRFOCAUIService
 */
export default class EMRFOCAUIService extends EMRFOCAUIServiceBase {

    /**
     * Creates an instance of  EMRFOCAUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMRFOCAUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}