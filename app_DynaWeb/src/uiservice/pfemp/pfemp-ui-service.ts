import PFEmpUIServiceBase from './pfemp-ui-service-base';

/**
 * 职员UI服务对象
 *
 * @export
 * @class PFEmpUIService
 */
export default class PFEmpUIService extends PFEmpUIServiceBase {

    /**
     * Creates an instance of  PFEmpUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  PFEmpUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}