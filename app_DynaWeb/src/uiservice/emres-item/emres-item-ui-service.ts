import EMResItemUIServiceBase from './emres-item-ui-service-base';

/**
 * 物品资源UI服务对象
 *
 * @export
 * @class EMResItemUIService
 */
export default class EMResItemUIService extends EMResItemUIServiceBase {

    /**
     * Creates an instance of  EMResItemUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMResItemUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}