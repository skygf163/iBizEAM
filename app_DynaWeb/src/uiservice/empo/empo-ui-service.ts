import EMPOUIServiceBase from './empo-ui-service-base';

/**
 * 订单UI服务对象
 *
 * @export
 * @class EMPOUIService
 */
export default class EMPOUIService extends EMPOUIServiceBase {

    /**
     * Creates an instance of  EMPOUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPOUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}