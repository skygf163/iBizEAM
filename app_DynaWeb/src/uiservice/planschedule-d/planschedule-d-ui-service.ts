import PLANSCHEDULE_DUIServiceBase from './planschedule-d-ui-service-base';

/**
 * 计划_按天UI服务对象
 *
 * @export
 * @class PLANSCHEDULE_DUIService
 */
export default class PLANSCHEDULE_DUIService extends PLANSCHEDULE_DUIServiceBase {

    /**
     * Creates an instance of  PLANSCHEDULE_DUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  PLANSCHEDULE_DUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}