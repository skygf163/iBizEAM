import EMEQCheckUIServiceBase from './emeqcheck-ui-service-base';

/**
 * 维修记录UI服务对象
 *
 * @export
 * @class EMEQCheckUIService
 */
export default class EMEQCheckUIService extends EMEQCheckUIServiceBase {

    /**
     * Creates an instance of  EMEQCheckUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQCheckUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}