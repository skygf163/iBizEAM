import EMEQDebugAuthServiceBase from './emeqdebug-auth-service-base';


/**
 * 事故记录权限服务对象
 *
 * @export
 * @class EMEQDebugAuthService
 * @extends {EMEQDebugAuthServiceBase}
 */
export default class EMEQDebugAuthService extends EMEQDebugAuthServiceBase {

    /**
     * Creates an instance of  EMEQDebugAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQDebugAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}