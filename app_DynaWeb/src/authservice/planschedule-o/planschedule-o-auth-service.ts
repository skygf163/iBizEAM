import PLANSCHEDULE_OAuthServiceBase from './planschedule-o-auth-service-base';


/**
 * 自定义间隔天数权限服务对象
 *
 * @export
 * @class PLANSCHEDULE_OAuthService
 * @extends {PLANSCHEDULE_OAuthServiceBase}
 */
export default class PLANSCHEDULE_OAuthService extends PLANSCHEDULE_OAuthServiceBase {

    /**
     * Creates an instance of  PLANSCHEDULE_OAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  PLANSCHEDULE_OAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}