import EMPurPlanAuthServiceBase from './empur-plan-auth-service-base';


/**
 * 计划修理权限服务对象
 *
 * @export
 * @class EMPurPlanAuthService
 * @extends {EMPurPlanAuthServiceBase}
 */
export default class EMPurPlanAuthService extends EMPurPlanAuthServiceBase {

    /**
     * Creates an instance of  EMPurPlanAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPurPlanAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}