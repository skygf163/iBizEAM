import EMOutputRctAuthServiceBase from './emoutput-rct-auth-service-base';


/**
 * 产能权限服务对象
 *
 * @export
 * @class EMOutputRctAuthService
 * @extends {EMOutputRctAuthServiceBase}
 */
export default class EMOutputRctAuthService extends EMOutputRctAuthServiceBase {

    /**
     * Creates an instance of  EMOutputRctAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMOutputRctAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}