import EMItemRInAuthServiceBase from './emitem-rin-auth-service-base';


/**
 * 入库单权限服务对象
 *
 * @export
 * @class EMItemRInAuthService
 * @extends {EMItemRInAuthServiceBase}
 */
export default class EMItemRInAuthService extends EMItemRInAuthServiceBase {

    /**
     * Creates an instance of  EMItemRInAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemRInAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}