import EMPlanTemplAuthServiceBase from './emplan-templ-auth-service-base';


/**
 * 计划模板权限服务对象
 *
 * @export
 * @class EMPlanTemplAuthService
 * @extends {EMPlanTemplAuthServiceBase}
 */
export default class EMPlanTemplAuthService extends EMPlanTemplAuthServiceBase {

    /**
     * Creates an instance of  EMPlanTemplAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPlanTemplAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}