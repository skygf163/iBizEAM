import EMWorkAuthServiceBase from './emwork-auth-service-base';


/**
 * 加班工单权限服务对象
 *
 * @export
 * @class EMWorkAuthService
 * @extends {EMWorkAuthServiceBase}
 */
export default class EMWorkAuthService extends EMWorkAuthServiceBase {

    /**
     * Creates an instance of  EMWorkAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWorkAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}