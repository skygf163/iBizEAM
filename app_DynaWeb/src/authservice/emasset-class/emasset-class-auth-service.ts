import EMAssetClassAuthServiceBase from './emasset-class-auth-service-base';


/**
 * 资产类别权限服务对象
 *
 * @export
 * @class EMAssetClassAuthService
 * @extends {EMAssetClassAuthServiceBase}
 */
export default class EMAssetClassAuthService extends EMAssetClassAuthServiceBase {

    /**
     * Creates an instance of  EMAssetClassAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMAssetClassAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}