import EMStockAuthServiceBase from './emstock-auth-service-base';


/**
 * 库存权限服务对象
 *
 * @export
 * @class EMStockAuthService
 * @extends {EMStockAuthServiceBase}
 */
export default class EMStockAuthService extends EMStockAuthServiceBase {

    /**
     * Creates an instance of  EMStockAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMStockAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}