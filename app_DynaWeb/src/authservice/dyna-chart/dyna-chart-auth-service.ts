import DynaChartAuthServiceBase from './dyna-chart-auth-service-base';


/**
 * 动态图表权限服务对象
 *
 * @export
 * @class DynaChartAuthService
 * @extends {DynaChartAuthServiceBase}
 */
export default class DynaChartAuthService extends DynaChartAuthServiceBase {

    /**
     * Creates an instance of  DynaChartAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  DynaChartAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}