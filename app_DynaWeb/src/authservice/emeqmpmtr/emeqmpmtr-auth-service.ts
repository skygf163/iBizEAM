import EMEQMPMTRAuthServiceBase from './emeqmpmtr-auth-service-base';


/**
 * 设备仪表读数权限服务对象
 *
 * @export
 * @class EMEQMPMTRAuthService
 * @extends {EMEQMPMTRAuthServiceBase}
 */
export default class EMEQMPMTRAuthService extends EMEQMPMTRAuthServiceBase {

    /**
     * Creates an instance of  EMEQMPMTRAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQMPMTRAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}