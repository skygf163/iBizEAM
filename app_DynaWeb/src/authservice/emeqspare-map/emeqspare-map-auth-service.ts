import EMEQSpareMapAuthServiceBase from './emeqspare-map-auth-service-base';


/**
 * 备件包引用权限服务对象
 *
 * @export
 * @class EMEQSpareMapAuthService
 * @extends {EMEQSpareMapAuthServiceBase}
 */
export default class EMEQSpareMapAuthService extends EMEQSpareMapAuthServiceBase {

    /**
     * Creates an instance of  EMEQSpareMapAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQSpareMapAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}