import EMEQAHAuthServiceBase from './emeqah-auth-service-base';


/**
 * 活动历史权限服务对象
 *
 * @export
 * @class EMEQAHAuthService
 * @extends {EMEQAHAuthServiceBase}
 */
export default class EMEQAHAuthService extends EMEQAHAuthServiceBase {

    /**
     * Creates an instance of  EMEQAHAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQAHAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}