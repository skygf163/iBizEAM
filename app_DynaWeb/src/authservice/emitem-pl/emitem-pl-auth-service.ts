import EMItemPLAuthServiceBase from './emitem-pl-auth-service-base';


/**
 * 损溢单权限服务对象
 *
 * @export
 * @class EMItemPLAuthService
 * @extends {EMItemPLAuthServiceBase}
 */
export default class EMItemPLAuthService extends EMItemPLAuthServiceBase {

    /**
     * Creates an instance of  EMItemPLAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemPLAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}