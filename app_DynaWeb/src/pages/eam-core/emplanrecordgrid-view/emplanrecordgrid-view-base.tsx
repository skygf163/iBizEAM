
import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { GridViewBase } from '@/studio-core';
import EMPLANRECORDService from '@/service/emplanrecord/emplanrecord-service';
import EMPLANRECORDAuthService from '@/authservice/emplanrecord/emplanrecord-auth-service';
import GridViewEngine from '@engine/view/grid-view-engine';
import EMPLANRECORDUIService from '@/uiservice/emplanrecord/emplanrecord-ui-service';
import CodeListService from '@service/app/codelist-service';


/**
 * 触发记录表格视图视图基类
 *
 * @export
 * @class EMPLANRECORDGridViewBase
 * @extends {GridViewBase}
 */
export class EMPLANRECORDGridViewBase extends GridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMPLANRECORDGridViewBase
     */
    protected appDeName: string = 'emplanrecord';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMPLANRECORDGridViewBase
     */
    protected appDeKey: string = 'emplanrecordid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMPLANRECORDGridViewBase
     */
    protected appDeMajor: string = 'emplanrecordname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMPLANRECORDGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMPLANRECORDService}
     * @memberof EMPLANRECORDGridViewBase
     */
    protected appEntityService: EMPLANRECORDService = new EMPLANRECORDService;

    /**
     * 实体权限服务对象
     *
     * @type EMPLANRECORDUIService
     * @memberof EMPLANRECORDGridViewBase
     */
    public appUIService: EMPLANRECORDUIService = new EMPLANRECORDUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMPLANRECORDGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emplanrecord.views.gridview.caption',
        srfTitle: 'entities.emplanrecord.views.gridview.title',
        srfSubTitle: 'entities.emplanrecord.views.gridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMPLANRECORDGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMPLANRECORDGridViewBase
     */
	protected viewtag: string = 'a840c26605a5707b568f74aa676a9dc0';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMPLANRECORDGridViewBase
     */ 
    protected viewName: string = 'EMPLANRECORDGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMPLANRECORDGridViewBase
     */
    public engine: GridViewEngine = new GridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMPLANRECORDGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMPLANRECORDGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            opendata: (args: any[], fullargs?: any[], params?: any, $event?: any, xData?: any) => {
                this.opendata(args, fullargs, params, $event, xData);
            },
            newdata: (args: any[], fullargs?: any[], params?: any, $event?: any, xData?: any) => {
                this.newdata(args, fullargs, params, $event, xData);
            },
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'emplanrecord',
            majorPSDEField: 'emplanrecordname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPLANRECORDGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPLANRECORDGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPLANRECORDGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPLANRECORDGridViewBase
     */
    public grid_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'remove', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPLANRECORDGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPLANRECORDGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPLANRECORDGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPLANRECORDGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMPLANRECORDGridView
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
        const data: any = {};
        if(args[0].srfsourcekey){
            data.srfsourcekey = args[0].srfsourcekey;
        }
        if(fullargs && (fullargs as any).copymode) {
            Object.assign(data, { copymode: (fullargs as any).copymode });
        }
        let tempContext = JSON.parse(JSON.stringify(this.context));
        delete tempContext.emplanrecord;
        if(args.length >0){
            Object.assign(tempContext,args[0]);
        }
        let deResParameters: any[] = [];
        if(tempContext.emplan && true){
            deResParameters = [
            { pathName: 'emplans', parameterName: 'emplan' },
            ]
        }
        const parameters: any[] = [
            { pathName: 'emplanrecords', parameterName: 'emplanrecord' },
            { pathName: 'editview', parameterName: 'editview' },
        ];
        const _this: any = this;
        const openIndexViewTab = (data: any) => {
            const _data: any = { w: (new Date().getTime()) };
            Object.assign(_data, data);
            const routePath = this.$viewTool.buildUpRoutePath(this.$route, tempContext, deResParameters, parameters, args, _data);
            this.$router.push(routePath);
        }
        openIndexViewTab(data);
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMPLANRECORDGridView
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
        const localContext: any = null;
        const localViewParam: any =null;
        const data: any = {};
        let tempContext = JSON.parse(JSON.stringify(this.context));
        if(args.length >0){
            Object.assign(tempContext,args[0]);
        }
        let deResParameters: any[] = [];
        if(tempContext.emplan && true){
            deResParameters = [
            { pathName: 'emplans', parameterName: 'emplan' },
            ]
        }
        const parameters: any[] = [
            { pathName: 'emplanrecords', parameterName: 'emplanrecord' },
            { pathName: 'editview', parameterName: 'editview' },
        ];
        const _this: any = this;
        const openIndexViewTab = (data: any) => {
            const routePath = this.$viewTool.buildUpRoutePath(this.$route, tempContext, deResParameters, parameters, args, data);
            this.$router.push(routePath);
        }
        openIndexViewTab(data);
    }


}