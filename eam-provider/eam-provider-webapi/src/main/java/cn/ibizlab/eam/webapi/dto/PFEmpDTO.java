package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 服务DTO对象[PFEmpDTO]
 */
@Data
@ApiModel("职员")
public class PFEmpDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    @ApiModelProperty("更新人")
    private String updateman;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    @ApiModelProperty("描述")
    private String description;

    /**
     * 属性 [TEL]
     *
     */
    @JSONField(name = "tel")
    @JsonProperty("tel")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("联系电话")
    private String tel;

    /**
     * 属性 [WORKDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "workdate" , format="yyyy-MM-dd")
    @JsonProperty("workdate")
    @ApiModelProperty("工作日期")
    private Timestamp workdate;

    /**
     * 属性 [PFEMPNAME]
     *
     */
    @JSONField(name = "pfempname")
    @JsonProperty("pfempname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("职员名称")
    private String pfempname;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;

    /**
     * 属性 [RAISEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "raisedate" , format="yyyy-MM-dd")
    @JsonProperty("raisedate")
    @ApiModelProperty("入本企业日期")
    private Timestamp raisedate;

    /**
     * 属性 [HOMETEL]
     *
     */
    @JSONField(name = "hometel")
    @JsonProperty("hometel")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("家庭电话")
    private String hometel;

    /**
     * 属性 [BIRTHDAY]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "birthday" , format="yyyy-MM-dd")
    @JsonProperty("birthday")
    @ApiModelProperty("出生日期")
    private Timestamp birthday;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;

    /**
     * 属性 [PFEMPID]
     *
     */
    @JSONField(name = "pfempid")
    @JsonProperty("pfempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("职员标识")
    private String pfempid;

    /**
     * 属性 [HOMEADDR]
     *
     */
    @JSONField(name = "homeaddr")
    @JsonProperty("homeaddr")
    @Size(min = 0, max = 255, message = "内容长度必须小于等于[255]")
    @ApiModelProperty("家庭地址")
    private String homeaddr;

    /**
     * 属性 [EMPINFO]
     *
     */
    @JSONField(name = "empinfo")
    @JsonProperty("empinfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("职员信息")
    private String empinfo;

    /**
     * 属性 [DEPTID]
     *
     */
    @JSONField(name = "deptid")
    @JsonProperty("deptid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("部门")
    private String deptid;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    @ApiModelProperty("建立人")
    private String createman;

    /**
     * 属性 [EMPSEX]
     *
     */
    @JSONField(name = "empsex")
    @JsonProperty("empsex")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    @ApiModelProperty("性别")
    private String empsex;

    /**
     * 属性 [CERTCODE]
     *
     */
    @JSONField(name = "certcode")
    @JsonProperty("certcode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("证件号码")
    private String certcode;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    @ApiModelProperty("组织")
    private String orgid;

    /**
     * 属性 [EMPCODE]
     *
     */
    @JSONField(name = "empcode")
    @JsonProperty("empcode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("职员代码")
    private String empcode;

    /**
     * 属性 [PSW]
     *
     */
    @JSONField(name = "psw")
    @JsonProperty("psw")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("口令")
    private String psw;

    /**
     * 属性 [TEAMID]
     *
     */
    @JSONField(name = "teamid")
    @JsonProperty("teamid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("班组")
    private String teamid;

    /**
     * 属性 [MAINDEPTCODE]
     *
     */
    @JSONField(name = "maindeptcode")
    @JsonProperty("maindeptcode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("主部门代码")
    private String maindeptcode;

    /**
     * 属性 [E_MAIL]
     *
     */
    @JSONField(name = "e_mail")
    @JsonProperty("e_mail")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("电子邮件")
    private String eMail;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;

    /**
     * 属性 [CELL]
     *
     */
    @JSONField(name = "cell")
    @JsonProperty("cell")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("移动电话")
    private String cell;

    /**
     * 属性 [ADDR]
     *
     */
    @JSONField(name = "addr")
    @JsonProperty("addr")
    @Size(min = 0, max = 255, message = "内容长度必须小于等于[255]")
    @ApiModelProperty("联系地址")
    private String addr;

    /**
     * 属性 [MAJORTEAMNAME]
     *
     */
    @JSONField(name = "majorteamname")
    @JsonProperty("majorteamname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("主班组")
    private String majorteamname;

    /**
     * 属性 [MAJORTEAMID]
     *
     */
    @JSONField(name = "majorteamid")
    @JsonProperty("majorteamid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("主班组")
    private String majorteamid;

    /**
     * 属性 [MAJORDEPTID]
     *
     */
    @JSONField(name = "majordeptid")
    @JsonProperty("majordeptid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("主部门")
    private String majordeptid;

    /**
     * 属性 [MAJORDEPTNAME]
     *
     */
    @JSONField(name = "majordeptname")
    @JsonProperty("majordeptname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("主部门")
    private String majordeptname;


    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [TEL]
     */
    public void setTel(String  tel){
        this.tel = tel ;
        this.modify("tel",tel);
    }

    /**
     * 设置 [WORKDATE]
     */
    public void setWorkdate(Timestamp  workdate){
        this.workdate = workdate ;
        this.modify("workdate",workdate);
    }

    /**
     * 设置 [PFEMPNAME]
     */
    public void setPfempname(String  pfempname){
        this.pfempname = pfempname ;
        this.modify("pfempname",pfempname);
    }

    /**
     * 设置 [RAISEDATE]
     */
    public void setRaisedate(Timestamp  raisedate){
        this.raisedate = raisedate ;
        this.modify("raisedate",raisedate);
    }

    /**
     * 设置 [HOMETEL]
     */
    public void setHometel(String  hometel){
        this.hometel = hometel ;
        this.modify("hometel",hometel);
    }

    /**
     * 设置 [BIRTHDAY]
     */
    public void setBirthday(Timestamp  birthday){
        this.birthday = birthday ;
        this.modify("birthday",birthday);
    }

    /**
     * 设置 [HOMEADDR]
     */
    public void setHomeaddr(String  homeaddr){
        this.homeaddr = homeaddr ;
        this.modify("homeaddr",homeaddr);
    }

    /**
     * 设置 [EMPSEX]
     */
    public void setEmpsex(String  empsex){
        this.empsex = empsex ;
        this.modify("empsex",empsex);
    }

    /**
     * 设置 [CERTCODE]
     */
    public void setCertcode(String  certcode){
        this.certcode = certcode ;
        this.modify("certcode",certcode);
    }

    /**
     * 设置 [EMPCODE]
     */
    public void setEmpcode(String  empcode){
        this.empcode = empcode ;
        this.modify("empcode",empcode);
    }

    /**
     * 设置 [PSW]
     */
    public void setPsw(String  psw){
        this.psw = psw ;
        this.modify("psw",psw);
    }

    /**
     * 设置 [MAINDEPTCODE]
     */
    public void setMaindeptcode(String  maindeptcode){
        this.maindeptcode = maindeptcode ;
        this.modify("maindeptcode",maindeptcode);
    }

    /**
     * 设置 [E_MAIL]
     */
    public void setEMail(String  eMail){
        this.eMail = eMail ;
        this.modify("e_mail",eMail);
    }

    /**
     * 设置 [CELL]
     */
    public void setCell(String  cell){
        this.cell = cell ;
        this.modify("cell",cell);
    }

    /**
     * 设置 [ADDR]
     */
    public void setAddr(String  addr){
        this.addr = addr ;
        this.modify("addr",addr);
    }

    /**
     * 设置 [MAJORTEAMID]
     */
    public void setMajorteamid(String  majorteamid){
        this.majorteamid = majorteamid ;
        this.modify("majorteamid",majorteamid);
    }

    /**
     * 设置 [MAJORDEPTID]
     */
    public void setMajordeptid(String  majordeptid){
        this.majordeptid = majordeptid ;
        this.modify("majordeptid",majordeptid);
    }


}


