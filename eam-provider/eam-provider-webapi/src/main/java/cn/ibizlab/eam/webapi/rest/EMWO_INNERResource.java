package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMWO_INNER;
import cn.ibizlab.eam.core.eam_core.service.IEMWO_INNERService;
import cn.ibizlab.eam.core.eam_core.filter.EMWO_INNERSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"内部工单" })
@RestController("WebApi-emwo_inner")
@RequestMapping("")
public class EMWO_INNERResource {

    @Autowired
    public IEMWO_INNERService emwo_innerService;

    @Autowired
    @Lazy
    public EMWO_INNERMapping emwo_innerMapping;

    @PreAuthorize("hasPermission(this.emwo_innerMapping.toDomain(#emwo_innerdto),'eam-EMWO_INNER-Create')")
    @ApiOperation(value = "新建内部工单", tags = {"内部工单" },  notes = "新建内部工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_inners")
    public ResponseEntity<EMWO_INNERDTO> create(@Validated @RequestBody EMWO_INNERDTO emwo_innerdto) {
        EMWO_INNER domain = emwo_innerMapping.toDomain(emwo_innerdto);
		emwo_innerService.create(domain);
        EMWO_INNERDTO dto = emwo_innerMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_innerMapping.toDomain(#emwo_innerdtos),'eam-EMWO_INNER-Create')")
    @ApiOperation(value = "批量新建内部工单", tags = {"内部工单" },  notes = "批量新建内部工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_inners/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMWO_INNERDTO> emwo_innerdtos) {
        emwo_innerService.createBatch(emwo_innerMapping.toDomain(emwo_innerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwo_inner" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwo_innerService.get(#emwo_inner_id),'eam-EMWO_INNER-Update')")
    @ApiOperation(value = "更新内部工单", tags = {"内部工单" },  notes = "更新内部工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emwo_inners/{emwo_inner_id}")
    public ResponseEntity<EMWO_INNERDTO> update(@PathVariable("emwo_inner_id") String emwo_inner_id, @RequestBody EMWO_INNERDTO emwo_innerdto) {
		EMWO_INNER domain  = emwo_innerMapping.toDomain(emwo_innerdto);
        domain .setEmwoInnerid(emwo_inner_id);
		emwo_innerService.update(domain );
		EMWO_INNERDTO dto = emwo_innerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_innerService.getEmwoInnerByEntities(this.emwo_innerMapping.toDomain(#emwo_innerdtos)),'eam-EMWO_INNER-Update')")
    @ApiOperation(value = "批量更新内部工单", tags = {"内部工单" },  notes = "批量更新内部工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emwo_inners/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMWO_INNERDTO> emwo_innerdtos) {
        emwo_innerService.updateBatch(emwo_innerMapping.toDomain(emwo_innerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwo_innerService.get(#emwo_inner_id),'eam-EMWO_INNER-Remove')")
    @ApiOperation(value = "删除内部工单", tags = {"内部工单" },  notes = "删除内部工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emwo_inners/{emwo_inner_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emwo_inner_id") String emwo_inner_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emwo_innerService.remove(emwo_inner_id));
    }

    @PreAuthorize("hasPermission(this.emwo_innerService.getEmwoInnerByIds(#ids),'eam-EMWO_INNER-Remove')")
    @ApiOperation(value = "批量删除内部工单", tags = {"内部工单" },  notes = "批量删除内部工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emwo_inners/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emwo_innerService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwo_innerMapping.toDomain(returnObject.body),'eam-EMWO_INNER-Get')")
    @ApiOperation(value = "获取内部工单", tags = {"内部工单" },  notes = "获取内部工单")
	@RequestMapping(method = RequestMethod.GET, value = "/emwo_inners/{emwo_inner_id}")
    public ResponseEntity<EMWO_INNERDTO> get(@PathVariable("emwo_inner_id") String emwo_inner_id) {
        EMWO_INNER domain = emwo_innerService.get(emwo_inner_id);
        EMWO_INNERDTO dto = emwo_innerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取内部工单草稿", tags = {"内部工单" },  notes = "获取内部工单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emwo_inners/getdraft")
    public ResponseEntity<EMWO_INNERDTO> getDraft(EMWO_INNERDTO dto) {
        EMWO_INNER domain = emwo_innerMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_innerMapping.toDto(emwo_innerService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-Acceptance-all')")
    @ApiOperation(value = "验收通过", tags = {"内部工单" },  notes = "验收通过")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_inners/{emwo_inner_id}/acceptance")
    public ResponseEntity<EMWO_INNERDTO> acceptance(@PathVariable("emwo_inner_id") String emwo_inner_id, @RequestBody EMWO_INNERDTO emwo_innerdto) {
        EMWO_INNER domain = emwo_innerMapping.toDomain(emwo_innerdto);
        domain.setEmwoInnerid(emwo_inner_id);
        domain = emwo_innerService.acceptance(domain);
        emwo_innerdto = emwo_innerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_innerdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-Acceptance-all')")
    @ApiOperation(value = "批量处理[验收通过]", tags = {"内部工单" },  notes = "批量处理[验收通过]")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_inners/acceptancebatch")
    public ResponseEntity<Boolean> acceptanceBatch(@RequestBody List<EMWO_INNERDTO> emwo_innerdtos) {
        List<EMWO_INNER> domains = emwo_innerMapping.toDomain(emwo_innerdtos);
        boolean result = emwo_innerService.acceptanceBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    @ApiOperation(value = "检查内部工单", tags = {"内部工单" },  notes = "检查内部工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_inners/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMWO_INNERDTO emwo_innerdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwo_innerService.checkKey(emwo_innerMapping.toDomain(emwo_innerdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-FormUpdateByEmequipid-all')")
    @ApiOperation(value = "表单更新-Emequipid", tags = {"内部工单" },  notes = "表单更新-Emequipid")
	@RequestMapping(method = RequestMethod.PUT, value = "/emwo_inners/{emwo_inner_id}/formupdatebyemequipid")
    public ResponseEntity<EMWO_INNERDTO> formUpdateByEmequipid(@PathVariable("emwo_inner_id") String emwo_inner_id, @RequestBody EMWO_INNERDTO emwo_innerdto) {
        EMWO_INNER domain = emwo_innerMapping.toDomain(emwo_innerdto);
        domain.setEmwoInnerid(emwo_inner_id);
        domain = emwo_innerService.formUpdateByEmequipid(domain);
        emwo_innerdto = emwo_innerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_innerdto);
    }

    @PreAuthorize("hasPermission(this.emwo_innerMapping.toDomain(#emwo_innerdto),'eam-EMWO_INNER-Save')")
    @ApiOperation(value = "保存内部工单", tags = {"内部工单" },  notes = "保存内部工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_inners/save")
    public ResponseEntity<EMWO_INNERDTO> save(@RequestBody EMWO_INNERDTO emwo_innerdto) {
        EMWO_INNER domain = emwo_innerMapping.toDomain(emwo_innerdto);
        emwo_innerService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_innerMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emwo_innerMapping.toDomain(#emwo_innerdtos),'eam-EMWO_INNER-Save')")
    @ApiOperation(value = "批量保存内部工单", tags = {"内部工单" },  notes = "批量保存内部工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_inners/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMWO_INNERDTO> emwo_innerdtos) {
        emwo_innerService.saveBatch(emwo_innerMapping.toDomain(emwo_innerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-Submit-all')")
    @ApiOperation(value = "提交", tags = {"内部工单" },  notes = "提交")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_inners/{emwo_inner_id}/submit")
    public ResponseEntity<EMWO_INNERDTO> submit(@PathVariable("emwo_inner_id") String emwo_inner_id, @RequestBody EMWO_INNERDTO emwo_innerdto) {
        EMWO_INNER domain = emwo_innerMapping.toDomain(emwo_innerdto);
        domain.setEmwoInnerid(emwo_inner_id);
        domain = emwo_innerService.submit(domain);
        emwo_innerdto = emwo_innerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_innerdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-UnAcceptance-all')")
    @ApiOperation(value = "验收不通过", tags = {"内部工单" },  notes = "验收不通过")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_inners/{emwo_inner_id}/unacceptance")
    public ResponseEntity<EMWO_INNERDTO> unAcceptance(@PathVariable("emwo_inner_id") String emwo_inner_id, @RequestBody EMWO_INNERDTO emwo_innerdto) {
        EMWO_INNER domain = emwo_innerMapping.toDomain(emwo_innerdto);
        domain.setEmwoInnerid(emwo_inner_id);
        domain = emwo_innerService.unAcceptance(domain);
        emwo_innerdto = emwo_innerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_innerdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-searchCalendar-all') and hasPermission(#context,'eam-EMWO_INNER-Get')")
	@ApiOperation(value = "获取日历查询", tags = {"内部工单" } ,notes = "获取日历查询")
    @RequestMapping(method= RequestMethod.GET , value="/emwo_inners/fetchcalendar")
	public ResponseEntity<List<EMWO_INNERDTO>> fetchCalendar(EMWO_INNERSearchContext context) {
        Page<EMWO_INNER> domains = emwo_innerService.searchCalendar(context) ;
        List<EMWO_INNERDTO> list = emwo_innerMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-searchCalendar-all') and hasPermission(#context,'eam-EMWO_INNER-Get')")
	@ApiOperation(value = "查询日历查询", tags = {"内部工单" } ,notes = "查询日历查询")
    @RequestMapping(method= RequestMethod.POST , value="/emwo_inners/searchcalendar")
	public ResponseEntity<Page<EMWO_INNERDTO>> searchCalendar(@RequestBody EMWO_INNERSearchContext context) {
        Page<EMWO_INNER> domains = emwo_innerService.searchCalendar(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_innerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-searchConfirmed-all') and hasPermission(#context,'eam-EMWO_INNER-Get')")
	@ApiOperation(value = "获取已完成", tags = {"内部工单" } ,notes = "获取已完成")
    @RequestMapping(method= RequestMethod.GET , value="/emwo_inners/fetchconfirmed")
	public ResponseEntity<List<EMWO_INNERDTO>> fetchConfirmed(EMWO_INNERSearchContext context) {
        Page<EMWO_INNER> domains = emwo_innerService.searchConfirmed(context) ;
        List<EMWO_INNERDTO> list = emwo_innerMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-searchConfirmed-all') and hasPermission(#context,'eam-EMWO_INNER-Get')")
	@ApiOperation(value = "查询已完成", tags = {"内部工单" } ,notes = "查询已完成")
    @RequestMapping(method= RequestMethod.POST , value="/emwo_inners/searchconfirmed")
	public ResponseEntity<Page<EMWO_INNERDTO>> searchConfirmed(@RequestBody EMWO_INNERSearchContext context) {
        Page<EMWO_INNER> domains = emwo_innerService.searchConfirmed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_innerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-searchDefault-all') and hasPermission(#context,'eam-EMWO_INNER-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"内部工单" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emwo_inners/fetchdefault")
	public ResponseEntity<List<EMWO_INNERDTO>> fetchDefault(EMWO_INNERSearchContext context) {
        Page<EMWO_INNER> domains = emwo_innerService.searchDefault(context) ;
        List<EMWO_INNERDTO> list = emwo_innerMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-searchDefault-all') and hasPermission(#context,'eam-EMWO_INNER-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"内部工单" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emwo_inners/searchdefault")
	public ResponseEntity<Page<EMWO_INNERDTO>> searchDefault(@RequestBody EMWO_INNERSearchContext context) {
        Page<EMWO_INNER> domains = emwo_innerService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_innerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-searchDraft-all') and hasPermission(#context,'eam-EMWO_INNER-Get')")
	@ApiOperation(value = "获取未提交", tags = {"内部工单" } ,notes = "获取未提交")
    @RequestMapping(method= RequestMethod.GET , value="/emwo_inners/fetchdraft")
	public ResponseEntity<List<EMWO_INNERDTO>> fetchDraft(EMWO_INNERSearchContext context) {
        Page<EMWO_INNER> domains = emwo_innerService.searchDraft(context) ;
        List<EMWO_INNERDTO> list = emwo_innerMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-searchDraft-all') and hasPermission(#context,'eam-EMWO_INNER-Get')")
	@ApiOperation(value = "查询未提交", tags = {"内部工单" } ,notes = "查询未提交")
    @RequestMapping(method= RequestMethod.POST , value="/emwo_inners/searchdraft")
	public ResponseEntity<Page<EMWO_INNERDTO>> searchDraft(@RequestBody EMWO_INNERSearchContext context) {
        Page<EMWO_INNER> domains = emwo_innerService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_innerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-searchToConfirm-all') and hasPermission(#context,'eam-EMWO_INNER-Get')")
	@ApiOperation(value = "获取进行中", tags = {"内部工单" } ,notes = "获取进行中")
    @RequestMapping(method= RequestMethod.GET , value="/emwo_inners/fetchtoconfirm")
	public ResponseEntity<List<EMWO_INNERDTO>> fetchToConfirm(EMWO_INNERSearchContext context) {
        Page<EMWO_INNER> domains = emwo_innerService.searchToConfirm(context) ;
        List<EMWO_INNERDTO> list = emwo_innerMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-searchToConfirm-all') and hasPermission(#context,'eam-EMWO_INNER-Get')")
	@ApiOperation(value = "查询进行中", tags = {"内部工单" } ,notes = "查询进行中")
    @RequestMapping(method= RequestMethod.POST , value="/emwo_inners/searchtoconfirm")
	public ResponseEntity<Page<EMWO_INNERDTO>> searchToConfirm(@RequestBody EMWO_INNERSearchContext context) {
        Page<EMWO_INNER> domains = emwo_innerService.searchToConfirm(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_innerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emwo_innerMapping.toDomain(#emwo_innerdto),'eam-EMWO_INNER-Create')")
    @ApiOperation(value = "根据设备档案建立内部工单", tags = {"内部工单" },  notes = "根据设备档案建立内部工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_inners")
    public ResponseEntity<EMWO_INNERDTO> createByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_INNERDTO emwo_innerdto) {
        EMWO_INNER domain = emwo_innerMapping.toDomain(emwo_innerdto);
        domain.setEquipid(emequip_id);
		emwo_innerService.create(domain);
        EMWO_INNERDTO dto = emwo_innerMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_innerMapping.toDomain(#emwo_innerdtos),'eam-EMWO_INNER-Create')")
    @ApiOperation(value = "根据设备档案批量建立内部工单", tags = {"内部工单" },  notes = "根据设备档案批量建立内部工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_inners/batch")
    public ResponseEntity<Boolean> createBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_INNERDTO> emwo_innerdtos) {
        List<EMWO_INNER> domainlist=emwo_innerMapping.toDomain(emwo_innerdtos);
        for(EMWO_INNER domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emwo_innerService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwo_inner" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwo_innerService.get(#emwo_inner_id),'eam-EMWO_INNER-Update')")
    @ApiOperation(value = "根据设备档案更新内部工单", tags = {"内部工单" },  notes = "根据设备档案更新内部工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emwo_inners/{emwo_inner_id}")
    public ResponseEntity<EMWO_INNERDTO> updateByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_inner_id") String emwo_inner_id, @RequestBody EMWO_INNERDTO emwo_innerdto) {
        EMWO_INNER domain = emwo_innerMapping.toDomain(emwo_innerdto);
        domain.setEquipid(emequip_id);
        domain.setEmwoInnerid(emwo_inner_id);
		emwo_innerService.update(domain);
        EMWO_INNERDTO dto = emwo_innerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_innerService.getEmwoInnerByEntities(this.emwo_innerMapping.toDomain(#emwo_innerdtos)),'eam-EMWO_INNER-Update')")
    @ApiOperation(value = "根据设备档案批量更新内部工单", tags = {"内部工单" },  notes = "根据设备档案批量更新内部工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emwo_inners/batch")
    public ResponseEntity<Boolean> updateBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_INNERDTO> emwo_innerdtos) {
        List<EMWO_INNER> domainlist=emwo_innerMapping.toDomain(emwo_innerdtos);
        for(EMWO_INNER domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emwo_innerService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwo_innerService.get(#emwo_inner_id),'eam-EMWO_INNER-Remove')")
    @ApiOperation(value = "根据设备档案删除内部工单", tags = {"内部工单" },  notes = "根据设备档案删除内部工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emwo_inners/{emwo_inner_id}")
    public ResponseEntity<Boolean> removeByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_inner_id") String emwo_inner_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emwo_innerService.remove(emwo_inner_id));
    }

    @PreAuthorize("hasPermission(this.emwo_innerService.getEmwoInnerByIds(#ids),'eam-EMWO_INNER-Remove')")
    @ApiOperation(value = "根据设备档案批量删除内部工单", tags = {"内部工单" },  notes = "根据设备档案批量删除内部工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emwo_inners/batch")
    public ResponseEntity<Boolean> removeBatchByEMEquip(@RequestBody List<String> ids) {
        emwo_innerService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwo_innerMapping.toDomain(returnObject.body),'eam-EMWO_INNER-Get')")
    @ApiOperation(value = "根据设备档案获取内部工单", tags = {"内部工单" },  notes = "根据设备档案获取内部工单")
	@RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emwo_inners/{emwo_inner_id}")
    public ResponseEntity<EMWO_INNERDTO> getByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_inner_id") String emwo_inner_id) {
        EMWO_INNER domain = emwo_innerService.get(emwo_inner_id);
        EMWO_INNERDTO dto = emwo_innerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据设备档案获取内部工单草稿", tags = {"内部工单" },  notes = "根据设备档案获取内部工单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emwo_inners/getdraft")
    public ResponseEntity<EMWO_INNERDTO> getDraftByEMEquip(@PathVariable("emequip_id") String emequip_id, EMWO_INNERDTO dto) {
        EMWO_INNER domain = emwo_innerMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_innerMapping.toDto(emwo_innerService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-Acceptance-all')")
    @ApiOperation(value = "根据设备档案内部工单", tags = {"内部工单" },  notes = "根据设备档案内部工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_inners/{emwo_inner_id}/acceptance")
    public ResponseEntity<EMWO_INNERDTO> acceptanceByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_inner_id") String emwo_inner_id, @RequestBody EMWO_INNERDTO emwo_innerdto) {
        EMWO_INNER domain = emwo_innerMapping.toDomain(emwo_innerdto);
        domain.setEquipid(emequip_id);
        domain = emwo_innerService.acceptance(domain) ;
        emwo_innerdto = emwo_innerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_innerdto);
    }
    @ApiOperation(value = "批量处理[根据设备档案内部工单]", tags = {"内部工单" },  notes = "批量处理[根据设备档案内部工单]")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_inners/acceptancebatch")
    public ResponseEntity<Boolean> acceptanceByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_INNERDTO> emwo_innerdtos) {
        List<EMWO_INNER> domains = emwo_innerMapping.toDomain(emwo_innerdtos);
        boolean result = emwo_innerService.acceptanceBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @ApiOperation(value = "根据设备档案检查内部工单", tags = {"内部工单" },  notes = "根据设备档案检查内部工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_inners/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_INNERDTO emwo_innerdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwo_innerService.checkKey(emwo_innerMapping.toDomain(emwo_innerdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-FormUpdateByEmequipid-all')")
    @ApiOperation(value = "根据设备档案内部工单", tags = {"内部工单" },  notes = "根据设备档案内部工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emwo_inners/{emwo_inner_id}/formupdatebyemequipid")
    public ResponseEntity<EMWO_INNERDTO> formUpdateByEmequipidByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_inner_id") String emwo_inner_id, @RequestBody EMWO_INNERDTO emwo_innerdto) {
        EMWO_INNER domain = emwo_innerMapping.toDomain(emwo_innerdto);
        domain.setEquipid(emequip_id);
        domain = emwo_innerService.formUpdateByEmequipid(domain) ;
        emwo_innerdto = emwo_innerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_innerdto);
    }
    @PreAuthorize("hasPermission(this.emwo_innerMapping.toDomain(#emwo_innerdto),'eam-EMWO_INNER-Save')")
    @ApiOperation(value = "根据设备档案保存内部工单", tags = {"内部工单" },  notes = "根据设备档案保存内部工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_inners/save")
    public ResponseEntity<EMWO_INNERDTO> saveByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_INNERDTO emwo_innerdto) {
        EMWO_INNER domain = emwo_innerMapping.toDomain(emwo_innerdto);
        domain.setEquipid(emequip_id);
        emwo_innerService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_innerMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emwo_innerMapping.toDomain(#emwo_innerdtos),'eam-EMWO_INNER-Save')")
    @ApiOperation(value = "根据设备档案批量保存内部工单", tags = {"内部工单" },  notes = "根据设备档案批量保存内部工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_inners/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_INNERDTO> emwo_innerdtos) {
        List<EMWO_INNER> domainlist=emwo_innerMapping.toDomain(emwo_innerdtos);
        for(EMWO_INNER domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emwo_innerService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-Submit-all')")
    @ApiOperation(value = "根据设备档案内部工单", tags = {"内部工单" },  notes = "根据设备档案内部工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_inners/{emwo_inner_id}/submit")
    public ResponseEntity<EMWO_INNERDTO> submitByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_inner_id") String emwo_inner_id, @RequestBody EMWO_INNERDTO emwo_innerdto) {
        EMWO_INNER domain = emwo_innerMapping.toDomain(emwo_innerdto);
        domain.setEquipid(emequip_id);
        domain = emwo_innerService.submit(domain) ;
        emwo_innerdto = emwo_innerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_innerdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-UnAcceptance-all')")
    @ApiOperation(value = "根据设备档案内部工单", tags = {"内部工单" },  notes = "根据设备档案内部工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_inners/{emwo_inner_id}/unacceptance")
    public ResponseEntity<EMWO_INNERDTO> unAcceptanceByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_inner_id") String emwo_inner_id, @RequestBody EMWO_INNERDTO emwo_innerdto) {
        EMWO_INNER domain = emwo_innerMapping.toDomain(emwo_innerdto);
        domain.setEquipid(emequip_id);
        domain = emwo_innerService.unAcceptance(domain) ;
        emwo_innerdto = emwo_innerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_innerdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-searchCalendar-all') and hasPermission(#context,'eam-EMWO_INNER-Get')")
	@ApiOperation(value = "根据设备档案获取日历查询", tags = {"内部工单" } ,notes = "根据设备档案获取日历查询")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emwo_inners/fetchcalendar")
	public ResponseEntity<List<EMWO_INNERDTO>> fetchEMWO_INNERCalendarByEMEquip(@PathVariable("emequip_id") String emequip_id,EMWO_INNERSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_INNER> domains = emwo_innerService.searchCalendar(context) ;
        List<EMWO_INNERDTO> list = emwo_innerMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-searchCalendar-all') and hasPermission(#context,'eam-EMWO_INNER-Get')")
	@ApiOperation(value = "根据设备档案查询日历查询", tags = {"内部工单" } ,notes = "根据设备档案查询日历查询")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emwo_inners/searchcalendar")
	public ResponseEntity<Page<EMWO_INNERDTO>> searchEMWO_INNERCalendarByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_INNERSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_INNER> domains = emwo_innerService.searchCalendar(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_innerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-searchConfirmed-all') and hasPermission(#context,'eam-EMWO_INNER-Get')")
	@ApiOperation(value = "根据设备档案获取已完成", tags = {"内部工单" } ,notes = "根据设备档案获取已完成")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emwo_inners/fetchconfirmed")
	public ResponseEntity<List<EMWO_INNERDTO>> fetchEMWO_INNERConfirmedByEMEquip(@PathVariable("emequip_id") String emequip_id,EMWO_INNERSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_INNER> domains = emwo_innerService.searchConfirmed(context) ;
        List<EMWO_INNERDTO> list = emwo_innerMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-searchConfirmed-all') and hasPermission(#context,'eam-EMWO_INNER-Get')")
	@ApiOperation(value = "根据设备档案查询已完成", tags = {"内部工单" } ,notes = "根据设备档案查询已完成")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emwo_inners/searchconfirmed")
	public ResponseEntity<Page<EMWO_INNERDTO>> searchEMWO_INNERConfirmedByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_INNERSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_INNER> domains = emwo_innerService.searchConfirmed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_innerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-searchDefault-all') and hasPermission(#context,'eam-EMWO_INNER-Get')")
	@ApiOperation(value = "根据设备档案获取DEFAULT", tags = {"内部工单" } ,notes = "根据设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emwo_inners/fetchdefault")
	public ResponseEntity<List<EMWO_INNERDTO>> fetchEMWO_INNERDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id,EMWO_INNERSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_INNER> domains = emwo_innerService.searchDefault(context) ;
        List<EMWO_INNERDTO> list = emwo_innerMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-searchDefault-all') and hasPermission(#context,'eam-EMWO_INNER-Get')")
	@ApiOperation(value = "根据设备档案查询DEFAULT", tags = {"内部工单" } ,notes = "根据设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emwo_inners/searchdefault")
	public ResponseEntity<Page<EMWO_INNERDTO>> searchEMWO_INNERDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_INNERSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_INNER> domains = emwo_innerService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_innerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-searchDraft-all') and hasPermission(#context,'eam-EMWO_INNER-Get')")
	@ApiOperation(value = "根据设备档案获取未提交", tags = {"内部工单" } ,notes = "根据设备档案获取未提交")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emwo_inners/fetchdraft")
	public ResponseEntity<List<EMWO_INNERDTO>> fetchEMWO_INNERDraftByEMEquip(@PathVariable("emequip_id") String emequip_id,EMWO_INNERSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_INNER> domains = emwo_innerService.searchDraft(context) ;
        List<EMWO_INNERDTO> list = emwo_innerMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-searchDraft-all') and hasPermission(#context,'eam-EMWO_INNER-Get')")
	@ApiOperation(value = "根据设备档案查询未提交", tags = {"内部工单" } ,notes = "根据设备档案查询未提交")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emwo_inners/searchdraft")
	public ResponseEntity<Page<EMWO_INNERDTO>> searchEMWO_INNERDraftByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_INNERSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_INNER> domains = emwo_innerService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_innerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-searchToConfirm-all') and hasPermission(#context,'eam-EMWO_INNER-Get')")
	@ApiOperation(value = "根据设备档案获取进行中", tags = {"内部工单" } ,notes = "根据设备档案获取进行中")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emwo_inners/fetchtoconfirm")
	public ResponseEntity<List<EMWO_INNERDTO>> fetchEMWO_INNERToConfirmByEMEquip(@PathVariable("emequip_id") String emequip_id,EMWO_INNERSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_INNER> domains = emwo_innerService.searchToConfirm(context) ;
        List<EMWO_INNERDTO> list = emwo_innerMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-searchToConfirm-all') and hasPermission(#context,'eam-EMWO_INNER-Get')")
	@ApiOperation(value = "根据设备档案查询进行中", tags = {"内部工单" } ,notes = "根据设备档案查询进行中")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emwo_inners/searchtoconfirm")
	public ResponseEntity<Page<EMWO_INNERDTO>> searchEMWO_INNERToConfirmByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_INNERSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_INNER> domains = emwo_innerService.searchToConfirm(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_innerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emwo_innerMapping.toDomain(#emwo_innerdto),'eam-EMWO_INNER-Create')")
    @ApiOperation(value = "根据班组设备档案建立内部工单", tags = {"内部工单" },  notes = "根据班组设备档案建立内部工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_inners")
    public ResponseEntity<EMWO_INNERDTO> createByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_INNERDTO emwo_innerdto) {
        EMWO_INNER domain = emwo_innerMapping.toDomain(emwo_innerdto);
        domain.setEquipid(emequip_id);
		emwo_innerService.create(domain);
        EMWO_INNERDTO dto = emwo_innerMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_innerMapping.toDomain(#emwo_innerdtos),'eam-EMWO_INNER-Create')")
    @ApiOperation(value = "根据班组设备档案批量建立内部工单", tags = {"内部工单" },  notes = "根据班组设备档案批量建立内部工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_inners/batch")
    public ResponseEntity<Boolean> createBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_INNERDTO> emwo_innerdtos) {
        List<EMWO_INNER> domainlist=emwo_innerMapping.toDomain(emwo_innerdtos);
        for(EMWO_INNER domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emwo_innerService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwo_inner" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwo_innerService.get(#emwo_inner_id),'eam-EMWO_INNER-Update')")
    @ApiOperation(value = "根据班组设备档案更新内部工单", tags = {"内部工单" },  notes = "根据班组设备档案更新内部工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_inners/{emwo_inner_id}")
    public ResponseEntity<EMWO_INNERDTO> updateByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_inner_id") String emwo_inner_id, @RequestBody EMWO_INNERDTO emwo_innerdto) {
        EMWO_INNER domain = emwo_innerMapping.toDomain(emwo_innerdto);
        domain.setEquipid(emequip_id);
        domain.setEmwoInnerid(emwo_inner_id);
		emwo_innerService.update(domain);
        EMWO_INNERDTO dto = emwo_innerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_innerService.getEmwoInnerByEntities(this.emwo_innerMapping.toDomain(#emwo_innerdtos)),'eam-EMWO_INNER-Update')")
    @ApiOperation(value = "根据班组设备档案批量更新内部工单", tags = {"内部工单" },  notes = "根据班组设备档案批量更新内部工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_inners/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_INNERDTO> emwo_innerdtos) {
        List<EMWO_INNER> domainlist=emwo_innerMapping.toDomain(emwo_innerdtos);
        for(EMWO_INNER domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emwo_innerService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwo_innerService.get(#emwo_inner_id),'eam-EMWO_INNER-Remove')")
    @ApiOperation(value = "根据班组设备档案删除内部工单", tags = {"内部工单" },  notes = "根据班组设备档案删除内部工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_inners/{emwo_inner_id}")
    public ResponseEntity<Boolean> removeByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_inner_id") String emwo_inner_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emwo_innerService.remove(emwo_inner_id));
    }

    @PreAuthorize("hasPermission(this.emwo_innerService.getEmwoInnerByIds(#ids),'eam-EMWO_INNER-Remove')")
    @ApiOperation(value = "根据班组设备档案批量删除内部工单", tags = {"内部工单" },  notes = "根据班组设备档案批量删除内部工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_inners/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeamEMEquip(@RequestBody List<String> ids) {
        emwo_innerService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwo_innerMapping.toDomain(returnObject.body),'eam-EMWO_INNER-Get')")
    @ApiOperation(value = "根据班组设备档案获取内部工单", tags = {"内部工单" },  notes = "根据班组设备档案获取内部工单")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_inners/{emwo_inner_id}")
    public ResponseEntity<EMWO_INNERDTO> getByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_inner_id") String emwo_inner_id) {
        EMWO_INNER domain = emwo_innerService.get(emwo_inner_id);
        EMWO_INNERDTO dto = emwo_innerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组设备档案获取内部工单草稿", tags = {"内部工单" },  notes = "根据班组设备档案获取内部工单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_inners/getdraft")
    public ResponseEntity<EMWO_INNERDTO> getDraftByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, EMWO_INNERDTO dto) {
        EMWO_INNER domain = emwo_innerMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_innerMapping.toDto(emwo_innerService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-Acceptance-all')")
    @ApiOperation(value = "根据班组设备档案内部工单", tags = {"内部工单" },  notes = "根据班组设备档案内部工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_inners/{emwo_inner_id}/acceptance")
    public ResponseEntity<EMWO_INNERDTO> acceptanceByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_inner_id") String emwo_inner_id, @RequestBody EMWO_INNERDTO emwo_innerdto) {
        EMWO_INNER domain = emwo_innerMapping.toDomain(emwo_innerdto);
        domain.setEquipid(emequip_id);
        domain = emwo_innerService.acceptance(domain) ;
        emwo_innerdto = emwo_innerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_innerdto);
    }
    @ApiOperation(value = "批量处理[根据班组设备档案内部工单]", tags = {"内部工单" },  notes = "批量处理[根据班组设备档案内部工单]")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_inners/acceptancebatch")
    public ResponseEntity<Boolean> acceptanceByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_INNERDTO> emwo_innerdtos) {
        List<EMWO_INNER> domains = emwo_innerMapping.toDomain(emwo_innerdtos);
        boolean result = emwo_innerService.acceptanceBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @ApiOperation(value = "根据班组设备档案检查内部工单", tags = {"内部工单" },  notes = "根据班组设备档案检查内部工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_inners/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_INNERDTO emwo_innerdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwo_innerService.checkKey(emwo_innerMapping.toDomain(emwo_innerdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-FormUpdateByEmequipid-all')")
    @ApiOperation(value = "根据班组设备档案内部工单", tags = {"内部工单" },  notes = "根据班组设备档案内部工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_inners/{emwo_inner_id}/formupdatebyemequipid")
    public ResponseEntity<EMWO_INNERDTO> formUpdateByEmequipidByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_inner_id") String emwo_inner_id, @RequestBody EMWO_INNERDTO emwo_innerdto) {
        EMWO_INNER domain = emwo_innerMapping.toDomain(emwo_innerdto);
        domain.setEquipid(emequip_id);
        domain = emwo_innerService.formUpdateByEmequipid(domain) ;
        emwo_innerdto = emwo_innerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_innerdto);
    }
    @PreAuthorize("hasPermission(this.emwo_innerMapping.toDomain(#emwo_innerdto),'eam-EMWO_INNER-Save')")
    @ApiOperation(value = "根据班组设备档案保存内部工单", tags = {"内部工单" },  notes = "根据班组设备档案保存内部工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_inners/save")
    public ResponseEntity<EMWO_INNERDTO> saveByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_INNERDTO emwo_innerdto) {
        EMWO_INNER domain = emwo_innerMapping.toDomain(emwo_innerdto);
        domain.setEquipid(emequip_id);
        emwo_innerService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_innerMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emwo_innerMapping.toDomain(#emwo_innerdtos),'eam-EMWO_INNER-Save')")
    @ApiOperation(value = "根据班组设备档案批量保存内部工单", tags = {"内部工单" },  notes = "根据班组设备档案批量保存内部工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_inners/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_INNERDTO> emwo_innerdtos) {
        List<EMWO_INNER> domainlist=emwo_innerMapping.toDomain(emwo_innerdtos);
        for(EMWO_INNER domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emwo_innerService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-Submit-all')")
    @ApiOperation(value = "根据班组设备档案内部工单", tags = {"内部工单" },  notes = "根据班组设备档案内部工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_inners/{emwo_inner_id}/submit")
    public ResponseEntity<EMWO_INNERDTO> submitByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_inner_id") String emwo_inner_id, @RequestBody EMWO_INNERDTO emwo_innerdto) {
        EMWO_INNER domain = emwo_innerMapping.toDomain(emwo_innerdto);
        domain.setEquipid(emequip_id);
        domain = emwo_innerService.submit(domain) ;
        emwo_innerdto = emwo_innerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_innerdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-UnAcceptance-all')")
    @ApiOperation(value = "根据班组设备档案内部工单", tags = {"内部工单" },  notes = "根据班组设备档案内部工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_inners/{emwo_inner_id}/unacceptance")
    public ResponseEntity<EMWO_INNERDTO> unAcceptanceByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_inner_id") String emwo_inner_id, @RequestBody EMWO_INNERDTO emwo_innerdto) {
        EMWO_INNER domain = emwo_innerMapping.toDomain(emwo_innerdto);
        domain.setEquipid(emequip_id);
        domain = emwo_innerService.unAcceptance(domain) ;
        emwo_innerdto = emwo_innerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_innerdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-searchCalendar-all') and hasPermission(#context,'eam-EMWO_INNER-Get')")
	@ApiOperation(value = "根据班组设备档案获取日历查询", tags = {"内部工单" } ,notes = "根据班组设备档案获取日历查询")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_inners/fetchcalendar")
	public ResponseEntity<List<EMWO_INNERDTO>> fetchEMWO_INNERCalendarByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMWO_INNERSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_INNER> domains = emwo_innerService.searchCalendar(context) ;
        List<EMWO_INNERDTO> list = emwo_innerMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-searchCalendar-all') and hasPermission(#context,'eam-EMWO_INNER-Get')")
	@ApiOperation(value = "根据班组设备档案查询日历查询", tags = {"内部工单" } ,notes = "根据班组设备档案查询日历查询")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_inners/searchcalendar")
	public ResponseEntity<Page<EMWO_INNERDTO>> searchEMWO_INNERCalendarByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_INNERSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_INNER> domains = emwo_innerService.searchCalendar(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_innerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-searchConfirmed-all') and hasPermission(#context,'eam-EMWO_INNER-Get')")
	@ApiOperation(value = "根据班组设备档案获取已完成", tags = {"内部工单" } ,notes = "根据班组设备档案获取已完成")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_inners/fetchconfirmed")
	public ResponseEntity<List<EMWO_INNERDTO>> fetchEMWO_INNERConfirmedByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMWO_INNERSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_INNER> domains = emwo_innerService.searchConfirmed(context) ;
        List<EMWO_INNERDTO> list = emwo_innerMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-searchConfirmed-all') and hasPermission(#context,'eam-EMWO_INNER-Get')")
	@ApiOperation(value = "根据班组设备档案查询已完成", tags = {"内部工单" } ,notes = "根据班组设备档案查询已完成")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_inners/searchconfirmed")
	public ResponseEntity<Page<EMWO_INNERDTO>> searchEMWO_INNERConfirmedByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_INNERSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_INNER> domains = emwo_innerService.searchConfirmed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_innerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-searchDefault-all') and hasPermission(#context,'eam-EMWO_INNER-Get')")
	@ApiOperation(value = "根据班组设备档案获取DEFAULT", tags = {"内部工单" } ,notes = "根据班组设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_inners/fetchdefault")
	public ResponseEntity<List<EMWO_INNERDTO>> fetchEMWO_INNERDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMWO_INNERSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_INNER> domains = emwo_innerService.searchDefault(context) ;
        List<EMWO_INNERDTO> list = emwo_innerMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-searchDefault-all') and hasPermission(#context,'eam-EMWO_INNER-Get')")
	@ApiOperation(value = "根据班组设备档案查询DEFAULT", tags = {"内部工单" } ,notes = "根据班组设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_inners/searchdefault")
	public ResponseEntity<Page<EMWO_INNERDTO>> searchEMWO_INNERDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_INNERSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_INNER> domains = emwo_innerService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_innerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-searchDraft-all') and hasPermission(#context,'eam-EMWO_INNER-Get')")
	@ApiOperation(value = "根据班组设备档案获取未提交", tags = {"内部工单" } ,notes = "根据班组设备档案获取未提交")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_inners/fetchdraft")
	public ResponseEntity<List<EMWO_INNERDTO>> fetchEMWO_INNERDraftByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMWO_INNERSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_INNER> domains = emwo_innerService.searchDraft(context) ;
        List<EMWO_INNERDTO> list = emwo_innerMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-searchDraft-all') and hasPermission(#context,'eam-EMWO_INNER-Get')")
	@ApiOperation(value = "根据班组设备档案查询未提交", tags = {"内部工单" } ,notes = "根据班组设备档案查询未提交")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_inners/searchdraft")
	public ResponseEntity<Page<EMWO_INNERDTO>> searchEMWO_INNERDraftByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_INNERSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_INNER> domains = emwo_innerService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_innerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-searchToConfirm-all') and hasPermission(#context,'eam-EMWO_INNER-Get')")
	@ApiOperation(value = "根据班组设备档案获取进行中", tags = {"内部工单" } ,notes = "根据班组设备档案获取进行中")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_inners/fetchtoconfirm")
	public ResponseEntity<List<EMWO_INNERDTO>> fetchEMWO_INNERToConfirmByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMWO_INNERSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_INNER> domains = emwo_innerService.searchToConfirm(context) ;
        List<EMWO_INNERDTO> list = emwo_innerMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_INNER-searchToConfirm-all') and hasPermission(#context,'eam-EMWO_INNER-Get')")
	@ApiOperation(value = "根据班组设备档案查询进行中", tags = {"内部工单" } ,notes = "根据班组设备档案查询进行中")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_inners/searchtoconfirm")
	public ResponseEntity<Page<EMWO_INNERDTO>> searchEMWO_INNERToConfirmByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_INNERSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_INNER> domains = emwo_innerService.searchToConfirm(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_innerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

