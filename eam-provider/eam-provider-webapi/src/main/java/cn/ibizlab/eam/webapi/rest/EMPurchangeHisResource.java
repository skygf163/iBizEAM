package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMPurchangeHis;
import cn.ibizlab.eam.core.eam_core.service.IEMPurchangeHisService;
import cn.ibizlab.eam.core.eam_core.filter.EMPurchangeHisSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"领料单换料记录" })
@RestController("WebApi-empurchangehis")
@RequestMapping("")
public class EMPurchangeHisResource {

    @Autowired
    public IEMPurchangeHisService empurchangehisService;

    @Autowired
    @Lazy
    public EMPurchangeHisMapping empurchangehisMapping;

    @PreAuthorize("hasPermission(this.empurchangehisMapping.toDomain(#empurchangehisdto),'eam-EMPurchangeHis-Create')")
    @ApiOperation(value = "新建领料单换料记录", tags = {"领料单换料记录" },  notes = "新建领料单换料记录")
	@RequestMapping(method = RequestMethod.POST, value = "/empurchangehis")
    public ResponseEntity<EMPurchangeHisDTO> create(@Validated @RequestBody EMPurchangeHisDTO empurchangehisdto) {
        EMPurchangeHis domain = empurchangehisMapping.toDomain(empurchangehisdto);
		empurchangehisService.create(domain);
        EMPurchangeHisDTO dto = empurchangehisMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empurchangehisMapping.toDomain(#empurchangehisdtos),'eam-EMPurchangeHis-Create')")
    @ApiOperation(value = "批量新建领料单换料记录", tags = {"领料单换料记录" },  notes = "批量新建领料单换料记录")
	@RequestMapping(method = RequestMethod.POST, value = "/empurchangehis/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMPurchangeHisDTO> empurchangehisdtos) {
        empurchangehisService.createBatch(empurchangehisMapping.toDomain(empurchangehisdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "empurchangehis" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.empurchangehisService.get(#empurchangehis_id),'eam-EMPurchangeHis-Update')")
    @ApiOperation(value = "更新领料单换料记录", tags = {"领料单换料记录" },  notes = "更新领料单换料记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/empurchangehis/{empurchangehis_id}")
    public ResponseEntity<EMPurchangeHisDTO> update(@PathVariable("empurchangehis_id") String empurchangehis_id, @RequestBody EMPurchangeHisDTO empurchangehisdto) {
		EMPurchangeHis domain  = empurchangehisMapping.toDomain(empurchangehisdto);
        domain .setEmpurchangehisid(empurchangehis_id);
		empurchangehisService.update(domain );
		EMPurchangeHisDTO dto = empurchangehisMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empurchangehisService.getEmpurchangehisByEntities(this.empurchangehisMapping.toDomain(#empurchangehisdtos)),'eam-EMPurchangeHis-Update')")
    @ApiOperation(value = "批量更新领料单换料记录", tags = {"领料单换料记录" },  notes = "批量更新领料单换料记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/empurchangehis/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMPurchangeHisDTO> empurchangehisdtos) {
        empurchangehisService.updateBatch(empurchangehisMapping.toDomain(empurchangehisdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.empurchangehisService.get(#empurchangehis_id),'eam-EMPurchangeHis-Remove')")
    @ApiOperation(value = "删除领料单换料记录", tags = {"领料单换料记录" },  notes = "删除领料单换料记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/empurchangehis/{empurchangehis_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("empurchangehis_id") String empurchangehis_id) {
         return ResponseEntity.status(HttpStatus.OK).body(empurchangehisService.remove(empurchangehis_id));
    }

    @PreAuthorize("hasPermission(this.empurchangehisService.getEmpurchangehisByIds(#ids),'eam-EMPurchangeHis-Remove')")
    @ApiOperation(value = "批量删除领料单换料记录", tags = {"领料单换料记录" },  notes = "批量删除领料单换料记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/empurchangehis/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        empurchangehisService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.empurchangehisMapping.toDomain(returnObject.body),'eam-EMPurchangeHis-Get')")
    @ApiOperation(value = "获取领料单换料记录", tags = {"领料单换料记录" },  notes = "获取领料单换料记录")
	@RequestMapping(method = RequestMethod.GET, value = "/empurchangehis/{empurchangehis_id}")
    public ResponseEntity<EMPurchangeHisDTO> get(@PathVariable("empurchangehis_id") String empurchangehis_id) {
        EMPurchangeHis domain = empurchangehisService.get(empurchangehis_id);
        EMPurchangeHisDTO dto = empurchangehisMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取领料单换料记录草稿", tags = {"领料单换料记录" },  notes = "获取领料单换料记录草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/empurchangehis/getdraft")
    public ResponseEntity<EMPurchangeHisDTO> getDraft(EMPurchangeHisDTO dto) {
        EMPurchangeHis domain = empurchangehisMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(empurchangehisMapping.toDto(empurchangehisService.getDraft(domain)));
    }

    @ApiOperation(value = "检查领料单换料记录", tags = {"领料单换料记录" },  notes = "检查领料单换料记录")
	@RequestMapping(method = RequestMethod.POST, value = "/empurchangehis/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMPurchangeHisDTO empurchangehisdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(empurchangehisService.checkKey(empurchangehisMapping.toDomain(empurchangehisdto)));
    }

    @PreAuthorize("hasPermission(this.empurchangehisMapping.toDomain(#empurchangehisdto),'eam-EMPurchangeHis-Save')")
    @ApiOperation(value = "保存领料单换料记录", tags = {"领料单换料记录" },  notes = "保存领料单换料记录")
	@RequestMapping(method = RequestMethod.POST, value = "/empurchangehis/save")
    public ResponseEntity<EMPurchangeHisDTO> save(@RequestBody EMPurchangeHisDTO empurchangehisdto) {
        EMPurchangeHis domain = empurchangehisMapping.toDomain(empurchangehisdto);
        empurchangehisService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empurchangehisMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.empurchangehisMapping.toDomain(#empurchangehisdtos),'eam-EMPurchangeHis-Save')")
    @ApiOperation(value = "批量保存领料单换料记录", tags = {"领料单换料记录" },  notes = "批量保存领料单换料记录")
	@RequestMapping(method = RequestMethod.POST, value = "/empurchangehis/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMPurchangeHisDTO> empurchangehisdtos) {
        empurchangehisService.saveBatch(empurchangehisMapping.toDomain(empurchangehisdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPurchangeHis-searchDefault-all') and hasPermission(#context,'eam-EMPurchangeHis-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"领料单换料记录" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/empurchangehis/fetchdefault")
	public ResponseEntity<List<EMPurchangeHisDTO>> fetchDefault(EMPurchangeHisSearchContext context) {
        Page<EMPurchangeHis> domains = empurchangehisService.searchDefault(context) ;
        List<EMPurchangeHisDTO> list = empurchangehisMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPurchangeHis-searchDefault-all') and hasPermission(#context,'eam-EMPurchangeHis-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"领料单换料记录" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/empurchangehis/searchdefault")
	public ResponseEntity<Page<EMPurchangeHisDTO>> searchDefault(@RequestBody EMPurchangeHisSearchContext context) {
        Page<EMPurchangeHis> domains = empurchangehisService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empurchangehisMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

