package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQLCTRHY;
import cn.ibizlab.eam.core.eam_core.service.IEMEQLCTRHYService;
import cn.ibizlab.eam.core.eam_core.filter.EMEQLCTRHYSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"润滑油位置" })
@RestController("WebApi-emeqlctrhy")
@RequestMapping("")
public class EMEQLCTRHYResource {

    @Autowired
    public IEMEQLCTRHYService emeqlctrhyService;

    @Autowired
    @Lazy
    public EMEQLCTRHYMapping emeqlctrhyMapping;

    @PreAuthorize("hasPermission(this.emeqlctrhyMapping.toDomain(#emeqlctrhydto),'eam-EMEQLCTRHY-Create')")
    @ApiOperation(value = "新建润滑油位置", tags = {"润滑油位置" },  notes = "新建润滑油位置")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqlctrhies")
    public ResponseEntity<EMEQLCTRHYDTO> create(@Validated @RequestBody EMEQLCTRHYDTO emeqlctrhydto) {
        EMEQLCTRHY domain = emeqlctrhyMapping.toDomain(emeqlctrhydto);
		emeqlctrhyService.create(domain);
        EMEQLCTRHYDTO dto = emeqlctrhyMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqlctrhyMapping.toDomain(#emeqlctrhydtos),'eam-EMEQLCTRHY-Create')")
    @ApiOperation(value = "批量新建润滑油位置", tags = {"润滑油位置" },  notes = "批量新建润滑油位置")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqlctrhies/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEQLCTRHYDTO> emeqlctrhydtos) {
        emeqlctrhyService.createBatch(emeqlctrhyMapping.toDomain(emeqlctrhydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqlctrhy" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqlctrhyService.get(#emeqlctrhy_id),'eam-EMEQLCTRHY-Update')")
    @ApiOperation(value = "更新润滑油位置", tags = {"润滑油位置" },  notes = "更新润滑油位置")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqlctrhies/{emeqlctrhy_id}")
    public ResponseEntity<EMEQLCTRHYDTO> update(@PathVariable("emeqlctrhy_id") String emeqlctrhy_id, @RequestBody EMEQLCTRHYDTO emeqlctrhydto) {
		EMEQLCTRHY domain  = emeqlctrhyMapping.toDomain(emeqlctrhydto);
        domain .setEmeqlocationid(emeqlctrhy_id);
		emeqlctrhyService.update(domain );
		EMEQLCTRHYDTO dto = emeqlctrhyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqlctrhyService.getEmeqlctrhyByEntities(this.emeqlctrhyMapping.toDomain(#emeqlctrhydtos)),'eam-EMEQLCTRHY-Update')")
    @ApiOperation(value = "批量更新润滑油位置", tags = {"润滑油位置" },  notes = "批量更新润滑油位置")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqlctrhies/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEQLCTRHYDTO> emeqlctrhydtos) {
        emeqlctrhyService.updateBatch(emeqlctrhyMapping.toDomain(emeqlctrhydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqlctrhyService.get(#emeqlctrhy_id),'eam-EMEQLCTRHY-Remove')")
    @ApiOperation(value = "删除润滑油位置", tags = {"润滑油位置" },  notes = "删除润滑油位置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqlctrhies/{emeqlctrhy_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeqlctrhy_id") String emeqlctrhy_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeqlctrhyService.remove(emeqlctrhy_id));
    }

    @PreAuthorize("hasPermission(this.emeqlctrhyService.getEmeqlctrhyByIds(#ids),'eam-EMEQLCTRHY-Remove')")
    @ApiOperation(value = "批量删除润滑油位置", tags = {"润滑油位置" },  notes = "批量删除润滑油位置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqlctrhies/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeqlctrhyService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqlctrhyMapping.toDomain(returnObject.body),'eam-EMEQLCTRHY-Get')")
    @ApiOperation(value = "获取润滑油位置", tags = {"润滑油位置" },  notes = "获取润滑油位置")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqlctrhies/{emeqlctrhy_id}")
    public ResponseEntity<EMEQLCTRHYDTO> get(@PathVariable("emeqlctrhy_id") String emeqlctrhy_id) {
        EMEQLCTRHY domain = emeqlctrhyService.get(emeqlctrhy_id);
        EMEQLCTRHYDTO dto = emeqlctrhyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取润滑油位置草稿", tags = {"润滑油位置" },  notes = "获取润滑油位置草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqlctrhies/getdraft")
    public ResponseEntity<EMEQLCTRHYDTO> getDraft(EMEQLCTRHYDTO dto) {
        EMEQLCTRHY domain = emeqlctrhyMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emeqlctrhyMapping.toDto(emeqlctrhyService.getDraft(domain)));
    }

    @ApiOperation(value = "检查润滑油位置", tags = {"润滑油位置" },  notes = "检查润滑油位置")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqlctrhies/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEQLCTRHYDTO emeqlctrhydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqlctrhyService.checkKey(emeqlctrhyMapping.toDomain(emeqlctrhydto)));
    }

    @PreAuthorize("hasPermission(this.emeqlctrhyMapping.toDomain(#emeqlctrhydto),'eam-EMEQLCTRHY-Save')")
    @ApiOperation(value = "保存润滑油位置", tags = {"润滑油位置" },  notes = "保存润滑油位置")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqlctrhies/save")
    public ResponseEntity<EMEQLCTRHYDTO> save(@RequestBody EMEQLCTRHYDTO emeqlctrhydto) {
        EMEQLCTRHY domain = emeqlctrhyMapping.toDomain(emeqlctrhydto);
        emeqlctrhyService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqlctrhyMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqlctrhyMapping.toDomain(#emeqlctrhydtos),'eam-EMEQLCTRHY-Save')")
    @ApiOperation(value = "批量保存润滑油位置", tags = {"润滑油位置" },  notes = "批量保存润滑油位置")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqlctrhies/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEQLCTRHYDTO> emeqlctrhydtos) {
        emeqlctrhyService.saveBatch(emeqlctrhyMapping.toDomain(emeqlctrhydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQLCTRHY-searchDefault-all') and hasPermission(#context,'eam-EMEQLCTRHY-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"润滑油位置" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeqlctrhies/fetchdefault")
	public ResponseEntity<List<EMEQLCTRHYDTO>> fetchDefault(EMEQLCTRHYSearchContext context) {
        Page<EMEQLCTRHY> domains = emeqlctrhyService.searchDefault(context) ;
        List<EMEQLCTRHYDTO> list = emeqlctrhyMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQLCTRHY-searchDefault-all') and hasPermission(#context,'eam-EMEQLCTRHY-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"润滑油位置" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeqlctrhies/searchdefault")
	public ResponseEntity<Page<EMEQLCTRHYDTO>> searchDefault(@RequestBody EMEQLCTRHYSearchContext context) {
        Page<EMEQLCTRHY> domains = emeqlctrhyService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqlctrhyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

