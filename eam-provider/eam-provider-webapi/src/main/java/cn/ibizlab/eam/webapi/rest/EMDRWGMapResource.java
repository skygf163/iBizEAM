package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMDRWGMap;
import cn.ibizlab.eam.core.eam_core.service.IEMDRWGMapService;
import cn.ibizlab.eam.core.eam_core.filter.EMDRWGMapSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"文档引用" })
@RestController("WebApi-emdrwgmap")
@RequestMapping("")
public class EMDRWGMapResource {

    @Autowired
    public IEMDRWGMapService emdrwgmapService;

    @Autowired
    @Lazy
    public EMDRWGMapMapping emdrwgmapMapping;

    @PreAuthorize("hasPermission(this.emdrwgmapMapping.toDomain(#emdrwgmapdto),'eam-EMDRWGMap-Create')")
    @ApiOperation(value = "新建文档引用", tags = {"文档引用" },  notes = "新建文档引用")
	@RequestMapping(method = RequestMethod.POST, value = "/emdrwgmaps")
    public ResponseEntity<EMDRWGMapDTO> create(@Validated @RequestBody EMDRWGMapDTO emdrwgmapdto) {
        EMDRWGMap domain = emdrwgmapMapping.toDomain(emdrwgmapdto);
		emdrwgmapService.create(domain);
        EMDRWGMapDTO dto = emdrwgmapMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emdrwgmapMapping.toDomain(#emdrwgmapdtos),'eam-EMDRWGMap-Create')")
    @ApiOperation(value = "批量新建文档引用", tags = {"文档引用" },  notes = "批量新建文档引用")
	@RequestMapping(method = RequestMethod.POST, value = "/emdrwgmaps/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMDRWGMapDTO> emdrwgmapdtos) {
        emdrwgmapService.createBatch(emdrwgmapMapping.toDomain(emdrwgmapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emdrwgmap" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emdrwgmapService.get(#emdrwgmap_id),'eam-EMDRWGMap-Update')")
    @ApiOperation(value = "更新文档引用", tags = {"文档引用" },  notes = "更新文档引用")
	@RequestMapping(method = RequestMethod.PUT, value = "/emdrwgmaps/{emdrwgmap_id}")
    public ResponseEntity<EMDRWGMapDTO> update(@PathVariable("emdrwgmap_id") String emdrwgmap_id, @RequestBody EMDRWGMapDTO emdrwgmapdto) {
		EMDRWGMap domain  = emdrwgmapMapping.toDomain(emdrwgmapdto);
        domain .setEmdrwgmapid(emdrwgmap_id);
		emdrwgmapService.update(domain );
		EMDRWGMapDTO dto = emdrwgmapMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emdrwgmapService.getEmdrwgmapByEntities(this.emdrwgmapMapping.toDomain(#emdrwgmapdtos)),'eam-EMDRWGMap-Update')")
    @ApiOperation(value = "批量更新文档引用", tags = {"文档引用" },  notes = "批量更新文档引用")
	@RequestMapping(method = RequestMethod.PUT, value = "/emdrwgmaps/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMDRWGMapDTO> emdrwgmapdtos) {
        emdrwgmapService.updateBatch(emdrwgmapMapping.toDomain(emdrwgmapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emdrwgmapService.get(#emdrwgmap_id),'eam-EMDRWGMap-Remove')")
    @ApiOperation(value = "删除文档引用", tags = {"文档引用" },  notes = "删除文档引用")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emdrwgmaps/{emdrwgmap_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emdrwgmap_id") String emdrwgmap_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emdrwgmapService.remove(emdrwgmap_id));
    }

    @PreAuthorize("hasPermission(this.emdrwgmapService.getEmdrwgmapByIds(#ids),'eam-EMDRWGMap-Remove')")
    @ApiOperation(value = "批量删除文档引用", tags = {"文档引用" },  notes = "批量删除文档引用")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emdrwgmaps/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emdrwgmapService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emdrwgmapMapping.toDomain(returnObject.body),'eam-EMDRWGMap-Get')")
    @ApiOperation(value = "获取文档引用", tags = {"文档引用" },  notes = "获取文档引用")
	@RequestMapping(method = RequestMethod.GET, value = "/emdrwgmaps/{emdrwgmap_id}")
    public ResponseEntity<EMDRWGMapDTO> get(@PathVariable("emdrwgmap_id") String emdrwgmap_id) {
        EMDRWGMap domain = emdrwgmapService.get(emdrwgmap_id);
        EMDRWGMapDTO dto = emdrwgmapMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取文档引用草稿", tags = {"文档引用" },  notes = "获取文档引用草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emdrwgmaps/getdraft")
    public ResponseEntity<EMDRWGMapDTO> getDraft(EMDRWGMapDTO dto) {
        EMDRWGMap domain = emdrwgmapMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emdrwgmapMapping.toDto(emdrwgmapService.getDraft(domain)));
    }

    @ApiOperation(value = "检查文档引用", tags = {"文档引用" },  notes = "检查文档引用")
	@RequestMapping(method = RequestMethod.POST, value = "/emdrwgmaps/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMDRWGMapDTO emdrwgmapdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emdrwgmapService.checkKey(emdrwgmapMapping.toDomain(emdrwgmapdto)));
    }

    @PreAuthorize("hasPermission(this.emdrwgmapMapping.toDomain(#emdrwgmapdto),'eam-EMDRWGMap-Save')")
    @ApiOperation(value = "保存文档引用", tags = {"文档引用" },  notes = "保存文档引用")
	@RequestMapping(method = RequestMethod.POST, value = "/emdrwgmaps/save")
    public ResponseEntity<EMDRWGMapDTO> save(@RequestBody EMDRWGMapDTO emdrwgmapdto) {
        EMDRWGMap domain = emdrwgmapMapping.toDomain(emdrwgmapdto);
        emdrwgmapService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emdrwgmapMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emdrwgmapMapping.toDomain(#emdrwgmapdtos),'eam-EMDRWGMap-Save')")
    @ApiOperation(value = "批量保存文档引用", tags = {"文档引用" },  notes = "批量保存文档引用")
	@RequestMapping(method = RequestMethod.POST, value = "/emdrwgmaps/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMDRWGMapDTO> emdrwgmapdtos) {
        emdrwgmapService.saveBatch(emdrwgmapMapping.toDomain(emdrwgmapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMDRWGMap-searchByEQ-all') and hasPermission(#context,'eam-EMDRWGMap-Get')")
	@ApiOperation(value = "获取ByEQ", tags = {"文档引用" } ,notes = "获取ByEQ")
    @RequestMapping(method= RequestMethod.GET , value="/emdrwgmaps/fetchbyeq")
	public ResponseEntity<List<EMDRWGMapDTO>> fetchByEQ(EMDRWGMapSearchContext context) {
        Page<EMDRWGMap> domains = emdrwgmapService.searchByEQ(context) ;
        List<EMDRWGMapDTO> list = emdrwgmapMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMDRWGMap-searchByEQ-all') and hasPermission(#context,'eam-EMDRWGMap-Get')")
	@ApiOperation(value = "查询ByEQ", tags = {"文档引用" } ,notes = "查询ByEQ")
    @RequestMapping(method= RequestMethod.POST , value="/emdrwgmaps/searchbyeq")
	public ResponseEntity<Page<EMDRWGMapDTO>> searchByEQ(@RequestBody EMDRWGMapSearchContext context) {
        Page<EMDRWGMap> domains = emdrwgmapService.searchByEQ(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emdrwgmapMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMDRWGMap-searchDefault-all') and hasPermission(#context,'eam-EMDRWGMap-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"文档引用" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emdrwgmaps/fetchdefault")
	public ResponseEntity<List<EMDRWGMapDTO>> fetchDefault(EMDRWGMapSearchContext context) {
        Page<EMDRWGMap> domains = emdrwgmapService.searchDefault(context) ;
        List<EMDRWGMapDTO> list = emdrwgmapMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMDRWGMap-searchDefault-all') and hasPermission(#context,'eam-EMDRWGMap-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"文档引用" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emdrwgmaps/searchdefault")
	public ResponseEntity<Page<EMDRWGMapDTO>> searchDefault(@RequestBody EMDRWGMapSearchContext context) {
        Page<EMDRWGMap> domains = emdrwgmapService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emdrwgmapMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

