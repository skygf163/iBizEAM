package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMObjMap;
import cn.ibizlab.eam.core.eam_core.service.IEMObjMapService;
import cn.ibizlab.eam.core.eam_core.filter.EMObjMapSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"对象关系" })
@RestController("WebApi-emobjmap")
@RequestMapping("")
public class EMObjMapResource {

    @Autowired
    public IEMObjMapService emobjmapService;

    @Autowired
    @Lazy
    public EMObjMapMapping emobjmapMapping;

    @PreAuthorize("hasPermission(this.emobjmapMapping.toDomain(#emobjmapdto),'eam-EMObjMap-Create')")
    @ApiOperation(value = "新建对象关系", tags = {"对象关系" },  notes = "新建对象关系")
	@RequestMapping(method = RequestMethod.POST, value = "/emobjmaps")
    public ResponseEntity<EMObjMapDTO> create(@Validated @RequestBody EMObjMapDTO emobjmapdto) {
        EMObjMap domain = emobjmapMapping.toDomain(emobjmapdto);
		emobjmapService.create(domain);
        EMObjMapDTO dto = emobjmapMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emobjmapMapping.toDomain(#emobjmapdtos),'eam-EMObjMap-Create')")
    @ApiOperation(value = "批量新建对象关系", tags = {"对象关系" },  notes = "批量新建对象关系")
	@RequestMapping(method = RequestMethod.POST, value = "/emobjmaps/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMObjMapDTO> emobjmapdtos) {
        emobjmapService.createBatch(emobjmapMapping.toDomain(emobjmapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emobjmap" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emobjmapService.get(#emobjmap_id),'eam-EMObjMap-Update')")
    @ApiOperation(value = "更新对象关系", tags = {"对象关系" },  notes = "更新对象关系")
	@RequestMapping(method = RequestMethod.PUT, value = "/emobjmaps/{emobjmap_id}")
    public ResponseEntity<EMObjMapDTO> update(@PathVariable("emobjmap_id") String emobjmap_id, @RequestBody EMObjMapDTO emobjmapdto) {
		EMObjMap domain  = emobjmapMapping.toDomain(emobjmapdto);
        domain .setEmobjmapid(emobjmap_id);
		emobjmapService.update(domain );
		EMObjMapDTO dto = emobjmapMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emobjmapService.getEmobjmapByEntities(this.emobjmapMapping.toDomain(#emobjmapdtos)),'eam-EMObjMap-Update')")
    @ApiOperation(value = "批量更新对象关系", tags = {"对象关系" },  notes = "批量更新对象关系")
	@RequestMapping(method = RequestMethod.PUT, value = "/emobjmaps/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMObjMapDTO> emobjmapdtos) {
        emobjmapService.updateBatch(emobjmapMapping.toDomain(emobjmapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emobjmapService.get(#emobjmap_id),'eam-EMObjMap-Remove')")
    @ApiOperation(value = "删除对象关系", tags = {"对象关系" },  notes = "删除对象关系")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emobjmaps/{emobjmap_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emobjmap_id") String emobjmap_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emobjmapService.remove(emobjmap_id));
    }

    @PreAuthorize("hasPermission(this.emobjmapService.getEmobjmapByIds(#ids),'eam-EMObjMap-Remove')")
    @ApiOperation(value = "批量删除对象关系", tags = {"对象关系" },  notes = "批量删除对象关系")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emobjmaps/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emobjmapService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emobjmapMapping.toDomain(returnObject.body),'eam-EMObjMap-Get')")
    @ApiOperation(value = "获取对象关系", tags = {"对象关系" },  notes = "获取对象关系")
	@RequestMapping(method = RequestMethod.GET, value = "/emobjmaps/{emobjmap_id}")
    public ResponseEntity<EMObjMapDTO> get(@PathVariable("emobjmap_id") String emobjmap_id) {
        EMObjMap domain = emobjmapService.get(emobjmap_id);
        EMObjMapDTO dto = emobjmapMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取对象关系草稿", tags = {"对象关系" },  notes = "获取对象关系草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emobjmaps/getdraft")
    public ResponseEntity<EMObjMapDTO> getDraft(EMObjMapDTO dto) {
        EMObjMap domain = emobjmapMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emobjmapMapping.toDto(emobjmapService.getDraft(domain)));
    }

    @ApiOperation(value = "检查对象关系", tags = {"对象关系" },  notes = "检查对象关系")
	@RequestMapping(method = RequestMethod.POST, value = "/emobjmaps/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMObjMapDTO emobjmapdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emobjmapService.checkKey(emobjmapMapping.toDomain(emobjmapdto)));
    }

    @PreAuthorize("hasPermission(this.emobjmapMapping.toDomain(#emobjmapdto),'eam-EMObjMap-Save')")
    @ApiOperation(value = "保存对象关系", tags = {"对象关系" },  notes = "保存对象关系")
	@RequestMapping(method = RequestMethod.POST, value = "/emobjmaps/save")
    public ResponseEntity<EMObjMapDTO> save(@RequestBody EMObjMapDTO emobjmapdto) {
        EMObjMap domain = emobjmapMapping.toDomain(emobjmapdto);
        emobjmapService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emobjmapMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emobjmapMapping.toDomain(#emobjmapdtos),'eam-EMObjMap-Save')")
    @ApiOperation(value = "批量保存对象关系", tags = {"对象关系" },  notes = "批量保存对象关系")
	@RequestMapping(method = RequestMethod.POST, value = "/emobjmaps/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMObjMapDTO> emobjmapdtos) {
        emobjmapService.saveBatch(emobjmapMapping.toDomain(emobjmapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMObjMap-searchChildLocation-all') and hasPermission(#context,'eam-EMObjMap-Get')")
	@ApiOperation(value = "获取子位置", tags = {"对象关系" } ,notes = "获取子位置")
    @RequestMapping(method= RequestMethod.GET , value="/emobjmaps/fetchchildlocation")
	public ResponseEntity<List<EMObjMapDTO>> fetchChildLocation(EMObjMapSearchContext context) {
        Page<EMObjMap> domains = emobjmapService.searchChildLocation(context) ;
        List<EMObjMapDTO> list = emobjmapMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMObjMap-searchChildLocation-all') and hasPermission(#context,'eam-EMObjMap-Get')")
	@ApiOperation(value = "查询子位置", tags = {"对象关系" } ,notes = "查询子位置")
    @RequestMapping(method= RequestMethod.POST , value="/emobjmaps/searchchildlocation")
	public ResponseEntity<Page<EMObjMapDTO>> searchChildLocation(@RequestBody EMObjMapSearchContext context) {
        Page<EMObjMap> domains = emobjmapService.searchChildLocation(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emobjmapMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMObjMap-searchDefault-all') and hasPermission(#context,'eam-EMObjMap-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"对象关系" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emobjmaps/fetchdefault")
	public ResponseEntity<List<EMObjMapDTO>> fetchDefault(EMObjMapSearchContext context) {
        Page<EMObjMap> domains = emobjmapService.searchDefault(context) ;
        List<EMObjMapDTO> list = emobjmapMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMObjMap-searchDefault-all') and hasPermission(#context,'eam-EMObjMap-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"对象关系" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emobjmaps/searchdefault")
	public ResponseEntity<Page<EMObjMapDTO>> searchDefault(@RequestBody EMObjMapSearchContext context) {
        Page<EMObjMap> domains = emobjmapService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emobjmapMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMObjMap-searchIndexDER-all') and hasPermission(#context,'eam-EMObjMap-Get')")
	@ApiOperation(value = "获取IndexDER", tags = {"对象关系" } ,notes = "获取IndexDER")
    @RequestMapping(method= RequestMethod.GET , value="/emobjmaps/fetchindexder")
	public ResponseEntity<List<EMObjMapDTO>> fetchIndexDER(EMObjMapSearchContext context) {
        Page<EMObjMap> domains = emobjmapService.searchIndexDER(context) ;
        List<EMObjMapDTO> list = emobjmapMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMObjMap-searchIndexDER-all') and hasPermission(#context,'eam-EMObjMap-Get')")
	@ApiOperation(value = "查询IndexDER", tags = {"对象关系" } ,notes = "查询IndexDER")
    @RequestMapping(method= RequestMethod.POST , value="/emobjmaps/searchindexder")
	public ResponseEntity<Page<EMObjMapDTO>> searchIndexDER(@RequestBody EMObjMapSearchContext context) {
        Page<EMObjMap> domains = emobjmapService.searchIndexDER(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emobjmapMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMObjMap-searchLocationByEQ-all') and hasPermission(#context,'eam-EMObjMap-Get')")
	@ApiOperation(value = "获取LocationByEQ", tags = {"对象关系" } ,notes = "获取LocationByEQ")
    @RequestMapping(method= RequestMethod.GET , value="/emobjmaps/fetchlocationbyeq")
	public ResponseEntity<List<EMObjMapDTO>> fetchLocationByEQ(EMObjMapSearchContext context) {
        Page<EMObjMap> domains = emobjmapService.searchLocationByEQ(context) ;
        List<EMObjMapDTO> list = emobjmapMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMObjMap-searchLocationByEQ-all') and hasPermission(#context,'eam-EMObjMap-Get')")
	@ApiOperation(value = "查询LocationByEQ", tags = {"对象关系" } ,notes = "查询LocationByEQ")
    @RequestMapping(method= RequestMethod.POST , value="/emobjmaps/searchlocationbyeq")
	public ResponseEntity<Page<EMObjMapDTO>> searchLocationByEQ(@RequestBody EMObjMapSearchContext context) {
        Page<EMObjMap> domains = emobjmapService.searchLocationByEQ(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emobjmapMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

