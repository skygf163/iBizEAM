package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQLocation;
import cn.ibizlab.eam.core.eam_core.service.IEMEQLocationService;
import cn.ibizlab.eam.core.eam_core.filter.EMEQLocationSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"位置" })
@RestController("WebApi-emeqlocation")
@RequestMapping("")
public class EMEQLocationResource {

    @Autowired
    public IEMEQLocationService emeqlocationService;

    @Autowired
    @Lazy
    public EMEQLocationMapping emeqlocationMapping;

    @PreAuthorize("hasPermission(this.emeqlocationMapping.toDomain(#emeqlocationdto),'eam-EMEQLocation-Create')")
    @ApiOperation(value = "新建位置", tags = {"位置" },  notes = "新建位置")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqlocations")
    public ResponseEntity<EMEQLocationDTO> create(@Validated @RequestBody EMEQLocationDTO emeqlocationdto) {
        EMEQLocation domain = emeqlocationMapping.toDomain(emeqlocationdto);
		emeqlocationService.create(domain);
        EMEQLocationDTO dto = emeqlocationMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqlocationMapping.toDomain(#emeqlocationdtos),'eam-EMEQLocation-Create')")
    @ApiOperation(value = "批量新建位置", tags = {"位置" },  notes = "批量新建位置")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqlocations/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEQLocationDTO> emeqlocationdtos) {
        emeqlocationService.createBatch(emeqlocationMapping.toDomain(emeqlocationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqlocation" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqlocationService.get(#emeqlocation_id),'eam-EMEQLocation-Update')")
    @ApiOperation(value = "更新位置", tags = {"位置" },  notes = "更新位置")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqlocations/{emeqlocation_id}")
    public ResponseEntity<EMEQLocationDTO> update(@PathVariable("emeqlocation_id") String emeqlocation_id, @RequestBody EMEQLocationDTO emeqlocationdto) {
		EMEQLocation domain  = emeqlocationMapping.toDomain(emeqlocationdto);
        domain .setEmeqlocationid(emeqlocation_id);
		emeqlocationService.update(domain );
		EMEQLocationDTO dto = emeqlocationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqlocationService.getEmeqlocationByEntities(this.emeqlocationMapping.toDomain(#emeqlocationdtos)),'eam-EMEQLocation-Update')")
    @ApiOperation(value = "批量更新位置", tags = {"位置" },  notes = "批量更新位置")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqlocations/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEQLocationDTO> emeqlocationdtos) {
        emeqlocationService.updateBatch(emeqlocationMapping.toDomain(emeqlocationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqlocationService.get(#emeqlocation_id),'eam-EMEQLocation-Remove')")
    @ApiOperation(value = "删除位置", tags = {"位置" },  notes = "删除位置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqlocations/{emeqlocation_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeqlocation_id") String emeqlocation_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeqlocationService.remove(emeqlocation_id));
    }

    @PreAuthorize("hasPermission(this.emeqlocationService.getEmeqlocationByIds(#ids),'eam-EMEQLocation-Remove')")
    @ApiOperation(value = "批量删除位置", tags = {"位置" },  notes = "批量删除位置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqlocations/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeqlocationService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqlocationMapping.toDomain(returnObject.body),'eam-EMEQLocation-Get')")
    @ApiOperation(value = "获取位置", tags = {"位置" },  notes = "获取位置")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqlocations/{emeqlocation_id}")
    public ResponseEntity<EMEQLocationDTO> get(@PathVariable("emeqlocation_id") String emeqlocation_id) {
        EMEQLocation domain = emeqlocationService.get(emeqlocation_id);
        EMEQLocationDTO dto = emeqlocationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取位置草稿", tags = {"位置" },  notes = "获取位置草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqlocations/getdraft")
    public ResponseEntity<EMEQLocationDTO> getDraft(EMEQLocationDTO dto) {
        EMEQLocation domain = emeqlocationMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emeqlocationMapping.toDto(emeqlocationService.getDraft(domain)));
    }

    @ApiOperation(value = "检查位置", tags = {"位置" },  notes = "检查位置")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqlocations/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEQLocationDTO emeqlocationdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqlocationService.checkKey(emeqlocationMapping.toDomain(emeqlocationdto)));
    }

    @PreAuthorize("hasPermission(this.emeqlocationMapping.toDomain(#emeqlocationdto),'eam-EMEQLocation-Save')")
    @ApiOperation(value = "保存位置", tags = {"位置" },  notes = "保存位置")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqlocations/save")
    public ResponseEntity<EMEQLocationDTO> save(@RequestBody EMEQLocationDTO emeqlocationdto) {
        EMEQLocation domain = emeqlocationMapping.toDomain(emeqlocationdto);
        emeqlocationService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqlocationMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqlocationMapping.toDomain(#emeqlocationdtos),'eam-EMEQLocation-Save')")
    @ApiOperation(value = "批量保存位置", tags = {"位置" },  notes = "批量保存位置")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqlocations/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEQLocationDTO> emeqlocationdtos) {
        emeqlocationService.saveBatch(emeqlocationMapping.toDomain(emeqlocationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQLocation-searchDefault-all') and hasPermission(#context,'eam-EMEQLocation-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"位置" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeqlocations/fetchdefault")
	public ResponseEntity<List<EMEQLocationDTO>> fetchDefault(EMEQLocationSearchContext context) {
        Page<EMEQLocation> domains = emeqlocationService.searchDefault(context) ;
        List<EMEQLocationDTO> list = emeqlocationMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQLocation-searchDefault-all') and hasPermission(#context,'eam-EMEQLocation-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"位置" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeqlocations/searchdefault")
	public ResponseEntity<Page<EMEQLocationDTO>> searchDefault(@RequestBody EMEQLocationSearchContext context) {
        Page<EMEQLocation> domains = emeqlocationService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqlocationMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQLocation-searchSub-all') and hasPermission(#context,'eam-EMEQLocation-Get')")
	@ApiOperation(value = "获取下级位置", tags = {"位置" } ,notes = "获取下级位置")
    @RequestMapping(method= RequestMethod.GET , value="/emeqlocations/fetchsub")
	public ResponseEntity<List<EMEQLocationDTO>> fetchSub(EMEQLocationSearchContext context) {
        Page<EMEQLocation> domains = emeqlocationService.searchSub(context) ;
        List<EMEQLocationDTO> list = emeqlocationMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQLocation-searchSub-all') and hasPermission(#context,'eam-EMEQLocation-Get')")
	@ApiOperation(value = "查询下级位置", tags = {"位置" } ,notes = "查询下级位置")
    @RequestMapping(method= RequestMethod.POST , value="/emeqlocations/searchsub")
	public ResponseEntity<Page<EMEQLocationDTO>> searchSub(@RequestBody EMEQLocationSearchContext context) {
        Page<EMEQLocation> domains = emeqlocationService.searchSub(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqlocationMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

