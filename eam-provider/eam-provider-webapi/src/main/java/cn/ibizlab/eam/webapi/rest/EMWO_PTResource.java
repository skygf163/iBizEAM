package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMWO_PT;
import cn.ibizlab.eam.core.eam_core.service.IEMWO_PTService;
import cn.ibizlab.eam.core.eam_core.filter.EMWO_PTSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"执勤工单" })
@RestController("WebApi-emwo_pt")
@RequestMapping("")
public class EMWO_PTResource {

    @Autowired
    public IEMWO_PTService emwo_ptService;

    @Autowired
    @Lazy
    public EMWO_PTMapping emwo_ptMapping;

    @PreAuthorize("hasPermission(this.emwo_ptMapping.toDomain(#emwo_ptdto),'eam-EMWO_PT-Create')")
    @ApiOperation(value = "新建执勤工单", tags = {"执勤工单" },  notes = "新建执勤工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_pts")
    public ResponseEntity<EMWO_PTDTO> create(@Validated @RequestBody EMWO_PTDTO emwo_ptdto) {
        EMWO_PT domain = emwo_ptMapping.toDomain(emwo_ptdto);
		emwo_ptService.create(domain);
        EMWO_PTDTO dto = emwo_ptMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_ptMapping.toDomain(#emwo_ptdtos),'eam-EMWO_PT-Create')")
    @ApiOperation(value = "批量新建执勤工单", tags = {"执勤工单" },  notes = "批量新建执勤工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_pts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMWO_PTDTO> emwo_ptdtos) {
        emwo_ptService.createBatch(emwo_ptMapping.toDomain(emwo_ptdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwo_pt" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwo_ptService.get(#emwo_pt_id),'eam-EMWO_PT-Update')")
    @ApiOperation(value = "更新执勤工单", tags = {"执勤工单" },  notes = "更新执勤工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emwo_pts/{emwo_pt_id}")
    public ResponseEntity<EMWO_PTDTO> update(@PathVariable("emwo_pt_id") String emwo_pt_id, @RequestBody EMWO_PTDTO emwo_ptdto) {
		EMWO_PT domain  = emwo_ptMapping.toDomain(emwo_ptdto);
        domain .setEmwoPtid(emwo_pt_id);
		emwo_ptService.update(domain );
		EMWO_PTDTO dto = emwo_ptMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_ptService.getEmwoPtByEntities(this.emwo_ptMapping.toDomain(#emwo_ptdtos)),'eam-EMWO_PT-Update')")
    @ApiOperation(value = "批量更新执勤工单", tags = {"执勤工单" },  notes = "批量更新执勤工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emwo_pts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMWO_PTDTO> emwo_ptdtos) {
        emwo_ptService.updateBatch(emwo_ptMapping.toDomain(emwo_ptdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwo_ptService.get(#emwo_pt_id),'eam-EMWO_PT-Remove')")
    @ApiOperation(value = "删除执勤工单", tags = {"执勤工单" },  notes = "删除执勤工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emwo_pts/{emwo_pt_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emwo_pt_id") String emwo_pt_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emwo_ptService.remove(emwo_pt_id));
    }

    @PreAuthorize("hasPermission(this.emwo_ptService.getEmwoPtByIds(#ids),'eam-EMWO_PT-Remove')")
    @ApiOperation(value = "批量删除执勤工单", tags = {"执勤工单" },  notes = "批量删除执勤工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emwo_pts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emwo_ptService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwo_ptMapping.toDomain(returnObject.body),'eam-EMWO_PT-Get')")
    @ApiOperation(value = "获取执勤工单", tags = {"执勤工单" },  notes = "获取执勤工单")
	@RequestMapping(method = RequestMethod.GET, value = "/emwo_pts/{emwo_pt_id}")
    public ResponseEntity<EMWO_PTDTO> get(@PathVariable("emwo_pt_id") String emwo_pt_id) {
        EMWO_PT domain = emwo_ptService.get(emwo_pt_id);
        EMWO_PTDTO dto = emwo_ptMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取执勤工单草稿", tags = {"执勤工单" },  notes = "获取执勤工单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emwo_pts/getdraft")
    public ResponseEntity<EMWO_PTDTO> getDraft(EMWO_PTDTO dto) {
        EMWO_PT domain = emwo_ptMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_ptMapping.toDto(emwo_ptService.getDraft(domain)));
    }

    @ApiOperation(value = "检查执勤工单", tags = {"执勤工单" },  notes = "检查执勤工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_pts/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMWO_PTDTO emwo_ptdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwo_ptService.checkKey(emwo_ptMapping.toDomain(emwo_ptdto)));
    }

    @PreAuthorize("hasPermission(this.emwo_ptMapping.toDomain(#emwo_ptdto),'eam-EMWO_PT-Save')")
    @ApiOperation(value = "保存执勤工单", tags = {"执勤工单" },  notes = "保存执勤工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_pts/save")
    public ResponseEntity<EMWO_PTDTO> save(@RequestBody EMWO_PTDTO emwo_ptdto) {
        EMWO_PT domain = emwo_ptMapping.toDomain(emwo_ptdto);
        emwo_ptService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_ptMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emwo_ptMapping.toDomain(#emwo_ptdtos),'eam-EMWO_PT-Save')")
    @ApiOperation(value = "批量保存执勤工单", tags = {"执勤工单" },  notes = "批量保存执勤工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_pts/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMWO_PTDTO> emwo_ptdtos) {
        emwo_ptService.saveBatch(emwo_ptMapping.toDomain(emwo_ptdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_PT-searchDefault-all') and hasPermission(#context,'eam-EMWO_PT-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"执勤工单" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emwo_pts/fetchdefault")
	public ResponseEntity<List<EMWO_PTDTO>> fetchDefault(EMWO_PTSearchContext context) {
        Page<EMWO_PT> domains = emwo_ptService.searchDefault(context) ;
        List<EMWO_PTDTO> list = emwo_ptMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_PT-searchDefault-all') and hasPermission(#context,'eam-EMWO_PT-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"执勤工单" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emwo_pts/searchdefault")
	public ResponseEntity<Page<EMWO_PTDTO>> searchDefault(@RequestBody EMWO_PTSearchContext context) {
        Page<EMWO_PT> domains = emwo_ptService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_ptMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emwo_ptMapping.toDomain(#emwo_ptdto),'eam-EMWO_PT-Create')")
    @ApiOperation(value = "根据设备档案建立执勤工单", tags = {"执勤工单" },  notes = "根据设备档案建立执勤工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_pts")
    public ResponseEntity<EMWO_PTDTO> createByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_PTDTO emwo_ptdto) {
        EMWO_PT domain = emwo_ptMapping.toDomain(emwo_ptdto);
        domain.setEquipid(emequip_id);
		emwo_ptService.create(domain);
        EMWO_PTDTO dto = emwo_ptMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_ptMapping.toDomain(#emwo_ptdtos),'eam-EMWO_PT-Create')")
    @ApiOperation(value = "根据设备档案批量建立执勤工单", tags = {"执勤工单" },  notes = "根据设备档案批量建立执勤工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_pts/batch")
    public ResponseEntity<Boolean> createBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_PTDTO> emwo_ptdtos) {
        List<EMWO_PT> domainlist=emwo_ptMapping.toDomain(emwo_ptdtos);
        for(EMWO_PT domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emwo_ptService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwo_pt" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwo_ptService.get(#emwo_pt_id),'eam-EMWO_PT-Update')")
    @ApiOperation(value = "根据设备档案更新执勤工单", tags = {"执勤工单" },  notes = "根据设备档案更新执勤工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emwo_pts/{emwo_pt_id}")
    public ResponseEntity<EMWO_PTDTO> updateByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_pt_id") String emwo_pt_id, @RequestBody EMWO_PTDTO emwo_ptdto) {
        EMWO_PT domain = emwo_ptMapping.toDomain(emwo_ptdto);
        domain.setEquipid(emequip_id);
        domain.setEmwoPtid(emwo_pt_id);
		emwo_ptService.update(domain);
        EMWO_PTDTO dto = emwo_ptMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_ptService.getEmwoPtByEntities(this.emwo_ptMapping.toDomain(#emwo_ptdtos)),'eam-EMWO_PT-Update')")
    @ApiOperation(value = "根据设备档案批量更新执勤工单", tags = {"执勤工单" },  notes = "根据设备档案批量更新执勤工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emwo_pts/batch")
    public ResponseEntity<Boolean> updateBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_PTDTO> emwo_ptdtos) {
        List<EMWO_PT> domainlist=emwo_ptMapping.toDomain(emwo_ptdtos);
        for(EMWO_PT domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emwo_ptService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwo_ptService.get(#emwo_pt_id),'eam-EMWO_PT-Remove')")
    @ApiOperation(value = "根据设备档案删除执勤工单", tags = {"执勤工单" },  notes = "根据设备档案删除执勤工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emwo_pts/{emwo_pt_id}")
    public ResponseEntity<Boolean> removeByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_pt_id") String emwo_pt_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emwo_ptService.remove(emwo_pt_id));
    }

    @PreAuthorize("hasPermission(this.emwo_ptService.getEmwoPtByIds(#ids),'eam-EMWO_PT-Remove')")
    @ApiOperation(value = "根据设备档案批量删除执勤工单", tags = {"执勤工单" },  notes = "根据设备档案批量删除执勤工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emwo_pts/batch")
    public ResponseEntity<Boolean> removeBatchByEMEquip(@RequestBody List<String> ids) {
        emwo_ptService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwo_ptMapping.toDomain(returnObject.body),'eam-EMWO_PT-Get')")
    @ApiOperation(value = "根据设备档案获取执勤工单", tags = {"执勤工单" },  notes = "根据设备档案获取执勤工单")
	@RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emwo_pts/{emwo_pt_id}")
    public ResponseEntity<EMWO_PTDTO> getByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_pt_id") String emwo_pt_id) {
        EMWO_PT domain = emwo_ptService.get(emwo_pt_id);
        EMWO_PTDTO dto = emwo_ptMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据设备档案获取执勤工单草稿", tags = {"执勤工单" },  notes = "根据设备档案获取执勤工单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emwo_pts/getdraft")
    public ResponseEntity<EMWO_PTDTO> getDraftByEMEquip(@PathVariable("emequip_id") String emequip_id, EMWO_PTDTO dto) {
        EMWO_PT domain = emwo_ptMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_ptMapping.toDto(emwo_ptService.getDraft(domain)));
    }

    @ApiOperation(value = "根据设备档案检查执勤工单", tags = {"执勤工单" },  notes = "根据设备档案检查执勤工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_pts/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_PTDTO emwo_ptdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwo_ptService.checkKey(emwo_ptMapping.toDomain(emwo_ptdto)));
    }

    @PreAuthorize("hasPermission(this.emwo_ptMapping.toDomain(#emwo_ptdto),'eam-EMWO_PT-Save')")
    @ApiOperation(value = "根据设备档案保存执勤工单", tags = {"执勤工单" },  notes = "根据设备档案保存执勤工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_pts/save")
    public ResponseEntity<EMWO_PTDTO> saveByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_PTDTO emwo_ptdto) {
        EMWO_PT domain = emwo_ptMapping.toDomain(emwo_ptdto);
        domain.setEquipid(emequip_id);
        emwo_ptService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_ptMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emwo_ptMapping.toDomain(#emwo_ptdtos),'eam-EMWO_PT-Save')")
    @ApiOperation(value = "根据设备档案批量保存执勤工单", tags = {"执勤工单" },  notes = "根据设备档案批量保存执勤工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_pts/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_PTDTO> emwo_ptdtos) {
        List<EMWO_PT> domainlist=emwo_ptMapping.toDomain(emwo_ptdtos);
        for(EMWO_PT domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emwo_ptService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_PT-searchDefault-all') and hasPermission(#context,'eam-EMWO_PT-Get')")
	@ApiOperation(value = "根据设备档案获取DEFAULT", tags = {"执勤工单" } ,notes = "根据设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emwo_pts/fetchdefault")
	public ResponseEntity<List<EMWO_PTDTO>> fetchEMWO_PTDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id,EMWO_PTSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_PT> domains = emwo_ptService.searchDefault(context) ;
        List<EMWO_PTDTO> list = emwo_ptMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_PT-searchDefault-all') and hasPermission(#context,'eam-EMWO_PT-Get')")
	@ApiOperation(value = "根据设备档案查询DEFAULT", tags = {"执勤工单" } ,notes = "根据设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emwo_pts/searchdefault")
	public ResponseEntity<Page<EMWO_PTDTO>> searchEMWO_PTDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_PTSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_PT> domains = emwo_ptService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_ptMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emwo_ptMapping.toDomain(#emwo_ptdto),'eam-EMWO_PT-Create')")
    @ApiOperation(value = "根据班组设备档案建立执勤工单", tags = {"执勤工单" },  notes = "根据班组设备档案建立执勤工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_pts")
    public ResponseEntity<EMWO_PTDTO> createByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_PTDTO emwo_ptdto) {
        EMWO_PT domain = emwo_ptMapping.toDomain(emwo_ptdto);
        domain.setEquipid(emequip_id);
		emwo_ptService.create(domain);
        EMWO_PTDTO dto = emwo_ptMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_ptMapping.toDomain(#emwo_ptdtos),'eam-EMWO_PT-Create')")
    @ApiOperation(value = "根据班组设备档案批量建立执勤工单", tags = {"执勤工单" },  notes = "根据班组设备档案批量建立执勤工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_pts/batch")
    public ResponseEntity<Boolean> createBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_PTDTO> emwo_ptdtos) {
        List<EMWO_PT> domainlist=emwo_ptMapping.toDomain(emwo_ptdtos);
        for(EMWO_PT domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emwo_ptService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwo_pt" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwo_ptService.get(#emwo_pt_id),'eam-EMWO_PT-Update')")
    @ApiOperation(value = "根据班组设备档案更新执勤工单", tags = {"执勤工单" },  notes = "根据班组设备档案更新执勤工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_pts/{emwo_pt_id}")
    public ResponseEntity<EMWO_PTDTO> updateByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_pt_id") String emwo_pt_id, @RequestBody EMWO_PTDTO emwo_ptdto) {
        EMWO_PT domain = emwo_ptMapping.toDomain(emwo_ptdto);
        domain.setEquipid(emequip_id);
        domain.setEmwoPtid(emwo_pt_id);
		emwo_ptService.update(domain);
        EMWO_PTDTO dto = emwo_ptMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_ptService.getEmwoPtByEntities(this.emwo_ptMapping.toDomain(#emwo_ptdtos)),'eam-EMWO_PT-Update')")
    @ApiOperation(value = "根据班组设备档案批量更新执勤工单", tags = {"执勤工单" },  notes = "根据班组设备档案批量更新执勤工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_pts/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_PTDTO> emwo_ptdtos) {
        List<EMWO_PT> domainlist=emwo_ptMapping.toDomain(emwo_ptdtos);
        for(EMWO_PT domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emwo_ptService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwo_ptService.get(#emwo_pt_id),'eam-EMWO_PT-Remove')")
    @ApiOperation(value = "根据班组设备档案删除执勤工单", tags = {"执勤工单" },  notes = "根据班组设备档案删除执勤工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_pts/{emwo_pt_id}")
    public ResponseEntity<Boolean> removeByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_pt_id") String emwo_pt_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emwo_ptService.remove(emwo_pt_id));
    }

    @PreAuthorize("hasPermission(this.emwo_ptService.getEmwoPtByIds(#ids),'eam-EMWO_PT-Remove')")
    @ApiOperation(value = "根据班组设备档案批量删除执勤工单", tags = {"执勤工单" },  notes = "根据班组设备档案批量删除执勤工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_pts/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeamEMEquip(@RequestBody List<String> ids) {
        emwo_ptService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwo_ptMapping.toDomain(returnObject.body),'eam-EMWO_PT-Get')")
    @ApiOperation(value = "根据班组设备档案获取执勤工单", tags = {"执勤工单" },  notes = "根据班组设备档案获取执勤工单")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_pts/{emwo_pt_id}")
    public ResponseEntity<EMWO_PTDTO> getByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_pt_id") String emwo_pt_id) {
        EMWO_PT domain = emwo_ptService.get(emwo_pt_id);
        EMWO_PTDTO dto = emwo_ptMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组设备档案获取执勤工单草稿", tags = {"执勤工单" },  notes = "根据班组设备档案获取执勤工单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_pts/getdraft")
    public ResponseEntity<EMWO_PTDTO> getDraftByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, EMWO_PTDTO dto) {
        EMWO_PT domain = emwo_ptMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_ptMapping.toDto(emwo_ptService.getDraft(domain)));
    }

    @ApiOperation(value = "根据班组设备档案检查执勤工单", tags = {"执勤工单" },  notes = "根据班组设备档案检查执勤工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_pts/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_PTDTO emwo_ptdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwo_ptService.checkKey(emwo_ptMapping.toDomain(emwo_ptdto)));
    }

    @PreAuthorize("hasPermission(this.emwo_ptMapping.toDomain(#emwo_ptdto),'eam-EMWO_PT-Save')")
    @ApiOperation(value = "根据班组设备档案保存执勤工单", tags = {"执勤工单" },  notes = "根据班组设备档案保存执勤工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_pts/save")
    public ResponseEntity<EMWO_PTDTO> saveByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_PTDTO emwo_ptdto) {
        EMWO_PT domain = emwo_ptMapping.toDomain(emwo_ptdto);
        domain.setEquipid(emequip_id);
        emwo_ptService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_ptMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emwo_ptMapping.toDomain(#emwo_ptdtos),'eam-EMWO_PT-Save')")
    @ApiOperation(value = "根据班组设备档案批量保存执勤工单", tags = {"执勤工单" },  notes = "根据班组设备档案批量保存执勤工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_pts/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_PTDTO> emwo_ptdtos) {
        List<EMWO_PT> domainlist=emwo_ptMapping.toDomain(emwo_ptdtos);
        for(EMWO_PT domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emwo_ptService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_PT-searchDefault-all') and hasPermission(#context,'eam-EMWO_PT-Get')")
	@ApiOperation(value = "根据班组设备档案获取DEFAULT", tags = {"执勤工单" } ,notes = "根据班组设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_pts/fetchdefault")
	public ResponseEntity<List<EMWO_PTDTO>> fetchEMWO_PTDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMWO_PTSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_PT> domains = emwo_ptService.searchDefault(context) ;
        List<EMWO_PTDTO> list = emwo_ptMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_PT-searchDefault-all') and hasPermission(#context,'eam-EMWO_PT-Get')")
	@ApiOperation(value = "根据班组设备档案查询DEFAULT", tags = {"执勤工单" } ,notes = "根据班组设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_pts/searchdefault")
	public ResponseEntity<Page<EMWO_PTDTO>> searchEMWO_PTDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_PTSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_PT> domains = emwo_ptService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_ptMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

