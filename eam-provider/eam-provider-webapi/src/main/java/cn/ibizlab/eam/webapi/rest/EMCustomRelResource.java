package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMCustomRel;
import cn.ibizlab.eam.core.eam_core.service.IEMCustomRelService;
import cn.ibizlab.eam.core.eam_core.filter.EMCustomRelSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"客户" })
@RestController("WebApi-emcustomrel")
@RequestMapping("")
public class EMCustomRelResource {

    @Autowired
    public IEMCustomRelService emcustomrelService;

    @Autowired
    @Lazy
    public EMCustomRelMapping emcustomrelMapping;

    @PreAuthorize("hasPermission(this.emcustomrelMapping.toDomain(#emcustomreldto),'eam-EMCustomRel-Create')")
    @ApiOperation(value = "新建客户", tags = {"客户" },  notes = "新建客户")
	@RequestMapping(method = RequestMethod.POST, value = "/emcustomrels")
    public ResponseEntity<EMCustomRelDTO> create(@Validated @RequestBody EMCustomRelDTO emcustomreldto) {
        EMCustomRel domain = emcustomrelMapping.toDomain(emcustomreldto);
		emcustomrelService.create(domain);
        EMCustomRelDTO dto = emcustomrelMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emcustomrelMapping.toDomain(#emcustomreldtos),'eam-EMCustomRel-Create')")
    @ApiOperation(value = "批量新建客户", tags = {"客户" },  notes = "批量新建客户")
	@RequestMapping(method = RequestMethod.POST, value = "/emcustomrels/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMCustomRelDTO> emcustomreldtos) {
        emcustomrelService.createBatch(emcustomrelMapping.toDomain(emcustomreldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emcustomrel" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emcustomrelService.get(#emcustomrel_id),'eam-EMCustomRel-Update')")
    @ApiOperation(value = "更新客户", tags = {"客户" },  notes = "更新客户")
	@RequestMapping(method = RequestMethod.PUT, value = "/emcustomrels/{emcustomrel_id}")
    public ResponseEntity<EMCustomRelDTO> update(@PathVariable("emcustomrel_id") String emcustomrel_id, @RequestBody EMCustomRelDTO emcustomreldto) {
		EMCustomRel domain  = emcustomrelMapping.toDomain(emcustomreldto);
        domain .setEmcustomrelid(emcustomrel_id);
		emcustomrelService.update(domain );
		EMCustomRelDTO dto = emcustomrelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emcustomrelService.getEmcustomrelByEntities(this.emcustomrelMapping.toDomain(#emcustomreldtos)),'eam-EMCustomRel-Update')")
    @ApiOperation(value = "批量更新客户", tags = {"客户" },  notes = "批量更新客户")
	@RequestMapping(method = RequestMethod.PUT, value = "/emcustomrels/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMCustomRelDTO> emcustomreldtos) {
        emcustomrelService.updateBatch(emcustomrelMapping.toDomain(emcustomreldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emcustomrelService.get(#emcustomrel_id),'eam-EMCustomRel-Remove')")
    @ApiOperation(value = "删除客户", tags = {"客户" },  notes = "删除客户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emcustomrels/{emcustomrel_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emcustomrel_id") String emcustomrel_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emcustomrelService.remove(emcustomrel_id));
    }

    @PreAuthorize("hasPermission(this.emcustomrelService.getEmcustomrelByIds(#ids),'eam-EMCustomRel-Remove')")
    @ApiOperation(value = "批量删除客户", tags = {"客户" },  notes = "批量删除客户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emcustomrels/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emcustomrelService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emcustomrelMapping.toDomain(returnObject.body),'eam-EMCustomRel-Get')")
    @ApiOperation(value = "获取客户", tags = {"客户" },  notes = "获取客户")
	@RequestMapping(method = RequestMethod.GET, value = "/emcustomrels/{emcustomrel_id}")
    public ResponseEntity<EMCustomRelDTO> get(@PathVariable("emcustomrel_id") String emcustomrel_id) {
        EMCustomRel domain = emcustomrelService.get(emcustomrel_id);
        EMCustomRelDTO dto = emcustomrelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取客户草稿", tags = {"客户" },  notes = "获取客户草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emcustomrels/getdraft")
    public ResponseEntity<EMCustomRelDTO> getDraft(EMCustomRelDTO dto) {
        EMCustomRel domain = emcustomrelMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emcustomrelMapping.toDto(emcustomrelService.getDraft(domain)));
    }

    @ApiOperation(value = "检查客户", tags = {"客户" },  notes = "检查客户")
	@RequestMapping(method = RequestMethod.POST, value = "/emcustomrels/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMCustomRelDTO emcustomreldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emcustomrelService.checkKey(emcustomrelMapping.toDomain(emcustomreldto)));
    }

    @PreAuthorize("hasPermission(this.emcustomrelMapping.toDomain(#emcustomreldto),'eam-EMCustomRel-Save')")
    @ApiOperation(value = "保存客户", tags = {"客户" },  notes = "保存客户")
	@RequestMapping(method = RequestMethod.POST, value = "/emcustomrels/save")
    public ResponseEntity<EMCustomRelDTO> save(@RequestBody EMCustomRelDTO emcustomreldto) {
        EMCustomRel domain = emcustomrelMapping.toDomain(emcustomreldto);
        emcustomrelService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emcustomrelMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emcustomrelMapping.toDomain(#emcustomreldtos),'eam-EMCustomRel-Save')")
    @ApiOperation(value = "批量保存客户", tags = {"客户" },  notes = "批量保存客户")
	@RequestMapping(method = RequestMethod.POST, value = "/emcustomrels/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMCustomRelDTO> emcustomreldtos) {
        emcustomrelService.saveBatch(emcustomrelMapping.toDomain(emcustomreldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMCustomRel-searchDefault-all') and hasPermission(#context,'eam-EMCustomRel-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"客户" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emcustomrels/fetchdefault")
	public ResponseEntity<List<EMCustomRelDTO>> fetchDefault(EMCustomRelSearchContext context) {
        Page<EMCustomRel> domains = emcustomrelService.searchDefault(context) ;
        List<EMCustomRelDTO> list = emcustomrelMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMCustomRel-searchDefault-all') and hasPermission(#context,'eam-EMCustomRel-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"客户" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emcustomrels/searchdefault")
	public ResponseEntity<Page<EMCustomRelDTO>> searchDefault(@RequestBody EMCustomRelSearchContext context) {
        Page<EMCustomRel> domains = emcustomrelService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emcustomrelMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

