package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMPLANRECORD;
import cn.ibizlab.eam.core.eam_core.service.IEMPLANRECORDService;
import cn.ibizlab.eam.core.eam_core.filter.EMPLANRECORDSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"触发记录" })
@RestController("WebApi-emplanrecord")
@RequestMapping("")
public class EMPLANRECORDResource {

    @Autowired
    public IEMPLANRECORDService emplanrecordService;

    @Autowired
    @Lazy
    public EMPLANRECORDMapping emplanrecordMapping;

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddto),'eam-EMPLANRECORD-Create')")
    @ApiOperation(value = "新建触发记录", tags = {"触发记录" },  notes = "新建触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emplanrecords")
    public ResponseEntity<EMPLANRECORDDTO> create(@Validated @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
		emplanrecordService.create(domain);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddtos),'eam-EMPLANRECORD-Create')")
    @ApiOperation(value = "批量新建触发记录", tags = {"触发记录" },  notes = "批量新建触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emplanrecords/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        emplanrecordService.createBatch(emplanrecordMapping.toDomain(emplanrecorddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplanrecord" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplanrecordService.get(#emplanrecord_id),'eam-EMPLANRECORD-Update')")
    @ApiOperation(value = "更新触发记录", tags = {"触发记录" },  notes = "更新触发记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<EMPLANRECORDDTO> update(@PathVariable("emplanrecord_id") String emplanrecord_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
		EMPLANRECORD domain  = emplanrecordMapping.toDomain(emplanrecorddto);
        domain .setEmplanrecordid(emplanrecord_id);
		emplanrecordService.update(domain );
		EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.getEmplanrecordByEntities(this.emplanrecordMapping.toDomain(#emplanrecorddtos)),'eam-EMPLANRECORD-Update')")
    @ApiOperation(value = "批量更新触发记录", tags = {"触发记录" },  notes = "批量更新触发记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplanrecords/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        emplanrecordService.updateBatch(emplanrecordMapping.toDomain(emplanrecorddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.get(#emplanrecord_id),'eam-EMPLANRECORD-Remove')")
    @ApiOperation(value = "删除触发记录", tags = {"触发记录" },  notes = "删除触发记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emplanrecord_id") String emplanrecord_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emplanrecordService.remove(emplanrecord_id));
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.getEmplanrecordByIds(#ids),'eam-EMPLANRECORD-Remove')")
    @ApiOperation(value = "批量删除触发记录", tags = {"触发记录" },  notes = "批量删除触发记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emplanrecords/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emplanrecordService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplanrecordMapping.toDomain(returnObject.body),'eam-EMPLANRECORD-Get')")
    @ApiOperation(value = "获取触发记录", tags = {"触发记录" },  notes = "获取触发记录")
	@RequestMapping(method = RequestMethod.GET, value = "/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<EMPLANRECORDDTO> get(@PathVariable("emplanrecord_id") String emplanrecord_id) {
        EMPLANRECORD domain = emplanrecordService.get(emplanrecord_id);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取触发记录草稿", tags = {"触发记录" },  notes = "获取触发记录草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emplanrecords/getdraft")
    public ResponseEntity<EMPLANRECORDDTO> getDraft(EMPLANRECORDDTO dto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emplanrecordMapping.toDto(emplanrecordService.getDraft(domain)));
    }

    @ApiOperation(value = "检查触发记录", tags = {"触发记录" },  notes = "检查触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emplanrecords/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMPLANRECORDDTO emplanrecorddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplanrecordService.checkKey(emplanrecordMapping.toDomain(emplanrecorddto)));
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddto),'eam-EMPLANRECORD-Save')")
    @ApiOperation(value = "保存触发记录", tags = {"触发记录" },  notes = "保存触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emplanrecords/save")
    public ResponseEntity<EMPLANRECORDDTO> save(@RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
        emplanrecordService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplanrecordMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddtos),'eam-EMPLANRECORD-Save')")
    @ApiOperation(value = "批量保存触发记录", tags = {"触发记录" },  notes = "批量保存触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emplanrecords/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        emplanrecordService.saveBatch(emplanrecordMapping.toDomain(emplanrecorddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPLANRECORD-searchDefault-all') and hasPermission(#context,'eam-EMPLANRECORD-Get')")
	@ApiOperation(value = "获取数据集", tags = {"触发记录" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/emplanrecords/fetchdefault")
	public ResponseEntity<List<EMPLANRECORDDTO>> fetchDefault(EMPLANRECORDSearchContext context) {
        Page<EMPLANRECORD> domains = emplanrecordService.searchDefault(context) ;
        List<EMPLANRECORDDTO> list = emplanrecordMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPLANRECORD-searchDefault-all') and hasPermission(#context,'eam-EMPLANRECORD-Get')")
	@ApiOperation(value = "查询数据集", tags = {"触发记录" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/emplanrecords/searchdefault")
	public ResponseEntity<Page<EMPLANRECORDDTO>> searchDefault(@RequestBody EMPLANRECORDSearchContext context) {
        Page<EMPLANRECORD> domains = emplanrecordService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplanrecordMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddto),'eam-EMPLANRECORD-Create')")
    @ApiOperation(value = "根据计划建立触发记录", tags = {"触发记录" },  notes = "根据计划建立触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emplans/{emplan_id}/emplanrecords")
    public ResponseEntity<EMPLANRECORDDTO> createByEMPlan(@PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
        domain.setEmplanid(emplan_id);
		emplanrecordService.create(domain);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddtos),'eam-EMPLANRECORD-Create')")
    @ApiOperation(value = "根据计划批量建立触发记录", tags = {"触发记录" },  notes = "根据计划批量建立触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emplans/{emplan_id}/emplanrecords/batch")
    public ResponseEntity<Boolean> createBatchByEMPlan(@PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        List<EMPLANRECORD> domainlist=emplanrecordMapping.toDomain(emplanrecorddtos);
        for(EMPLANRECORD domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        emplanrecordService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplanrecord" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplanrecordService.get(#emplanrecord_id),'eam-EMPLANRECORD-Update')")
    @ApiOperation(value = "根据计划更新触发记录", tags = {"触发记录" },  notes = "根据计划更新触发记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplans/{emplan_id}/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<EMPLANRECORDDTO> updateByEMPlan(@PathVariable("emplan_id") String emplan_id, @PathVariable("emplanrecord_id") String emplanrecord_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
        domain.setEmplanid(emplan_id);
        domain.setEmplanrecordid(emplanrecord_id);
		emplanrecordService.update(domain);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.getEmplanrecordByEntities(this.emplanrecordMapping.toDomain(#emplanrecorddtos)),'eam-EMPLANRECORD-Update')")
    @ApiOperation(value = "根据计划批量更新触发记录", tags = {"触发记录" },  notes = "根据计划批量更新触发记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplans/{emplan_id}/emplanrecords/batch")
    public ResponseEntity<Boolean> updateBatchByEMPlan(@PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        List<EMPLANRECORD> domainlist=emplanrecordMapping.toDomain(emplanrecorddtos);
        for(EMPLANRECORD domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        emplanrecordService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.get(#emplanrecord_id),'eam-EMPLANRECORD-Remove')")
    @ApiOperation(value = "根据计划删除触发记录", tags = {"触发记录" },  notes = "根据计划删除触发记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emplans/{emplan_id}/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<Boolean> removeByEMPlan(@PathVariable("emplan_id") String emplan_id, @PathVariable("emplanrecord_id") String emplanrecord_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplanrecordService.remove(emplanrecord_id));
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.getEmplanrecordByIds(#ids),'eam-EMPLANRECORD-Remove')")
    @ApiOperation(value = "根据计划批量删除触发记录", tags = {"触发记录" },  notes = "根据计划批量删除触发记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emplans/{emplan_id}/emplanrecords/batch")
    public ResponseEntity<Boolean> removeBatchByEMPlan(@RequestBody List<String> ids) {
        emplanrecordService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplanrecordMapping.toDomain(returnObject.body),'eam-EMPLANRECORD-Get')")
    @ApiOperation(value = "根据计划获取触发记录", tags = {"触发记录" },  notes = "根据计划获取触发记录")
	@RequestMapping(method = RequestMethod.GET, value = "/emplans/{emplan_id}/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<EMPLANRECORDDTO> getByEMPlan(@PathVariable("emplan_id") String emplan_id, @PathVariable("emplanrecord_id") String emplanrecord_id) {
        EMPLANRECORD domain = emplanrecordService.get(emplanrecord_id);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据计划获取触发记录草稿", tags = {"触发记录" },  notes = "根据计划获取触发记录草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emplans/{emplan_id}/emplanrecords/getdraft")
    public ResponseEntity<EMPLANRECORDDTO> getDraftByEMPlan(@PathVariable("emplan_id") String emplan_id, EMPLANRECORDDTO dto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(dto);
        domain.setEmplanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplanrecordMapping.toDto(emplanrecordService.getDraft(domain)));
    }

    @ApiOperation(value = "根据计划检查触发记录", tags = {"触发记录" },  notes = "根据计划检查触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emplans/{emplan_id}/emplanrecords/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMPlan(@PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplanrecordService.checkKey(emplanrecordMapping.toDomain(emplanrecorddto)));
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddto),'eam-EMPLANRECORD-Save')")
    @ApiOperation(value = "根据计划保存触发记录", tags = {"触发记录" },  notes = "根据计划保存触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emplans/{emplan_id}/emplanrecords/save")
    public ResponseEntity<EMPLANRECORDDTO> saveByEMPlan(@PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
        domain.setEmplanid(emplan_id);
        emplanrecordService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplanrecordMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddtos),'eam-EMPLANRECORD-Save')")
    @ApiOperation(value = "根据计划批量保存触发记录", tags = {"触发记录" },  notes = "根据计划批量保存触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emplans/{emplan_id}/emplanrecords/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMPlan(@PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        List<EMPLANRECORD> domainlist=emplanrecordMapping.toDomain(emplanrecorddtos);
        for(EMPLANRECORD domain:domainlist){
             domain.setEmplanid(emplan_id);
        }
        emplanrecordService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPLANRECORD-searchDefault-all') and hasPermission(#context,'eam-EMPLANRECORD-Get')")
	@ApiOperation(value = "根据计划获取数据集", tags = {"触发记录" } ,notes = "根据计划获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/emplans/{emplan_id}/emplanrecords/fetchdefault")
	public ResponseEntity<List<EMPLANRECORDDTO>> fetchEMPLANRECORDDefaultByEMPlan(@PathVariable("emplan_id") String emplan_id,EMPLANRECORDSearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<EMPLANRECORD> domains = emplanrecordService.searchDefault(context) ;
        List<EMPLANRECORDDTO> list = emplanrecordMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPLANRECORD-searchDefault-all') and hasPermission(#context,'eam-EMPLANRECORD-Get')")
	@ApiOperation(value = "根据计划查询数据集", tags = {"触发记录" } ,notes = "根据计划查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/emplans/{emplan_id}/emplanrecords/searchdefault")
	public ResponseEntity<Page<EMPLANRECORDDTO>> searchEMPLANRECORDDefaultByEMPlan(@PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDSearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<EMPLANRECORD> domains = emplanrecordService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplanrecordMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddto),'eam-EMPLANRECORD-Create')")
    @ApiOperation(value = "根据总帐科目计划建立触发记录", tags = {"触发记录" },  notes = "根据总帐科目计划建立触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplanrecords")
    public ResponseEntity<EMPLANRECORDDTO> createByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
        domain.setEmplanid(emplan_id);
		emplanrecordService.create(domain);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddtos),'eam-EMPLANRECORD-Create')")
    @ApiOperation(value = "根据总帐科目计划批量建立触发记录", tags = {"触发记录" },  notes = "根据总帐科目计划批量建立触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplanrecords/batch")
    public ResponseEntity<Boolean> createBatchByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        List<EMPLANRECORD> domainlist=emplanrecordMapping.toDomain(emplanrecorddtos);
        for(EMPLANRECORD domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        emplanrecordService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplanrecord" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplanrecordService.get(#emplanrecord_id),'eam-EMPLANRECORD-Update')")
    @ApiOperation(value = "根据总帐科目计划更新触发记录", tags = {"触发记录" },  notes = "根据总帐科目计划更新触发记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<EMPLANRECORDDTO> updateByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplanrecord_id") String emplanrecord_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
        domain.setEmplanid(emplan_id);
        domain.setEmplanrecordid(emplanrecord_id);
		emplanrecordService.update(domain);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.getEmplanrecordByEntities(this.emplanrecordMapping.toDomain(#emplanrecorddtos)),'eam-EMPLANRECORD-Update')")
    @ApiOperation(value = "根据总帐科目计划批量更新触发记录", tags = {"触发记录" },  notes = "根据总帐科目计划批量更新触发记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplanrecords/batch")
    public ResponseEntity<Boolean> updateBatchByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        List<EMPLANRECORD> domainlist=emplanrecordMapping.toDomain(emplanrecorddtos);
        for(EMPLANRECORD domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        emplanrecordService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.get(#emplanrecord_id),'eam-EMPLANRECORD-Remove')")
    @ApiOperation(value = "根据总帐科目计划删除触发记录", tags = {"触发记录" },  notes = "根据总帐科目计划删除触发记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<Boolean> removeByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplanrecord_id") String emplanrecord_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplanrecordService.remove(emplanrecord_id));
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.getEmplanrecordByIds(#ids),'eam-EMPLANRECORD-Remove')")
    @ApiOperation(value = "根据总帐科目计划批量删除触发记录", tags = {"触发记录" },  notes = "根据总帐科目计划批量删除触发记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplanrecords/batch")
    public ResponseEntity<Boolean> removeBatchByEMACClassEMPlan(@RequestBody List<String> ids) {
        emplanrecordService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplanrecordMapping.toDomain(returnObject.body),'eam-EMPLANRECORD-Get')")
    @ApiOperation(value = "根据总帐科目计划获取触发记录", tags = {"触发记录" },  notes = "根据总帐科目计划获取触发记录")
	@RequestMapping(method = RequestMethod.GET, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<EMPLANRECORDDTO> getByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplanrecord_id") String emplanrecord_id) {
        EMPLANRECORD domain = emplanrecordService.get(emplanrecord_id);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据总帐科目计划获取触发记录草稿", tags = {"触发记录" },  notes = "根据总帐科目计划获取触发记录草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplanrecords/getdraft")
    public ResponseEntity<EMPLANRECORDDTO> getDraftByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, EMPLANRECORDDTO dto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(dto);
        domain.setEmplanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplanrecordMapping.toDto(emplanrecordService.getDraft(domain)));
    }

    @ApiOperation(value = "根据总帐科目计划检查触发记录", tags = {"触发记录" },  notes = "根据总帐科目计划检查触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplanrecords/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplanrecordService.checkKey(emplanrecordMapping.toDomain(emplanrecorddto)));
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddto),'eam-EMPLANRECORD-Save')")
    @ApiOperation(value = "根据总帐科目计划保存触发记录", tags = {"触发记录" },  notes = "根据总帐科目计划保存触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplanrecords/save")
    public ResponseEntity<EMPLANRECORDDTO> saveByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
        domain.setEmplanid(emplan_id);
        emplanrecordService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplanrecordMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddtos),'eam-EMPLANRECORD-Save')")
    @ApiOperation(value = "根据总帐科目计划批量保存触发记录", tags = {"触发记录" },  notes = "根据总帐科目计划批量保存触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplanrecords/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        List<EMPLANRECORD> domainlist=emplanrecordMapping.toDomain(emplanrecorddtos);
        for(EMPLANRECORD domain:domainlist){
             domain.setEmplanid(emplan_id);
        }
        emplanrecordService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPLANRECORD-searchDefault-all') and hasPermission(#context,'eam-EMPLANRECORD-Get')")
	@ApiOperation(value = "根据总帐科目计划获取数据集", tags = {"触发记录" } ,notes = "根据总帐科目计划获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplanrecords/fetchdefault")
	public ResponseEntity<List<EMPLANRECORDDTO>> fetchEMPLANRECORDDefaultByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id,EMPLANRECORDSearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<EMPLANRECORD> domains = emplanrecordService.searchDefault(context) ;
        List<EMPLANRECORDDTO> list = emplanrecordMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPLANRECORD-searchDefault-all') and hasPermission(#context,'eam-EMPLANRECORD-Get')")
	@ApiOperation(value = "根据总帐科目计划查询数据集", tags = {"触发记录" } ,notes = "根据总帐科目计划查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/emacclasses/{emacclass_id}/emplans/{emplan_id}/emplanrecords/searchdefault")
	public ResponseEntity<Page<EMPLANRECORDDTO>> searchEMPLANRECORDDefaultByEMACClassEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDSearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<EMPLANRECORD> domains = emplanrecordService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplanrecordMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddto),'eam-EMPLANRECORD-Create')")
    @ApiOperation(value = "根据设备档案计划建立触发记录", tags = {"触发记录" },  notes = "根据设备档案计划建立触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplanrecords")
    public ResponseEntity<EMPLANRECORDDTO> createByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
        domain.setEmplanid(emplan_id);
		emplanrecordService.create(domain);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddtos),'eam-EMPLANRECORD-Create')")
    @ApiOperation(value = "根据设备档案计划批量建立触发记录", tags = {"触发记录" },  notes = "根据设备档案计划批量建立触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplanrecords/batch")
    public ResponseEntity<Boolean> createBatchByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        List<EMPLANRECORD> domainlist=emplanrecordMapping.toDomain(emplanrecorddtos);
        for(EMPLANRECORD domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        emplanrecordService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplanrecord" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplanrecordService.get(#emplanrecord_id),'eam-EMPLANRECORD-Update')")
    @ApiOperation(value = "根据设备档案计划更新触发记录", tags = {"触发记录" },  notes = "根据设备档案计划更新触发记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<EMPLANRECORDDTO> updateByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplanrecord_id") String emplanrecord_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
        domain.setEmplanid(emplan_id);
        domain.setEmplanrecordid(emplanrecord_id);
		emplanrecordService.update(domain);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.getEmplanrecordByEntities(this.emplanrecordMapping.toDomain(#emplanrecorddtos)),'eam-EMPLANRECORD-Update')")
    @ApiOperation(value = "根据设备档案计划批量更新触发记录", tags = {"触发记录" },  notes = "根据设备档案计划批量更新触发记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplanrecords/batch")
    public ResponseEntity<Boolean> updateBatchByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        List<EMPLANRECORD> domainlist=emplanrecordMapping.toDomain(emplanrecorddtos);
        for(EMPLANRECORD domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        emplanrecordService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.get(#emplanrecord_id),'eam-EMPLANRECORD-Remove')")
    @ApiOperation(value = "根据设备档案计划删除触发记录", tags = {"触发记录" },  notes = "根据设备档案计划删除触发记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<Boolean> removeByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplanrecord_id") String emplanrecord_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplanrecordService.remove(emplanrecord_id));
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.getEmplanrecordByIds(#ids),'eam-EMPLANRECORD-Remove')")
    @ApiOperation(value = "根据设备档案计划批量删除触发记录", tags = {"触发记录" },  notes = "根据设备档案计划批量删除触发记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplanrecords/batch")
    public ResponseEntity<Boolean> removeBatchByEMEquipEMPlan(@RequestBody List<String> ids) {
        emplanrecordService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplanrecordMapping.toDomain(returnObject.body),'eam-EMPLANRECORD-Get')")
    @ApiOperation(value = "根据设备档案计划获取触发记录", tags = {"触发记录" },  notes = "根据设备档案计划获取触发记录")
	@RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<EMPLANRECORDDTO> getByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplanrecord_id") String emplanrecord_id) {
        EMPLANRECORD domain = emplanrecordService.get(emplanrecord_id);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据设备档案计划获取触发记录草稿", tags = {"触发记录" },  notes = "根据设备档案计划获取触发记录草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplanrecords/getdraft")
    public ResponseEntity<EMPLANRECORDDTO> getDraftByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, EMPLANRECORDDTO dto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(dto);
        domain.setEmplanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplanrecordMapping.toDto(emplanrecordService.getDraft(domain)));
    }

    @ApiOperation(value = "根据设备档案计划检查触发记录", tags = {"触发记录" },  notes = "根据设备档案计划检查触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplanrecords/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplanrecordService.checkKey(emplanrecordMapping.toDomain(emplanrecorddto)));
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddto),'eam-EMPLANRECORD-Save')")
    @ApiOperation(value = "根据设备档案计划保存触发记录", tags = {"触发记录" },  notes = "根据设备档案计划保存触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplanrecords/save")
    public ResponseEntity<EMPLANRECORDDTO> saveByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
        domain.setEmplanid(emplan_id);
        emplanrecordService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplanrecordMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddtos),'eam-EMPLANRECORD-Save')")
    @ApiOperation(value = "根据设备档案计划批量保存触发记录", tags = {"触发记录" },  notes = "根据设备档案计划批量保存触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emplans/{emplan_id}/emplanrecords/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        List<EMPLANRECORD> domainlist=emplanrecordMapping.toDomain(emplanrecorddtos);
        for(EMPLANRECORD domain:domainlist){
             domain.setEmplanid(emplan_id);
        }
        emplanrecordService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPLANRECORD-searchDefault-all') and hasPermission(#context,'eam-EMPLANRECORD-Get')")
	@ApiOperation(value = "根据设备档案计划获取数据集", tags = {"触发记录" } ,notes = "根据设备档案计划获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emplans/{emplan_id}/emplanrecords/fetchdefault")
	public ResponseEntity<List<EMPLANRECORDDTO>> fetchEMPLANRECORDDefaultByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id,EMPLANRECORDSearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<EMPLANRECORD> domains = emplanrecordService.searchDefault(context) ;
        List<EMPLANRECORDDTO> list = emplanrecordMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPLANRECORD-searchDefault-all') and hasPermission(#context,'eam-EMPLANRECORD-Get')")
	@ApiOperation(value = "根据设备档案计划查询数据集", tags = {"触发记录" } ,notes = "根据设备档案计划查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emplans/{emplan_id}/emplanrecords/searchdefault")
	public ResponseEntity<Page<EMPLANRECORDDTO>> searchEMPLANRECORDDefaultByEMEquipEMPlan(@PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDSearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<EMPLANRECORD> domains = emplanrecordService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplanrecordMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddto),'eam-EMPLANRECORD-Create')")
    @ApiOperation(value = "根据计划模板计划建立触发记录", tags = {"触发记录" },  notes = "根据计划模板计划建立触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords")
    public ResponseEntity<EMPLANRECORDDTO> createByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
        domain.setEmplanid(emplan_id);
		emplanrecordService.create(domain);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddtos),'eam-EMPLANRECORD-Create')")
    @ApiOperation(value = "根据计划模板计划批量建立触发记录", tags = {"触发记录" },  notes = "根据计划模板计划批量建立触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/batch")
    public ResponseEntity<Boolean> createBatchByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        List<EMPLANRECORD> domainlist=emplanrecordMapping.toDomain(emplanrecorddtos);
        for(EMPLANRECORD domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        emplanrecordService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplanrecord" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplanrecordService.get(#emplanrecord_id),'eam-EMPLANRECORD-Update')")
    @ApiOperation(value = "根据计划模板计划更新触发记录", tags = {"触发记录" },  notes = "根据计划模板计划更新触发记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<EMPLANRECORDDTO> updateByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplanrecord_id") String emplanrecord_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
        domain.setEmplanid(emplan_id);
        domain.setEmplanrecordid(emplanrecord_id);
		emplanrecordService.update(domain);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.getEmplanrecordByEntities(this.emplanrecordMapping.toDomain(#emplanrecorddtos)),'eam-EMPLANRECORD-Update')")
    @ApiOperation(value = "根据计划模板计划批量更新触发记录", tags = {"触发记录" },  notes = "根据计划模板计划批量更新触发记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/batch")
    public ResponseEntity<Boolean> updateBatchByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        List<EMPLANRECORD> domainlist=emplanrecordMapping.toDomain(emplanrecorddtos);
        for(EMPLANRECORD domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        emplanrecordService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.get(#emplanrecord_id),'eam-EMPLANRECORD-Remove')")
    @ApiOperation(value = "根据计划模板计划删除触发记录", tags = {"触发记录" },  notes = "根据计划模板计划删除触发记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<Boolean> removeByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplanrecord_id") String emplanrecord_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplanrecordService.remove(emplanrecord_id));
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.getEmplanrecordByIds(#ids),'eam-EMPLANRECORD-Remove')")
    @ApiOperation(value = "根据计划模板计划批量删除触发记录", tags = {"触发记录" },  notes = "根据计划模板计划批量删除触发记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/batch")
    public ResponseEntity<Boolean> removeBatchByEMPlanTemplEMPlan(@RequestBody List<String> ids) {
        emplanrecordService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplanrecordMapping.toDomain(returnObject.body),'eam-EMPLANRECORD-Get')")
    @ApiOperation(value = "根据计划模板计划获取触发记录", tags = {"触发记录" },  notes = "根据计划模板计划获取触发记录")
	@RequestMapping(method = RequestMethod.GET, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<EMPLANRECORDDTO> getByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplanrecord_id") String emplanrecord_id) {
        EMPLANRECORD domain = emplanrecordService.get(emplanrecord_id);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据计划模板计划获取触发记录草稿", tags = {"触发记录" },  notes = "根据计划模板计划获取触发记录草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/getdraft")
    public ResponseEntity<EMPLANRECORDDTO> getDraftByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, EMPLANRECORDDTO dto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(dto);
        domain.setEmplanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplanrecordMapping.toDto(emplanrecordService.getDraft(domain)));
    }

    @ApiOperation(value = "根据计划模板计划检查触发记录", tags = {"触发记录" },  notes = "根据计划模板计划检查触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplanrecordService.checkKey(emplanrecordMapping.toDomain(emplanrecorddto)));
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddto),'eam-EMPLANRECORD-Save')")
    @ApiOperation(value = "根据计划模板计划保存触发记录", tags = {"触发记录" },  notes = "根据计划模板计划保存触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/save")
    public ResponseEntity<EMPLANRECORDDTO> saveByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
        domain.setEmplanid(emplan_id);
        emplanrecordService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplanrecordMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddtos),'eam-EMPLANRECORD-Save')")
    @ApiOperation(value = "根据计划模板计划批量保存触发记录", tags = {"触发记录" },  notes = "根据计划模板计划批量保存触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        List<EMPLANRECORD> domainlist=emplanrecordMapping.toDomain(emplanrecorddtos);
        for(EMPLANRECORD domain:domainlist){
             domain.setEmplanid(emplan_id);
        }
        emplanrecordService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPLANRECORD-searchDefault-all') and hasPermission(#context,'eam-EMPLANRECORD-Get')")
	@ApiOperation(value = "根据计划模板计划获取数据集", tags = {"触发记录" } ,notes = "根据计划模板计划获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/fetchdefault")
	public ResponseEntity<List<EMPLANRECORDDTO>> fetchEMPLANRECORDDefaultByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id,EMPLANRECORDSearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<EMPLANRECORD> domains = emplanrecordService.searchDefault(context) ;
        List<EMPLANRECORDDTO> list = emplanrecordMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPLANRECORD-searchDefault-all') and hasPermission(#context,'eam-EMPLANRECORD-Get')")
	@ApiOperation(value = "根据计划模板计划查询数据集", tags = {"触发记录" } ,notes = "根据计划模板计划查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/searchdefault")
	public ResponseEntity<Page<EMPLANRECORDDTO>> searchEMPLANRECORDDefaultByEMPlanTemplEMPlan(@PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDSearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<EMPLANRECORD> domains = emplanrecordService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplanrecordMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddto),'eam-EMPLANRECORD-Create')")
    @ApiOperation(value = "根据服务商计划建立触发记录", tags = {"触发记录" },  notes = "根据服务商计划建立触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplanrecords")
    public ResponseEntity<EMPLANRECORDDTO> createByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
        domain.setEmplanid(emplan_id);
		emplanrecordService.create(domain);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddtos),'eam-EMPLANRECORD-Create')")
    @ApiOperation(value = "根据服务商计划批量建立触发记录", tags = {"触发记录" },  notes = "根据服务商计划批量建立触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplanrecords/batch")
    public ResponseEntity<Boolean> createBatchByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        List<EMPLANRECORD> domainlist=emplanrecordMapping.toDomain(emplanrecorddtos);
        for(EMPLANRECORD domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        emplanrecordService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplanrecord" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplanrecordService.get(#emplanrecord_id),'eam-EMPLANRECORD-Update')")
    @ApiOperation(value = "根据服务商计划更新触发记录", tags = {"触发记录" },  notes = "根据服务商计划更新触发记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<EMPLANRECORDDTO> updateByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplanrecord_id") String emplanrecord_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
        domain.setEmplanid(emplan_id);
        domain.setEmplanrecordid(emplanrecord_id);
		emplanrecordService.update(domain);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.getEmplanrecordByEntities(this.emplanrecordMapping.toDomain(#emplanrecorddtos)),'eam-EMPLANRECORD-Update')")
    @ApiOperation(value = "根据服务商计划批量更新触发记录", tags = {"触发记录" },  notes = "根据服务商计划批量更新触发记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplanrecords/batch")
    public ResponseEntity<Boolean> updateBatchByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        List<EMPLANRECORD> domainlist=emplanrecordMapping.toDomain(emplanrecorddtos);
        for(EMPLANRECORD domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        emplanrecordService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.get(#emplanrecord_id),'eam-EMPLANRECORD-Remove')")
    @ApiOperation(value = "根据服务商计划删除触发记录", tags = {"触发记录" },  notes = "根据服务商计划删除触发记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<Boolean> removeByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplanrecord_id") String emplanrecord_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplanrecordService.remove(emplanrecord_id));
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.getEmplanrecordByIds(#ids),'eam-EMPLANRECORD-Remove')")
    @ApiOperation(value = "根据服务商计划批量删除触发记录", tags = {"触发记录" },  notes = "根据服务商计划批量删除触发记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplanrecords/batch")
    public ResponseEntity<Boolean> removeBatchByEMServiceEMPlan(@RequestBody List<String> ids) {
        emplanrecordService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplanrecordMapping.toDomain(returnObject.body),'eam-EMPLANRECORD-Get')")
    @ApiOperation(value = "根据服务商计划获取触发记录", tags = {"触发记录" },  notes = "根据服务商计划获取触发记录")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<EMPLANRECORDDTO> getByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplanrecord_id") String emplanrecord_id) {
        EMPLANRECORD domain = emplanrecordService.get(emplanrecord_id);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据服务商计划获取触发记录草稿", tags = {"触发记录" },  notes = "根据服务商计划获取触发记录草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplanrecords/getdraft")
    public ResponseEntity<EMPLANRECORDDTO> getDraftByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, EMPLANRECORDDTO dto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(dto);
        domain.setEmplanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplanrecordMapping.toDto(emplanrecordService.getDraft(domain)));
    }

    @ApiOperation(value = "根据服务商计划检查触发记录", tags = {"触发记录" },  notes = "根据服务商计划检查触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplanrecords/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplanrecordService.checkKey(emplanrecordMapping.toDomain(emplanrecorddto)));
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddto),'eam-EMPLANRECORD-Save')")
    @ApiOperation(value = "根据服务商计划保存触发记录", tags = {"触发记录" },  notes = "根据服务商计划保存触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplanrecords/save")
    public ResponseEntity<EMPLANRECORDDTO> saveByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
        domain.setEmplanid(emplan_id);
        emplanrecordService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplanrecordMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddtos),'eam-EMPLANRECORD-Save')")
    @ApiOperation(value = "根据服务商计划批量保存触发记录", tags = {"触发记录" },  notes = "根据服务商计划批量保存触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplans/{emplan_id}/emplanrecords/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        List<EMPLANRECORD> domainlist=emplanrecordMapping.toDomain(emplanrecorddtos);
        for(EMPLANRECORD domain:domainlist){
             domain.setEmplanid(emplan_id);
        }
        emplanrecordService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPLANRECORD-searchDefault-all') and hasPermission(#context,'eam-EMPLANRECORD-Get')")
	@ApiOperation(value = "根据服务商计划获取数据集", tags = {"触发记录" } ,notes = "根据服务商计划获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emplans/{emplan_id}/emplanrecords/fetchdefault")
	public ResponseEntity<List<EMPLANRECORDDTO>> fetchEMPLANRECORDDefaultByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id,EMPLANRECORDSearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<EMPLANRECORD> domains = emplanrecordService.searchDefault(context) ;
        List<EMPLANRECORDDTO> list = emplanrecordMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPLANRECORD-searchDefault-all') and hasPermission(#context,'eam-EMPLANRECORD-Get')")
	@ApiOperation(value = "根据服务商计划查询数据集", tags = {"触发记录" } ,notes = "根据服务商计划查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emplans/{emplan_id}/emplanrecords/searchdefault")
	public ResponseEntity<Page<EMPLANRECORDDTO>> searchEMPLANRECORDDefaultByEMServiceEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDSearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<EMPLANRECORD> domains = emplanrecordService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplanrecordMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddto),'eam-EMPLANRECORD-Create')")
    @ApiOperation(value = "根据班组计划建立触发记录", tags = {"触发记录" },  notes = "根据班组计划建立触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplanrecords")
    public ResponseEntity<EMPLANRECORDDTO> createByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
        domain.setEmplanid(emplan_id);
		emplanrecordService.create(domain);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddtos),'eam-EMPLANRECORD-Create')")
    @ApiOperation(value = "根据班组计划批量建立触发记录", tags = {"触发记录" },  notes = "根据班组计划批量建立触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplanrecords/batch")
    public ResponseEntity<Boolean> createBatchByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        List<EMPLANRECORD> domainlist=emplanrecordMapping.toDomain(emplanrecorddtos);
        for(EMPLANRECORD domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        emplanrecordService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplanrecord" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplanrecordService.get(#emplanrecord_id),'eam-EMPLANRECORD-Update')")
    @ApiOperation(value = "根据班组计划更新触发记录", tags = {"触发记录" },  notes = "根据班组计划更新触发记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<EMPLANRECORDDTO> updateByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplanrecord_id") String emplanrecord_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
        domain.setEmplanid(emplan_id);
        domain.setEmplanrecordid(emplanrecord_id);
		emplanrecordService.update(domain);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.getEmplanrecordByEntities(this.emplanrecordMapping.toDomain(#emplanrecorddtos)),'eam-EMPLANRECORD-Update')")
    @ApiOperation(value = "根据班组计划批量更新触发记录", tags = {"触发记录" },  notes = "根据班组计划批量更新触发记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplanrecords/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        List<EMPLANRECORD> domainlist=emplanrecordMapping.toDomain(emplanrecorddtos);
        for(EMPLANRECORD domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        emplanrecordService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.get(#emplanrecord_id),'eam-EMPLANRECORD-Remove')")
    @ApiOperation(value = "根据班组计划删除触发记录", tags = {"触发记录" },  notes = "根据班组计划删除触发记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<Boolean> removeByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplanrecord_id") String emplanrecord_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplanrecordService.remove(emplanrecord_id));
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.getEmplanrecordByIds(#ids),'eam-EMPLANRECORD-Remove')")
    @ApiOperation(value = "根据班组计划批量删除触发记录", tags = {"触发记录" },  notes = "根据班组计划批量删除触发记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplanrecords/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeamEMPlan(@RequestBody List<String> ids) {
        emplanrecordService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplanrecordMapping.toDomain(returnObject.body),'eam-EMPLANRECORD-Get')")
    @ApiOperation(value = "根据班组计划获取触发记录", tags = {"触发记录" },  notes = "根据班组计划获取触发记录")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<EMPLANRECORDDTO> getByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplanrecord_id") String emplanrecord_id) {
        EMPLANRECORD domain = emplanrecordService.get(emplanrecord_id);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组计划获取触发记录草稿", tags = {"触发记录" },  notes = "根据班组计划获取触发记录草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplanrecords/getdraft")
    public ResponseEntity<EMPLANRECORDDTO> getDraftByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, EMPLANRECORDDTO dto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(dto);
        domain.setEmplanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplanrecordMapping.toDto(emplanrecordService.getDraft(domain)));
    }

    @ApiOperation(value = "根据班组计划检查触发记录", tags = {"触发记录" },  notes = "根据班组计划检查触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplanrecords/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplanrecordService.checkKey(emplanrecordMapping.toDomain(emplanrecorddto)));
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddto),'eam-EMPLANRECORD-Save')")
    @ApiOperation(value = "根据班组计划保存触发记录", tags = {"触发记录" },  notes = "根据班组计划保存触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplanrecords/save")
    public ResponseEntity<EMPLANRECORDDTO> saveByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
        domain.setEmplanid(emplan_id);
        emplanrecordService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplanrecordMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddtos),'eam-EMPLANRECORD-Save')")
    @ApiOperation(value = "根据班组计划批量保存触发记录", tags = {"触发记录" },  notes = "根据班组计划批量保存触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplans/{emplan_id}/emplanrecords/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        List<EMPLANRECORD> domainlist=emplanrecordMapping.toDomain(emplanrecorddtos);
        for(EMPLANRECORD domain:domainlist){
             domain.setEmplanid(emplan_id);
        }
        emplanrecordService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPLANRECORD-searchDefault-all') and hasPermission(#context,'eam-EMPLANRECORD-Get')")
	@ApiOperation(value = "根据班组计划获取数据集", tags = {"触发记录" } ,notes = "根据班组计划获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emplans/{emplan_id}/emplanrecords/fetchdefault")
	public ResponseEntity<List<EMPLANRECORDDTO>> fetchEMPLANRECORDDefaultByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id,EMPLANRECORDSearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<EMPLANRECORD> domains = emplanrecordService.searchDefault(context) ;
        List<EMPLANRECORDDTO> list = emplanrecordMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPLANRECORD-searchDefault-all') and hasPermission(#context,'eam-EMPLANRECORD-Get')")
	@ApiOperation(value = "根据班组计划查询数据集", tags = {"触发记录" } ,notes = "根据班组计划查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emplans/{emplan_id}/emplanrecords/searchdefault")
	public ResponseEntity<Page<EMPLANRECORDDTO>> searchEMPLANRECORDDefaultByPFTeamEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDSearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<EMPLANRECORD> domains = emplanrecordService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplanrecordMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddto),'eam-EMPLANRECORD-Create')")
    @ApiOperation(value = "根据班组设备档案计划建立触发记录", tags = {"触发记录" },  notes = "根据班组设备档案计划建立触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplanrecords")
    public ResponseEntity<EMPLANRECORDDTO> createByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
        domain.setEmplanid(emplan_id);
		emplanrecordService.create(domain);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddtos),'eam-EMPLANRECORD-Create')")
    @ApiOperation(value = "根据班组设备档案计划批量建立触发记录", tags = {"触发记录" },  notes = "根据班组设备档案计划批量建立触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplanrecords/batch")
    public ResponseEntity<Boolean> createBatchByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        List<EMPLANRECORD> domainlist=emplanrecordMapping.toDomain(emplanrecorddtos);
        for(EMPLANRECORD domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        emplanrecordService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplanrecord" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplanrecordService.get(#emplanrecord_id),'eam-EMPLANRECORD-Update')")
    @ApiOperation(value = "根据班组设备档案计划更新触发记录", tags = {"触发记录" },  notes = "根据班组设备档案计划更新触发记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<EMPLANRECORDDTO> updateByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplanrecord_id") String emplanrecord_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
        domain.setEmplanid(emplan_id);
        domain.setEmplanrecordid(emplanrecord_id);
		emplanrecordService.update(domain);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.getEmplanrecordByEntities(this.emplanrecordMapping.toDomain(#emplanrecorddtos)),'eam-EMPLANRECORD-Update')")
    @ApiOperation(value = "根据班组设备档案计划批量更新触发记录", tags = {"触发记录" },  notes = "根据班组设备档案计划批量更新触发记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplanrecords/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        List<EMPLANRECORD> domainlist=emplanrecordMapping.toDomain(emplanrecorddtos);
        for(EMPLANRECORD domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        emplanrecordService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.get(#emplanrecord_id),'eam-EMPLANRECORD-Remove')")
    @ApiOperation(value = "根据班组设备档案计划删除触发记录", tags = {"触发记录" },  notes = "根据班组设备档案计划删除触发记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<Boolean> removeByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplanrecord_id") String emplanrecord_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplanrecordService.remove(emplanrecord_id));
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.getEmplanrecordByIds(#ids),'eam-EMPLANRECORD-Remove')")
    @ApiOperation(value = "根据班组设备档案计划批量删除触发记录", tags = {"触发记录" },  notes = "根据班组设备档案计划批量删除触发记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplanrecords/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeamEMEquipEMPlan(@RequestBody List<String> ids) {
        emplanrecordService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplanrecordMapping.toDomain(returnObject.body),'eam-EMPLANRECORD-Get')")
    @ApiOperation(value = "根据班组设备档案计划获取触发记录", tags = {"触发记录" },  notes = "根据班组设备档案计划获取触发记录")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<EMPLANRECORDDTO> getByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplanrecord_id") String emplanrecord_id) {
        EMPLANRECORD domain = emplanrecordService.get(emplanrecord_id);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组设备档案计划获取触发记录草稿", tags = {"触发记录" },  notes = "根据班组设备档案计划获取触发记录草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplanrecords/getdraft")
    public ResponseEntity<EMPLANRECORDDTO> getDraftByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, EMPLANRECORDDTO dto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(dto);
        domain.setEmplanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplanrecordMapping.toDto(emplanrecordService.getDraft(domain)));
    }

    @ApiOperation(value = "根据班组设备档案计划检查触发记录", tags = {"触发记录" },  notes = "根据班组设备档案计划检查触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplanrecords/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplanrecordService.checkKey(emplanrecordMapping.toDomain(emplanrecorddto)));
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddto),'eam-EMPLANRECORD-Save')")
    @ApiOperation(value = "根据班组设备档案计划保存触发记录", tags = {"触发记录" },  notes = "根据班组设备档案计划保存触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplanrecords/save")
    public ResponseEntity<EMPLANRECORDDTO> saveByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
        domain.setEmplanid(emplan_id);
        emplanrecordService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplanrecordMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddtos),'eam-EMPLANRECORD-Save')")
    @ApiOperation(value = "根据班组设备档案计划批量保存触发记录", tags = {"触发记录" },  notes = "根据班组设备档案计划批量保存触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplanrecords/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        List<EMPLANRECORD> domainlist=emplanrecordMapping.toDomain(emplanrecorddtos);
        for(EMPLANRECORD domain:domainlist){
             domain.setEmplanid(emplan_id);
        }
        emplanrecordService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPLANRECORD-searchDefault-all') and hasPermission(#context,'eam-EMPLANRECORD-Get')")
	@ApiOperation(value = "根据班组设备档案计划获取数据集", tags = {"触发记录" } ,notes = "根据班组设备档案计划获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplanrecords/fetchdefault")
	public ResponseEntity<List<EMPLANRECORDDTO>> fetchEMPLANRECORDDefaultByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id,EMPLANRECORDSearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<EMPLANRECORD> domains = emplanrecordService.searchDefault(context) ;
        List<EMPLANRECORDDTO> list = emplanrecordMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPLANRECORD-searchDefault-all') and hasPermission(#context,'eam-EMPLANRECORD-Get')")
	@ApiOperation(value = "根据班组设备档案计划查询数据集", tags = {"触发记录" } ,notes = "根据班组设备档案计划查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emplans/{emplan_id}/emplanrecords/searchdefault")
	public ResponseEntity<Page<EMPLANRECORDDTO>> searchEMPLANRECORDDefaultByPFTeamEMEquipEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDSearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<EMPLANRECORD> domains = emplanrecordService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplanrecordMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddto),'eam-EMPLANRECORD-Create')")
    @ApiOperation(value = "根据总帐科目计划模板计划建立触发记录", tags = {"触发记录" },  notes = "根据总帐科目计划模板计划建立触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords")
    public ResponseEntity<EMPLANRECORDDTO> createByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
        domain.setEmplanid(emplan_id);
		emplanrecordService.create(domain);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddtos),'eam-EMPLANRECORD-Create')")
    @ApiOperation(value = "根据总帐科目计划模板计划批量建立触发记录", tags = {"触发记录" },  notes = "根据总帐科目计划模板计划批量建立触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/batch")
    public ResponseEntity<Boolean> createBatchByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        List<EMPLANRECORD> domainlist=emplanrecordMapping.toDomain(emplanrecorddtos);
        for(EMPLANRECORD domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        emplanrecordService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplanrecord" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplanrecordService.get(#emplanrecord_id),'eam-EMPLANRECORD-Update')")
    @ApiOperation(value = "根据总帐科目计划模板计划更新触发记录", tags = {"触发记录" },  notes = "根据总帐科目计划模板计划更新触发记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<EMPLANRECORDDTO> updateByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplanrecord_id") String emplanrecord_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
        domain.setEmplanid(emplan_id);
        domain.setEmplanrecordid(emplanrecord_id);
		emplanrecordService.update(domain);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.getEmplanrecordByEntities(this.emplanrecordMapping.toDomain(#emplanrecorddtos)),'eam-EMPLANRECORD-Update')")
    @ApiOperation(value = "根据总帐科目计划模板计划批量更新触发记录", tags = {"触发记录" },  notes = "根据总帐科目计划模板计划批量更新触发记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/batch")
    public ResponseEntity<Boolean> updateBatchByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        List<EMPLANRECORD> domainlist=emplanrecordMapping.toDomain(emplanrecorddtos);
        for(EMPLANRECORD domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        emplanrecordService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.get(#emplanrecord_id),'eam-EMPLANRECORD-Remove')")
    @ApiOperation(value = "根据总帐科目计划模板计划删除触发记录", tags = {"触发记录" },  notes = "根据总帐科目计划模板计划删除触发记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<Boolean> removeByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplanrecord_id") String emplanrecord_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplanrecordService.remove(emplanrecord_id));
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.getEmplanrecordByIds(#ids),'eam-EMPLANRECORD-Remove')")
    @ApiOperation(value = "根据总帐科目计划模板计划批量删除触发记录", tags = {"触发记录" },  notes = "根据总帐科目计划模板计划批量删除触发记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/batch")
    public ResponseEntity<Boolean> removeBatchByEMACClassEMPlanTemplEMPlan(@RequestBody List<String> ids) {
        emplanrecordService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplanrecordMapping.toDomain(returnObject.body),'eam-EMPLANRECORD-Get')")
    @ApiOperation(value = "根据总帐科目计划模板计划获取触发记录", tags = {"触发记录" },  notes = "根据总帐科目计划模板计划获取触发记录")
	@RequestMapping(method = RequestMethod.GET, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<EMPLANRECORDDTO> getByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplanrecord_id") String emplanrecord_id) {
        EMPLANRECORD domain = emplanrecordService.get(emplanrecord_id);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据总帐科目计划模板计划获取触发记录草稿", tags = {"触发记录" },  notes = "根据总帐科目计划模板计划获取触发记录草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/getdraft")
    public ResponseEntity<EMPLANRECORDDTO> getDraftByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, EMPLANRECORDDTO dto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(dto);
        domain.setEmplanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplanrecordMapping.toDto(emplanrecordService.getDraft(domain)));
    }

    @ApiOperation(value = "根据总帐科目计划模板计划检查触发记录", tags = {"触发记录" },  notes = "根据总帐科目计划模板计划检查触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplanrecordService.checkKey(emplanrecordMapping.toDomain(emplanrecorddto)));
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddto),'eam-EMPLANRECORD-Save')")
    @ApiOperation(value = "根据总帐科目计划模板计划保存触发记录", tags = {"触发记录" },  notes = "根据总帐科目计划模板计划保存触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/save")
    public ResponseEntity<EMPLANRECORDDTO> saveByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
        domain.setEmplanid(emplan_id);
        emplanrecordService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplanrecordMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddtos),'eam-EMPLANRECORD-Save')")
    @ApiOperation(value = "根据总帐科目计划模板计划批量保存触发记录", tags = {"触发记录" },  notes = "根据总帐科目计划模板计划批量保存触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        List<EMPLANRECORD> domainlist=emplanrecordMapping.toDomain(emplanrecorddtos);
        for(EMPLANRECORD domain:domainlist){
             domain.setEmplanid(emplan_id);
        }
        emplanrecordService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPLANRECORD-searchDefault-all') and hasPermission(#context,'eam-EMPLANRECORD-Get')")
	@ApiOperation(value = "根据总帐科目计划模板计划获取数据集", tags = {"触发记录" } ,notes = "根据总帐科目计划模板计划获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/fetchdefault")
	public ResponseEntity<List<EMPLANRECORDDTO>> fetchEMPLANRECORDDefaultByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id,EMPLANRECORDSearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<EMPLANRECORD> domains = emplanrecordService.searchDefault(context) ;
        List<EMPLANRECORDDTO> list = emplanrecordMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPLANRECORD-searchDefault-all') and hasPermission(#context,'eam-EMPLANRECORD-Get')")
	@ApiOperation(value = "根据总帐科目计划模板计划查询数据集", tags = {"触发记录" } ,notes = "根据总帐科目计划模板计划查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/searchdefault")
	public ResponseEntity<Page<EMPLANRECORDDTO>> searchEMPLANRECORDDefaultByEMACClassEMPlanTemplEMPlan(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDSearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<EMPLANRECORD> domains = emplanrecordService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplanrecordMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddto),'eam-EMPLANRECORD-Create')")
    @ApiOperation(value = "根据服务商计划模板计划建立触发记录", tags = {"触发记录" },  notes = "根据服务商计划模板计划建立触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords")
    public ResponseEntity<EMPLANRECORDDTO> createByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
        domain.setEmplanid(emplan_id);
		emplanrecordService.create(domain);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddtos),'eam-EMPLANRECORD-Create')")
    @ApiOperation(value = "根据服务商计划模板计划批量建立触发记录", tags = {"触发记录" },  notes = "根据服务商计划模板计划批量建立触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/batch")
    public ResponseEntity<Boolean> createBatchByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        List<EMPLANRECORD> domainlist=emplanrecordMapping.toDomain(emplanrecorddtos);
        for(EMPLANRECORD domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        emplanrecordService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplanrecord" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplanrecordService.get(#emplanrecord_id),'eam-EMPLANRECORD-Update')")
    @ApiOperation(value = "根据服务商计划模板计划更新触发记录", tags = {"触发记录" },  notes = "根据服务商计划模板计划更新触发记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<EMPLANRECORDDTO> updateByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplanrecord_id") String emplanrecord_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
        domain.setEmplanid(emplan_id);
        domain.setEmplanrecordid(emplanrecord_id);
		emplanrecordService.update(domain);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.getEmplanrecordByEntities(this.emplanrecordMapping.toDomain(#emplanrecorddtos)),'eam-EMPLANRECORD-Update')")
    @ApiOperation(value = "根据服务商计划模板计划批量更新触发记录", tags = {"触发记录" },  notes = "根据服务商计划模板计划批量更新触发记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/batch")
    public ResponseEntity<Boolean> updateBatchByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        List<EMPLANRECORD> domainlist=emplanrecordMapping.toDomain(emplanrecorddtos);
        for(EMPLANRECORD domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        emplanrecordService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.get(#emplanrecord_id),'eam-EMPLANRECORD-Remove')")
    @ApiOperation(value = "根据服务商计划模板计划删除触发记录", tags = {"触发记录" },  notes = "根据服务商计划模板计划删除触发记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<Boolean> removeByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplanrecord_id") String emplanrecord_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplanrecordService.remove(emplanrecord_id));
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.getEmplanrecordByIds(#ids),'eam-EMPLANRECORD-Remove')")
    @ApiOperation(value = "根据服务商计划模板计划批量删除触发记录", tags = {"触发记录" },  notes = "根据服务商计划模板计划批量删除触发记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/batch")
    public ResponseEntity<Boolean> removeBatchByEMServiceEMPlanTemplEMPlan(@RequestBody List<String> ids) {
        emplanrecordService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplanrecordMapping.toDomain(returnObject.body),'eam-EMPLANRECORD-Get')")
    @ApiOperation(value = "根据服务商计划模板计划获取触发记录", tags = {"触发记录" },  notes = "根据服务商计划模板计划获取触发记录")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<EMPLANRECORDDTO> getByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplanrecord_id") String emplanrecord_id) {
        EMPLANRECORD domain = emplanrecordService.get(emplanrecord_id);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据服务商计划模板计划获取触发记录草稿", tags = {"触发记录" },  notes = "根据服务商计划模板计划获取触发记录草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/getdraft")
    public ResponseEntity<EMPLANRECORDDTO> getDraftByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, EMPLANRECORDDTO dto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(dto);
        domain.setEmplanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplanrecordMapping.toDto(emplanrecordService.getDraft(domain)));
    }

    @ApiOperation(value = "根据服务商计划模板计划检查触发记录", tags = {"触发记录" },  notes = "根据服务商计划模板计划检查触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplanrecordService.checkKey(emplanrecordMapping.toDomain(emplanrecorddto)));
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddto),'eam-EMPLANRECORD-Save')")
    @ApiOperation(value = "根据服务商计划模板计划保存触发记录", tags = {"触发记录" },  notes = "根据服务商计划模板计划保存触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/save")
    public ResponseEntity<EMPLANRECORDDTO> saveByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
        domain.setEmplanid(emplan_id);
        emplanrecordService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplanrecordMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddtos),'eam-EMPLANRECORD-Save')")
    @ApiOperation(value = "根据服务商计划模板计划批量保存触发记录", tags = {"触发记录" },  notes = "根据服务商计划模板计划批量保存触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        List<EMPLANRECORD> domainlist=emplanrecordMapping.toDomain(emplanrecorddtos);
        for(EMPLANRECORD domain:domainlist){
             domain.setEmplanid(emplan_id);
        }
        emplanrecordService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPLANRECORD-searchDefault-all') and hasPermission(#context,'eam-EMPLANRECORD-Get')")
	@ApiOperation(value = "根据服务商计划模板计划获取数据集", tags = {"触发记录" } ,notes = "根据服务商计划模板计划获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/fetchdefault")
	public ResponseEntity<List<EMPLANRECORDDTO>> fetchEMPLANRECORDDefaultByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id,EMPLANRECORDSearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<EMPLANRECORD> domains = emplanrecordService.searchDefault(context) ;
        List<EMPLANRECORDDTO> list = emplanrecordMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPLANRECORD-searchDefault-all') and hasPermission(#context,'eam-EMPLANRECORD-Get')")
	@ApiOperation(value = "根据服务商计划模板计划查询数据集", tags = {"触发记录" } ,notes = "根据服务商计划模板计划查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/searchdefault")
	public ResponseEntity<Page<EMPLANRECORDDTO>> searchEMPLANRECORDDefaultByEMServiceEMPlanTemplEMPlan(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDSearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<EMPLANRECORD> domains = emplanrecordService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplanrecordMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddto),'eam-EMPLANRECORD-Create')")
    @ApiOperation(value = "根据班组计划模板计划建立触发记录", tags = {"触发记录" },  notes = "根据班组计划模板计划建立触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords")
    public ResponseEntity<EMPLANRECORDDTO> createByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
        domain.setEmplanid(emplan_id);
		emplanrecordService.create(domain);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddtos),'eam-EMPLANRECORD-Create')")
    @ApiOperation(value = "根据班组计划模板计划批量建立触发记录", tags = {"触发记录" },  notes = "根据班组计划模板计划批量建立触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/batch")
    public ResponseEntity<Boolean> createBatchByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        List<EMPLANRECORD> domainlist=emplanrecordMapping.toDomain(emplanrecorddtos);
        for(EMPLANRECORD domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        emplanrecordService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplanrecord" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplanrecordService.get(#emplanrecord_id),'eam-EMPLANRECORD-Update')")
    @ApiOperation(value = "根据班组计划模板计划更新触发记录", tags = {"触发记录" },  notes = "根据班组计划模板计划更新触发记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<EMPLANRECORDDTO> updateByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplanrecord_id") String emplanrecord_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
        domain.setEmplanid(emplan_id);
        domain.setEmplanrecordid(emplanrecord_id);
		emplanrecordService.update(domain);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.getEmplanrecordByEntities(this.emplanrecordMapping.toDomain(#emplanrecorddtos)),'eam-EMPLANRECORD-Update')")
    @ApiOperation(value = "根据班组计划模板计划批量更新触发记录", tags = {"触发记录" },  notes = "根据班组计划模板计划批量更新触发记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        List<EMPLANRECORD> domainlist=emplanrecordMapping.toDomain(emplanrecorddtos);
        for(EMPLANRECORD domain:domainlist){
            domain.setEmplanid(emplan_id);
        }
        emplanrecordService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.get(#emplanrecord_id),'eam-EMPLANRECORD-Remove')")
    @ApiOperation(value = "根据班组计划模板计划删除触发记录", tags = {"触发记录" },  notes = "根据班组计划模板计划删除触发记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<Boolean> removeByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplanrecord_id") String emplanrecord_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplanrecordService.remove(emplanrecord_id));
    }

    @PreAuthorize("hasPermission(this.emplanrecordService.getEmplanrecordByIds(#ids),'eam-EMPLANRECORD-Remove')")
    @ApiOperation(value = "根据班组计划模板计划批量删除触发记录", tags = {"触发记录" },  notes = "根据班组计划模板计划批量删除触发记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeamEMPlanTemplEMPlan(@RequestBody List<String> ids) {
        emplanrecordService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplanrecordMapping.toDomain(returnObject.body),'eam-EMPLANRECORD-Get')")
    @ApiOperation(value = "根据班组计划模板计划获取触发记录", tags = {"触发记录" },  notes = "根据班组计划模板计划获取触发记录")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/{emplanrecord_id}")
    public ResponseEntity<EMPLANRECORDDTO> getByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @PathVariable("emplanrecord_id") String emplanrecord_id) {
        EMPLANRECORD domain = emplanrecordService.get(emplanrecord_id);
        EMPLANRECORDDTO dto = emplanrecordMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组计划模板计划获取触发记录草稿", tags = {"触发记录" },  notes = "根据班组计划模板计划获取触发记录草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/getdraft")
    public ResponseEntity<EMPLANRECORDDTO> getDraftByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, EMPLANRECORDDTO dto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(dto);
        domain.setEmplanid(emplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplanrecordMapping.toDto(emplanrecordService.getDraft(domain)));
    }

    @ApiOperation(value = "根据班组计划模板计划检查触发记录", tags = {"触发记录" },  notes = "根据班组计划模板计划检查触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplanrecordService.checkKey(emplanrecordMapping.toDomain(emplanrecorddto)));
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddto),'eam-EMPLANRECORD-Save')")
    @ApiOperation(value = "根据班组计划模板计划保存触发记录", tags = {"触发记录" },  notes = "根据班组计划模板计划保存触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/save")
    public ResponseEntity<EMPLANRECORDDTO> saveByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDDTO emplanrecorddto) {
        EMPLANRECORD domain = emplanrecordMapping.toDomain(emplanrecorddto);
        domain.setEmplanid(emplan_id);
        emplanrecordService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emplanrecordMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emplanrecordMapping.toDomain(#emplanrecorddtos),'eam-EMPLANRECORD-Save')")
    @ApiOperation(value = "根据班组计划模板计划批量保存触发记录", tags = {"触发记录" },  notes = "根据班组计划模板计划批量保存触发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody List<EMPLANRECORDDTO> emplanrecorddtos) {
        List<EMPLANRECORD> domainlist=emplanrecordMapping.toDomain(emplanrecorddtos);
        for(EMPLANRECORD domain:domainlist){
             domain.setEmplanid(emplan_id);
        }
        emplanrecordService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPLANRECORD-searchDefault-all') and hasPermission(#context,'eam-EMPLANRECORD-Get')")
	@ApiOperation(value = "根据班组计划模板计划获取数据集", tags = {"触发记录" } ,notes = "根据班组计划模板计划获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/fetchdefault")
	public ResponseEntity<List<EMPLANRECORDDTO>> fetchEMPLANRECORDDefaultByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id,EMPLANRECORDSearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<EMPLANRECORD> domains = emplanrecordService.searchDefault(context) ;
        List<EMPLANRECORDDTO> list = emplanrecordMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPLANRECORD-searchDefault-all') and hasPermission(#context,'eam-EMPLANRECORD-Get')")
	@ApiOperation(value = "根据班组计划模板计划查询数据集", tags = {"触发记录" } ,notes = "根据班组计划模板计划查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}/emplans/{emplan_id}/emplanrecords/searchdefault")
	public ResponseEntity<Page<EMPLANRECORDDTO>> searchEMPLANRECORDDefaultByPFTeamEMPlanTemplEMPlan(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @PathVariable("emplan_id") String emplan_id, @RequestBody EMPLANRECORDSearchContext context) {
        context.setN_emplanid_eq(emplan_id);
        Page<EMPLANRECORD> domains = emplanrecordService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplanrecordMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

