package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 服务DTO对象[PLANSCHEDULE_ODTO]
 */
@Data
@ApiModel("自定义间隔天数")
public class PLANSCHEDULE_ODTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [PLANSCHEDULE_OID]
     *
     */
    @JSONField(name = "planschedule_oid")
    @JsonProperty("planschedule_oid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("自定义间隔天数标识")
    private String planscheduleOid;

    /**
     * 属性 [PLANSCHEDULE_ONAME]
     *
     */
    @JSONField(name = "planschedule_oname")
    @JsonProperty("planschedule_oname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("自定义间隔天数名称")
    private String planscheduleOname;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    @ApiModelProperty("更新人")
    private String updateman;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    @ApiModelProperty("建立人")
    private String createman;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;

    /**
     * 属性 [INTERVALMINUTE]
     *
     */
    @JSONField(name = "intervalminute")
    @JsonProperty("intervalminute")
    @ApiModelProperty("间隔时间")
    private Integer intervalminute;

    /**
     * 属性 [EMPLANID]
     *
     */
    @JSONField(name = "emplanid")
    @JsonProperty("emplanid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("计划编号")
    private String emplanid;

    /**
     * 属性 [SCHEDULEPARAM]
     *
     */
    @JSONField(name = "scheduleparam")
    @JsonProperty("scheduleparam")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("时刻参数")
    private String scheduleparam;

    /**
     * 属性 [CYCLESTARTTIME]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "cyclestarttime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("cyclestarttime")
    @ApiModelProperty("循环开始时间")
    private Timestamp cyclestarttime;

    /**
     * 属性 [SCHEDULETYPE]
     *
     */
    @JSONField(name = "scheduletype")
    @JsonProperty("scheduletype")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    @ApiModelProperty("时刻类型")
    private String scheduletype;

    /**
     * 属性 [CYCLEENDTIME]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "cycleendtime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("cycleendtime")
    @ApiModelProperty("循环结束时间")
    private Timestamp cycleendtime;

    /**
     * 属性 [EMPLANNAME]
     *
     */
    @JSONField(name = "emplanname")
    @JsonProperty("emplanname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("计划名称")
    private String emplanname;

    /**
     * 属性 [SCHEDULESTATE]
     *
     */
    @JSONField(name = "schedulestate")
    @JsonProperty("schedulestate")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    @ApiModelProperty("时刻设置状态")
    private String schedulestate;

    /**
     * 属性 [LASTMINUTE]
     *
     */
    @JSONField(name = "lastminute")
    @JsonProperty("lastminute")
    @ApiModelProperty("持续时间")
    private Integer lastminute;

    /**
     * 属性 [SCHEDULEPARAM4]
     *
     */
    @JSONField(name = "scheduleparam4")
    @JsonProperty("scheduleparam4")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("时刻参数")
    private String scheduleparam4;

    /**
     * 属性 [SCHEDULEPARAM2]
     *
     */
    @JSONField(name = "scheduleparam2")
    @JsonProperty("scheduleparam2")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("时刻参数")
    private String scheduleparam2;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    @ApiModelProperty("描述")
    private String description;

    /**
     * 属性 [RUNTIME]
     *
     */
    @JsonFormat(pattern="HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "runtime" , format="HH:mm:ss")
    @JsonProperty("runtime")
    @ApiModelProperty("执行时间")
    private Timestamp runtime;

    /**
     * 属性 [RUNDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "rundate" , format="yyyy-MM-dd")
    @JsonProperty("rundate")
    @ApiModelProperty("运行日期")
    private Timestamp rundate;

    /**
     * 属性 [SCHEDULEPARAM3]
     *
     */
    @JSONField(name = "scheduleparam3")
    @JsonProperty("scheduleparam3")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("时刻参数")
    private String scheduleparam3;

    /**
     * 属性 [TASKID]
     *
     */
    @JSONField(name = "taskid")
    @JsonProperty("taskid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("定时任务")
    private String taskid;


    /**
     * 设置 [PLANSCHEDULE_ONAME]
     */
    public void setPlanscheduleOname(String  planscheduleOname){
        this.planscheduleOname = planscheduleOname ;
        this.modify("planschedule_oname",planscheduleOname);
    }


}


