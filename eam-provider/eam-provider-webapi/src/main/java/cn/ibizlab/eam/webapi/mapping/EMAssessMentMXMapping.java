package cn.ibizlab.eam.webapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMAssessMentMX;
import cn.ibizlab.eam.webapi.dto.EMAssessMentMXDTO;
import cn.ibizlab.eam.util.domain.MappingBase;

@Mapper(componentModel = "spring", uses = {}, implementationName = "WebApiEMAssessMentMXMapping",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface EMAssessMentMXMapping extends MappingBase<EMAssessMentMXDTO, EMAssessMentMX> {


}

