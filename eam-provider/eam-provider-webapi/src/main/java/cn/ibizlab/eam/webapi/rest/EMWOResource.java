package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMWO;
import cn.ibizlab.eam.core.eam_core.service.IEMWOService;
import cn.ibizlab.eam.core.eam_core.filter.EMWOSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"工单" })
@RestController("WebApi-emwo")
@RequestMapping("")
public class EMWOResource {

    @Autowired
    public IEMWOService emwoService;

    @Autowired
    @Lazy
    public EMWOMapping emwoMapping;

    @PreAuthorize("hasPermission(this.emwoMapping.toDomain(#emwodto),'eam-EMWO-Create')")
    @ApiOperation(value = "新建工单", tags = {"工单" },  notes = "新建工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwos")
    public ResponseEntity<EMWODTO> create(@Validated @RequestBody EMWODTO emwodto) {
        EMWO domain = emwoMapping.toDomain(emwodto);
		emwoService.create(domain);
        EMWODTO dto = emwoMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwoMapping.toDomain(#emwodtos),'eam-EMWO-Create')")
    @ApiOperation(value = "批量新建工单", tags = {"工单" },  notes = "批量新建工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwos/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMWODTO> emwodtos) {
        emwoService.createBatch(emwoMapping.toDomain(emwodtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwo" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwoService.get(#emwo_id),'eam-EMWO-Update')")
    @ApiOperation(value = "更新工单", tags = {"工单" },  notes = "更新工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emwos/{emwo_id}")
    public ResponseEntity<EMWODTO> update(@PathVariable("emwo_id") String emwo_id, @RequestBody EMWODTO emwodto) {
		EMWO domain  = emwoMapping.toDomain(emwodto);
        domain .setEmwoid(emwo_id);
		emwoService.update(domain );
		EMWODTO dto = emwoMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwoService.getEmwoByEntities(this.emwoMapping.toDomain(#emwodtos)),'eam-EMWO-Update')")
    @ApiOperation(value = "批量更新工单", tags = {"工单" },  notes = "批量更新工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emwos/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMWODTO> emwodtos) {
        emwoService.updateBatch(emwoMapping.toDomain(emwodtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwoService.get(#emwo_id),'eam-EMWO-Remove')")
    @ApiOperation(value = "删除工单", tags = {"工单" },  notes = "删除工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emwos/{emwo_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emwo_id") String emwo_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emwoService.remove(emwo_id));
    }

    @PreAuthorize("hasPermission(this.emwoService.getEmwoByIds(#ids),'eam-EMWO-Remove')")
    @ApiOperation(value = "批量删除工单", tags = {"工单" },  notes = "批量删除工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emwos/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emwoService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwoMapping.toDomain(returnObject.body),'eam-EMWO-Get')")
    @ApiOperation(value = "获取工单", tags = {"工单" },  notes = "获取工单")
	@RequestMapping(method = RequestMethod.GET, value = "/emwos/{emwo_id}")
    public ResponseEntity<EMWODTO> get(@PathVariable("emwo_id") String emwo_id) {
        EMWO domain = emwoService.get(emwo_id);
        EMWODTO dto = emwoMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取工单草稿", tags = {"工单" },  notes = "获取工单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emwos/getdraft")
    public ResponseEntity<EMWODTO> getDraft(EMWODTO dto) {
        EMWO domain = emwoMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emwoMapping.toDto(emwoService.getDraft(domain)));
    }

    @ApiOperation(value = "检查工单", tags = {"工单" },  notes = "检查工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwos/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMWODTO emwodto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwoService.checkKey(emwoMapping.toDomain(emwodto)));
    }

    @PreAuthorize("hasPermission(this.emwoMapping.toDomain(#emwodto),'eam-EMWO-Save')")
    @ApiOperation(value = "保存工单", tags = {"工单" },  notes = "保存工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwos/save")
    public ResponseEntity<EMWODTO> save(@RequestBody EMWODTO emwodto) {
        EMWO domain = emwoMapping.toDomain(emwodto);
        emwoService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwoMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emwoMapping.toDomain(#emwodtos),'eam-EMWO-Save')")
    @ApiOperation(value = "批量保存工单", tags = {"工单" },  notes = "批量保存工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwos/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMWODTO> emwodtos) {
        emwoService.saveBatch(emwoMapping.toDomain(emwodtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO-searchDefault-all') and hasPermission(#context,'eam-EMWO-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"工单" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emwos/fetchdefault")
	public ResponseEntity<List<EMWODTO>> fetchDefault(EMWOSearchContext context) {
        Page<EMWO> domains = emwoService.searchDefault(context) ;
        List<EMWODTO> list = emwoMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO-searchDefault-all') and hasPermission(#context,'eam-EMWO-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"工单" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emwos/searchdefault")
	public ResponseEntity<Page<EMWODTO>> searchDefault(@RequestBody EMWOSearchContext context) {
        Page<EMWO> domains = emwoService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwoMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

	@ApiOperation(value = "获取设备年度工单数量", tags = {"工单" } ,notes = "获取设备年度工单数量")
    @RequestMapping(method= RequestMethod.GET , value="/emwos/fetcheqyearwo")
	public ResponseEntity<List<Map>> fetchEQYearWO(EMWOSearchContext context) {
        Page<Map> domains = emwoService.searchEQYearWO(context) ;
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "查询设备年度工单数量", tags = {"工单" } ,notes = "查询设备年度工单数量")
    @RequestMapping(method= RequestMethod.POST , value="/emwos/searcheqyearwo")
	public ResponseEntity<Page<Map>> searchEQYearWO(@RequestBody EMWOSearchContext context) {
        Page<Map> domains = emwoService.searchEQYearWO(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}

	@ApiOperation(value = "获取近三年工单", tags = {"工单" } ,notes = "获取近三年工单")
    @RequestMapping(method= RequestMethod.GET , value="/emwos/fetchlaterthreeyear")
	public ResponseEntity<List<Map>> fetchLaterThreeYear(EMWOSearchContext context) {
        Page<Map> domains = emwoService.searchLaterThreeYear(context) ;
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "查询近三年工单", tags = {"工单" } ,notes = "查询近三年工单")
    @RequestMapping(method= RequestMethod.POST , value="/emwos/searchlaterthreeyear")
	public ResponseEntity<Page<Map>> searchLaterThreeYear(@RequestBody EMWOSearchContext context) {
        Page<Map> domains = emwoService.searchLaterThreeYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}

	@ApiOperation(value = "获取工单种类数量", tags = {"工单" } ,notes = "获取工单种类数量")
    @RequestMapping(method= RequestMethod.GET , value="/emwos/fetchwotypenum")
	public ResponseEntity<List<Map>> fetchWoTypeNum(EMWOSearchContext context) {
        Page<Map> domains = emwoService.searchWoTypeNum(context) ;
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "查询工单种类数量", tags = {"工单" } ,notes = "查询工单种类数量")
    @RequestMapping(method= RequestMethod.POST , value="/emwos/searchwotypenum")
	public ResponseEntity<Page<Map>> searchWoTypeNum(@RequestBody EMWOSearchContext context) {
        Page<Map> domains = emwoService.searchWoTypeNum(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}

	@ApiOperation(value = "获取年度计划生成工单数量", tags = {"工单" } ,notes = "获取年度计划生成工单数量")
    @RequestMapping(method= RequestMethod.GET , value="/emwos/fetchyearwonumbyplan")
	public ResponseEntity<List<Map>> fetchYearWONumByPlan(EMWOSearchContext context) {
        Page<Map> domains = emwoService.searchYearWONumByPlan(context) ;
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "查询年度计划生成工单数量", tags = {"工单" } ,notes = "查询年度计划生成工单数量")
    @RequestMapping(method= RequestMethod.POST , value="/emwos/searchyearwonumbyplan")
	public ResponseEntity<Page<Map>> searchYearWONumByPlan(@RequestBody EMWOSearchContext context) {
        Page<Map> domains = emwoService.searchYearWONumByPlan(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emwoMapping.toDomain(#emwodto),'eam-EMWO-Create')")
    @ApiOperation(value = "根据设备档案建立工单", tags = {"工单" },  notes = "根据设备档案建立工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwos")
    public ResponseEntity<EMWODTO> createByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWODTO emwodto) {
        EMWO domain = emwoMapping.toDomain(emwodto);
        domain.setEquipid(emequip_id);
		emwoService.create(domain);
        EMWODTO dto = emwoMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwoMapping.toDomain(#emwodtos),'eam-EMWO-Create')")
    @ApiOperation(value = "根据设备档案批量建立工单", tags = {"工单" },  notes = "根据设备档案批量建立工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwos/batch")
    public ResponseEntity<Boolean> createBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWODTO> emwodtos) {
        List<EMWO> domainlist=emwoMapping.toDomain(emwodtos);
        for(EMWO domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emwoService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwo" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwoService.get(#emwo_id),'eam-EMWO-Update')")
    @ApiOperation(value = "根据设备档案更新工单", tags = {"工单" },  notes = "根据设备档案更新工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emwos/{emwo_id}")
    public ResponseEntity<EMWODTO> updateByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_id") String emwo_id, @RequestBody EMWODTO emwodto) {
        EMWO domain = emwoMapping.toDomain(emwodto);
        domain.setEquipid(emequip_id);
        domain.setEmwoid(emwo_id);
		emwoService.update(domain);
        EMWODTO dto = emwoMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwoService.getEmwoByEntities(this.emwoMapping.toDomain(#emwodtos)),'eam-EMWO-Update')")
    @ApiOperation(value = "根据设备档案批量更新工单", tags = {"工单" },  notes = "根据设备档案批量更新工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emwos/batch")
    public ResponseEntity<Boolean> updateBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWODTO> emwodtos) {
        List<EMWO> domainlist=emwoMapping.toDomain(emwodtos);
        for(EMWO domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emwoService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwoService.get(#emwo_id),'eam-EMWO-Remove')")
    @ApiOperation(value = "根据设备档案删除工单", tags = {"工单" },  notes = "根据设备档案删除工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emwos/{emwo_id}")
    public ResponseEntity<Boolean> removeByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_id") String emwo_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emwoService.remove(emwo_id));
    }

    @PreAuthorize("hasPermission(this.emwoService.getEmwoByIds(#ids),'eam-EMWO-Remove')")
    @ApiOperation(value = "根据设备档案批量删除工单", tags = {"工单" },  notes = "根据设备档案批量删除工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emwos/batch")
    public ResponseEntity<Boolean> removeBatchByEMEquip(@RequestBody List<String> ids) {
        emwoService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwoMapping.toDomain(returnObject.body),'eam-EMWO-Get')")
    @ApiOperation(value = "根据设备档案获取工单", tags = {"工单" },  notes = "根据设备档案获取工单")
	@RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emwos/{emwo_id}")
    public ResponseEntity<EMWODTO> getByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_id") String emwo_id) {
        EMWO domain = emwoService.get(emwo_id);
        EMWODTO dto = emwoMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据设备档案获取工单草稿", tags = {"工单" },  notes = "根据设备档案获取工单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emwos/getdraft")
    public ResponseEntity<EMWODTO> getDraftByEMEquip(@PathVariable("emequip_id") String emequip_id, EMWODTO dto) {
        EMWO domain = emwoMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwoMapping.toDto(emwoService.getDraft(domain)));
    }

    @ApiOperation(value = "根据设备档案检查工单", tags = {"工单" },  notes = "根据设备档案检查工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwos/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWODTO emwodto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwoService.checkKey(emwoMapping.toDomain(emwodto)));
    }

    @PreAuthorize("hasPermission(this.emwoMapping.toDomain(#emwodto),'eam-EMWO-Save')")
    @ApiOperation(value = "根据设备档案保存工单", tags = {"工单" },  notes = "根据设备档案保存工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwos/save")
    public ResponseEntity<EMWODTO> saveByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWODTO emwodto) {
        EMWO domain = emwoMapping.toDomain(emwodto);
        domain.setEquipid(emequip_id);
        emwoService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwoMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emwoMapping.toDomain(#emwodtos),'eam-EMWO-Save')")
    @ApiOperation(value = "根据设备档案批量保存工单", tags = {"工单" },  notes = "根据设备档案批量保存工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwos/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWODTO> emwodtos) {
        List<EMWO> domainlist=emwoMapping.toDomain(emwodtos);
        for(EMWO domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emwoService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO-searchDefault-all') and hasPermission(#context,'eam-EMWO-Get')")
	@ApiOperation(value = "根据设备档案获取DEFAULT", tags = {"工单" } ,notes = "根据设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emwos/fetchdefault")
	public ResponseEntity<List<EMWODTO>> fetchEMWODefaultByEMEquip(@PathVariable("emequip_id") String emequip_id,EMWOSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO> domains = emwoService.searchDefault(context) ;
        List<EMWODTO> list = emwoMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO-searchDefault-all') and hasPermission(#context,'eam-EMWO-Get')")
	@ApiOperation(value = "根据设备档案查询DEFAULT", tags = {"工单" } ,notes = "根据设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emwos/searchdefault")
	public ResponseEntity<Page<EMWODTO>> searchEMWODefaultByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWOSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO> domains = emwoService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwoMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据设备档案获取设备年度工单数量", tags = {"工单" } ,notes = "根据设备档案获取设备年度工单数量")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emwos/fetcheqyearwo")
	public ResponseEntity<List<Map>> fetchEMWOEQYearWOByEMEquip(@PathVariable("emequip_id") String emequip_id,EMWOSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<Map> domains = emwoService.searchEQYearWO(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据设备档案查询设备年度工单数量", tags = {"工单" } ,notes = "根据设备档案查询设备年度工单数量")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emwos/searcheqyearwo")
	public ResponseEntity<Page<Map>> searchEMWOEQYearWOByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWOSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<Map> domains = emwoService.searchEQYearWO(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据设备档案获取近三年工单", tags = {"工单" } ,notes = "根据设备档案获取近三年工单")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emwos/fetchlaterthreeyear")
	public ResponseEntity<List<Map>> fetchEMWOLaterThreeYearByEMEquip(@PathVariable("emequip_id") String emequip_id,EMWOSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<Map> domains = emwoService.searchLaterThreeYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据设备档案查询近三年工单", tags = {"工单" } ,notes = "根据设备档案查询近三年工单")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emwos/searchlaterthreeyear")
	public ResponseEntity<Page<Map>> searchEMWOLaterThreeYearByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWOSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<Map> domains = emwoService.searchLaterThreeYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据设备档案获取工单种类数量", tags = {"工单" } ,notes = "根据设备档案获取工单种类数量")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emwos/fetchwotypenum")
	public ResponseEntity<List<Map>> fetchEMWOWoTypeNumByEMEquip(@PathVariable("emequip_id") String emequip_id,EMWOSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<Map> domains = emwoService.searchWoTypeNum(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据设备档案查询工单种类数量", tags = {"工单" } ,notes = "根据设备档案查询工单种类数量")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emwos/searchwotypenum")
	public ResponseEntity<Page<Map>> searchEMWOWoTypeNumByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWOSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<Map> domains = emwoService.searchWoTypeNum(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据设备档案获取年度计划生成工单数量", tags = {"工单" } ,notes = "根据设备档案获取年度计划生成工单数量")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emwos/fetchyearwonumbyplan")
	public ResponseEntity<List<Map>> fetchEMWOYearWONumByPlanByEMEquip(@PathVariable("emequip_id") String emequip_id,EMWOSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<Map> domains = emwoService.searchYearWONumByPlan(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据设备档案查询年度计划生成工单数量", tags = {"工单" } ,notes = "根据设备档案查询年度计划生成工单数量")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emwos/searchyearwonumbyplan")
	public ResponseEntity<Page<Map>> searchEMWOYearWONumByPlanByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWOSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<Map> domains = emwoService.searchYearWONumByPlan(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emwoMapping.toDomain(#emwodto),'eam-EMWO-Create')")
    @ApiOperation(value = "根据班组设备档案建立工单", tags = {"工单" },  notes = "根据班组设备档案建立工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwos")
    public ResponseEntity<EMWODTO> createByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWODTO emwodto) {
        EMWO domain = emwoMapping.toDomain(emwodto);
        domain.setEquipid(emequip_id);
		emwoService.create(domain);
        EMWODTO dto = emwoMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwoMapping.toDomain(#emwodtos),'eam-EMWO-Create')")
    @ApiOperation(value = "根据班组设备档案批量建立工单", tags = {"工单" },  notes = "根据班组设备档案批量建立工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwos/batch")
    public ResponseEntity<Boolean> createBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWODTO> emwodtos) {
        List<EMWO> domainlist=emwoMapping.toDomain(emwodtos);
        for(EMWO domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emwoService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwo" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwoService.get(#emwo_id),'eam-EMWO-Update')")
    @ApiOperation(value = "根据班组设备档案更新工单", tags = {"工单" },  notes = "根据班组设备档案更新工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwos/{emwo_id}")
    public ResponseEntity<EMWODTO> updateByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_id") String emwo_id, @RequestBody EMWODTO emwodto) {
        EMWO domain = emwoMapping.toDomain(emwodto);
        domain.setEquipid(emequip_id);
        domain.setEmwoid(emwo_id);
		emwoService.update(domain);
        EMWODTO dto = emwoMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwoService.getEmwoByEntities(this.emwoMapping.toDomain(#emwodtos)),'eam-EMWO-Update')")
    @ApiOperation(value = "根据班组设备档案批量更新工单", tags = {"工单" },  notes = "根据班组设备档案批量更新工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwos/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWODTO> emwodtos) {
        List<EMWO> domainlist=emwoMapping.toDomain(emwodtos);
        for(EMWO domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emwoService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwoService.get(#emwo_id),'eam-EMWO-Remove')")
    @ApiOperation(value = "根据班组设备档案删除工单", tags = {"工单" },  notes = "根据班组设备档案删除工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwos/{emwo_id}")
    public ResponseEntity<Boolean> removeByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_id") String emwo_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emwoService.remove(emwo_id));
    }

    @PreAuthorize("hasPermission(this.emwoService.getEmwoByIds(#ids),'eam-EMWO-Remove')")
    @ApiOperation(value = "根据班组设备档案批量删除工单", tags = {"工单" },  notes = "根据班组设备档案批量删除工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwos/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeamEMEquip(@RequestBody List<String> ids) {
        emwoService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwoMapping.toDomain(returnObject.body),'eam-EMWO-Get')")
    @ApiOperation(value = "根据班组设备档案获取工单", tags = {"工单" },  notes = "根据班组设备档案获取工单")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwos/{emwo_id}")
    public ResponseEntity<EMWODTO> getByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_id") String emwo_id) {
        EMWO domain = emwoService.get(emwo_id);
        EMWODTO dto = emwoMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组设备档案获取工单草稿", tags = {"工单" },  notes = "根据班组设备档案获取工单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwos/getdraft")
    public ResponseEntity<EMWODTO> getDraftByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, EMWODTO dto) {
        EMWO domain = emwoMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwoMapping.toDto(emwoService.getDraft(domain)));
    }

    @ApiOperation(value = "根据班组设备档案检查工单", tags = {"工单" },  notes = "根据班组设备档案检查工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwos/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWODTO emwodto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwoService.checkKey(emwoMapping.toDomain(emwodto)));
    }

    @PreAuthorize("hasPermission(this.emwoMapping.toDomain(#emwodto),'eam-EMWO-Save')")
    @ApiOperation(value = "根据班组设备档案保存工单", tags = {"工单" },  notes = "根据班组设备档案保存工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwos/save")
    public ResponseEntity<EMWODTO> saveByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWODTO emwodto) {
        EMWO domain = emwoMapping.toDomain(emwodto);
        domain.setEquipid(emequip_id);
        emwoService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwoMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emwoMapping.toDomain(#emwodtos),'eam-EMWO-Save')")
    @ApiOperation(value = "根据班组设备档案批量保存工单", tags = {"工单" },  notes = "根据班组设备档案批量保存工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwos/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWODTO> emwodtos) {
        List<EMWO> domainlist=emwoMapping.toDomain(emwodtos);
        for(EMWO domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emwoService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO-searchDefault-all') and hasPermission(#context,'eam-EMWO-Get')")
	@ApiOperation(value = "根据班组设备档案获取DEFAULT", tags = {"工单" } ,notes = "根据班组设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwos/fetchdefault")
	public ResponseEntity<List<EMWODTO>> fetchEMWODefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMWOSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO> domains = emwoService.searchDefault(context) ;
        List<EMWODTO> list = emwoMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO-searchDefault-all') and hasPermission(#context,'eam-EMWO-Get')")
	@ApiOperation(value = "根据班组设备档案查询DEFAULT", tags = {"工单" } ,notes = "根据班组设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwos/searchdefault")
	public ResponseEntity<Page<EMWODTO>> searchEMWODefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWOSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO> domains = emwoService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwoMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据班组设备档案获取设备年度工单数量", tags = {"工单" } ,notes = "根据班组设备档案获取设备年度工单数量")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwos/fetcheqyearwo")
	public ResponseEntity<List<Map>> fetchEMWOEQYearWOByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMWOSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<Map> domains = emwoService.searchEQYearWO(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据班组设备档案查询设备年度工单数量", tags = {"工单" } ,notes = "根据班组设备档案查询设备年度工单数量")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwos/searcheqyearwo")
	public ResponseEntity<Page<Map>> searchEMWOEQYearWOByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWOSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<Map> domains = emwoService.searchEQYearWO(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据班组设备档案获取近三年工单", tags = {"工单" } ,notes = "根据班组设备档案获取近三年工单")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwos/fetchlaterthreeyear")
	public ResponseEntity<List<Map>> fetchEMWOLaterThreeYearByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMWOSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<Map> domains = emwoService.searchLaterThreeYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据班组设备档案查询近三年工单", tags = {"工单" } ,notes = "根据班组设备档案查询近三年工单")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwos/searchlaterthreeyear")
	public ResponseEntity<Page<Map>> searchEMWOLaterThreeYearByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWOSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<Map> domains = emwoService.searchLaterThreeYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据班组设备档案获取工单种类数量", tags = {"工单" } ,notes = "根据班组设备档案获取工单种类数量")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwos/fetchwotypenum")
	public ResponseEntity<List<Map>> fetchEMWOWoTypeNumByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMWOSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<Map> domains = emwoService.searchWoTypeNum(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据班组设备档案查询工单种类数量", tags = {"工单" } ,notes = "根据班组设备档案查询工单种类数量")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwos/searchwotypenum")
	public ResponseEntity<Page<Map>> searchEMWOWoTypeNumByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWOSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<Map> domains = emwoService.searchWoTypeNum(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据班组设备档案获取年度计划生成工单数量", tags = {"工单" } ,notes = "根据班组设备档案获取年度计划生成工单数量")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwos/fetchyearwonumbyplan")
	public ResponseEntity<List<Map>> fetchEMWOYearWONumByPlanByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMWOSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<Map> domains = emwoService.searchYearWONumByPlan(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据班组设备档案查询年度计划生成工单数量", tags = {"工单" } ,notes = "根据班组设备档案查询年度计划生成工单数量")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwos/searchyearwonumbyplan")
	public ResponseEntity<Page<Map>> searchEMWOYearWONumByPlanByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWOSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<Map> domains = emwoService.searchYearWONumByPlan(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
}

