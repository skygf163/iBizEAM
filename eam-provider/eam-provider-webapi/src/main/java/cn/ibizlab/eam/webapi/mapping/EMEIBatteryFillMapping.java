package cn.ibizlab.eam.webapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEIBatteryFill;
import cn.ibizlab.eam.webapi.dto.EMEIBatteryFillDTO;
import cn.ibizlab.eam.util.domain.MappingBase;

@Mapper(componentModel = "spring", uses = {}, implementationName = "WebApiEMEIBatteryFillMapping",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface EMEIBatteryFillMapping extends MappingBase<EMEIBatteryFillDTO, EMEIBatteryFill> {


}

