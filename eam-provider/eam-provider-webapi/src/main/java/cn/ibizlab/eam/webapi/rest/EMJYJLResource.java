package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMJYJL;
import cn.ibizlab.eam.core.eam_core.service.IEMJYJLService;
import cn.ibizlab.eam.core.eam_core.filter.EMJYJLSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"加油记录" })
@RestController("WebApi-emjyjl")
@RequestMapping("")
public class EMJYJLResource {

    @Autowired
    public IEMJYJLService emjyjlService;

    @Autowired
    @Lazy
    public EMJYJLMapping emjyjlMapping;

    @PreAuthorize("hasPermission(this.emjyjlMapping.toDomain(#emjyjldto),'eam-EMJYJL-Create')")
    @ApiOperation(value = "新建加油记录", tags = {"加油记录" },  notes = "新建加油记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emjyjls")
    public ResponseEntity<EMJYJLDTO> create(@Validated @RequestBody EMJYJLDTO emjyjldto) {
        EMJYJL domain = emjyjlMapping.toDomain(emjyjldto);
		emjyjlService.create(domain);
        EMJYJLDTO dto = emjyjlMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emjyjlMapping.toDomain(#emjyjldtos),'eam-EMJYJL-Create')")
    @ApiOperation(value = "批量新建加油记录", tags = {"加油记录" },  notes = "批量新建加油记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emjyjls/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMJYJLDTO> emjyjldtos) {
        emjyjlService.createBatch(emjyjlMapping.toDomain(emjyjldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emjyjl" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emjyjlService.get(#emjyjl_id),'eam-EMJYJL-Update')")
    @ApiOperation(value = "更新加油记录", tags = {"加油记录" },  notes = "更新加油记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emjyjls/{emjyjl_id}")
    public ResponseEntity<EMJYJLDTO> update(@PathVariable("emjyjl_id") String emjyjl_id, @RequestBody EMJYJLDTO emjyjldto) {
		EMJYJL domain  = emjyjlMapping.toDomain(emjyjldto);
        domain .setEmjyjlid(emjyjl_id);
		emjyjlService.update(domain );
		EMJYJLDTO dto = emjyjlMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emjyjlService.getEmjyjlByEntities(this.emjyjlMapping.toDomain(#emjyjldtos)),'eam-EMJYJL-Update')")
    @ApiOperation(value = "批量更新加油记录", tags = {"加油记录" },  notes = "批量更新加油记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emjyjls/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMJYJLDTO> emjyjldtos) {
        emjyjlService.updateBatch(emjyjlMapping.toDomain(emjyjldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emjyjlService.get(#emjyjl_id),'eam-EMJYJL-Remove')")
    @ApiOperation(value = "删除加油记录", tags = {"加油记录" },  notes = "删除加油记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emjyjls/{emjyjl_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emjyjl_id") String emjyjl_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emjyjlService.remove(emjyjl_id));
    }

    @PreAuthorize("hasPermission(this.emjyjlService.getEmjyjlByIds(#ids),'eam-EMJYJL-Remove')")
    @ApiOperation(value = "批量删除加油记录", tags = {"加油记录" },  notes = "批量删除加油记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emjyjls/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emjyjlService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emjyjlMapping.toDomain(returnObject.body),'eam-EMJYJL-Get')")
    @ApiOperation(value = "获取加油记录", tags = {"加油记录" },  notes = "获取加油记录")
	@RequestMapping(method = RequestMethod.GET, value = "/emjyjls/{emjyjl_id}")
    public ResponseEntity<EMJYJLDTO> get(@PathVariable("emjyjl_id") String emjyjl_id) {
        EMJYJL domain = emjyjlService.get(emjyjl_id);
        EMJYJLDTO dto = emjyjlMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取加油记录草稿", tags = {"加油记录" },  notes = "获取加油记录草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emjyjls/getdraft")
    public ResponseEntity<EMJYJLDTO> getDraft(EMJYJLDTO dto) {
        EMJYJL domain = emjyjlMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emjyjlMapping.toDto(emjyjlService.getDraft(domain)));
    }

    @ApiOperation(value = "检查加油记录", tags = {"加油记录" },  notes = "检查加油记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emjyjls/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMJYJLDTO emjyjldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emjyjlService.checkKey(emjyjlMapping.toDomain(emjyjldto)));
    }

    @PreAuthorize("hasPermission(this.emjyjlMapping.toDomain(#emjyjldto),'eam-EMJYJL-Save')")
    @ApiOperation(value = "保存加油记录", tags = {"加油记录" },  notes = "保存加油记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emjyjls/save")
    public ResponseEntity<EMJYJLDTO> save(@RequestBody EMJYJLDTO emjyjldto) {
        EMJYJL domain = emjyjlMapping.toDomain(emjyjldto);
        emjyjlService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emjyjlMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emjyjlMapping.toDomain(#emjyjldtos),'eam-EMJYJL-Save')")
    @ApiOperation(value = "批量保存加油记录", tags = {"加油记录" },  notes = "批量保存加油记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emjyjls/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMJYJLDTO> emjyjldtos) {
        emjyjlService.saveBatch(emjyjlMapping.toDomain(emjyjldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMJYJL-searchDefault-all') and hasPermission(#context,'eam-EMJYJL-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"加油记录" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emjyjls/fetchdefault")
	public ResponseEntity<List<EMJYJLDTO>> fetchDefault(EMJYJLSearchContext context) {
        Page<EMJYJL> domains = emjyjlService.searchDefault(context) ;
        List<EMJYJLDTO> list = emjyjlMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMJYJL-searchDefault-all') and hasPermission(#context,'eam-EMJYJL-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"加油记录" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emjyjls/searchdefault")
	public ResponseEntity<Page<EMJYJLDTO>> searchDefault(@RequestBody EMJYJLSearchContext context) {
        Page<EMJYJL> domains = emjyjlService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emjyjlMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

