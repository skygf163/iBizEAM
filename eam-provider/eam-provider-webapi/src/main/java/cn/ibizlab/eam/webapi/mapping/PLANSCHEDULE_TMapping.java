package cn.ibizlab.eam.webapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE_T;
import cn.ibizlab.eam.webapi.dto.PLANSCHEDULE_TDTO;
import cn.ibizlab.eam.util.domain.MappingBase;

@Mapper(componentModel = "spring", uses = {}, implementationName = "WebApiPLANSCHEDULE_TMapping",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface PLANSCHEDULE_TMapping extends MappingBase<PLANSCHEDULE_TDTO, PLANSCHEDULE_T> {


}

