package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 服务DTO对象[EMMonthlyDTO]
 */
@Data
@ApiModel("维修中心月度计划")
public class EMMonthlyDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;

    /**
     * 属性 [EMMONTHLYID]
     *
     */
    @JSONField(name = "emmonthlyid")
    @JsonProperty("emmonthlyid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("编号")
    private String emmonthlyid;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;

    /**
     * 属性 [SAFEWORK]
     *
     */
    @JSONField(name = "safework")
    @JsonProperty("safework")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    @ApiModelProperty("安全工作")
    private String safework;

    /**
     * 属性 [LASTMONTH2]
     *
     */
    @JSONField(name = "lastmonth2")
    @JsonProperty("lastmonth2")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    @ApiModelProperty("上个月设备例会布置任务完成情况")
    private String lastmonth2;

    /**
     * 属性 [EMMONTHLYNAME]
     *
     */
    @JSONField(name = "emmonthlyname")
    @JsonProperty("emmonthlyname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("报告名称")
    private String emmonthlyname;

    /**
     * 属性 [LASTMONTH]
     *
     */
    @JSONField(name = "lastmonth")
    @JsonProperty("lastmonth")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    @ApiModelProperty("上月份其他主要应急维修及临时任务完成情况")
    private String lastmonth;

    /**
     * 属性 [THISMONTH]
     *
     */
    @JSONField(name = "thismonth")
    @JsonProperty("thismonth")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    @ApiModelProperty("本月份计划维修工作")
    private String thismonth;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    @ApiModelProperty("更新人")
    private String updateman;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    @ApiModelProperty("建立人")
    private String createman;


    /**
     * 设置 [SAFEWORK]
     */
    public void setSafework(String  safework){
        this.safework = safework ;
        this.modify("safework",safework);
    }

    /**
     * 设置 [LASTMONTH2]
     */
    public void setLastmonth2(String  lastmonth2){
        this.lastmonth2 = lastmonth2 ;
        this.modify("lastmonth2",lastmonth2);
    }

    /**
     * 设置 [EMMONTHLYNAME]
     */
    public void setEmmonthlyname(String  emmonthlyname){
        this.emmonthlyname = emmonthlyname ;
        this.modify("emmonthlyname",emmonthlyname);
    }

    /**
     * 设置 [LASTMONTH]
     */
    public void setLastmonth(String  lastmonth){
        this.lastmonth = lastmonth ;
        this.modify("lastmonth",lastmonth);
    }

    /**
     * 设置 [THISMONTH]
     */
    public void setThismonth(String  thismonth){
        this.thismonth = thismonth ;
        this.modify("thismonth",thismonth);
    }


}


