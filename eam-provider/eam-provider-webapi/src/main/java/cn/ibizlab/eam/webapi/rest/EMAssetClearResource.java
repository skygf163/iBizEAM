package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMAssetClear;
import cn.ibizlab.eam.core.eam_core.service.IEMAssetClearService;
import cn.ibizlab.eam.core.eam_core.filter.EMAssetClearSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"资产清盘记录" })
@RestController("WebApi-emassetclear")
@RequestMapping("")
public class EMAssetClearResource {

    @Autowired
    public IEMAssetClearService emassetclearService;

    @Autowired
    @Lazy
    public EMAssetClearMapping emassetclearMapping;

    @PreAuthorize("hasPermission(this.emassetclearMapping.toDomain(#emassetcleardto),'eam-EMAssetClear-Create')")
    @ApiOperation(value = "新建资产清盘记录", tags = {"资产清盘记录" },  notes = "新建资产清盘记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emassetclears")
    public ResponseEntity<EMAssetClearDTO> create(@Validated @RequestBody EMAssetClearDTO emassetcleardto) {
        EMAssetClear domain = emassetclearMapping.toDomain(emassetcleardto);
		emassetclearService.create(domain);
        EMAssetClearDTO dto = emassetclearMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emassetclearMapping.toDomain(#emassetcleardtos),'eam-EMAssetClear-Create')")
    @ApiOperation(value = "批量新建资产清盘记录", tags = {"资产清盘记录" },  notes = "批量新建资产清盘记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emassetclears/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMAssetClearDTO> emassetcleardtos) {
        emassetclearService.createBatch(emassetclearMapping.toDomain(emassetcleardtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emassetclear" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emassetclearService.get(#emassetclear_id),'eam-EMAssetClear-Update')")
    @ApiOperation(value = "更新资产清盘记录", tags = {"资产清盘记录" },  notes = "更新资产清盘记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emassetclears/{emassetclear_id}")
    public ResponseEntity<EMAssetClearDTO> update(@PathVariable("emassetclear_id") String emassetclear_id, @RequestBody EMAssetClearDTO emassetcleardto) {
		EMAssetClear domain  = emassetclearMapping.toDomain(emassetcleardto);
        domain .setEmassetclearid(emassetclear_id);
		emassetclearService.update(domain );
		EMAssetClearDTO dto = emassetclearMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emassetclearService.getEmassetclearByEntities(this.emassetclearMapping.toDomain(#emassetcleardtos)),'eam-EMAssetClear-Update')")
    @ApiOperation(value = "批量更新资产清盘记录", tags = {"资产清盘记录" },  notes = "批量更新资产清盘记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emassetclears/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMAssetClearDTO> emassetcleardtos) {
        emassetclearService.updateBatch(emassetclearMapping.toDomain(emassetcleardtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emassetclearService.get(#emassetclear_id),'eam-EMAssetClear-Remove')")
    @ApiOperation(value = "删除资产清盘记录", tags = {"资产清盘记录" },  notes = "删除资产清盘记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emassetclears/{emassetclear_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emassetclear_id") String emassetclear_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emassetclearService.remove(emassetclear_id));
    }

    @PreAuthorize("hasPermission(this.emassetclearService.getEmassetclearByIds(#ids),'eam-EMAssetClear-Remove')")
    @ApiOperation(value = "批量删除资产清盘记录", tags = {"资产清盘记录" },  notes = "批量删除资产清盘记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emassetclears/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emassetclearService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emassetclearMapping.toDomain(returnObject.body),'eam-EMAssetClear-Get')")
    @ApiOperation(value = "获取资产清盘记录", tags = {"资产清盘记录" },  notes = "获取资产清盘记录")
	@RequestMapping(method = RequestMethod.GET, value = "/emassetclears/{emassetclear_id}")
    public ResponseEntity<EMAssetClearDTO> get(@PathVariable("emassetclear_id") String emassetclear_id) {
        EMAssetClear domain = emassetclearService.get(emassetclear_id);
        EMAssetClearDTO dto = emassetclearMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取资产清盘记录草稿", tags = {"资产清盘记录" },  notes = "获取资产清盘记录草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emassetclears/getdraft")
    public ResponseEntity<EMAssetClearDTO> getDraft(EMAssetClearDTO dto) {
        EMAssetClear domain = emassetclearMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emassetclearMapping.toDto(emassetclearService.getDraft(domain)));
    }

    @ApiOperation(value = "检查资产清盘记录", tags = {"资产清盘记录" },  notes = "检查资产清盘记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emassetclears/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMAssetClearDTO emassetcleardto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emassetclearService.checkKey(emassetclearMapping.toDomain(emassetcleardto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMAssetClear-Clear-all')")
    @ApiOperation(value = "资产盘点", tags = {"资产清盘记录" },  notes = "资产盘点")
	@RequestMapping(method = RequestMethod.POST, value = "/emassetclears/{emassetclear_id}/clear")
    public ResponseEntity<EMAssetClearDTO> clear(@PathVariable("emassetclear_id") String emassetclear_id, @RequestBody EMAssetClearDTO emassetcleardto) {
        EMAssetClear domain = emassetclearMapping.toDomain(emassetcleardto);
        domain.setEmassetclearid(emassetclear_id);
        domain = emassetclearService.clear(domain);
        emassetcleardto = emassetclearMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emassetcleardto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMAssetClear-Clear-all')")
    @ApiOperation(value = "批量处理[资产盘点]", tags = {"资产清盘记录" },  notes = "批量处理[资产盘点]")
	@RequestMapping(method = RequestMethod.POST, value = "/emassetclears/clearbatch")
    public ResponseEntity<Boolean> clearBatch(@RequestBody List<EMAssetClearDTO> emassetcleardtos) {
        List<EMAssetClear> domains = emassetclearMapping.toDomain(emassetcleardtos);
        boolean result = emassetclearService.clearBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    @PreAuthorize("hasPermission(this.emassetclearMapping.toDomain(#emassetcleardto),'eam-EMAssetClear-Save')")
    @ApiOperation(value = "保存资产清盘记录", tags = {"资产清盘记录" },  notes = "保存资产清盘记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emassetclears/save")
    public ResponseEntity<EMAssetClearDTO> save(@RequestBody EMAssetClearDTO emassetcleardto) {
        EMAssetClear domain = emassetclearMapping.toDomain(emassetcleardto);
        emassetclearService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emassetclearMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emassetclearMapping.toDomain(#emassetcleardtos),'eam-EMAssetClear-Save')")
    @ApiOperation(value = "批量保存资产清盘记录", tags = {"资产清盘记录" },  notes = "批量保存资产清盘记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emassetclears/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMAssetClearDTO> emassetcleardtos) {
        emassetclearService.saveBatch(emassetclearMapping.toDomain(emassetcleardtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMAssetClear-searchDefault-all') and hasPermission(#context,'eam-EMAssetClear-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"资产清盘记录" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emassetclears/fetchdefault")
	public ResponseEntity<List<EMAssetClearDTO>> fetchDefault(EMAssetClearSearchContext context) {
        Page<EMAssetClear> domains = emassetclearService.searchDefault(context) ;
        List<EMAssetClearDTO> list = emassetclearMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMAssetClear-searchDefault-all') and hasPermission(#context,'eam-EMAssetClear-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"资产清盘记录" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emassetclears/searchdefault")
	public ResponseEntity<Page<EMAssetClearDTO>> searchDefault(@RequestBody EMAssetClearSearchContext context) {
        Page<EMAssetClear> domains = emassetclearService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emassetclearMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emassetclearMapping.toDomain(#emassetcleardto),'eam-EMAssetClear-Create')")
    @ApiOperation(value = "根据资产建立资产清盘记录", tags = {"资产清盘记录" },  notes = "根据资产建立资产清盘记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emassets/{emasset_id}/emassetclears")
    public ResponseEntity<EMAssetClearDTO> createByEMAsset(@PathVariable("emasset_id") String emasset_id, @RequestBody EMAssetClearDTO emassetcleardto) {
        EMAssetClear domain = emassetclearMapping.toDomain(emassetcleardto);
        domain.setEmassetid(emasset_id);
		emassetclearService.create(domain);
        EMAssetClearDTO dto = emassetclearMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emassetclearMapping.toDomain(#emassetcleardtos),'eam-EMAssetClear-Create')")
    @ApiOperation(value = "根据资产批量建立资产清盘记录", tags = {"资产清盘记录" },  notes = "根据资产批量建立资产清盘记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emassets/{emasset_id}/emassetclears/batch")
    public ResponseEntity<Boolean> createBatchByEMAsset(@PathVariable("emasset_id") String emasset_id, @RequestBody List<EMAssetClearDTO> emassetcleardtos) {
        List<EMAssetClear> domainlist=emassetclearMapping.toDomain(emassetcleardtos);
        for(EMAssetClear domain:domainlist){
            domain.setEmassetid(emasset_id);
        }
        emassetclearService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emassetclear" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emassetclearService.get(#emassetclear_id),'eam-EMAssetClear-Update')")
    @ApiOperation(value = "根据资产更新资产清盘记录", tags = {"资产清盘记录" },  notes = "根据资产更新资产清盘记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emassets/{emasset_id}/emassetclears/{emassetclear_id}")
    public ResponseEntity<EMAssetClearDTO> updateByEMAsset(@PathVariable("emasset_id") String emasset_id, @PathVariable("emassetclear_id") String emassetclear_id, @RequestBody EMAssetClearDTO emassetcleardto) {
        EMAssetClear domain = emassetclearMapping.toDomain(emassetcleardto);
        domain.setEmassetid(emasset_id);
        domain.setEmassetclearid(emassetclear_id);
		emassetclearService.update(domain);
        EMAssetClearDTO dto = emassetclearMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emassetclearService.getEmassetclearByEntities(this.emassetclearMapping.toDomain(#emassetcleardtos)),'eam-EMAssetClear-Update')")
    @ApiOperation(value = "根据资产批量更新资产清盘记录", tags = {"资产清盘记录" },  notes = "根据资产批量更新资产清盘记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emassets/{emasset_id}/emassetclears/batch")
    public ResponseEntity<Boolean> updateBatchByEMAsset(@PathVariable("emasset_id") String emasset_id, @RequestBody List<EMAssetClearDTO> emassetcleardtos) {
        List<EMAssetClear> domainlist=emassetclearMapping.toDomain(emassetcleardtos);
        for(EMAssetClear domain:domainlist){
            domain.setEmassetid(emasset_id);
        }
        emassetclearService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emassetclearService.get(#emassetclear_id),'eam-EMAssetClear-Remove')")
    @ApiOperation(value = "根据资产删除资产清盘记录", tags = {"资产清盘记录" },  notes = "根据资产删除资产清盘记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emassets/{emasset_id}/emassetclears/{emassetclear_id}")
    public ResponseEntity<Boolean> removeByEMAsset(@PathVariable("emasset_id") String emasset_id, @PathVariable("emassetclear_id") String emassetclear_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emassetclearService.remove(emassetclear_id));
    }

    @PreAuthorize("hasPermission(this.emassetclearService.getEmassetclearByIds(#ids),'eam-EMAssetClear-Remove')")
    @ApiOperation(value = "根据资产批量删除资产清盘记录", tags = {"资产清盘记录" },  notes = "根据资产批量删除资产清盘记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emassets/{emasset_id}/emassetclears/batch")
    public ResponseEntity<Boolean> removeBatchByEMAsset(@RequestBody List<String> ids) {
        emassetclearService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emassetclearMapping.toDomain(returnObject.body),'eam-EMAssetClear-Get')")
    @ApiOperation(value = "根据资产获取资产清盘记录", tags = {"资产清盘记录" },  notes = "根据资产获取资产清盘记录")
	@RequestMapping(method = RequestMethod.GET, value = "/emassets/{emasset_id}/emassetclears/{emassetclear_id}")
    public ResponseEntity<EMAssetClearDTO> getByEMAsset(@PathVariable("emasset_id") String emasset_id, @PathVariable("emassetclear_id") String emassetclear_id) {
        EMAssetClear domain = emassetclearService.get(emassetclear_id);
        EMAssetClearDTO dto = emassetclearMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据资产获取资产清盘记录草稿", tags = {"资产清盘记录" },  notes = "根据资产获取资产清盘记录草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emassets/{emasset_id}/emassetclears/getdraft")
    public ResponseEntity<EMAssetClearDTO> getDraftByEMAsset(@PathVariable("emasset_id") String emasset_id, EMAssetClearDTO dto) {
        EMAssetClear domain = emassetclearMapping.toDomain(dto);
        domain.setEmassetid(emasset_id);
        return ResponseEntity.status(HttpStatus.OK).body(emassetclearMapping.toDto(emassetclearService.getDraft(domain)));
    }

    @ApiOperation(value = "根据资产检查资产清盘记录", tags = {"资产清盘记录" },  notes = "根据资产检查资产清盘记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emassets/{emasset_id}/emassetclears/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMAsset(@PathVariable("emasset_id") String emasset_id, @RequestBody EMAssetClearDTO emassetcleardto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emassetclearService.checkKey(emassetclearMapping.toDomain(emassetcleardto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMAssetClear-Clear-all')")
    @ApiOperation(value = "根据资产资产清盘记录", tags = {"资产清盘记录" },  notes = "根据资产资产清盘记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emassets/{emasset_id}/emassetclears/{emassetclear_id}/clear")
    public ResponseEntity<EMAssetClearDTO> clearByEMAsset(@PathVariable("emasset_id") String emasset_id, @PathVariable("emassetclear_id") String emassetclear_id, @RequestBody EMAssetClearDTO emassetcleardto) {
        EMAssetClear domain = emassetclearMapping.toDomain(emassetcleardto);
        domain.setEmassetid(emasset_id);
        domain = emassetclearService.clear(domain) ;
        emassetcleardto = emassetclearMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emassetcleardto);
    }
    @ApiOperation(value = "批量处理[根据资产资产清盘记录]", tags = {"资产清盘记录" },  notes = "批量处理[根据资产资产清盘记录]")
	@RequestMapping(method = RequestMethod.POST, value = "/emassets/{emasset_id}/emassetclears/clearbatch")
    public ResponseEntity<Boolean> clearByEMAsset(@PathVariable("emasset_id") String emasset_id, @RequestBody List<EMAssetClearDTO> emassetcleardtos) {
        List<EMAssetClear> domains = emassetclearMapping.toDomain(emassetcleardtos);
        boolean result = emassetclearService.clearBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasPermission(this.emassetclearMapping.toDomain(#emassetcleardto),'eam-EMAssetClear-Save')")
    @ApiOperation(value = "根据资产保存资产清盘记录", tags = {"资产清盘记录" },  notes = "根据资产保存资产清盘记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emassets/{emasset_id}/emassetclears/save")
    public ResponseEntity<EMAssetClearDTO> saveByEMAsset(@PathVariable("emasset_id") String emasset_id, @RequestBody EMAssetClearDTO emassetcleardto) {
        EMAssetClear domain = emassetclearMapping.toDomain(emassetcleardto);
        domain.setEmassetid(emasset_id);
        emassetclearService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emassetclearMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emassetclearMapping.toDomain(#emassetcleardtos),'eam-EMAssetClear-Save')")
    @ApiOperation(value = "根据资产批量保存资产清盘记录", tags = {"资产清盘记录" },  notes = "根据资产批量保存资产清盘记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emassets/{emasset_id}/emassetclears/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMAsset(@PathVariable("emasset_id") String emasset_id, @RequestBody List<EMAssetClearDTO> emassetcleardtos) {
        List<EMAssetClear> domainlist=emassetclearMapping.toDomain(emassetcleardtos);
        for(EMAssetClear domain:domainlist){
             domain.setEmassetid(emasset_id);
        }
        emassetclearService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMAssetClear-searchDefault-all') and hasPermission(#context,'eam-EMAssetClear-Get')")
	@ApiOperation(value = "根据资产获取DEFAULT", tags = {"资产清盘记录" } ,notes = "根据资产获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emassets/{emasset_id}/emassetclears/fetchdefault")
	public ResponseEntity<List<EMAssetClearDTO>> fetchEMAssetClearDefaultByEMAsset(@PathVariable("emasset_id") String emasset_id,EMAssetClearSearchContext context) {
        context.setN_emassetid_eq(emasset_id);
        Page<EMAssetClear> domains = emassetclearService.searchDefault(context) ;
        List<EMAssetClearDTO> list = emassetclearMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMAssetClear-searchDefault-all') and hasPermission(#context,'eam-EMAssetClear-Get')")
	@ApiOperation(value = "根据资产查询DEFAULT", tags = {"资产清盘记录" } ,notes = "根据资产查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emassets/{emasset_id}/emassetclears/searchdefault")
	public ResponseEntity<Page<EMAssetClearDTO>> searchEMAssetClearDefaultByEMAsset(@PathVariable("emasset_id") String emasset_id, @RequestBody EMAssetClearSearchContext context) {
        context.setN_emassetid_eq(emasset_id);
        Page<EMAssetClear> domains = emassetclearService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emassetclearMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

