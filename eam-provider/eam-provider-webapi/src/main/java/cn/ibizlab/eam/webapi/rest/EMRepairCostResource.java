package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMRepairCost;
import cn.ibizlab.eam.core.eam_core.service.IEMRepairCostService;
import cn.ibizlab.eam.core.eam_core.filter.EMRepairCostSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"各月维修成本" })
@RestController("WebApi-emrepaircost")
@RequestMapping("")
public class EMRepairCostResource {

    @Autowired
    public IEMRepairCostService emrepaircostService;

    @Autowired
    @Lazy
    public EMRepairCostMapping emrepaircostMapping;

    @PreAuthorize("hasPermission(this.emrepaircostMapping.toDomain(#emrepaircostdto),'eam-EMRepairCost-Create')")
    @ApiOperation(value = "新建各月维修成本", tags = {"各月维修成本" },  notes = "新建各月维修成本")
	@RequestMapping(method = RequestMethod.POST, value = "/emrepaircosts")
    public ResponseEntity<EMRepairCostDTO> create(@Validated @RequestBody EMRepairCostDTO emrepaircostdto) {
        EMRepairCost domain = emrepaircostMapping.toDomain(emrepaircostdto);
		emrepaircostService.create(domain);
        EMRepairCostDTO dto = emrepaircostMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrepaircostMapping.toDomain(#emrepaircostdtos),'eam-EMRepairCost-Create')")
    @ApiOperation(value = "批量新建各月维修成本", tags = {"各月维修成本" },  notes = "批量新建各月维修成本")
	@RequestMapping(method = RequestMethod.POST, value = "/emrepaircosts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMRepairCostDTO> emrepaircostdtos) {
        emrepaircostService.createBatch(emrepaircostMapping.toDomain(emrepaircostdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emrepaircost" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emrepaircostService.get(#emrepaircost_id),'eam-EMRepairCost-Update')")
    @ApiOperation(value = "更新各月维修成本", tags = {"各月维修成本" },  notes = "更新各月维修成本")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrepaircosts/{emrepaircost_id}")
    public ResponseEntity<EMRepairCostDTO> update(@PathVariable("emrepaircost_id") String emrepaircost_id, @RequestBody EMRepairCostDTO emrepaircostdto) {
		EMRepairCost domain  = emrepaircostMapping.toDomain(emrepaircostdto);
        domain .setEmrepaircostid(emrepaircost_id);
		emrepaircostService.update(domain );
		EMRepairCostDTO dto = emrepaircostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrepaircostService.getEmrepaircostByEntities(this.emrepaircostMapping.toDomain(#emrepaircostdtos)),'eam-EMRepairCost-Update')")
    @ApiOperation(value = "批量更新各月维修成本", tags = {"各月维修成本" },  notes = "批量更新各月维修成本")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrepaircosts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMRepairCostDTO> emrepaircostdtos) {
        emrepaircostService.updateBatch(emrepaircostMapping.toDomain(emrepaircostdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emrepaircostService.get(#emrepaircost_id),'eam-EMRepairCost-Remove')")
    @ApiOperation(value = "删除各月维修成本", tags = {"各月维修成本" },  notes = "删除各月维修成本")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrepaircosts/{emrepaircost_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emrepaircost_id") String emrepaircost_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emrepaircostService.remove(emrepaircost_id));
    }

    @PreAuthorize("hasPermission(this.emrepaircostService.getEmrepaircostByIds(#ids),'eam-EMRepairCost-Remove')")
    @ApiOperation(value = "批量删除各月维修成本", tags = {"各月维修成本" },  notes = "批量删除各月维修成本")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrepaircosts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emrepaircostService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emrepaircostMapping.toDomain(returnObject.body),'eam-EMRepairCost-Get')")
    @ApiOperation(value = "获取各月维修成本", tags = {"各月维修成本" },  notes = "获取各月维修成本")
	@RequestMapping(method = RequestMethod.GET, value = "/emrepaircosts/{emrepaircost_id}")
    public ResponseEntity<EMRepairCostDTO> get(@PathVariable("emrepaircost_id") String emrepaircost_id) {
        EMRepairCost domain = emrepaircostService.get(emrepaircost_id);
        EMRepairCostDTO dto = emrepaircostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取各月维修成本草稿", tags = {"各月维修成本" },  notes = "获取各月维修成本草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emrepaircosts/getdraft")
    public ResponseEntity<EMRepairCostDTO> getDraft(EMRepairCostDTO dto) {
        EMRepairCost domain = emrepaircostMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emrepaircostMapping.toDto(emrepaircostService.getDraft(domain)));
    }

    @ApiOperation(value = "检查各月维修成本", tags = {"各月维修成本" },  notes = "检查各月维修成本")
	@RequestMapping(method = RequestMethod.POST, value = "/emrepaircosts/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMRepairCostDTO emrepaircostdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emrepaircostService.checkKey(emrepaircostMapping.toDomain(emrepaircostdto)));
    }

    @PreAuthorize("hasPermission(this.emrepaircostMapping.toDomain(#emrepaircostdto),'eam-EMRepairCost-Save')")
    @ApiOperation(value = "保存各月维修成本", tags = {"各月维修成本" },  notes = "保存各月维修成本")
	@RequestMapping(method = RequestMethod.POST, value = "/emrepaircosts/save")
    public ResponseEntity<EMRepairCostDTO> save(@RequestBody EMRepairCostDTO emrepaircostdto) {
        EMRepairCost domain = emrepaircostMapping.toDomain(emrepaircostdto);
        emrepaircostService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emrepaircostMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emrepaircostMapping.toDomain(#emrepaircostdtos),'eam-EMRepairCost-Save')")
    @ApiOperation(value = "批量保存各月维修成本", tags = {"各月维修成本" },  notes = "批量保存各月维修成本")
	@RequestMapping(method = RequestMethod.POST, value = "/emrepaircosts/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMRepairCostDTO> emrepaircostdtos) {
        emrepaircostService.saveBatch(emrepaircostMapping.toDomain(emrepaircostdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRepairCost-searchDefault-all') and hasPermission(#context,'eam-EMRepairCost-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"各月维修成本" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emrepaircosts/fetchdefault")
	public ResponseEntity<List<EMRepairCostDTO>> fetchDefault(EMRepairCostSearchContext context) {
        Page<EMRepairCost> domains = emrepaircostService.searchDefault(context) ;
        List<EMRepairCostDTO> list = emrepaircostMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRepairCost-searchDefault-all') and hasPermission(#context,'eam-EMRepairCost-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"各月维修成本" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emrepaircosts/searchdefault")
	public ResponseEntity<Page<EMRepairCostDTO>> searchDefault(@RequestBody EMRepairCostSearchContext context) {
        Page<EMRepairCost> domains = emrepaircostService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emrepaircostMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

