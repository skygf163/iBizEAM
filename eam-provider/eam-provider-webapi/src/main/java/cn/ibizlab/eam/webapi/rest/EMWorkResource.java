package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMWork;
import cn.ibizlab.eam.core.eam_core.service.IEMWorkService;
import cn.ibizlab.eam.core.eam_core.filter.EMWorkSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"加班工单" })
@RestController("WebApi-emwork")
@RequestMapping("")
public class EMWorkResource {

    @Autowired
    public IEMWorkService emworkService;

    @Autowired
    @Lazy
    public EMWorkMapping emworkMapping;

    @PreAuthorize("hasPermission(this.emworkMapping.toDomain(#emworkdto),'eam-EMWork-Create')")
    @ApiOperation(value = "新建加班工单", tags = {"加班工单" },  notes = "新建加班工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emworks")
    public ResponseEntity<EMWorkDTO> create(@Validated @RequestBody EMWorkDTO emworkdto) {
        EMWork domain = emworkMapping.toDomain(emworkdto);
		emworkService.create(domain);
        EMWorkDTO dto = emworkMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emworkMapping.toDomain(#emworkdtos),'eam-EMWork-Create')")
    @ApiOperation(value = "批量新建加班工单", tags = {"加班工单" },  notes = "批量新建加班工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emworks/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMWorkDTO> emworkdtos) {
        emworkService.createBatch(emworkMapping.toDomain(emworkdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwork" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emworkService.get(#emwork_id),'eam-EMWork-Update')")
    @ApiOperation(value = "更新加班工单", tags = {"加班工单" },  notes = "更新加班工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emworks/{emwork_id}")
    public ResponseEntity<EMWorkDTO> update(@PathVariable("emwork_id") String emwork_id, @RequestBody EMWorkDTO emworkdto) {
		EMWork domain  = emworkMapping.toDomain(emworkdto);
        domain .setEmworkid(emwork_id);
		emworkService.update(domain );
		EMWorkDTO dto = emworkMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emworkService.getEmworkByEntities(this.emworkMapping.toDomain(#emworkdtos)),'eam-EMWork-Update')")
    @ApiOperation(value = "批量更新加班工单", tags = {"加班工单" },  notes = "批量更新加班工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emworks/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMWorkDTO> emworkdtos) {
        emworkService.updateBatch(emworkMapping.toDomain(emworkdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emworkService.get(#emwork_id),'eam-EMWork-Remove')")
    @ApiOperation(value = "删除加班工单", tags = {"加班工单" },  notes = "删除加班工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emworks/{emwork_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emwork_id") String emwork_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emworkService.remove(emwork_id));
    }

    @PreAuthorize("hasPermission(this.emworkService.getEmworkByIds(#ids),'eam-EMWork-Remove')")
    @ApiOperation(value = "批量删除加班工单", tags = {"加班工单" },  notes = "批量删除加班工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emworks/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emworkService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emworkMapping.toDomain(returnObject.body),'eam-EMWork-Get')")
    @ApiOperation(value = "获取加班工单", tags = {"加班工单" },  notes = "获取加班工单")
	@RequestMapping(method = RequestMethod.GET, value = "/emworks/{emwork_id}")
    public ResponseEntity<EMWorkDTO> get(@PathVariable("emwork_id") String emwork_id) {
        EMWork domain = emworkService.get(emwork_id);
        EMWorkDTO dto = emworkMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取加班工单草稿", tags = {"加班工单" },  notes = "获取加班工单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emworks/getdraft")
    public ResponseEntity<EMWorkDTO> getDraft(EMWorkDTO dto) {
        EMWork domain = emworkMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emworkMapping.toDto(emworkService.getDraft(domain)));
    }

    @ApiOperation(value = "检查加班工单", tags = {"加班工单" },  notes = "检查加班工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emworks/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMWorkDTO emworkdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emworkService.checkKey(emworkMapping.toDomain(emworkdto)));
    }

    @PreAuthorize("hasPermission(this.emworkMapping.toDomain(#emworkdto),'eam-EMWork-Save')")
    @ApiOperation(value = "保存加班工单", tags = {"加班工单" },  notes = "保存加班工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emworks/save")
    public ResponseEntity<EMWorkDTO> save(@RequestBody EMWorkDTO emworkdto) {
        EMWork domain = emworkMapping.toDomain(emworkdto);
        emworkService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emworkMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emworkMapping.toDomain(#emworkdtos),'eam-EMWork-Save')")
    @ApiOperation(value = "批量保存加班工单", tags = {"加班工单" },  notes = "批量保存加班工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emworks/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMWorkDTO> emworkdtos) {
        emworkService.saveBatch(emworkMapping.toDomain(emworkdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWork-searchDefault-all') and hasPermission(#context,'eam-EMWork-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"加班工单" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emworks/fetchdefault")
	public ResponseEntity<List<EMWorkDTO>> fetchDefault(EMWorkSearchContext context) {
        Page<EMWork> domains = emworkService.searchDefault(context) ;
        List<EMWorkDTO> list = emworkMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWork-searchDefault-all') and hasPermission(#context,'eam-EMWork-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"加班工单" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emworks/searchdefault")
	public ResponseEntity<Page<EMWorkDTO>> searchDefault(@RequestBody EMWorkSearchContext context) {
        Page<EMWork> domains = emworkService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emworkMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

