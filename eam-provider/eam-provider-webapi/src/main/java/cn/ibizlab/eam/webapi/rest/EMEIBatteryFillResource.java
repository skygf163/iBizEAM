package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEIBatteryFill;
import cn.ibizlab.eam.core.eam_core.service.IEMEIBatteryFillService;
import cn.ibizlab.eam.core.eam_core.filter.EMEIBatteryFillSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"电瓶充电记录" })
@RestController("WebApi-emeibatteryfill")
@RequestMapping("")
public class EMEIBatteryFillResource {

    @Autowired
    public IEMEIBatteryFillService emeibatteryfillService;

    @Autowired
    @Lazy
    public EMEIBatteryFillMapping emeibatteryfillMapping;

    @PreAuthorize("hasPermission(this.emeibatteryfillMapping.toDomain(#emeibatteryfilldto),'eam-EMEIBatteryFill-Create')")
    @ApiOperation(value = "新建电瓶充电记录", tags = {"电瓶充电记录" },  notes = "新建电瓶充电记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeibatteryfills")
    public ResponseEntity<EMEIBatteryFillDTO> create(@Validated @RequestBody EMEIBatteryFillDTO emeibatteryfilldto) {
        EMEIBatteryFill domain = emeibatteryfillMapping.toDomain(emeibatteryfilldto);
		emeibatteryfillService.create(domain);
        EMEIBatteryFillDTO dto = emeibatteryfillMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeibatteryfillMapping.toDomain(#emeibatteryfilldtos),'eam-EMEIBatteryFill-Create')")
    @ApiOperation(value = "批量新建电瓶充电记录", tags = {"电瓶充电记录" },  notes = "批量新建电瓶充电记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeibatteryfills/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEIBatteryFillDTO> emeibatteryfilldtos) {
        emeibatteryfillService.createBatch(emeibatteryfillMapping.toDomain(emeibatteryfilldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeibatteryfill" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeibatteryfillService.get(#emeibatteryfill_id),'eam-EMEIBatteryFill-Update')")
    @ApiOperation(value = "更新电瓶充电记录", tags = {"电瓶充电记录" },  notes = "更新电瓶充电记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeibatteryfills/{emeibatteryfill_id}")
    public ResponseEntity<EMEIBatteryFillDTO> update(@PathVariable("emeibatteryfill_id") String emeibatteryfill_id, @RequestBody EMEIBatteryFillDTO emeibatteryfilldto) {
		EMEIBatteryFill domain  = emeibatteryfillMapping.toDomain(emeibatteryfilldto);
        domain .setEmeibatteryfillid(emeibatteryfill_id);
		emeibatteryfillService.update(domain );
		EMEIBatteryFillDTO dto = emeibatteryfillMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeibatteryfillService.getEmeibatteryfillByEntities(this.emeibatteryfillMapping.toDomain(#emeibatteryfilldtos)),'eam-EMEIBatteryFill-Update')")
    @ApiOperation(value = "批量更新电瓶充电记录", tags = {"电瓶充电记录" },  notes = "批量更新电瓶充电记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeibatteryfills/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEIBatteryFillDTO> emeibatteryfilldtos) {
        emeibatteryfillService.updateBatch(emeibatteryfillMapping.toDomain(emeibatteryfilldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeibatteryfillService.get(#emeibatteryfill_id),'eam-EMEIBatteryFill-Remove')")
    @ApiOperation(value = "删除电瓶充电记录", tags = {"电瓶充电记录" },  notes = "删除电瓶充电记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeibatteryfills/{emeibatteryfill_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeibatteryfill_id") String emeibatteryfill_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeibatteryfillService.remove(emeibatteryfill_id));
    }

    @PreAuthorize("hasPermission(this.emeibatteryfillService.getEmeibatteryfillByIds(#ids),'eam-EMEIBatteryFill-Remove')")
    @ApiOperation(value = "批量删除电瓶充电记录", tags = {"电瓶充电记录" },  notes = "批量删除电瓶充电记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeibatteryfills/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeibatteryfillService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeibatteryfillMapping.toDomain(returnObject.body),'eam-EMEIBatteryFill-Get')")
    @ApiOperation(value = "获取电瓶充电记录", tags = {"电瓶充电记录" },  notes = "获取电瓶充电记录")
	@RequestMapping(method = RequestMethod.GET, value = "/emeibatteryfills/{emeibatteryfill_id}")
    public ResponseEntity<EMEIBatteryFillDTO> get(@PathVariable("emeibatteryfill_id") String emeibatteryfill_id) {
        EMEIBatteryFill domain = emeibatteryfillService.get(emeibatteryfill_id);
        EMEIBatteryFillDTO dto = emeibatteryfillMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取电瓶充电记录草稿", tags = {"电瓶充电记录" },  notes = "获取电瓶充电记录草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeibatteryfills/getdraft")
    public ResponseEntity<EMEIBatteryFillDTO> getDraft(EMEIBatteryFillDTO dto) {
        EMEIBatteryFill domain = emeibatteryfillMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emeibatteryfillMapping.toDto(emeibatteryfillService.getDraft(domain)));
    }

    @ApiOperation(value = "检查电瓶充电记录", tags = {"电瓶充电记录" },  notes = "检查电瓶充电记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeibatteryfills/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEIBatteryFillDTO emeibatteryfilldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeibatteryfillService.checkKey(emeibatteryfillMapping.toDomain(emeibatteryfilldto)));
    }

    @PreAuthorize("hasPermission(this.emeibatteryfillMapping.toDomain(#emeibatteryfilldto),'eam-EMEIBatteryFill-Save')")
    @ApiOperation(value = "保存电瓶充电记录", tags = {"电瓶充电记录" },  notes = "保存电瓶充电记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeibatteryfills/save")
    public ResponseEntity<EMEIBatteryFillDTO> save(@RequestBody EMEIBatteryFillDTO emeibatteryfilldto) {
        EMEIBatteryFill domain = emeibatteryfillMapping.toDomain(emeibatteryfilldto);
        emeibatteryfillService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeibatteryfillMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeibatteryfillMapping.toDomain(#emeibatteryfilldtos),'eam-EMEIBatteryFill-Save')")
    @ApiOperation(value = "批量保存电瓶充电记录", tags = {"电瓶充电记录" },  notes = "批量保存电瓶充电记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeibatteryfills/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEIBatteryFillDTO> emeibatteryfilldtos) {
        emeibatteryfillService.saveBatch(emeibatteryfillMapping.toDomain(emeibatteryfilldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEIBatteryFill-searchDefault-all') and hasPermission(#context,'eam-EMEIBatteryFill-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"电瓶充电记录" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeibatteryfills/fetchdefault")
	public ResponseEntity<List<EMEIBatteryFillDTO>> fetchDefault(EMEIBatteryFillSearchContext context) {
        Page<EMEIBatteryFill> domains = emeibatteryfillService.searchDefault(context) ;
        List<EMEIBatteryFillDTO> list = emeibatteryfillMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEIBatteryFill-searchDefault-all') and hasPermission(#context,'eam-EMEIBatteryFill-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"电瓶充电记录" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeibatteryfills/searchdefault")
	public ResponseEntity<Page<EMEIBatteryFillDTO>> searchDefault(@RequestBody EMEIBatteryFillSearchContext context) {
        Page<EMEIBatteryFill> domains = emeibatteryfillService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeibatteryfillMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

