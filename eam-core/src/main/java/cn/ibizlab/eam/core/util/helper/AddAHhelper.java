package cn.ibizlab.eam.core.util.helper;

import cn.ibizlab.eam.core.eam_core.domain.*;
import cn.ibizlab.eam.core.eam_core.service.*;
import cn.ibizlab.eam.util.dict.StaticDict;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Component;

import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;

@Component
@Slf4j
public class AddAHhelper {

    @Autowired
    IEMEQMaintanceService iemeqMaintanceService;

    @Autowired
    IEMEQKeepService iemeqKeepService;

    @Autowired
    IEMEQCheckService iemeqCheckService;

    @Autowired
    IEMEQDebugService iemeqDebugService;

    @Autowired
    IEMEQKPRCDService iemeqkprcdService;

    @Autowired
    IEMEQMPMTRService iemeqmpmtrService;

    @Autowired
    IEMWO_ENService iemwoEnService;

    @Autowired
    IEMENConsumService iemenConsumService;

    @Autowired
    IEMOutputRctService iemOutputRctService;

    @Autowired
    IEMEQWLService iemeqwlService;

    @Autowired
    IEMEQAHService iemeqahService;

    @Autowired
    IEMDPRCTService iemdprctService;

    @Autowired
    IEMEQSetupService iemeqSetupService;
    @Autowired
    IEMWO_INNERService iemwo_innerService;

    @Autowired
    IEMENService iemenService;

    String[] ignoreProperties = new String[]{"activebdesc","activeadesc","enid","pfee","curval"};

    /**
     *
     * @param et
     */
    public void genRecord(EMWO et) {
        // TODO: 插入带默认周期计划的下次执行日期
        if (et.getArchive() != null ){
            EMEQWL emeqwl = new EMEQWL();
            //BeanUtils.copyProperties(et,emeqwl);
            BeanCopier copiereqwl=BeanCopier.create(EMWO.class, EMEQWL.class,false);
            copiereqwl.copy(et, emeqwl,null);
            emeqwl.setEmeqwlname(et.getEmwoname());
            emeqwl.setEdate(et.getRegionenddate());
            emeqwl.setBdate(et.getRegionbegindate());
            emeqwl.setWoid(et.getEmwoid());
            emeqwl.setEmeqwlname(et.getEmwoname());

            //持续时间保留两位小数
            EMEQSetup emeqSetup = new EMEQSetup();
            emeqSetup.setActivelengths(NumberUtils.toScaledBigDecimal(et.getWorklength()/60, 2, RoundingMode.DOWN).doubleValue());
            emeqSetup.setActivebdesc(et.getWodesc());
            emeqSetup.setActivedesc(et.getEmwoname());
            emeqSetup.setActivedate(et.getWodate());
            emeqSetup.setWoid(et.getEmwoid());
            BeanUtils.copyProperties(et,emeqwl,ignoreProperties);
            //根据工单类型，在关闭是插入对应的工单历史日志记录
            List<String> emeqahtypes = Arrays.asList(et.getArchive().split(";"));
            for(String emeqahtype : emeqahtypes){

                if (StringUtils.compare(emeqahtype, StaticDict.EMARCHIVE.MAINTANCE.getValue()) == 0){
                    EMEQMaintance emeqMaintance = new EMEQMaintance();
                    BeanCopier copierMaintance=BeanCopier.create(EMEQSetup.class,EMEQMaintance.class,false);
                    copierMaintance.copy(emeqSetup, emeqMaintance,null);
                    iemeqMaintanceService.create(emeqMaintance);
                    emeqahOrder(et,StaticDict.EMARCHIVE.MAINTANCE.getValue());
                }

                if (StringUtils.compare(emeqahtype, StaticDict.EMARCHIVE.KEEP.getValue()) == 0){
                    EMEQKeep emeqKeep = new EMEQKeep();
                    //BeanUtils.copyProperties(emeqSetup,emeqKeep,ignoreProperties);
                    BeanCopier copierKeep=BeanCopier.create(EMEQSetup.class,EMEQKeep.class,false);
                    copierKeep.copy(emeqSetup,emeqKeep,null);
                    iemeqKeepService.create(emeqKeep);
                    emeqahOrder(et,StaticDict.EMARCHIVE.KEEP.getValue());
                }

                if ( StringUtils.compare(emeqahtype, StaticDict.EMARCHIVE.CHECK.getValue()) == 0){
                    EMEQCheck emeqCheck = new EMEQCheck();
                    BeanCopier copierCheck=BeanCopier.create(EMEQSetup.class,EMEQCheck.class,false);
                    copierCheck.copy(emeqSetup,emeqCheck,null);
                    iemeqCheckService.create(emeqCheck);
                    emeqahOrder(et,StaticDict.EMARCHIVE.CHECK.getValue());
                }

                if (StringUtils.compare(emeqahtype, StaticDict.EMARCHIVE.DEBUG.getValue()) == 0){
                    EMEQDebug emeqDebug = new EMEQDebug();
                    BeanCopier copierDebug=BeanCopier.create(EMEQSetup.class,EMEQDebug.class,false);
                    copierDebug.copy(emeqSetup,emeqDebug,null);
                    iemeqDebugService.create(emeqDebug);
                    emeqahOrder(et,StaticDict.EMARCHIVE.DEBUG.getValue());
                }

                if (StringUtils.compare(emeqahtype, StaticDict.EMARCHIVE.SETUP.getValue()) == 0){
                    iemeqSetupService.create(emeqSetup);
                    emeqahOrder(et,StaticDict.EMARCHIVE.SETUP.getValue());
                }

                if ( StringUtils.compare(emeqahtype, StaticDict.EMARCHIVE.EQKPRCT.getValue()) == 0){
                    EMEQKPRCD emeqkprcd = new EMEQKPRCD();
                    BeanCopier copierkprcd=BeanCopier.create(EMEQWL.class,EMEQKPRCD.class,false);
                    copierkprcd.copy(emeqwl,copierkprcd,null);
                    iemeqkprcdService.create(emeqkprcd);
                    emDprctRecord(et,StaticDict.EMARCHIVE.EQKPRCT.getValue());
                }
                //设备仪表读数
                if (StringUtils.compare(emeqahtype, StaticDict.EMARCHIVE.EQMPMTR.getValue()) == 0) {
                    EMEQMPMTR emeqmpmtr = new EMEQMPMTR();
                    BeanCopier copiermpmtr=BeanCopier.create(EMEQWL.class,EMEQMPMTR.class,false);
                    copiermpmtr.copy(emeqwl,emeqmpmtr,null);
                    iemeqmpmtrService.create(emeqmpmtr);
                    emDprctRecord(et,StaticDict.EMARCHIVE.EQMPMTR.getValue());
                }
                //能耗
                if ( StringUtils.compare(emeqahtype, StaticDict.EMARCHIVE.ENCONSUM.getValue()) == 0) {
                    EMENConsum emenConsum = new EMENConsum();
                    BeanCopier copierConsum=BeanCopier.create(EMEQWL.class,EMENConsum.class,false);
                    copierConsum.copy(emeqwl,emenConsum,null);
                    EMEN emen = iemenService.get(et.getEmwoid());
                    Double emenPrice = (emen.getPrice() != null) ? emen.getPrice() : emen.getItemprice();
                    emenConsum.setPrice(emenPrice);
                    emenConsum.setDeptid(et.getRdeptid());
                    emenConsum.setEnid(et.getEmwoid());
                    iemenConsumService.create(emenConsum);
                    emDprctRecord(et,StaticDict.EMARCHIVE.ENCONSUM.getValue());
                }

                if (StringUtils.compare(emeqahtype, StaticDict.EMARCHIVE.OUTPUTRCT.getValue()) == 0) {
                    EMOutputRct emOutputRct = new EMOutputRct();
                    BeanCopier copierOutputRct=BeanCopier.create(EMEQWL.class,EMOutputRct.class,false);
                    copierOutputRct.copy(emeqwl,emOutputRct,null);
                    iemOutputRctService.create(emOutputRct);
                    emDprctRecord(et,StaticDict.EMARCHIVE.OUTPUTRCT.getValue());
                }

                if (StringUtils.compare(emeqahtype, StaticDict.EMARCHIVE.EQWL.getValue()) == 0) {
                    iemeqwlService.create(emeqwl);
                    emDprctRecord(et,StaticDict.EMARCHIVE.EQWL.getValue());
                }
            }
        }
    }

    /**
     *  插入工单的测点记录
     * @param et
     */
    public void emDprctRecord(EMWO et,String achive) {
        EMDPRCT emdprct = new EMDPRCT();
        emdprct.setEmdprcttype(achive);
        measuringRecord(et, emdprct);
    }

    /**
     * 插入工单的活动历史记录
     * @param et
     * @param achive
     */

    public void emeqahOrder(EMWO et,String achive) {
        EMEQAH emeqah = new EMEQAH();
        emeqah.setEmeqahtype(achive);
        orderRecord(et,emeqah);
    }

    /**
     * 如果是安装和替换方面的工单，修改指定数据
     * @param et
     */
    public void orderType(EMWO_INNER et) {
        if (StringUtils.compare(et.getWotype(), StaticDict.EMWOTYPE1.INSTALL.getValue()) == 0) {
            EMEQSetup emeqSetup = getEmeqSetup(et);
            emeqSetup.setEitiresid(et.getEmeitiresid());
            iemeqSetupService.create(emeqSetup);
        } else if (StringUtils.compare(et.getWotype(), StaticDict.EMWOTYPE1.REPLACE.getValue()) == 0) {
            EMEQSetup emeqSetup = getEmeqSetup(et);
            iemeqSetupService.create(emeqSetup);
        }
    }

    /**
     * orderType方法抽取
     * @param et
     * @return
     */
    public EMEQSetup getEmeqSetup(EMWO_INNER et) {
        EMEQSetup emeqSetup = new EMEQSetup();
        BeanUtils.copyProperties(et,emeqSetup,ignoreProperties);
        emeqSetup.setActivedate(et.getWodate());
        emeqSetup.setWoid(et.getEmwoInnerid());
        return emeqSetup;
    }

    /**
     *  插入工单的历史记录
     * @param emwo
     * @param emeqah
     */

    public  void orderRecord(EMWO emwo,EMEQAH emeqah){  //插入工单的历史记录
        emeqah.setEmeqahname(emwo.getEmwoname());
        emeqah.setWoid(emwo.getEmwoid());
        emeqah.setActivebdesc(emwo.getWodesc());
        emeqah.setActivedesc(emwo.getEmwoname());
        emeqah.setActiveadesc(emwo.getWresult());
        emeqah.setActivedate(emwo.getWodate());
        BeanUtils.copyProperties(emwo,emeqah,ignoreProperties);
        //持续时间保留两位小数
        emeqah.setActivelengths(NumberUtils.toScaledBigDecimal(emwo.getWorklength()/60,2,RoundingMode.DOWN).doubleValue());
        iemeqahService.create(emeqah);
    }


    /**
     *  测点记录标识
     * @param emwo
     * @param emdprct
     */
    public void measuringRecord(EMWO emwo,EMDPRCT emdprct){

        BeanUtils.copyProperties(emwo,emdprct,ignoreProperties);
        emdprct.setEmdprctname(emwo.getEmwoname());
        emdprct.setBdate(emwo.getRegionbegindate());
        emdprct.setEdate(emwo.getRegionenddate());

        iemdprctService.create(emdprct);
    }
}
