package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMRFOCA;
import cn.ibizlab.eam.core.eam_core.filter.EMRFOCASearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMRFOCAService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMRFOCAMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[原因] 服务对象接口实现
 */
@Slf4j
@Service("EMRFOCAServiceImpl")
public class EMRFOCAServiceImpl extends ServiceImpl<EMRFOCAMapper, EMRFOCA> implements IEMRFOCAService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMApplyService emapplyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQAHService emeqahService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQCheckService emeqcheckService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQDebugService emeqdebugService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQKeepService emeqkeepService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQMaintanceService emeqmaintanceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQSetupService emeqsetupService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_DPService emwoDpService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWOService emwoService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_ENService emwoEnService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_INNERService emwoInnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_OSCService emwoOscService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_PTService emwoPtService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMRFODEService emrfodeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMRFOMOService emrfomoService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMRFOCA et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmrfocaid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMRFOCA> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMRFOCA et) {
        fillParentData(et);
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emrfocaid", et.getEmrfocaid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmrfocaid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMRFOCA> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMRFOCA get(String key) {
        EMRFOCA et = getById(key);
        if(et == null){
            et = new EMRFOCA();
            et.setEmrfocaid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMRFOCA getDraft(EMRFOCA et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMRFOCA et) {
        return (!ObjectUtils.isEmpty(et.getEmrfocaid())) && (!Objects.isNull(this.getById(et.getEmrfocaid())));
    }
    @Override
    @Transactional
    public boolean save(EMRFOCA et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMRFOCA et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMRFOCA> list) {
        list.forEach(item->fillParentData(item));
        List<EMRFOCA> create = new ArrayList<>();
        List<EMRFOCA> update = new ArrayList<>();
        for (EMRFOCA et : list) {
            if (ObjectUtils.isEmpty(et.getEmrfocaid()) || ObjectUtils.isEmpty(getById(et.getEmrfocaid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMRFOCA> list) {
        list.forEach(item->fillParentData(item));
        List<EMRFOCA> create = new ArrayList<>();
        List<EMRFOCA> update = new ArrayList<>();
        for (EMRFOCA et : list) {
            if (ObjectUtils.isEmpty(et.getEmrfocaid()) || ObjectUtils.isEmpty(getById(et.getEmrfocaid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMRFOCA> selectByRfodeid(String emrfodeid) {
        return baseMapper.selectByRfodeid(emrfodeid);
    }
    @Override
    public void removeByRfodeid(String emrfodeid) {
        this.remove(new QueryWrapper<EMRFOCA>().eq("rfodeid",emrfodeid));
    }

	@Override
    public List<EMRFOCA> selectByRfomoid(String emrfomoid) {
        return baseMapper.selectByRfomoid(emrfomoid);
    }
    @Override
    public void removeByRfomoid(String emrfomoid) {
        this.remove(new QueryWrapper<EMRFOCA>().eq("rfomoid",emrfomoid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMRFOCA> searchDefault(EMRFOCASearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMRFOCA> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMRFOCA>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMRFOCA et){
        //实体关系[DER1N_EMRFOCA_EMRFODE_RFODEID]
        if(!ObjectUtils.isEmpty(et.getRfodeid())){
            cn.ibizlab.eam.core.eam_core.domain.EMRFODE rfode=et.getRfode();
            if(ObjectUtils.isEmpty(rfode)){
                cn.ibizlab.eam.core.eam_core.domain.EMRFODE majorEntity=emrfodeService.get(et.getRfodeid());
                et.setRfode(majorEntity);
                rfode=majorEntity;
            }
            et.setRfodename(rfode.getEmrfodename());
        }
        //实体关系[DER1N_EMRFOCA_EMRFOMO_RFOMOID]
        if(!ObjectUtils.isEmpty(et.getRfomoid())){
            cn.ibizlab.eam.core.eam_core.domain.EMRFOMO rfomo=et.getRfomo();
            if(ObjectUtils.isEmpty(rfomo)){
                cn.ibizlab.eam.core.eam_core.domain.EMRFOMO majorEntity=emrfomoService.get(et.getRfomoid());
                et.setRfomo(majorEntity);
                rfomo=majorEntity;
            }
            et.setRfomoname(rfomo.getEmrfomoname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMRFOCA> getEmrfocaByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMRFOCA> getEmrfocaByEntities(List<EMRFOCA> entities) {
        List ids =new ArrayList();
        for(EMRFOCA entity : entities){
            Serializable id=entity.getEmrfocaid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMRFOCAService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



