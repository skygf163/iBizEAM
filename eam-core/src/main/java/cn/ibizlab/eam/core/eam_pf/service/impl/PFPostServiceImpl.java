package cn.ibizlab.eam.core.eam_pf.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_pf.domain.PFPost;
import cn.ibizlab.eam.core.eam_pf.filter.PFPostSearchContext;
import cn.ibizlab.eam.core.eam_pf.service.IPFPostService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_pf.mapper.PFPostMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[岗位] 服务对象接口实现
 */
@Slf4j
@Service("PFPostServiceImpl")
public class PFPostServiceImpl extends ServiceImpl<PFPostMapper, PFPost> implements IPFPostService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFEmpPostMapService pfemppostmapService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(PFPost et) {
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getPfpostid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<PFPost> list) {
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(PFPost et) {
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("pfpostid", et.getPfpostid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getPfpostid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<PFPost> list) {
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public PFPost get(String key) {
        PFPost et = getById(key);
        if(et == null){
            et = new PFPost();
            et.setPfpostid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public PFPost getDraft(PFPost et) {
        return et;
    }

    @Override
    public boolean checkKey(PFPost et) {
        return (!ObjectUtils.isEmpty(et.getPfpostid())) && (!Objects.isNull(this.getById(et.getPfpostid())));
    }
    @Override
    @Transactional
    public boolean save(PFPost et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(PFPost et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<PFPost> list) {
        List<PFPost> create = new ArrayList<>();
        List<PFPost> update = new ArrayList<>();
        for (PFPost et : list) {
            if (ObjectUtils.isEmpty(et.getPfpostid()) || ObjectUtils.isEmpty(getById(et.getPfpostid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<PFPost> list) {
        List<PFPost> create = new ArrayList<>();
        List<PFPost> update = new ArrayList<>();
        for (PFPost et : list) {
            if (ObjectUtils.isEmpty(et.getPfpostid()) || ObjectUtils.isEmpty(getById(et.getPfpostid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }



    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<PFPost> searchDefault(PFPostSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<PFPost> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<PFPost>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<PFPost> getPfpostByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<PFPost> getPfpostByEntities(List<PFPost> entities) {
        List ids =new ArrayList();
        for(PFPost entity : entities){
            Serializable id=entity.getPfpostid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IPFPostService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



