package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE_O;
import cn.ibizlab.eam.core.eam_core.filter.PLANSCHEDULE_OSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[PLANSCHEDULE_O] 服务对象接口
 */
public interface IPLANSCHEDULE_OService extends IService<PLANSCHEDULE_O> {

    boolean create(PLANSCHEDULE_O et);
    void createBatch(List<PLANSCHEDULE_O> list);
    boolean update(PLANSCHEDULE_O et);
    void updateBatch(List<PLANSCHEDULE_O> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    PLANSCHEDULE_O get(String key);
    PLANSCHEDULE_O getDraft(PLANSCHEDULE_O et);
    boolean checkKey(PLANSCHEDULE_O et);
    boolean save(PLANSCHEDULE_O et);
    void saveBatch(List<PLANSCHEDULE_O> list);
    Page<PLANSCHEDULE_O> searchDefault(PLANSCHEDULE_OSearchContext context);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<PLANSCHEDULE_O> getPlanscheduleOByIds(List<String> ids);
    List<PLANSCHEDULE_O> getPlanscheduleOByEntities(List<PLANSCHEDULE_O> entities);
}


