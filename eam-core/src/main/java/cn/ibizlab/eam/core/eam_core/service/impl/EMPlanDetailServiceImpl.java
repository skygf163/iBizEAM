package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMPlanDetail;
import cn.ibizlab.eam.core.eam_core.filter.EMPlanDetailSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMPlanDetailService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMPlanDetailMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[计划步骤] 服务对象接口实现
 */
@Slf4j
@Service("EMPlanDetailServiceImpl")
public class EMPlanDetailServiceImpl extends ServiceImpl<EMPlanDetailMapper, EMPlanDetail> implements IEMPlanDetailService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjectService emobjectService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMPlanService emplanService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMPlanDetail et) {
        fillParentData(et);
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmplandetailid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMPlanDetail> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMPlanDetail et) {
        fillParentData(et);
        emresrefobjService.update(emplandetailInheritMapping.toEmresrefobj(et));
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emplandetailid", et.getEmplandetailid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmplandetailid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMPlanDetail> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emresrefobjService.remove(key);
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMPlanDetail get(String key) {
        EMPlanDetail et = getById(key);
        if(et == null){
            et = new EMPlanDetail();
            et.setEmplandetailid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMPlanDetail getDraft(EMPlanDetail et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMPlanDetail et) {
        return (!ObjectUtils.isEmpty(et.getEmplandetailid())) && (!Objects.isNull(this.getById(et.getEmplandetailid())));
    }
    @Override
    @Transactional
    public boolean save(EMPlanDetail et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMPlanDetail et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMPlanDetail> list) {
        list.forEach(item->fillParentData(item));
        List<EMPlanDetail> create = new ArrayList<>();
        List<EMPlanDetail> update = new ArrayList<>();
        for (EMPlanDetail et : list) {
            if (ObjectUtils.isEmpty(et.getEmplandetailid()) || ObjectUtils.isEmpty(getById(et.getEmplandetailid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMPlanDetail> list) {
        list.forEach(item->fillParentData(item));
        List<EMPlanDetail> create = new ArrayList<>();
        List<EMPlanDetail> update = new ArrayList<>();
        for (EMPlanDetail et : list) {
            if (ObjectUtils.isEmpty(et.getEmplandetailid()) || ObjectUtils.isEmpty(getById(et.getEmplandetailid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMPlanDetail> selectByEquipid(String emequipid) {
        return baseMapper.selectByEquipid(emequipid);
    }
    @Override
    public void removeByEquipid(String emequipid) {
        this.remove(new QueryWrapper<EMPlanDetail>().eq("equipid",emequipid));
    }

	@Override
    public List<EMPlanDetail> selectByDpid(String emobjectid) {
        return baseMapper.selectByDpid(emobjectid);
    }
    @Override
    public void removeByDpid(String emobjectid) {
        this.remove(new QueryWrapper<EMPlanDetail>().eq("dpid",emobjectid));
    }

	@Override
    public List<EMPlanDetail> selectByObjid(String emobjectid) {
        return baseMapper.selectByObjid(emobjectid);
    }
    @Override
    public void removeByObjid(String emobjectid) {
        this.remove(new QueryWrapper<EMPlanDetail>().eq("objid",emobjectid));
    }

	@Override
    public List<EMPlanDetail> selectByPlanid(String emplanid) {
        return baseMapper.selectByPlanid(emplanid);
    }
    @Override
    public void removeByPlanid(String emplanid) {
        this.remove(new QueryWrapper<EMPlanDetail>().eq("planid",emplanid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMPlanDetail> searchDefault(EMPlanDetailSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMPlanDetail> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMPlanDetail>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMPlanDetail et){
        //实体关系[DER1N_EMPLANDETAIL_EMEQUIP_EQUIPID]
        if(!ObjectUtils.isEmpty(et.getEquipid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEquip equip=et.getEquip();
            if(ObjectUtils.isEmpty(equip)){
                cn.ibizlab.eam.core.eam_core.domain.EMEquip majorEntity=emequipService.get(et.getEquipid());
                et.setEquip(majorEntity);
                equip=majorEntity;
            }
            et.setEquipname(equip.getEquipinfo());
        }
        //实体关系[DER1N_EMPLANDETAIL_EMOBJECT_DPID]
        if(!ObjectUtils.isEmpty(et.getDpid())){
            cn.ibizlab.eam.core.eam_core.domain.EMObject dp=et.getDp();
            if(ObjectUtils.isEmpty(dp)){
                cn.ibizlab.eam.core.eam_core.domain.EMObject majorEntity=emobjectService.get(et.getDpid());
                et.setDp(majorEntity);
                dp=majorEntity;
            }
            et.setDptype(dp.getEmobjecttype());
            et.setDpname(dp.getEmobjectname());
        }
        //实体关系[DER1N_EMPLANDETAIL_EMOBJECT_OBJID]
        if(!ObjectUtils.isEmpty(et.getObjid())){
            cn.ibizlab.eam.core.eam_core.domain.EMObject obj=et.getObj();
            if(ObjectUtils.isEmpty(obj)){
                cn.ibizlab.eam.core.eam_core.domain.EMObject majorEntity=emobjectService.get(et.getObjid());
                et.setObj(majorEntity);
                obj=majorEntity;
            }
            et.setObjname(obj.getEmobjectname());
        }
        //实体关系[DER1N_EMPLANDETAIL_EMPLAN_PLANID]
        if(!ObjectUtils.isEmpty(et.getPlanid())){
            cn.ibizlab.eam.core.eam_core.domain.EMPlan plan=et.getPlan();
            if(ObjectUtils.isEmpty(plan)){
                cn.ibizlab.eam.core.eam_core.domain.EMPlan majorEntity=emplanService.get(et.getPlanid());
                et.setPlan(majorEntity);
                plan=majorEntity;
            }
            et.setPlanname(plan.getEmplanname());
        }
    }



    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMPlanDetailInheritMapping emplandetailInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMResRefObjService emresrefobjService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMPlanDetail et){
        if(ObjectUtils.isEmpty(et.getEmplandetailid()))
            et.setEmplandetailid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMResRefObj emresrefobj =emplandetailInheritMapping.toEmresrefobj(et);
        emresrefobj.set("emresrefobjtype","PLANDETAIL");
        emresrefobjService.create(emresrefobj);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMPlanDetail> getEmplandetailByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMPlanDetail> getEmplandetailByEntities(List<EMPlanDetail> entities) {
        List ids =new ArrayList();
        for(EMPlanDetail entity : entities){
            Serializable id=entity.getEmplandetailid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMPlanDetailService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



