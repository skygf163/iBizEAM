package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMObjMap;
import cn.ibizlab.eam.core.eam_core.filter.EMObjMapSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMObjMap] 服务对象接口
 */
public interface IEMObjMapService extends IService<EMObjMap> {

    boolean create(EMObjMap et);
    void createBatch(List<EMObjMap> list);
    boolean update(EMObjMap et);
    void updateBatch(List<EMObjMap> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMObjMap get(String key);
    EMObjMap getDraft(EMObjMap et);
    boolean checkKey(EMObjMap et);
    boolean save(EMObjMap et);
    void saveBatch(List<EMObjMap> list);
    Page<EMObjMap> searchChildLocation(EMObjMapSearchContext context);
    Page<EMObjMap> searchDefault(EMObjMapSearchContext context);
    Page<EMObjMap> searchIndexDER(EMObjMapSearchContext context);
    Page<EMObjMap> searchLocationByEQ(EMObjMapSearchContext context);
    List<EMObjMap> selectByObjid(String emobjectid);
    void removeByObjid(String emobjectid);
    List<EMObjMap> selectByObjpid(String emobjectid);
    void removeByObjpid(String emobjectid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMObjMap> getEmobjmapByIds(List<String> ids);
    List<EMObjMap> getEmobjmapByEntities(List<EMObjMap> entities);
}


