package cn.ibizlab.eam.core.eam_core.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.eam.core.eam_core.domain.EMEQLCTTIRes;
import cn.ibizlab.eam.core.eam_core.filter.EMEQLCTTIResSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface EMEQLCTTIResMapper extends BaseMapper<EMEQLCTTIRes> {

    Page<EMEQLCTTIRes> searchDefault(IPage page, @Param("srf") EMEQLCTTIResSearchContext context, @Param("ew") Wrapper<EMEQLCTTIRes> wrapper);
    @Override
    EMEQLCTTIRes selectById(Serializable id);
    @Override
    int insert(EMEQLCTTIRes entity);
    @Override
    int updateById(@Param(Constants.ENTITY) EMEQLCTTIRes entity);
    @Override
    int update(@Param(Constants.ENTITY) EMEQLCTTIRes entity, @Param("ew") Wrapper<EMEQLCTTIRes> updateWrapper);
    @Override
    int deleteById(Serializable id);
    /**
    * 自定义查询SQL
    * @param sql
    * @return
    */
    @Select("${sql}")
    List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<EMEQLCTTIRes> selectByEquipid(@Param("emequipid") Serializable emequipid);

    List<EMEQLCTTIRes> selectByLabserviceid(@Param("emserviceid") Serializable emserviceid);

    List<EMEQLCTTIRes> selectByMserviceid(@Param("emserviceid") Serializable emserviceid);

}
