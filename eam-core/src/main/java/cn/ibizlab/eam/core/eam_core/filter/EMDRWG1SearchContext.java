package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMDRWG1;
/**
 * 关系型数据实体[EMDRWG1] 查询条件对象
 */
@Slf4j
@Data
public class EMDRWG1SearchContext extends QueryWrapperContext<EMDRWG1> {

	private String n_emdrwg1name_like;//[特种设备标准化管理名称]
	public void setN_emdrwg1name_like(String n_emdrwg1name_like) {
        this.n_emdrwg1name_like = n_emdrwg1name_like;
        if(!ObjectUtils.isEmpty(this.n_emdrwg1name_like)){
            this.getSearchCond().like("emdrwg1name", n_emdrwg1name_like);
        }
    }
	private String n_drwginfo_like;//[文档信息]
	public void setN_drwginfo_like(String n_drwginfo_like) {
        this.n_drwginfo_like = n_drwginfo_like;
        if(!ObjectUtils.isEmpty(this.n_drwginfo_like)){
            this.getSearchCond().like("drwginfo", n_drwginfo_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emdrwg1name", query)
            );
		 }
	}
}



