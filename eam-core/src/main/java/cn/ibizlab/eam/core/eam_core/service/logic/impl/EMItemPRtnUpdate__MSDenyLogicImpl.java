package cn.ibizlab.eam.core.eam_core.service.logic.impl;

import java.util.Map;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;
import cn.ibizlab.eam.core.eam_core.service.logic.IEMItemPRtnUpdate__MSDenyLogic;
import cn.ibizlab.eam.core.eam_core.domain.EMItemPRtn;

/**
 * 关系型数据实体[Update__MSDeny] 对象
 */
@Slf4j
@Service
public class EMItemPRtnUpdate__MSDenyLogicImpl implements IEMItemPRtnUpdate__MSDenyLogic {

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMItemPRtnService emitemprtnservice;

    public cn.ibizlab.eam.core.eam_core.service.IEMItemPRtnService getEmitemprtnService() {
        return this.emitemprtnservice;
    }


    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMItemPRtnService iBzSysDefaultService;

    public cn.ibizlab.eam.core.eam_core.service.IEMItemPRtnService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    @Override
    public void execute(EMItemPRtn et) {

        KieSession kieSession = null;
        try {
            kieSession = kieContainer.newKieSession();
            kieSession.insert(et); 
            kieSession.setGlobal("emitemprtnupdate__msdenydefault", et);
            cn.ibizlab.eam.core.eam_core.domain.EMItemPRtn emitemprtnupdate__msdenytemp = new cn.ibizlab.eam.core.eam_core.domain.EMItemPRtn();
            kieSession.insert(emitemprtnupdate__msdenytemp); 
            kieSession.setGlobal("emitemprtnupdate__msdenytemp", emitemprtnupdate__msdenytemp);
            kieSession.setGlobal("emitemprtnservice", emitemprtnservice);
            kieSession.setGlobal("iBzSysEmitemprtnDefaultService", iBzSysDefaultService);
            kieSession.setGlobal("curuser", cn.ibizlab.eam.util.security.AuthenticationUser.getAuthenticationUser());
            kieSession.startProcess("cn.ibizlab.eam.core.eam_core.service.logic.emitemprtnupdate__msdeny");

        } catch (Exception e) {
            throw new RuntimeException("执行[行为[Update]主状态拒绝逻辑]处理逻辑发生异常" + e);
        } finally {
            if(kieSession != null) {
                kieSession.destroy();
            }
        }
    }
}
