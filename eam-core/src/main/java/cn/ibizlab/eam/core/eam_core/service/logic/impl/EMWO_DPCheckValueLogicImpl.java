package cn.ibizlab.eam.core.eam_core.service.logic.impl;

import java.util.Map;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;
import cn.ibizlab.eam.core.eam_core.service.logic.IEMWO_DPCheckValueLogic;
import cn.ibizlab.eam.core.eam_core.domain.EMWO_DP;

/**
 * 关系型数据实体[CheckValue] 对象
 */
@Slf4j
@Service
public class EMWO_DPCheckValueLogicImpl implements IEMWO_DPCheckValueLogic {

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMWO_DPService emwo_dpservice;

    public cn.ibizlab.eam.core.eam_core.service.IEMWO_DPService getEmwo_dpService() {
        return this.emwo_dpservice;
    }


    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMWO_DPService iBzSysDefaultService;

    public cn.ibizlab.eam.core.eam_core.service.IEMWO_DPService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    @Override
    public void execute(EMWO_DP et) {

        KieSession kieSession = null;
        try {
            kieSession = kieContainer.newKieSession();
            kieSession.insert(et); 
            kieSession.setGlobal("emwo_dpcheckvaluedefault", et);
            kieSession.setGlobal("emwo_dpservice", emwo_dpservice);
            kieSession.setGlobal("iBzSysEmwo_dpDefaultService", iBzSysDefaultService);
            kieSession.setGlobal("curuser", cn.ibizlab.eam.util.security.AuthenticationUser.getAuthenticationUser());
            kieSession.startProcess("cn.ibizlab.eam.core.eam_core.service.logic.emwo_dpcheckvalue");

        } catch (Exception e) {
            throw new RuntimeException("执行[新建时值检查]处理逻辑发生异常" + e);
        } finally {
            if(kieSession != null) {
                kieSession.destroy();
            }
        }
    }
}
