package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMResRefObj;
import cn.ibizlab.eam.core.eam_core.filter.EMResRefObjSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMResRefObjService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMResRefObjMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[资源引用对象] 服务对象接口实现
 */
@Slf4j
@Service("EMResRefObjServiceImpl")
public class EMResRefObjServiceImpl extends ServiceImpl<EMResRefObjMapper, EMResRefObj> implements IEMResRefObjService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMResEmpService emresempService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMResItemService emresitemService;

    protected cn.ibizlab.eam.core.eam_core.service.IEMResRefObjService emresrefobjService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMResServiceService emresserviceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMResRefObj et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmresrefobjid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMResRefObj> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMResRefObj et) {
        fillParentData(et);
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emresrefobjid", et.getEmresrefobjid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmresrefobjid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMResRefObj> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMResRefObj get(String key) {
        EMResRefObj et = getById(key);
        if(et == null){
            et = new EMResRefObj();
            et.setEmresrefobjid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMResRefObj getDraft(EMResRefObj et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMResRefObj et) {
        return (!ObjectUtils.isEmpty(et.getEmresrefobjid())) && (!Objects.isNull(this.getById(et.getEmresrefobjid())));
    }
    @Override
    @Transactional
    public boolean save(EMResRefObj et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMResRefObj et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMResRefObj> list) {
        list.forEach(item->fillParentData(item));
        List<EMResRefObj> create = new ArrayList<>();
        List<EMResRefObj> update = new ArrayList<>();
        for (EMResRefObj et : list) {
            if (ObjectUtils.isEmpty(et.getEmresrefobjid()) || ObjectUtils.isEmpty(getById(et.getEmresrefobjid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMResRefObj> list) {
        list.forEach(item->fillParentData(item));
        List<EMResRefObj> create = new ArrayList<>();
        List<EMResRefObj> update = new ArrayList<>();
        for (EMResRefObj et : list) {
            if (ObjectUtils.isEmpty(et.getEmresrefobjid()) || ObjectUtils.isEmpty(getById(et.getEmresrefobjid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMResRefObj> selectByEquipid(String emequipid) {
        return baseMapper.selectByEquipid(emequipid);
    }
    @Override
    public void removeByEquipid(String emequipid) {
        this.remove(new QueryWrapper<EMResRefObj>().eq("equipid",emequipid));
    }

	@Override
    public List<EMResRefObj> selectByResrefobjpid(String emresrefobjid) {
        return baseMapper.selectByResrefobjpid(emresrefobjid);
    }
    @Override
    public void removeByResrefobjpid(String emresrefobjid) {
        this.remove(new QueryWrapper<EMResRefObj>().eq("resrefobjpid",emresrefobjid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMResRefObj> searchDefault(EMResRefObjSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMResRefObj> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMResRefObj>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 IndexDER
     */
    @Override
    public Page<EMResRefObj> searchIndexDER(EMResRefObjSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMResRefObj> pages=baseMapper.searchIndexDER(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMResRefObj>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMResRefObj et){
        //实体关系[DER1N_EMRESREFOBJ_EMEQUIP_EQUIPID]
        if(!ObjectUtils.isEmpty(et.getEquipid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEquip equip=et.getEquip();
            if(ObjectUtils.isEmpty(equip)){
                cn.ibizlab.eam.core.eam_core.domain.EMEquip majorEntity=emequipService.get(et.getEquipid());
                et.setEquip(majorEntity);
                equip=majorEntity;
            }
            et.setEquipname(equip.getEquipinfo());
        }
        //实体关系[DER1N_EMRESREFOBJ_EMRESREFOBJ_RESREFOBJPID]
        if(!ObjectUtils.isEmpty(et.getResrefobjpid())){
            cn.ibizlab.eam.core.eam_core.domain.EMResRefObj resrefobjp=et.getResrefobjp();
            if(ObjectUtils.isEmpty(resrefobjp)){
                cn.ibizlab.eam.core.eam_core.domain.EMResRefObj majorEntity=emresrefobjService.get(et.getResrefobjpid());
                et.setResrefobjp(majorEntity);
                resrefobjp=majorEntity;
            }
            et.setResrefobjpname(resrefobjp.getEmresrefobjname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMResRefObj> getEmresrefobjByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMResRefObj> getEmresrefobjByEntities(List<EMResRefObj> entities) {
        List ids =new ArrayList();
        for(EMResRefObj entity : entities){
            Serializable id=entity.getEmresrefobjid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMResRefObjService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



