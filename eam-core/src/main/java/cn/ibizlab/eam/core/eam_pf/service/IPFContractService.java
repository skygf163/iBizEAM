package cn.ibizlab.eam.core.eam_pf.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_pf.domain.PFContract;
import cn.ibizlab.eam.core.eam_pf.filter.PFContractSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[PFContract] 服务对象接口
 */
public interface IPFContractService extends IService<PFContract> {

    boolean create(PFContract et);
    void createBatch(List<PFContract> list);
    boolean update(PFContract et);
    void updateBatch(List<PFContract> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    PFContract get(String key);
    PFContract getDraft(PFContract et);
    boolean checkKey(PFContract et);
    boolean save(PFContract et);
    void saveBatch(List<PFContract> list);
    Page<PFContract> searchDefault(PFContractSearchContext context);
    List<PFContract> selectByEmserviceid(String emserviceid);
    void removeByEmserviceid(String emserviceid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<PFContract> getPfcontractByIds(List<String> ids);
    List<PFContract> getPfcontractByEntities(List<PFContract> entities);
}


