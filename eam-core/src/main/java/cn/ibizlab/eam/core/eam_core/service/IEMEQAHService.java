package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEQAH;
import cn.ibizlab.eam.core.eam_core.filter.EMEQAHSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEQAH] 服务对象接口
 */
public interface IEMEQAHService extends IService<EMEQAH> {

    boolean create(EMEQAH et);
    void createBatch(List<EMEQAH> list);
    boolean update(EMEQAH et);
    void updateBatch(List<EMEQAH> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMEQAH get(String key);
    EMEQAH getDraft(EMEQAH et);
    boolean checkKey(EMEQAH et);
    boolean save(EMEQAH et);
    void saveBatch(List<EMEQAH> list);
    Page<EMEQAH> searchDefault(EMEQAHSearchContext context);
    Page<EMEQAH> searchIndexDER(EMEQAHSearchContext context);
    List<EMEQAH> selectByAcclassid(String emacclassid);
    void removeByAcclassid(String emacclassid);
    List<EMEQAH> selectByEquipid(String emequipid);
    void removeByEquipid(String emequipid);
    List<EMEQAH> selectByObjid(String emobjectid);
    void removeByObjid(String emobjectid);
    List<EMEQAH> selectByRfoacid(String emrfoacid);
    void removeByRfoacid(String emrfoacid);
    List<EMEQAH> selectByRfocaid(String emrfocaid);
    void removeByRfocaid(String emrfocaid);
    List<EMEQAH> selectByRfodeid(String emrfodeid);
    void removeByRfodeid(String emrfodeid);
    List<EMEQAH> selectByRfomoid(String emrfomoid);
    void removeByRfomoid(String emrfomoid);
    List<EMEQAH> selectByRserviceid(String emserviceid);
    void removeByRserviceid(String emserviceid);
    List<EMEQAH> selectByWoid(String emwoid);
    void removeByWoid(String emwoid);
    List<EMEQAH> selectByRteamid(String pfteamid);
    void removeByRteamid(String pfteamid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEQAH> getEmeqahByIds(List<String> ids);
    List<EMEQAH> getEmeqahByEntities(List<EMEQAH> entities);
}


