package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.IBZSEQ;
import cn.ibizlab.eam.core.eam_core.filter.IBZSEQSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IIBZSEQService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.IBZSEQMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[序列号] 服务对象接口实现
 */
@Slf4j
@Service("IBZSEQServiceImpl")
public class IBZSEQServiceImpl extends ServiceImpl<IBZSEQMapper, IBZSEQ> implements IIBZSEQService {


    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(IBZSEQ et) {
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getIbzseqid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<IBZSEQ> list) {
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(IBZSEQ et) {
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("ibzseqid", et.getIbzseqid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getIbzseqid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<IBZSEQ> list) {
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public IBZSEQ get(String key) {
        IBZSEQ et = getById(key);
        if(et == null){
            et = new IBZSEQ();
            et.setIbzseqid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public IBZSEQ getDraft(IBZSEQ et) {
        return et;
    }

    @Override
    public boolean checkKey(IBZSEQ et) {
        return (!ObjectUtils.isEmpty(et.getIbzseqid())) && (!Objects.isNull(this.getById(et.getIbzseqid())));
    }
    @Override
    @Transactional
    public IBZSEQ genId(IBZSEQ et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public boolean genIdBatch(List<IBZSEQ> etList) {
        for(IBZSEQ et : etList) {
            genId(et);
        }
        return true;
    }

    @Override
    @Transactional
    public boolean save(IBZSEQ et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(IBZSEQ et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<IBZSEQ> list) {
        List<IBZSEQ> create = new ArrayList<>();
        List<IBZSEQ> update = new ArrayList<>();
        for (IBZSEQ et : list) {
            if (ObjectUtils.isEmpty(et.getIbzseqid()) || ObjectUtils.isEmpty(getById(et.getIbzseqid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<IBZSEQ> list) {
        List<IBZSEQ> create = new ArrayList<>();
        List<IBZSEQ> update = new ArrayList<>();
        for (IBZSEQ et : list) {
            if (ObjectUtils.isEmpty(et.getIbzseqid()) || ObjectUtils.isEmpty(getById(et.getIbzseqid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }



    /**
     * 查询集合 数据集
     */
    @Override
    public Page<IBZSEQ> searchDefault(IBZSEQSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<IBZSEQ> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<IBZSEQ>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<IBZSEQ> getIbzseqByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<IBZSEQ> getIbzseqByEntities(List<IBZSEQ> entities) {
        List ids =new ArrayList();
        for(IBZSEQ entity : entities){
            Serializable id=entity.getIbzseqid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IIBZSEQService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



