package cn.ibizlab.eam.core.eam_pf.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[职员]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_PFEMP_BASE", resultMap = "PFEmpResultMap")
@ApiModel("职员")
public class PFEmp extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @ApiModelProperty("更新人")
    private String updateman;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    @ApiModelProperty("描述")
    private String description;
    /**
     * 联系电话
     */
    @TableField(value = "tel")
    @JSONField(name = "tel")
    @JsonProperty("tel")
    @ApiModelProperty("联系电话")
    private String tel;
    /**
     * 工作日期
     */
    @TableField(value = "workdate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "workdate", format = "yyyy-MM-dd")
    @JsonProperty("workdate")
    @ApiModelProperty("工作日期")
    private Timestamp workdate;
    /**
     * 职员名称
     */
    @TableField(value = "pfempname")
    @JSONField(name = "pfempname")
    @JsonProperty("pfempname")
    @ApiModelProperty("职员名称")
    private String pfempname;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;
    /**
     * 入本企业日期
     */
    @TableField(value = "raisedate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "raisedate", format = "yyyy-MM-dd")
    @JsonProperty("raisedate")
    @ApiModelProperty("入本企业日期")
    private Timestamp raisedate;
    /**
     * 家庭电话
     */
    @TableField(value = "hometel")
    @JSONField(name = "hometel")
    @JsonProperty("hometel")
    @ApiModelProperty("家庭电话")
    private String hometel;
    /**
     * 出生日期
     */
    @TableField(value = "birthday")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "birthday", format = "yyyy-MM-dd")
    @JsonProperty("birthday")
    @ApiModelProperty("出生日期")
    private Timestamp birthday;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;
    /**
     * 职员标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "pfempid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "pfempid")
    @JsonProperty("pfempid")
    @ApiModelProperty("职员标识")
    private String pfempid;
    /**
     * 家庭地址
     */
    @TableField(value = "homeaddr")
    @JSONField(name = "homeaddr")
    @JsonProperty("homeaddr")
    @ApiModelProperty("家庭地址")
    private String homeaddr;
    /**
     * 职员信息
     */
    @TableField(exist = false)
    @JSONField(name = "empinfo")
    @JsonProperty("empinfo")
    @ApiModelProperty("职员信息")
    private String empinfo;
    /**
     * 部门
     */
    @TableField(exist = false)
    @JSONField(name = "deptid")
    @JsonProperty("deptid")
    @ApiModelProperty("部门")
    private String deptid;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @ApiModelProperty("建立人")
    private String createman;
    /**
     * 性别
     */
    @TableField(value = "empsex")
    @JSONField(name = "empsex")
    @JsonProperty("empsex")
    @ApiModelProperty("性别")
    private String empsex;
    /**
     * 证件号码
     */
    @TableField(value = "certcode")
    @JSONField(name = "certcode")
    @JsonProperty("certcode")
    @ApiModelProperty("证件号码")
    private String certcode;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @ApiModelProperty("组织")
    private String orgid;
    /**
     * 职员代码
     */
    @TableField(value = "empcode")
    @JSONField(name = "empcode")
    @JsonProperty("empcode")
    @ApiModelProperty("职员代码")
    private String empcode;
    /**
     * 口令
     */
    @TableField(value = "psw")
    @JSONField(name = "psw")
    @JsonProperty("psw")
    @ApiModelProperty("口令")
    private String psw;
    /**
     * 班组
     */
    @TableField(exist = false)
    @JSONField(name = "teamid")
    @JsonProperty("teamid")
    @ApiModelProperty("班组")
    private String teamid;
    /**
     * 主部门代码
     */
    @TableField(value = "maindeptcode")
    @JSONField(name = "maindeptcode")
    @JsonProperty("maindeptcode")
    @ApiModelProperty("主部门代码")
    private String maindeptcode;
    /**
     * 电子邮件
     */
    @DEField(name = "e_mail")
    @TableField(value = "e_mail")
    @JSONField(name = "e_mail")
    @JsonProperty("e_mail")
    @ApiModelProperty("电子邮件")
    private String eMail;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;
    /**
     * 移动电话
     */
    @TableField(value = "cell")
    @JSONField(name = "cell")
    @JsonProperty("cell")
    @ApiModelProperty("移动电话")
    private String cell;
    /**
     * 联系地址
     */
    @TableField(value = "addr")
    @JSONField(name = "addr")
    @JsonProperty("addr")
    @ApiModelProperty("联系地址")
    private String addr;
    /**
     * 主班组
     */
    @TableField(exist = false)
    @JSONField(name = "majorteamname")
    @JsonProperty("majorteamname")
    @ApiModelProperty("主班组")
    private String majorteamname;
    /**
     * 主班组
     */
    @TableField(value = "majorteamid")
    @JSONField(name = "majorteamid")
    @JsonProperty("majorteamid")
    @ApiModelProperty("主班组")
    private String majorteamid;
    /**
     * 主部门
     */
    @TableField(value = "majordeptid")
    @JSONField(name = "majordeptid")
    @JsonProperty("majordeptid")
    @ApiModelProperty("主部门")
    private String majordeptid;
    /**
     * 主部门
     */
    @TableField(exist = false)
    @JSONField(name = "majordeptname")
    @JsonProperty("majordeptname")
    @ApiModelProperty("主部门")
    private String majordeptname;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFDept pfdeptmp;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFTeam majorteam;



    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [联系电话]
     */
    public void setTel(String tel) {
        this.tel = tel;
        this.modify("tel", tel);
    }

    /**
     * 设置 [工作日期]
     */
    public void setWorkdate(Timestamp workdate) {
        this.workdate = workdate;
        this.modify("workdate", workdate);
    }

    /**
     * 格式化日期 [工作日期]
     */
    public String formatWorkdate() {
        if (this.workdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(workdate);
    }
    /**
     * 设置 [职员名称]
     */
    public void setPfempname(String pfempname) {
        this.pfempname = pfempname;
        this.modify("pfempname", pfempname);
    }

    /**
     * 设置 [入本企业日期]
     */
    public void setRaisedate(Timestamp raisedate) {
        this.raisedate = raisedate;
        this.modify("raisedate", raisedate);
    }

    /**
     * 格式化日期 [入本企业日期]
     */
    public String formatRaisedate() {
        if (this.raisedate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(raisedate);
    }
    /**
     * 设置 [家庭电话]
     */
    public void setHometel(String hometel) {
        this.hometel = hometel;
        this.modify("hometel", hometel);
    }

    /**
     * 设置 [出生日期]
     */
    public void setBirthday(Timestamp birthday) {
        this.birthday = birthday;
        this.modify("birthday", birthday);
    }

    /**
     * 格式化日期 [出生日期]
     */
    public String formatBirthday() {
        if (this.birthday == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(birthday);
    }
    /**
     * 设置 [家庭地址]
     */
    public void setHomeaddr(String homeaddr) {
        this.homeaddr = homeaddr;
        this.modify("homeaddr", homeaddr);
    }

    /**
     * 设置 [性别]
     */
    public void setEmpsex(String empsex) {
        this.empsex = empsex;
        this.modify("empsex", empsex);
    }

    /**
     * 设置 [证件号码]
     */
    public void setCertcode(String certcode) {
        this.certcode = certcode;
        this.modify("certcode", certcode);
    }

    /**
     * 设置 [职员代码]
     */
    public void setEmpcode(String empcode) {
        this.empcode = empcode;
        this.modify("empcode", empcode);
    }

    /**
     * 设置 [口令]
     */
    public void setPsw(String psw) {
        this.psw = psw;
        this.modify("psw", psw);
    }

    /**
     * 设置 [主部门代码]
     */
    public void setMaindeptcode(String maindeptcode) {
        this.maindeptcode = maindeptcode;
        this.modify("maindeptcode", maindeptcode);
    }

    /**
     * 设置 [电子邮件]
     */
    public void setEMail(String eMail) {
        this.eMail = eMail;
        this.modify("e_mail", eMail);
    }

    /**
     * 设置 [移动电话]
     */
    public void setCell(String cell) {
        this.cell = cell;
        this.modify("cell", cell);
    }

    /**
     * 设置 [联系地址]
     */
    public void setAddr(String addr) {
        this.addr = addr;
        this.modify("addr", addr);
    }

    /**
     * 设置 [主班组]
     */
    public void setMajorteamid(String majorteamid) {
        this.majorteamid = majorteamid;
        this.modify("majorteamid", majorteamid);
    }

    /**
     * 设置 [主部门]
     */
    public void setMajordeptid(String majordeptid) {
        this.majordeptid = majordeptid;
        this.modify("majordeptid", majordeptid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("pfempid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


