package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[能耗登记工单]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMWO_EN_BASE", resultMap = "EMWO_ENResultMap")
@ApiModel("能耗登记工单")
public class EMWO_EN extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;
    /**
     * 工单组
     */
    @TableField(exist = false)
    @JSONField(name = "woteam")
    @JsonProperty("woteam")
    @ApiModelProperty("工单组")
    private String woteam;
    /**
     * 工作流状态
     */
    @TableField(value = "wfstate")
    @JSONField(name = "wfstate")
    @JsonProperty("wfstate")
    @ApiModelProperty("工作流状态")
    private Integer wfstate;
    /**
     * 实际抄表时间
     */
    @TableField(value = "regionbegindate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "regionbegindate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("regionbegindate")
    @ApiModelProperty("实际抄表时间")
    private Timestamp regionbegindate;
    /**
     * 过期日期
     */
    @TableField(value = "expiredate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "expiredate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("expiredate")
    @ApiModelProperty("过期日期")
    private Timestamp expiredate;
    /**
     * 流程步骤
     */
    @TableField(value = "wfstep")
    @JSONField(name = "wfstep")
    @JsonProperty("wfstep")
    @ApiModelProperty("流程步骤")
    private String wfstep;
    /**
     * 预算(￥)
     */
    @TableField(value = "prefee")
    @JSONField(name = "prefee")
    @JsonProperty("prefee")
    @ApiModelProperty("预算(￥)")
    private Double prefee;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @ApiModelProperty("更新人")
    private String updateman;
    /**
     * 详细内容
     */
    @TableField(value = "content")
    @JSONField(name = "content")
    @JsonProperty("content")
    @ApiModelProperty("详细内容")
    private String content;
    /**
     * 优先级
     */
    @DEField(defaultValue = "2")
    @TableField(value = "priority")
    @JSONField(name = "priority")
    @JsonProperty("priority")
    @ApiModelProperty("优先级")
    private String priority;
    /**
     * 制定时间
     */
    @TableField(value = "mdate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "mdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("mdate")
    @ApiModelProperty("制定时间")
    private Timestamp mdate;
    /**
     * 工单名称
     */
    @DEField(name = "emwo_enname")
    @TableField(value = "emwo_enname")
    @JSONField(name = "emwo_enname")
    @JsonProperty("emwo_enname")
    @ApiModelProperty("工单名称")
    private String emwoEnname;
    /**
     * 值
     */
    @TableField(value = "val")
    @JSONField(name = "val")
    @JsonProperty("val")
    @ApiModelProperty("值")
    private String val;
    /**
     * 结束时间
     */
    @TableField(value = "regionenddate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "regionenddate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("regionenddate")
    @ApiModelProperty("结束时间")
    private Timestamp regionenddate;
    /**
     * 执行日期
     */
    @TableField(value = "wodate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "wodate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("wodate")
    @ApiModelProperty("执行日期")
    private Timestamp wodate;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @ApiModelProperty("建立人")
    private String createman;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    @ApiModelProperty("描述")
    private String description;
    /**
     * 持续时间(H)
     */
    @DEField(defaultValue = "8")
    @TableField(value = "activelengths")
    @JSONField(name = "activelengths")
    @JsonProperty("activelengths")
    @ApiModelProperty("持续时间(H)")
    private Double activelengths;
    /**
     * 倍率
     */
    @DEField(defaultValue = "1")
    @TableField(value = "vrate")
    @JSONField(name = "vrate")
    @JsonProperty("vrate")
    @ApiModelProperty("倍率")
    private Double vrate;
    /**
     * 本次记录值
     */
    @TableField(value = "curval")
    @JSONField(name = "curval")
    @JsonProperty("curval")
    @ApiModelProperty("本次记录值")
    private Double curval;
    /**
     * 工单状态
     */
    @DEField(defaultValue = "0")
    @TableField(value = "wostate")
    @JSONField(name = "wostate")
    @JsonProperty("wostate")
    @ApiModelProperty("工单状态")
    private Integer wostate;
    /**
     * 实际工时(分)
     */
    @DEField(defaultValue = "0")
    @TableField(value = "worklength")
    @JSONField(name = "worklength")
    @JsonProperty("worklength")
    @ApiModelProperty("实际工时(分)")
    private Double worklength;
    /**
     * 工单分组
     */
    @DEField(defaultValue = "TASK")
    @TableField(value = "wogroup")
    @JSONField(name = "wogroup")
    @JsonProperty("wogroup")
    @ApiModelProperty("工单分组")
    private String wogroup;
    /**
     * 工单分组
     */
    @TableField(exist = false)
    @JSONField(name = "emwotype")
    @JsonProperty("emwotype")
    @ApiModelProperty("工单分组")
    private String emwotype;
    /**
     * 工单编号
     */
    @DEField(name = "emwo_enid", isKeyField = true)
    @TableId(value = "emwo_enid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emwo_enid")
    @JsonProperty("emwo_enid")
    @ApiModelProperty("工单编号")
    private String emwoEnid;
    /**
     * 上次记录值
     */
    @TableField(value = "lastval")
    @JSONField(name = "lastval")
    @JsonProperty("lastval")
    @ApiModelProperty("上次记录值")
    private Double lastval;
    /**
     * 工作流实例
     */
    @TableField(value = "wfinstanceid")
    @JSONField(name = "wfinstanceid")
    @JsonProperty("wfinstanceid")
    @ApiModelProperty("工作流实例")
    private String wfinstanceid;
    /**
     * 工单组
     */
    @TableField(exist = false)
    @JSONField(name = "woteam_show")
    @JsonProperty("woteam_show")
    @ApiModelProperty("工单组")
    private String woteamShow;
    /**
     * 上次采集时间
     */
    @TableField(value = "bdate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "bdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("bdate")
    @ApiModelProperty("上次采集时间")
    private Timestamp bdate;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;
    /**
     * 工单类型
     */
    @DEField(defaultValue = "EN")
    @TableField(value = "wotype")
    @JSONField(name = "wotype")
    @JsonProperty("wotype")
    @ApiModelProperty("工单类型")
    private String wotype;
    /**
     * 停运时间(分)
     */
    @TableField(value = "eqstoplength")
    @JSONField(name = "eqstoplength")
    @JsonProperty("eqstoplength")
    @ApiModelProperty("停运时间(分)")
    private Double eqstoplength;
    /**
     * 执行结果
     */
    @TableField(value = "wresult")
    @JSONField(name = "wresult")
    @JsonProperty("wresult")
    @ApiModelProperty("执行结果")
    private String wresult;
    /**
     * 能耗值
     */
    @TableField(value = "nval")
    @JSONField(name = "nval")
    @JsonProperty("nval")
    @ApiModelProperty("能耗值")
    private Double nval;
    /**
     * 归档
     */
    @TableField(value = "archive")
    @JSONField(name = "archive")
    @JsonProperty("archive")
    @ApiModelProperty("归档")
    private String archive;
    /**
     * 工单内容
     */
    @TableField(value = "wodesc")
    @JSONField(name = "wodesc")
    @JsonProperty("wodesc")
    @ApiModelProperty("工单内容")
    private String wodesc;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @ApiModelProperty("组织")
    private String orgid;
    /**
     * 模式
     */
    @TableField(exist = false)
    @JSONField(name = "rfomoname")
    @JsonProperty("rfomoname")
    @ApiModelProperty("模式")
    private String rfomoname;
    /**
     * 方案
     */
    @TableField(exist = false)
    @JSONField(name = "rfoacname")
    @JsonProperty("rfoacname")
    @ApiModelProperty("方案")
    private String rfoacname;
    /**
     * 责任班组
     */
    @TableField(exist = false)
    @JSONField(name = "rteamname")
    @JsonProperty("rteamname")
    @ApiModelProperty("责任班组")
    private String rteamname;
    /**
     * 原因
     */
    @TableField(exist = false)
    @JSONField(name = "rfocaname")
    @JsonProperty("rfocaname")
    @ApiModelProperty("原因")
    private String rfocaname;
    /**
     * 服务商
     */
    @TableField(exist = false)
    @JSONField(name = "rservicename")
    @JsonProperty("rservicename")
    @ApiModelProperty("服务商")
    private String rservicename;
    /**
     * 测点类型
     */
    @TableField(exist = false)
    @JSONField(name = "dptype")
    @JsonProperty("dptype")
    @ApiModelProperty("测点类型")
    private String dptype;
    /**
     * 位置
     */
    @TableField(exist = false)
    @JSONField(name = "objname")
    @JsonProperty("objname")
    @ApiModelProperty("位置")
    private String objname;
    /**
     * 上级工单
     */
    @TableField(exist = false)
    @JSONField(name = "wopname_show")
    @JsonProperty("wopname_show")
    @ApiModelProperty("上级工单")
    private String wopnameShow;
    /**
     * 上级工单
     */
    @TableField(exist = false)
    @JSONField(name = "wopname")
    @JsonProperty("wopname")
    @ApiModelProperty("上级工单")
    private String wopname;
    /**
     * 总帐科目
     */
    @TableField(exist = false)
    @JSONField(name = "acclassname")
    @JsonProperty("acclassname")
    @ApiModelProperty("总帐科目")
    private String acclassname;
    /**
     * 现象
     */
    @TableField(exist = false)
    @JSONField(name = "rfodename")
    @JsonProperty("rfodename")
    @ApiModelProperty("现象")
    private String rfodename;
    /**
     * 能源
     */
    @TableField(exist = false)
    @JSONField(name = "dpname")
    @JsonProperty("dpname")
    @ApiModelProperty("能源")
    private String dpname;
    /**
     * 工单来源
     */
    @TableField(exist = false)
    @JSONField(name = "wooriname")
    @JsonProperty("wooriname")
    @ApiModelProperty("工单来源")
    private String wooriname;
    /**
     * 来源类型
     */
    @TableField(exist = false)
    @JSONField(name = "wooritype")
    @JsonProperty("wooritype")
    @ApiModelProperty("来源类型")
    private String wooritype;
    /**
     * 设备
     */
    @TableField(exist = false)
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    @ApiModelProperty("设备")
    private String equipname;
    /**
     * 能源
     */
    @TableField(value = "dpid")
    @JSONField(name = "dpid")
    @JsonProperty("dpid")
    @ApiModelProperty("能源")
    private String dpid;
    /**
     * 位置
     */
    @TableField(value = "objid")
    @JSONField(name = "objid")
    @JsonProperty("objid")
    @ApiModelProperty("位置")
    private String objid;
    /**
     * 服务商
     */
    @TableField(value = "rserviceid")
    @JSONField(name = "rserviceid")
    @JsonProperty("rserviceid")
    @ApiModelProperty("服务商")
    private String rserviceid;
    /**
     * 设备
     */
    @TableField(value = "equipid")
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    @ApiModelProperty("设备")
    private String equipid;
    /**
     * 工单来源
     */
    @TableField(value = "wooriid")
    @JSONField(name = "wooriid")
    @JsonProperty("wooriid")
    @ApiModelProperty("工单来源")
    private String wooriid;
    /**
     * 总帐科目
     */
    @TableField(value = "acclassid")
    @JSONField(name = "acclassid")
    @JsonProperty("acclassid")
    @ApiModelProperty("总帐科目")
    private String acclassid;
    /**
     * 模式
     */
    @TableField(value = "rfomoid")
    @JSONField(name = "rfomoid")
    @JsonProperty("rfomoid")
    @ApiModelProperty("模式")
    private String rfomoid;
    /**
     * 责任班组
     */
    @TableField(value = "rteamid")
    @JSONField(name = "rteamid")
    @JsonProperty("rteamid")
    @ApiModelProperty("责任班组")
    private String rteamid;
    /**
     * 原因
     */
    @TableField(value = "rfocaid")
    @JSONField(name = "rfocaid")
    @JsonProperty("rfocaid")
    @ApiModelProperty("原因")
    private String rfocaid;
    /**
     * 现象
     */
    @TableField(value = "rfodeid")
    @JSONField(name = "rfodeid")
    @JsonProperty("rfodeid")
    @ApiModelProperty("现象")
    private String rfodeid;
    /**
     * 上级工单
     */
    @TableField(value = "wopid")
    @JSONField(name = "wopid")
    @JsonProperty("wopid")
    @ApiModelProperty("上级工单")
    private String wopid;
    /**
     * 方案
     */
    @TableField(value = "rfoacid")
    @JSONField(name = "rfoacid")
    @JsonProperty("rfoacid")
    @ApiModelProperty("方案")
    private String rfoacid;
    /**
     * 制定人
     */
    @TableField(value = "mpersonid")
    @JSONField(name = "mpersonid")
    @JsonProperty("mpersonid")
    @ApiModelProperty("制定人")
    private String mpersonid;
    /**
     * 制定人
     */
    @TableField(exist = false)
    @JSONField(name = "mpersonname")
    @JsonProperty("mpersonname")
    @ApiModelProperty("制定人")
    private String mpersonname;
    /**
     * 责任部门
     */
    @TableField(value = "rdeptid")
    @JSONField(name = "rdeptid")
    @JsonProperty("rdeptid")
    @ApiModelProperty("责任部门")
    private String rdeptid;
    /**
     * 责任部门
     */
    @TableField(exist = false)
    @JSONField(name = "rdeptname")
    @JsonProperty("rdeptname")
    @ApiModelProperty("责任部门")
    private String rdeptname;
    /**
     * 抄表人
     */
    @TableField(value = "wpersonid")
    @JSONField(name = "wpersonid")
    @JsonProperty("wpersonid")
    @ApiModelProperty("抄表人")
    private String wpersonid;
    /**
     * 抄表人
     */
    @TableField(exist = false)
    @JSONField(name = "wpersonname")
    @JsonProperty("wpersonname")
    @ApiModelProperty("抄表人")
    private String wpersonname;
    /**
     * 责任人
     */
    @TableField(value = "rempid")
    @JSONField(name = "rempid")
    @JsonProperty("rempid")
    @ApiModelProperty("责任人")
    private String rempid;
    /**
     * 责任人
     */
    @TableField(exist = false)
    @JSONField(name = "rempname")
    @JsonProperty("rempname")
    @ApiModelProperty("责任人")
    private String rempname;
    /**
     * 指派抄表人
     */
    @TableField(value = "recvpersonid")
    @JSONField(name = "recvpersonid")
    @JsonProperty("recvpersonid")
    @ApiModelProperty("指派抄表人")
    private String recvpersonid;
    /**
     * 指派抄表人
     */
    @TableField(exist = false)
    @JSONField(name = "recvpersonname")
    @JsonProperty("recvpersonname")
    @ApiModelProperty("指派抄表人")
    private String recvpersonname;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMACClass acclass;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEquip equip;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMObject dp;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMObject obj;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMRFOAC rfoac;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMRFOCA rfoca;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMRFODE rfode;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMRFOMO rfomo;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMService rservice;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMWOORI woori;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMWO wop;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFDept pfdeptid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp mpfempid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp pferecvpersonid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp pferempid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfewpersonid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFTeam rteam;



    /**
     * 设置 [工作流状态]
     */
    public void setWfstate(Integer wfstate) {
        this.wfstate = wfstate;
        this.modify("wfstate", wfstate);
    }

    /**
     * 设置 [实际抄表时间]
     */
    public void setRegionbegindate(Timestamp regionbegindate) {
        this.regionbegindate = regionbegindate;
        this.modify("regionbegindate", regionbegindate);
    }

    /**
     * 格式化日期 [实际抄表时间]
     */
    public String formatRegionbegindate() {
        if (this.regionbegindate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(regionbegindate);
    }
    /**
     * 设置 [过期日期]
     */
    public void setExpiredate(Timestamp expiredate) {
        this.expiredate = expiredate;
        this.modify("expiredate", expiredate);
    }

    /**
     * 格式化日期 [过期日期]
     */
    public String formatExpiredate() {
        if (this.expiredate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(expiredate);
    }
    /**
     * 设置 [流程步骤]
     */
    public void setWfstep(String wfstep) {
        this.wfstep = wfstep;
        this.modify("wfstep", wfstep);
    }

    /**
     * 设置 [预算(￥)]
     */
    public void setPrefee(Double prefee) {
        this.prefee = prefee;
        this.modify("prefee", prefee);
    }

    /**
     * 设置 [详细内容]
     */
    public void setContent(String content) {
        this.content = content;
        this.modify("content", content);
    }

    /**
     * 设置 [优先级]
     */
    public void setPriority(String priority) {
        this.priority = priority;
        this.modify("priority", priority);
    }

    /**
     * 设置 [制定时间]
     */
    public void setMdate(Timestamp mdate) {
        this.mdate = mdate;
        this.modify("mdate", mdate);
    }

    /**
     * 格式化日期 [制定时间]
     */
    public String formatMdate() {
        if (this.mdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(mdate);
    }
    /**
     * 设置 [工单名称]
     */
    public void setEmwoEnname(String emwoEnname) {
        this.emwoEnname = emwoEnname;
        this.modify("emwo_enname", emwoEnname);
    }

    /**
     * 设置 [值]
     */
    public void setVal(String val) {
        this.val = val;
        this.modify("val", val);
    }

    /**
     * 设置 [结束时间]
     */
    public void setRegionenddate(Timestamp regionenddate) {
        this.regionenddate = regionenddate;
        this.modify("regionenddate", regionenddate);
    }

    /**
     * 格式化日期 [结束时间]
     */
    public String formatRegionenddate() {
        if (this.regionenddate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(regionenddate);
    }
    /**
     * 设置 [执行日期]
     */
    public void setWodate(Timestamp wodate) {
        this.wodate = wodate;
        this.modify("wodate", wodate);
    }

    /**
     * 格式化日期 [执行日期]
     */
    public String formatWodate() {
        if (this.wodate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(wodate);
    }
    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [持续时间(H)]
     */
    public void setActivelengths(Double activelengths) {
        this.activelengths = activelengths;
        this.modify("activelengths", activelengths);
    }

    /**
     * 设置 [倍率]
     */
    public void setVrate(Double vrate) {
        this.vrate = vrate;
        this.modify("vrate", vrate);
    }

    /**
     * 设置 [本次记录值]
     */
    public void setCurval(Double curval) {
        this.curval = curval;
        this.modify("curval", curval);
    }

    /**
     * 设置 [工单状态]
     */
    public void setWostate(Integer wostate) {
        this.wostate = wostate;
        this.modify("wostate", wostate);
    }

    /**
     * 设置 [实际工时(分)]
     */
    public void setWorklength(Double worklength) {
        this.worklength = worklength;
        this.modify("worklength", worklength);
    }

    /**
     * 设置 [工单分组]
     */
    public void setWogroup(String wogroup) {
        this.wogroup = wogroup;
        this.modify("wogroup", wogroup);
    }

    /**
     * 设置 [上次记录值]
     */
    public void setLastval(Double lastval) {
        this.lastval = lastval;
        this.modify("lastval", lastval);
    }

    /**
     * 设置 [工作流实例]
     */
    public void setWfinstanceid(String wfinstanceid) {
        this.wfinstanceid = wfinstanceid;
        this.modify("wfinstanceid", wfinstanceid);
    }

    /**
     * 设置 [上次采集时间]
     */
    public void setBdate(Timestamp bdate) {
        this.bdate = bdate;
        this.modify("bdate", bdate);
    }

    /**
     * 格式化日期 [上次采集时间]
     */
    public String formatBdate() {
        if (this.bdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(bdate);
    }
    /**
     * 设置 [工单类型]
     */
    public void setWotype(String wotype) {
        this.wotype = wotype;
        this.modify("wotype", wotype);
    }

    /**
     * 设置 [停运时间(分)]
     */
    public void setEqstoplength(Double eqstoplength) {
        this.eqstoplength = eqstoplength;
        this.modify("eqstoplength", eqstoplength);
    }

    /**
     * 设置 [执行结果]
     */
    public void setWresult(String wresult) {
        this.wresult = wresult;
        this.modify("wresult", wresult);
    }

    /**
     * 设置 [能耗值]
     */
    public void setNval(Double nval) {
        this.nval = nval;
        this.modify("nval", nval);
    }

    /**
     * 设置 [归档]
     */
    public void setArchive(String archive) {
        this.archive = archive;
        this.modify("archive", archive);
    }

    /**
     * 设置 [工单内容]
     */
    public void setWodesc(String wodesc) {
        this.wodesc = wodesc;
        this.modify("wodesc", wodesc);
    }

    /**
     * 设置 [能源]
     */
    public void setDpid(String dpid) {
        this.dpid = dpid;
        this.modify("dpid", dpid);
    }

    /**
     * 设置 [位置]
     */
    public void setObjid(String objid) {
        this.objid = objid;
        this.modify("objid", objid);
    }

    /**
     * 设置 [服务商]
     */
    public void setRserviceid(String rserviceid) {
        this.rserviceid = rserviceid;
        this.modify("rserviceid", rserviceid);
    }

    /**
     * 设置 [设备]
     */
    public void setEquipid(String equipid) {
        this.equipid = equipid;
        this.modify("equipid", equipid);
    }

    /**
     * 设置 [工单来源]
     */
    public void setWooriid(String wooriid) {
        this.wooriid = wooriid;
        this.modify("wooriid", wooriid);
    }

    /**
     * 设置 [总帐科目]
     */
    public void setAcclassid(String acclassid) {
        this.acclassid = acclassid;
        this.modify("acclassid", acclassid);
    }

    /**
     * 设置 [模式]
     */
    public void setRfomoid(String rfomoid) {
        this.rfomoid = rfomoid;
        this.modify("rfomoid", rfomoid);
    }

    /**
     * 设置 [责任班组]
     */
    public void setRteamid(String rteamid) {
        this.rteamid = rteamid;
        this.modify("rteamid", rteamid);
    }

    /**
     * 设置 [原因]
     */
    public void setRfocaid(String rfocaid) {
        this.rfocaid = rfocaid;
        this.modify("rfocaid", rfocaid);
    }

    /**
     * 设置 [现象]
     */
    public void setRfodeid(String rfodeid) {
        this.rfodeid = rfodeid;
        this.modify("rfodeid", rfodeid);
    }

    /**
     * 设置 [上级工单]
     */
    public void setWopid(String wopid) {
        this.wopid = wopid;
        this.modify("wopid", wopid);
    }

    /**
     * 设置 [方案]
     */
    public void setRfoacid(String rfoacid) {
        this.rfoacid = rfoacid;
        this.modify("rfoacid", rfoacid);
    }

    /**
     * 设置 [制定人]
     */
    public void setMpersonid(String mpersonid) {
        this.mpersonid = mpersonid;
        this.modify("mpersonid", mpersonid);
    }

    /**
     * 设置 [责任部门]
     */
    public void setRdeptid(String rdeptid) {
        this.rdeptid = rdeptid;
        this.modify("rdeptid", rdeptid);
    }

    /**
     * 设置 [抄表人]
     */
    public void setWpersonid(String wpersonid) {
        this.wpersonid = wpersonid;
        this.modify("wpersonid", wpersonid);
    }

    /**
     * 设置 [责任人]
     */
    public void setRempid(String rempid) {
        this.rempid = rempid;
        this.modify("rempid", rempid);
    }

    /**
     * 设置 [指派抄表人]
     */
    public void setRecvpersonid(String recvpersonid) {
        this.recvpersonid = recvpersonid;
        this.modify("recvpersonid", recvpersonid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emwo_enid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


