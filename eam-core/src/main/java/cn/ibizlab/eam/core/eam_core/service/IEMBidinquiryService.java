package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMBidinquiry;
import cn.ibizlab.eam.core.eam_core.filter.EMBidinquirySearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMBidinquiry] 服务对象接口
 */
public interface IEMBidinquiryService extends IService<EMBidinquiry> {

    boolean create(EMBidinquiry et);
    void createBatch(List<EMBidinquiry> list);
    boolean update(EMBidinquiry et);
    void updateBatch(List<EMBidinquiry> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMBidinquiry get(String key);
    EMBidinquiry getDraft(EMBidinquiry et);
    boolean checkKey(EMBidinquiry et);
    boolean save(EMBidinquiry et);
    void saveBatch(List<EMBidinquiry> list);
    Page<EMBidinquiry> searchDefault(EMBidinquirySearchContext context);
    List<EMBidinquiry> selectByEmpurplanid(String empurplanid);
    void removeByEmpurplanid(String empurplanid);
    List<EMBidinquiry> selectByEmserviceid(String emserviceid);
    void removeByEmserviceid(String emserviceid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMBidinquiry> getEmbidinquiryByIds(List<String> ids);
    List<EMBidinquiry> getEmbidinquiryByEntities(List<EMBidinquiry> entities);
}


