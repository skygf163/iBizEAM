package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[资产]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMASSET_BASE", resultMap = "EMAssetResultMap")
@ApiModel("资产")
public class EMAsset extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 第几号
     */
    @TableField(value = "num")
    @JSONField(name = "num")
    @JsonProperty("num")
    @ApiModelProperty("第几号")
    private String num;
    /**
     * 是否停机
     */
    @TableField(value = "eqisservice")
    @JSONField(name = "eqisservice")
    @JsonProperty("eqisservice")
    @ApiModelProperty("是否停机")
    private Integer eqisservice;
    /**
     * 使用人
     */
    @TableField(value = "empname")
    @JSONField(name = "empname")
    @JsonProperty("empname")
    @ApiModelProperty("使用人")
    private String empname;
    /**
     * 保修日期
     */
    @TableField(value = "warrantydate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "warrantydate", format = "yyyy-MM-dd")
    @JsonProperty("warrantydate")
    @ApiModelProperty("保修日期")
    private Timestamp warrantydate;
    /**
     * 内部总成本
     */
    @TableField(value = "innerlaborcost")
    @JSONField(name = "innerlaborcost")
    @JsonProperty("innerlaborcost")
    @ApiModelProperty("内部总成本")
    private String innerlaborcost;
    /**
     * 设备代码
     */
    @TableField(value = "keyattparam")
    @JSONField(name = "keyattparam")
    @JsonProperty("keyattparam")
    @ApiModelProperty("设备代码")
    private String keyattparam;
    /**
     * 外部总成本
     */
    @TableField(value = "foreignlaborcost")
    @JSONField(name = "foreignlaborcost")
    @JsonProperty("foreignlaborcost")
    @ApiModelProperty("外部总成本")
    private String foreignlaborcost;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @ApiModelProperty("建立人")
    private String createman;
    /**
     * 集团设备编码
     */
    @TableField(value = "jtsb")
    @JSONField(name = "jtsb")
    @JsonProperty("jtsb")
    @ApiModelProperty("集团设备编码")
    private String jtsb;
    /**
     * 使用期限
     */
    @TableField(value = "eqlife")
    @JSONField(name = "eqlife")
    @JsonProperty("eqlife")
    @ApiModelProperty("使用期限")
    private Double eqlife;
    /**
     * 最后折旧日期
     */
    @TableField(value = "lastzjdate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "lastzjdate", format = "yyyy-MM-dd")
    @JsonProperty("lastzjdate")
    @ApiModelProperty("最后折旧日期")
    private Timestamp lastzjdate;
    /**
     * 使用部门
     */
    @TableField(value = "deptname")
    @JSONField(name = "deptname")
    @JsonProperty("deptname")
    @ApiModelProperty("使用部门")
    private String deptname;
    /**
     * 资产类别
     */
    @TableField(value = "assettype")
    @JSONField(name = "assettype")
    @JsonProperty("assettype")
    @ApiModelProperty("资产类别")
    private String assettype;
    /**
     * 资产余值
     */
    @TableField(exist = false)
    @JSONField(name = "now")
    @JsonProperty("now")
    @ApiModelProperty("资产余值")
    private Double now;
    /**
     * 已提折旧
     */
    @TableField(value = "ytzj")
    @JSONField(name = "ytzj")
    @JsonProperty("ytzj")
    @ApiModelProperty("已提折旧")
    private Double ytzj;
    /**
     * 报废日期
     */
    @TableField(value = "disdate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "disdate", format = "yyyy-MM-dd")
    @JsonProperty("disdate")
    @ApiModelProperty("报废日期")
    private Timestamp disdate;
    /**
     * 资产状态
     */
    @TableField(value = "assetstate")
    @JSONField(name = "assetstate")
    @JsonProperty("assetstate")
    @ApiModelProperty("资产状态")
    private String assetstate;
    /**
     * 资产原值
     */
    @TableField(value = "originalcost")
    @JSONField(name = "originalcost")
    @JsonProperty("originalcost")
    @ApiModelProperty("资产原值")
    private String originalcost;
    /**
     * 使用部门
     */
    @TableField(value = "deptid")
    @JSONField(name = "deptid")
    @JsonProperty("deptid")
    @ApiModelProperty("使用部门")
    private String deptid;
    /**
     * 工艺编号
     */
    @TableField(value = "techcode")
    @JSONField(name = "techcode")
    @JsonProperty("techcode")
    @ApiModelProperty("工艺编号")
    private String techcode;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @ApiModelProperty("组织")
    private String orgid;
    /**
     * 材料费
     */
    @TableField(value = "materialcost")
    @JSONField(name = "materialcost")
    @JsonProperty("materialcost")
    @ApiModelProperty("材料费")
    private String materialcost;
    /**
     * 残值
     */
    @TableField(value = "discost")
    @JSONField(name = "discost")
    @JsonProperty("discost")
    @ApiModelProperty("残值")
    private String discost;
    /**
     * 经办人
     */
    @TableField(value = "rempid")
    @JSONField(name = "rempid")
    @JsonProperty("rempid")
    @ApiModelProperty("经办人")
    private String rempid;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    @ApiModelProperty("描述")
    private String description;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @ApiModelProperty("更新人")
    private String updateman;
    /**
     * 已使用年限
     */
    @TableField(exist = false)
    @JSONField(name = "usedyear")
    @JsonProperty("usedyear")
    @ApiModelProperty("已使用年限")
    private Integer usedyear;
    /**
     * 资产名称
     */
    @TableField(value = "emassetname")
    @JSONField(name = "emassetname")
    @JsonProperty("emassetname")
    @ApiModelProperty("资产名称")
    private String emassetname;
    /**
     * 预计残值
     */
    @TableField(exist = false)
    @JSONField(name = "replacecost")
    @JsonProperty("replacecost")
    @ApiModelProperty("预计残值")
    private Double replacecost;
    /**
     * 设备编号
     */
    @TableField(exist = false)
    @JSONField(name = "assetequipid")
    @JsonProperty("assetequipid")
    @ApiModelProperty("设备编号")
    private String assetequipid;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;
    /**
     * 使用人
     */
    @TableField(value = "empid")
    @JSONField(name = "empid")
    @JsonProperty("empid")
    @ApiModelProperty("使用人")
    private String empid;
    /**
     * 使用年限
     */
    @TableField(exist = false)
    @JSONField(name = "eqlifeyear")
    @JsonProperty("eqlifeyear")
    @ApiModelProperty("使用年限")
    private Integer eqlifeyear;
    /**
     * 排序
     */
    @TableField(exist = false)
    @JSONField(name = "assetsort")
    @JsonProperty("assetsort")
    @ApiModelProperty("排序")
    private String assetsort;
    /**
     * 管理部门
     */
    @TableField(value = "mgrdeptid")
    @JSONField(name = "mgrdeptid")
    @JsonProperty("mgrdeptid")
    @ApiModelProperty("管理部门")
    private String mgrdeptid;
    /**
     * 发动机号
     */
    @TableField(value = "blsystemdesc")
    @JSONField(name = "blsystemdesc")
    @JsonProperty("blsystemdesc")
    @ApiModelProperty("发动机号")
    private String blsystemdesc;
    /**
     * 经办人
     */
    @TableField(value = "rempname")
    @JSONField(name = "rempname")
    @JsonProperty("rempname")
    @ApiModelProperty("经办人")
    private String rempname;
    /**
     * 成本中心
     */
    @TableField(value = "costcenterid")
    @JSONField(name = "costcenterid")
    @JsonProperty("costcenterid")
    @ApiModelProperty("成本中心")
    private String costcenterid;
    /**
     * 采购日期
     */
    @TableField(value = "purchdate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "purchdate", format = "yyyy-MM-dd")
    @JsonProperty("purchdate")
    @ApiModelProperty("采购日期")
    private Timestamp purchdate;
    /**
     * 投运日期
     */
    @TableField(value = "eqstartdate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "eqstartdate", format = "yyyy-MM-dd")
    @JsonProperty("eqstartdate")
    @ApiModelProperty("投运日期")
    private Timestamp eqstartdate;
    /**
     * 优先级
     */
    @TableField(value = "eqpriority")
    @JSONField(name = "eqpriority")
    @JsonProperty("eqpriority")
    @ApiModelProperty("优先级")
    private Double eqpriority;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;
    /**
     * 报废原因
     */
    @TableField(value = "disdesc")
    @JSONField(name = "disdesc")
    @JsonProperty("disdesc")
    @ApiModelProperty("报废原因")
    private String disdesc;
    /**
     * 剩余期限
     */
    @TableField(exist = false)
    @JSONField(name = "syqx")
    @JsonProperty("syqx")
    @ApiModelProperty("剩余期限")
    private String syqx;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;
    /**
     * 残值率(%)
     */
    @DEField(defaultValue = "4")
    @TableField(value = "replacerate")
    @JSONField(name = "replacerate")
    @JsonProperty("replacerate")
    @ApiModelProperty("残值率(%)")
    private Double replacerate;
    /**
     * 资产信息
     */
    @TableField(exist = false)
    @JSONField(name = "assetinfo")
    @JsonProperty("assetinfo")
    @ApiModelProperty("资产信息")
    private String assetinfo;
    /**
     * 资产标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emassetid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emassetid")
    @JsonProperty("emassetid")
    @ApiModelProperty("资产标识")
    private String emassetid;
    /**
     * 合计
     */
    @TableField(exist = false)
    @JSONField(name = "hj")
    @JsonProperty("hj")
    @ApiModelProperty("合计")
    private Double hj;
    /**
     * 资产代码
     */
    @TableField(value = "assetcode")
    @JSONField(name = "assetcode")
    @JsonProperty("assetcode")
    @ApiModelProperty("资产代码")
    private String assetcode;
    /**
     * 资产备注
     */
    @TableField(value = "assetdesc")
    @JSONField(name = "assetdesc")
    @JsonProperty("assetdesc")
    @ApiModelProperty("资产备注")
    private String assetdesc;
    /**
     * 管理部门
     */
    @TableField(value = "mgrdeptname")
    @JSONField(name = "mgrdeptname")
    @JsonProperty("mgrdeptname")
    @ApiModelProperty("管理部门")
    private String mgrdeptname;
    /**
     * 车架号
     */
    @TableField(value = "eqserialcode")
    @JSONField(name = "eqserialcode")
    @JsonProperty("eqserialcode")
    @ApiModelProperty("车架号")
    private String eqserialcode;
    /**
     * 存放地点
     */
    @TableField(value = "assetlct")
    @JSONField(name = "assetlct")
    @JsonProperty("assetlct")
    @ApiModelProperty("存放地点")
    private String assetlct;
    /**
     * 规格型号
     */
    @TableField(value = "eqmodelcode")
    @JSONField(name = "eqmodelcode")
    @JsonProperty("eqmodelcode")
    @ApiModelProperty("规格型号")
    private String eqmodelcode;
    /**
     * 总运行时间
     */
    @TableField(value = "eqsumstoptime")
    @JSONField(name = "eqsumstoptime")
    @JsonProperty("eqsumstoptime")
    @ApiModelProperty("总运行时间")
    private Double eqsumstoptime;
    /**
     * 税费
     */
    @TableField(value = "sf")
    @JSONField(name = "sf")
    @JsonProperty("sf")
    @ApiModelProperty("税费")
    private String sf;
    /**
     * 产地
     */
    @TableField(value = "pplace")
    @JSONField(name = "pplace")
    @JsonProperty("pplace")
    @ApiModelProperty("产地")
    private String pplace;
    /**
     * 产品供应商
     */
    @TableField(exist = false)
    @JSONField(name = "labservicename")
    @JsonProperty("labservicename")
    @ApiModelProperty("产品供应商")
    private String labservicename;
    /**
     * 合同
     */
    @TableField(exist = false)
    @JSONField(name = "contractname")
    @JsonProperty("contractname")
    @ApiModelProperty("合同")
    private String contractname;
    /**
     * 资产科目
     */
    @TableField(exist = false)
    @JSONField(name = "assetclassname")
    @JsonProperty("assetclassname")
    @ApiModelProperty("资产科目")
    private String assetclassname;
    /**
     * 总帐科目
     */
    @TableField(exist = false)
    @JSONField(name = "acclassname")
    @JsonProperty("acclassname")
    @ApiModelProperty("总帐科目")
    private String acclassname;
    /**
     * 位置
     */
    @TableField(exist = false)
    @JSONField(name = "eqlocationname")
    @JsonProperty("eqlocationname")
    @ApiModelProperty("位置")
    private String eqlocationname;
    /**
     * 服务提供商
     */
    @TableField(exist = false)
    @JSONField(name = "rservicename")
    @JsonProperty("rservicename")
    @ApiModelProperty("服务提供商")
    private String rservicename;
    /**
     * 资产科目代码
     */
    @TableField(exist = false)
    @JSONField(name = "assetclasscode")
    @JsonProperty("assetclasscode")
    @ApiModelProperty("资产科目代码")
    private String assetclasscode;
    /**
     * 单位
     */
    @TableField(exist = false)
    @JSONField(name = "unitname")
    @JsonProperty("unitname")
    @ApiModelProperty("单位")
    private String unitname;
    /**
     * 制造商
     */
    @TableField(exist = false)
    @JSONField(name = "mservicename")
    @JsonProperty("mservicename")
    @ApiModelProperty("制造商")
    private String mservicename;
    /**
     * 资产科目
     */
    @TableField(value = "assetclassid")
    @JSONField(name = "assetclassid")
    @JsonProperty("assetclassid")
    @ApiModelProperty("资产科目")
    private String assetclassid;
    /**
     * 总帐科目
     */
    @TableField(value = "acclassid")
    @JSONField(name = "acclassid")
    @JsonProperty("acclassid")
    @ApiModelProperty("总帐科目")
    private String acclassid;
    /**
     * 位置
     */
    @TableField(value = "eqlocationid")
    @JSONField(name = "eqlocationid")
    @JsonProperty("eqlocationid")
    @ApiModelProperty("位置")
    private String eqlocationid;
    /**
     * 单位
     */
    @TableField(value = "unitid")
    @JSONField(name = "unitid")
    @JsonProperty("unitid")
    @ApiModelProperty("单位")
    private String unitid;
    /**
     * 制造商
     */
    @TableField(value = "mserviceid")
    @JSONField(name = "mserviceid")
    @JsonProperty("mserviceid")
    @ApiModelProperty("制造商")
    private String mserviceid;
    /**
     * 产品供应商
     */
    @TableField(value = "labserviceid")
    @JSONField(name = "labserviceid")
    @JsonProperty("labserviceid")
    @ApiModelProperty("产品供应商")
    private String labserviceid;
    /**
     * 服务提供商
     */
    @TableField(value = "rserviceid")
    @JSONField(name = "rserviceid")
    @JsonProperty("rserviceid")
    @ApiModelProperty("服务提供商")
    private String rserviceid;
    /**
     * 合同
     */
    @TableField(value = "contractid")
    @JSONField(name = "contractid")
    @JsonProperty("contractid")
    @ApiModelProperty("合同")
    private String contractid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMACClass acclass;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMAssetClass assetclass;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEQLocation eqlocation;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMService labservice;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMService mservice;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMService rservice;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFContract contract;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFUnit unit;



    /**
     * 设置 [第几号]
     */
    public void setNum(String num) {
        this.num = num;
        this.modify("num", num);
    }

    /**
     * 设置 [是否停机]
     */
    public void setEqisservice(Integer eqisservice) {
        this.eqisservice = eqisservice;
        this.modify("eqisservice", eqisservice);
    }

    /**
     * 设置 [使用人]
     */
    public void setEmpname(String empname) {
        this.empname = empname;
        this.modify("empname", empname);
    }

    /**
     * 设置 [保修日期]
     */
    public void setWarrantydate(Timestamp warrantydate) {
        this.warrantydate = warrantydate;
        this.modify("warrantydate", warrantydate);
    }

    /**
     * 格式化日期 [保修日期]
     */
    public String formatWarrantydate() {
        if (this.warrantydate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(warrantydate);
    }
    /**
     * 设置 [内部总成本]
     */
    public void setInnerlaborcost(String innerlaborcost) {
        this.innerlaborcost = innerlaborcost;
        this.modify("innerlaborcost", innerlaborcost);
    }

    /**
     * 设置 [设备代码]
     */
    public void setKeyattparam(String keyattparam) {
        this.keyattparam = keyattparam;
        this.modify("keyattparam", keyattparam);
    }

    /**
     * 设置 [外部总成本]
     */
    public void setForeignlaborcost(String foreignlaborcost) {
        this.foreignlaborcost = foreignlaborcost;
        this.modify("foreignlaborcost", foreignlaborcost);
    }

    /**
     * 设置 [集团设备编码]
     */
    public void setJtsb(String jtsb) {
        this.jtsb = jtsb;
        this.modify("jtsb", jtsb);
    }

    /**
     * 设置 [使用期限]
     */
    public void setEqlife(Double eqlife) {
        this.eqlife = eqlife;
        this.modify("eqlife", eqlife);
    }

    /**
     * 设置 [最后折旧日期]
     */
    public void setLastzjdate(Timestamp lastzjdate) {
        this.lastzjdate = lastzjdate;
        this.modify("lastzjdate", lastzjdate);
    }

    /**
     * 格式化日期 [最后折旧日期]
     */
    public String formatLastzjdate() {
        if (this.lastzjdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(lastzjdate);
    }
    /**
     * 设置 [使用部门]
     */
    public void setDeptname(String deptname) {
        this.deptname = deptname;
        this.modify("deptname", deptname);
    }

    /**
     * 设置 [资产类别]
     */
    public void setAssettype(String assettype) {
        this.assettype = assettype;
        this.modify("assettype", assettype);
    }

    /**
     * 设置 [已提折旧]
     */
    public void setYtzj(Double ytzj) {
        this.ytzj = ytzj;
        this.modify("ytzj", ytzj);
    }

    /**
     * 设置 [报废日期]
     */
    public void setDisdate(Timestamp disdate) {
        this.disdate = disdate;
        this.modify("disdate", disdate);
    }

    /**
     * 格式化日期 [报废日期]
     */
    public String formatDisdate() {
        if (this.disdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(disdate);
    }
    /**
     * 设置 [资产状态]
     */
    public void setAssetstate(String assetstate) {
        this.assetstate = assetstate;
        this.modify("assetstate", assetstate);
    }

    /**
     * 设置 [资产原值]
     */
    public void setOriginalcost(String originalcost) {
        this.originalcost = originalcost;
        this.modify("originalcost", originalcost);
    }

    /**
     * 设置 [使用部门]
     */
    public void setDeptid(String deptid) {
        this.deptid = deptid;
        this.modify("deptid", deptid);
    }

    /**
     * 设置 [工艺编号]
     */
    public void setTechcode(String techcode) {
        this.techcode = techcode;
        this.modify("techcode", techcode);
    }

    /**
     * 设置 [材料费]
     */
    public void setMaterialcost(String materialcost) {
        this.materialcost = materialcost;
        this.modify("materialcost", materialcost);
    }

    /**
     * 设置 [残值]
     */
    public void setDiscost(String discost) {
        this.discost = discost;
        this.modify("discost", discost);
    }

    /**
     * 设置 [经办人]
     */
    public void setRempid(String rempid) {
        this.rempid = rempid;
        this.modify("rempid", rempid);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [资产名称]
     */
    public void setEmassetname(String emassetname) {
        this.emassetname = emassetname;
        this.modify("emassetname", emassetname);
    }

    /**
     * 设置 [使用人]
     */
    public void setEmpid(String empid) {
        this.empid = empid;
        this.modify("empid", empid);
    }

    /**
     * 设置 [管理部门]
     */
    public void setMgrdeptid(String mgrdeptid) {
        this.mgrdeptid = mgrdeptid;
        this.modify("mgrdeptid", mgrdeptid);
    }

    /**
     * 设置 [发动机号]
     */
    public void setBlsystemdesc(String blsystemdesc) {
        this.blsystemdesc = blsystemdesc;
        this.modify("blsystemdesc", blsystemdesc);
    }

    /**
     * 设置 [经办人]
     */
    public void setRempname(String rempname) {
        this.rempname = rempname;
        this.modify("rempname", rempname);
    }

    /**
     * 设置 [成本中心]
     */
    public void setCostcenterid(String costcenterid) {
        this.costcenterid = costcenterid;
        this.modify("costcenterid", costcenterid);
    }

    /**
     * 设置 [采购日期]
     */
    public void setPurchdate(Timestamp purchdate) {
        this.purchdate = purchdate;
        this.modify("purchdate", purchdate);
    }

    /**
     * 格式化日期 [采购日期]
     */
    public String formatPurchdate() {
        if (this.purchdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(purchdate);
    }
    /**
     * 设置 [投运日期]
     */
    public void setEqstartdate(Timestamp eqstartdate) {
        this.eqstartdate = eqstartdate;
        this.modify("eqstartdate", eqstartdate);
    }

    /**
     * 格式化日期 [投运日期]
     */
    public String formatEqstartdate() {
        if (this.eqstartdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(eqstartdate);
    }
    /**
     * 设置 [优先级]
     */
    public void setEqpriority(Double eqpriority) {
        this.eqpriority = eqpriority;
        this.modify("eqpriority", eqpriority);
    }

    /**
     * 设置 [报废原因]
     */
    public void setDisdesc(String disdesc) {
        this.disdesc = disdesc;
        this.modify("disdesc", disdesc);
    }

    /**
     * 设置 [残值率(%)]
     */
    public void setReplacerate(Double replacerate) {
        this.replacerate = replacerate;
        this.modify("replacerate", replacerate);
    }

    /**
     * 设置 [资产代码]
     */
    public void setAssetcode(String assetcode) {
        this.assetcode = assetcode;
        this.modify("assetcode", assetcode);
    }

    /**
     * 设置 [资产备注]
     */
    public void setAssetdesc(String assetdesc) {
        this.assetdesc = assetdesc;
        this.modify("assetdesc", assetdesc);
    }

    /**
     * 设置 [管理部门]
     */
    public void setMgrdeptname(String mgrdeptname) {
        this.mgrdeptname = mgrdeptname;
        this.modify("mgrdeptname", mgrdeptname);
    }

    /**
     * 设置 [车架号]
     */
    public void setEqserialcode(String eqserialcode) {
        this.eqserialcode = eqserialcode;
        this.modify("eqserialcode", eqserialcode);
    }

    /**
     * 设置 [存放地点]
     */
    public void setAssetlct(String assetlct) {
        this.assetlct = assetlct;
        this.modify("assetlct", assetlct);
    }

    /**
     * 设置 [规格型号]
     */
    public void setEqmodelcode(String eqmodelcode) {
        this.eqmodelcode = eqmodelcode;
        this.modify("eqmodelcode", eqmodelcode);
    }

    /**
     * 设置 [总运行时间]
     */
    public void setEqsumstoptime(Double eqsumstoptime) {
        this.eqsumstoptime = eqsumstoptime;
        this.modify("eqsumstoptime", eqsumstoptime);
    }

    /**
     * 设置 [税费]
     */
    public void setSf(String sf) {
        this.sf = sf;
        this.modify("sf", sf);
    }

    /**
     * 设置 [产地]
     */
    public void setPplace(String pplace) {
        this.pplace = pplace;
        this.modify("pplace", pplace);
    }

    /**
     * 设置 [资产科目]
     */
    public void setAssetclassid(String assetclassid) {
        this.assetclassid = assetclassid;
        this.modify("assetclassid", assetclassid);
    }

    /**
     * 设置 [总帐科目]
     */
    public void setAcclassid(String acclassid) {
        this.acclassid = acclassid;
        this.modify("acclassid", acclassid);
    }

    /**
     * 设置 [位置]
     */
    public void setEqlocationid(String eqlocationid) {
        this.eqlocationid = eqlocationid;
        this.modify("eqlocationid", eqlocationid);
    }

    /**
     * 设置 [单位]
     */
    public void setUnitid(String unitid) {
        this.unitid = unitid;
        this.modify("unitid", unitid);
    }

    /**
     * 设置 [制造商]
     */
    public void setMserviceid(String mserviceid) {
        this.mserviceid = mserviceid;
        this.modify("mserviceid", mserviceid);
    }

    /**
     * 设置 [产品供应商]
     */
    public void setLabserviceid(String labserviceid) {
        this.labserviceid = labserviceid;
        this.modify("labserviceid", labserviceid);
    }

    /**
     * 设置 [服务提供商]
     */
    public void setRserviceid(String rserviceid) {
        this.rserviceid = rserviceid;
        this.modify("rserviceid", rserviceid);
    }

    /**
     * 设置 [合同]
     */
    public void setContractid(String contractid) {
        this.contractid = contractid;
        this.modify("contractid", contractid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emassetid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


