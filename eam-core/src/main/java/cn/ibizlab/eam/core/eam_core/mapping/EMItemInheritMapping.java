

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMItem;
import cn.ibizlab.eam.core.eam_core.domain.EMObject;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMItemInheritMapping {

    @Mappings({
        @Mapping(source ="emitemid",target = "emobjectid"),
        @Mapping(source ="emitemname",target = "emobjectname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="description",target = "description"),
        @Mapping(source ="itemcode",target = "objectcode"),
        @Mapping(source ="orgid",target = "orgid"),
    })
    EMObject toEmobject(EMItem minorEntity);

    @Mappings({
        @Mapping(source ="emobjectid" ,target = "emitemid"),
        @Mapping(source ="emobjectname" ,target = "emitemname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="objectcode",target = "itemcode"),
    })
    EMItem toEmitem(EMObject majorEntity);

    List<EMObject> toEmobject(List<EMItem> minorEntities);

    List<EMItem> toEmitem(List<EMObject> majorEntities);

}


