package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMStorePart;
/**
 * 关系型数据实体[EMStorePart] 查询条件对象
 */
@Slf4j
@Data
public class EMStorePartSearchContext extends QueryWrapperContext<EMStorePart> {

	private String n_emstorepartname_like;//[仓库库位名称]
	public void setN_emstorepartname_like(String n_emstorepartname_like) {
        this.n_emstorepartname_like = n_emstorepartname_like;
        if(!ObjectUtils.isEmpty(this.n_emstorepartname_like)){
            this.getSearchCond().like("emstorepartname", n_emstorepartname_like);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_storename_eq;//[仓库]
	public void setN_storename_eq(String n_storename_eq) {
        this.n_storename_eq = n_storename_eq;
        if(!ObjectUtils.isEmpty(this.n_storename_eq)){
            this.getSearchCond().eq("storename", n_storename_eq);
        }
    }
	private String n_storename_like;//[仓库]
	public void setN_storename_like(String n_storename_like) {
        this.n_storename_like = n_storename_like;
        if(!ObjectUtils.isEmpty(this.n_storename_like)){
            this.getSearchCond().like("storename", n_storename_like);
        }
    }
	private String n_storeid_eq;//[仓库]
	public void setN_storeid_eq(String n_storeid_eq) {
        this.n_storeid_eq = n_storeid_eq;
        if(!ObjectUtils.isEmpty(this.n_storeid_eq)){
            this.getSearchCond().eq("storeid", n_storeid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emstorepartname", query)
            );
		 }
	}
}



