package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMSubList;
/**
 * 关系型数据实体[EMSubList] 查询条件对象
 */
@Slf4j
@Data
public class EMSubListSearchContext extends QueryWrapperContext<EMSubList> {

	private String n_emsublistname_like;//[材料配件名称]
	public void setN_emsublistname_like(String n_emsublistname_like) {
        this.n_emsublistname_like = n_emsublistname_like;
        if(!ObjectUtils.isEmpty(this.n_emsublistname_like)){
            this.getSearchCond().like("emsublistname", n_emsublistname_like);
        }
    }
	private String n_preliminary_eq;//[初步审核]
	public void setN_preliminary_eq(String n_preliminary_eq) {
        this.n_preliminary_eq = n_preliminary_eq;
        if(!ObjectUtils.isEmpty(this.n_preliminary_eq)){
            this.getSearchCond().eq("preliminary", n_preliminary_eq);
        }
    }
	private String n_pfteamname_eq;//[班组]
	public void setN_pfteamname_eq(String n_pfteamname_eq) {
        this.n_pfteamname_eq = n_pfteamname_eq;
        if(!ObjectUtils.isEmpty(this.n_pfteamname_eq)){
            this.getSearchCond().eq("pfteamname", n_pfteamname_eq);
        }
    }
	private String n_pfteamname_like;//[班组]
	public void setN_pfteamname_like(String n_pfteamname_like) {
        this.n_pfteamname_like = n_pfteamname_like;
        if(!ObjectUtils.isEmpty(this.n_pfteamname_like)){
            this.getSearchCond().like("pfteamname", n_pfteamname_like);
        }
    }
	private String n_pfteamid_eq;//[班组]
	public void setN_pfteamid_eq(String n_pfteamid_eq) {
        this.n_pfteamid_eq = n_pfteamid_eq;
        if(!ObjectUtils.isEmpty(this.n_pfteamid_eq)){
            this.getSearchCond().eq("pfteamid", n_pfteamid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emsublistname", query)
            );
		 }
	}
}



