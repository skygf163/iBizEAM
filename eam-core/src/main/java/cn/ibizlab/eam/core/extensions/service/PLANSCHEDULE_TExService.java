package cn.ibizlab.eam.core.extensions.service;

import cn.ibizlab.eam.core.eam_core.service.impl.PLANSCHEDULE_TServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.context.annotation.Primary;

/**
 * 实体[计划_定时] 自定义服务对象
 */
@Slf4j
@Primary
@Service("PLANSCHEDULE_TExService")
public class PLANSCHEDULE_TExService extends PLANSCHEDULE_TServiceImpl {

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

}

