

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQWL;
import cn.ibizlab.eam.core.eam_core.domain.EMDPRCT;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMEQWLInheritMapping {

    @Mappings({
        @Mapping(source ="emeqwlid",target = "emdprctid"),
        @Mapping(source ="emeqwlname",target = "emdprctname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    EMDPRCT toEmdprct(EMEQWL minorEntity);

    @Mappings({
        @Mapping(source ="emdprctid" ,target = "emeqwlid"),
        @Mapping(source ="emdprctname" ,target = "emeqwlname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    EMEQWL toEmeqwl(EMDPRCT majorEntity);

    List<EMDPRCT> toEmdprct(List<EMEQWL> minorEntities);

    List<EMEQWL> toEmeqwl(List<EMDPRCT> majorEntities);

}


