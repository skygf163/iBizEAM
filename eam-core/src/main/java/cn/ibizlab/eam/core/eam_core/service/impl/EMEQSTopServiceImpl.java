package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMEQSTop;
import cn.ibizlab.eam.core.eam_core.filter.EMEQSTopSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMEQSTopService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMEQSTopMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[设备停机监控表] 服务对象接口实现
 */
@Slf4j
@Service("EMEQSTopServiceImpl")
public class EMEQSTopServiceImpl extends ServiceImpl<EMEQSTopMapper, EMEQSTop> implements IEMEQSTopService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQSTopMoniService emeqstopmoniService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQTypeService emeqtypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMEQSTop et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeqstopid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMEQSTop> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMEQSTop et) {
        fillParentData(et);
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emeqstopid", et.getEmeqstopid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeqstopid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMEQSTop> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMEQSTop get(String key) {
        EMEQSTop et = getById(key);
        if(et == null){
            et = new EMEQSTop();
            et.setEmeqstopid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMEQSTop getDraft(EMEQSTop et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMEQSTop et) {
        return (!ObjectUtils.isEmpty(et.getEmeqstopid())) && (!Objects.isNull(this.getById(et.getEmeqstopid())));
    }
    @Override
    @Transactional
    public boolean save(EMEQSTop et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMEQSTop et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMEQSTop> list) {
        list.forEach(item->fillParentData(item));
        List<EMEQSTop> create = new ArrayList<>();
        List<EMEQSTop> update = new ArrayList<>();
        for (EMEQSTop et : list) {
            if (ObjectUtils.isEmpty(et.getEmeqstopid()) || ObjectUtils.isEmpty(getById(et.getEmeqstopid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMEQSTop> list) {
        list.forEach(item->fillParentData(item));
        List<EMEQSTop> create = new ArrayList<>();
        List<EMEQSTop> update = new ArrayList<>();
        for (EMEQSTop et : list) {
            if (ObjectUtils.isEmpty(et.getEmeqstopid()) || ObjectUtils.isEmpty(getById(et.getEmeqstopid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMEQSTop> selectByEmeqtypeid(String emeqtypeid) {
        return baseMapper.selectByEmeqtypeid(emeqtypeid);
    }
    @Override
    public void removeByEmeqtypeid(String emeqtypeid) {
        this.remove(new QueryWrapper<EMEQSTop>().eq("emeqtypeid",emeqtypeid));
    }

	@Override
    public List<EMEQSTop> selectByEmequipid(String emequipid) {
        return baseMapper.selectByEmequipid(emequipid);
    }
    @Override
    public void removeByEmequipid(String emequipid) {
        this.remove(new QueryWrapper<EMEQSTop>().eq("emequipid",emequipid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMEQSTop> searchDefault(EMEQSTopSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEQSTop> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEQSTop>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMEQSTop et){
        //实体关系[DER1N_EMEQSTOP_EMEQTYPE_EMEQTYPEID]
        if(!ObjectUtils.isEmpty(et.getEmeqtypeid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEQType emeqtype=et.getEmeqtype();
            if(ObjectUtils.isEmpty(emeqtype)){
                cn.ibizlab.eam.core.eam_core.domain.EMEQType majorEntity=emeqtypeService.get(et.getEmeqtypeid());
                et.setEmeqtype(majorEntity);
                emeqtype=majorEntity;
            }
            et.setEmeqtypename(emeqtype.getEqtypeinfo());
        }
        //实体关系[DER1N_EMEQSTOP_EMEQUIP_EMEQUIPID]
        if(!ObjectUtils.isEmpty(et.getEmequipid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEquip emequip=et.getEmequip();
            if(ObjectUtils.isEmpty(emequip)){
                cn.ibizlab.eam.core.eam_core.domain.EMEquip majorEntity=emequipService.get(et.getEmequipid());
                et.setEmequip(majorEntity);
                emequip=majorEntity;
            }
            et.setEmequipname(emequip.getEquipinfo());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMEQSTop> getEmeqstopByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMEQSTop> getEmeqstopByEntities(List<EMEQSTop> entities) {
        List ids =new ArrayList();
        for(EMEQSTop entity : entities){
            Serializable id=entity.getEmeqstopid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMEQSTopService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



