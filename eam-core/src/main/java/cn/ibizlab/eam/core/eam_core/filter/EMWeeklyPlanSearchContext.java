package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMWeeklyPlan;
/**
 * 关系型数据实体[EMWeeklyPlan] 查询条件对象
 */
@Slf4j
@Data
public class EMWeeklyPlanSearchContext extends QueryWrapperContext<EMWeeklyPlan> {

	private String n_state_eq;//[状态]
	public void setN_state_eq(String n_state_eq) {
        this.n_state_eq = n_state_eq;
        if(!ObjectUtils.isEmpty(this.n_state_eq)){
            this.getSearchCond().eq("state", n_state_eq);
        }
    }
	private String n_emweeklyplanname_like;//[班组周计划工作名称]
	public void setN_emweeklyplanname_like(String n_emweeklyplanname_like) {
        this.n_emweeklyplanname_like = n_emweeklyplanname_like;
        if(!ObjectUtils.isEmpty(this.n_emweeklyplanname_like)){
            this.getSearchCond().like("emweeklyplanname", n_emweeklyplanname_like);
        }
    }
	private String n_pfteamname_eq;//[班组]
	public void setN_pfteamname_eq(String n_pfteamname_eq) {
        this.n_pfteamname_eq = n_pfteamname_eq;
        if(!ObjectUtils.isEmpty(this.n_pfteamname_eq)){
            this.getSearchCond().eq("pfteamname", n_pfteamname_eq);
        }
    }
	private String n_pfteamname_like;//[班组]
	public void setN_pfteamname_like(String n_pfteamname_like) {
        this.n_pfteamname_like = n_pfteamname_like;
        if(!ObjectUtils.isEmpty(this.n_pfteamname_like)){
            this.getSearchCond().like("pfteamname", n_pfteamname_like);
        }
    }
	private String n_pfteamid_eq;//[班组]
	public void setN_pfteamid_eq(String n_pfteamid_eq) {
        this.n_pfteamid_eq = n_pfteamid_eq;
        if(!ObjectUtils.isEmpty(this.n_pfteamid_eq)){
            this.getSearchCond().eq("pfteamid", n_pfteamid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emweeklyplanname", query)
            );
		 }
	}
}



