package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMEIGSJRB;
/**
 * 关系型数据实体[EMEIGSJRB] 查询条件对象
 */
@Slf4j
@Data
public class EMEIGSJRBSearchContext extends QueryWrapperContext<EMEIGSJRB> {

	private String n_emeigsjrbname_like;//[工索具日报名称]
	public void setN_emeigsjrbname_like(String n_emeigsjrbname_like) {
        this.n_emeigsjrbname_like = n_emeigsjrbname_like;
        if(!ObjectUtils.isEmpty(this.n_emeigsjrbname_like)){
            this.getSearchCond().like("emeigsjrbname", n_emeigsjrbname_like);
        }
    }
	private String n_itemname_eq;//[工索具名称]
	public void setN_itemname_eq(String n_itemname_eq) {
        this.n_itemname_eq = n_itemname_eq;
        if(!ObjectUtils.isEmpty(this.n_itemname_eq)){
            this.getSearchCond().eq("itemname", n_itemname_eq);
        }
    }
	private String n_itemname_like;//[工索具名称]
	public void setN_itemname_like(String n_itemname_like) {
        this.n_itemname_like = n_itemname_like;
        if(!ObjectUtils.isEmpty(this.n_itemname_like)){
            this.getSearchCond().like("itemname", n_itemname_like);
        }
    }
	private String n_itemid_eq;//[工索具名称]
	public void setN_itemid_eq(String n_itemid_eq) {
        this.n_itemid_eq = n_itemid_eq;
        if(!ObjectUtils.isEmpty(this.n_itemid_eq)){
            this.getSearchCond().eq("itemid", n_itemid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emeigsjrbname", query)
            );
		 }
	}
}



