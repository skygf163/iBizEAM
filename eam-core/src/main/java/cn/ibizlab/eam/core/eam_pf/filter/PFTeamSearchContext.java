package cn.ibizlab.eam.core.eam_pf.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_pf.domain.PFTeam;
/**
 * 关系型数据实体[PFTeam] 查询条件对象
 */
@Slf4j
@Data
public class PFTeamSearchContext extends QueryWrapperContext<PFTeam> {

	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_teamtypeid_eq;//[班组类型]
	public void setN_teamtypeid_eq(String n_teamtypeid_eq) {
        this.n_teamtypeid_eq = n_teamtypeid_eq;
        if(!ObjectUtils.isEmpty(this.n_teamtypeid_eq)){
            this.getSearchCond().eq("teamtypeid", n_teamtypeid_eq);
        }
    }
	private String n_pfteamname_like;//[班组名称]
	public void setN_pfteamname_like(String n_pfteamname_like) {
        this.n_pfteamname_like = n_pfteamname_like;
        if(!ObjectUtils.isEmpty(this.n_pfteamname_like)){
            this.getSearchCond().like("pfteamname", n_pfteamname_like);
        }
    }
	private String n_teaminfo_like;//[班组信息]
	public void setN_teaminfo_like(String n_teaminfo_like) {
        this.n_teaminfo_like = n_teaminfo_like;
        if(!ObjectUtils.isEmpty(this.n_teaminfo_like)){
            this.getSearchCond().like("teaminfo", n_teaminfo_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("pfteamname", query)
            );
		 }
	}
}



