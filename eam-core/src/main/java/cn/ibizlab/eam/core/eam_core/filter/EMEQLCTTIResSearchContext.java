package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMEQLCTTIRes;
/**
 * 关系型数据实体[EMEQLCTTIRes] 查询条件对象
 */
@Slf4j
@Data
public class EMEQLCTTIResSearchContext extends QueryWrapperContext<EMEQLCTTIRes> {

	private String n_tiresstate_eq;//[轮胎状态]
	public void setN_tiresstate_eq(String n_tiresstate_eq) {
        this.n_tiresstate_eq = n_tiresstate_eq;
        if(!ObjectUtils.isEmpty(this.n_tiresstate_eq)){
            this.getSearchCond().eq("tiresstate", n_tiresstate_eq);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_newoldflag_eq;//[新旧标志]
	public void setN_newoldflag_eq(String n_newoldflag_eq) {
        this.n_newoldflag_eq = n_newoldflag_eq;
        if(!ObjectUtils.isEmpty(this.n_newoldflag_eq)){
            this.getSearchCond().eq("newoldflag", n_newoldflag_eq);
        }
    }
	private String n_tirestype_eq;//[轮胎车型]
	public void setN_tirestype_eq(String n_tirestype_eq) {
        this.n_tirestype_eq = n_tirestype_eq;
        if(!ObjectUtils.isEmpty(this.n_tirestype_eq)){
            this.getSearchCond().eq("tirestype", n_tirestype_eq);
        }
    }
	private String n_lcttiresinfo_like;//[轮胎信息]
	public void setN_lcttiresinfo_like(String n_lcttiresinfo_like) {
        this.n_lcttiresinfo_like = n_lcttiresinfo_like;
        if(!ObjectUtils.isEmpty(this.n_lcttiresinfo_like)){
            this.getSearchCond().like("lcttiresinfo", n_lcttiresinfo_like);
        }
    }
	private String n_labservicename_eq;//[供应商]
	public void setN_labservicename_eq(String n_labservicename_eq) {
        this.n_labservicename_eq = n_labservicename_eq;
        if(!ObjectUtils.isEmpty(this.n_labservicename_eq)){
            this.getSearchCond().eq("labservicename", n_labservicename_eq);
        }
    }
	private String n_labservicename_like;//[供应商]
	public void setN_labservicename_like(String n_labservicename_like) {
        this.n_labservicename_like = n_labservicename_like;
        if(!ObjectUtils.isEmpty(this.n_labservicename_like)){
            this.getSearchCond().like("labservicename", n_labservicename_like);
        }
    }
	private String n_mservicename_eq;//[制造商]
	public void setN_mservicename_eq(String n_mservicename_eq) {
        this.n_mservicename_eq = n_mservicename_eq;
        if(!ObjectUtils.isEmpty(this.n_mservicename_eq)){
            this.getSearchCond().eq("mservicename", n_mservicename_eq);
        }
    }
	private String n_mservicename_like;//[制造商]
	public void setN_mservicename_like(String n_mservicename_like) {
        this.n_mservicename_like = n_mservicename_like;
        if(!ObjectUtils.isEmpty(this.n_mservicename_like)){
            this.getSearchCond().like("mservicename", n_mservicename_like);
        }
    }
	private String n_eqlocationinfo_eq;//[位置信息]
	public void setN_eqlocationinfo_eq(String n_eqlocationinfo_eq) {
        this.n_eqlocationinfo_eq = n_eqlocationinfo_eq;
        if(!ObjectUtils.isEmpty(this.n_eqlocationinfo_eq)){
            this.getSearchCond().eq("eqlocationinfo", n_eqlocationinfo_eq);
        }
    }
	private String n_eqlocationinfo_like;//[位置信息]
	public void setN_eqlocationinfo_like(String n_eqlocationinfo_like) {
        this.n_eqlocationinfo_like = n_eqlocationinfo_like;
        if(!ObjectUtils.isEmpty(this.n_eqlocationinfo_like)){
            this.getSearchCond().like("eqlocationinfo", n_eqlocationinfo_like);
        }
    }
	private String n_equipname_eq;//[设备]
	public void setN_equipname_eq(String n_equipname_eq) {
        this.n_equipname_eq = n_equipname_eq;
        if(!ObjectUtils.isEmpty(this.n_equipname_eq)){
            this.getSearchCond().eq("equipname", n_equipname_eq);
        }
    }
	private String n_equipname_like;//[设备]
	public void setN_equipname_like(String n_equipname_like) {
        this.n_equipname_like = n_equipname_like;
        if(!ObjectUtils.isEmpty(this.n_equipname_like)){
            this.getSearchCond().like("equipname", n_equipname_like);
        }
    }
	private String n_equipid_eq;//[设备]
	public void setN_equipid_eq(String n_equipid_eq) {
        this.n_equipid_eq = n_equipid_eq;
        if(!ObjectUtils.isEmpty(this.n_equipid_eq)){
            this.getSearchCond().eq("equipid", n_equipid_eq);
        }
    }
	private String n_emeqlocationid_eq;//[位置标识]
	public void setN_emeqlocationid_eq(String n_emeqlocationid_eq) {
        this.n_emeqlocationid_eq = n_emeqlocationid_eq;
        if(!ObjectUtils.isEmpty(this.n_emeqlocationid_eq)){
            this.getSearchCond().eq("emeqlocationid", n_emeqlocationid_eq);
        }
    }
	private String n_labserviceid_eq;//[供应商]
	public void setN_labserviceid_eq(String n_labserviceid_eq) {
        this.n_labserviceid_eq = n_labserviceid_eq;
        if(!ObjectUtils.isEmpty(this.n_labserviceid_eq)){
            this.getSearchCond().eq("labserviceid", n_labserviceid_eq);
        }
    }
	private String n_mserviceid_eq;//[制造商]
	public void setN_mserviceid_eq(String n_mserviceid_eq) {
        this.n_mserviceid_eq = n_mserviceid_eq;
        if(!ObjectUtils.isEmpty(this.n_mserviceid_eq)){
            this.getSearchCond().eq("mserviceid", n_mserviceid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("eqlocationinfo", query)
            );
		 }
	}
}



