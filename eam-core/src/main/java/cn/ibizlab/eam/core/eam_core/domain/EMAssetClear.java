package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[资产清盘记录]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMASSETCLEAR_BASE", resultMap = "EMAssetClearResultMap")
@ApiModel("资产清盘记录")
public class EMAssetClear extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 盘盈金额
     */
    @TableField(value = "assetinprice")
    @JSONField(name = "assetinprice")
    @JsonProperty("assetinprice")
    @ApiModelProperty("盘盈金额")
    private String assetinprice;
    /**
     * 盘点清查金额
     */
    @TableField(value = "assetcheckprice")
    @JSONField(name = "assetcheckprice")
    @JsonProperty("assetcheckprice")
    @ApiModelProperty("盘点清查金额")
    private String assetcheckprice;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @ApiModelProperty("组织")
    private String orgid;
    /**
     * 最新清盘
     */
    @TableField(value = "isnew")
    @JSONField(name = "isnew")
    @JsonProperty("isnew")
    @ApiModelProperty("最新清盘")
    private Integer isnew;
    /**
     * 盘亏金额
     */
    @TableField(value = "assetoutprice")
    @JSONField(name = "assetoutprice")
    @JsonProperty("assetoutprice")
    @ApiModelProperty("盘亏金额")
    private String assetoutprice;
    /**
     * 使用部门
     */
    @TableField(value = "assetclearlct")
    @JSONField(name = "assetclearlct")
    @JsonProperty("assetclearlct")
    @ApiModelProperty("使用部门")
    private String assetclearlct;
    /**
     * 资产清盘记录名称
     */
    @TableField(value = "emassetclearname")
    @JSONField(name = "emassetclearname")
    @JsonProperty("emassetclearname")
    @ApiModelProperty("资产清盘记录名称")
    private String emassetclearname;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;
    /**
     * 盘亏数量
     */
    @TableField(value = "assetoutnum")
    @JSONField(name = "assetoutnum")
    @JsonProperty("assetoutnum")
    @ApiModelProperty("盘亏数量")
    private Integer assetoutnum;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @ApiModelProperty("更新人")
    private String updateman;
    /**
     * 状况
     */
    @TableField(value = "assetclearstate")
    @JSONField(name = "assetclearstate")
    @JsonProperty("assetclearstate")
    @ApiModelProperty("状况")
    private String assetclearstate;
    /**
     * 盘点日期
     */
    @TableField(value = "assetcleardate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "assetcleardate", format = "yyyy-MM-dd")
    @JsonProperty("assetcleardate")
    @ApiModelProperty("盘点日期")
    private Timestamp assetcleardate;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;
    /**
     * 盘盈数量
     */
    @TableField(value = "assetinnum")
    @JSONField(name = "assetinnum")
    @JsonProperty("assetinnum")
    @ApiModelProperty("盘盈数量")
    private Integer assetinnum;
    /**
     * 盘点清查数量
     */
    @TableField(value = "assetchecknum")
    @JSONField(name = "assetchecknum")
    @JsonProperty("assetchecknum")
    @ApiModelProperty("盘点清查数量")
    private Integer assetchecknum;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @ApiModelProperty("建立人")
    private String createman;
    /**
     * 资产清盘记录标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emassetclearid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emassetclearid")
    @JsonProperty("emassetclearid")
    @ApiModelProperty("资产清盘记录标识")
    private String emassetclearid;
    /**
     * 资产排序
     */
    @TableField(exist = false)
    @JSONField(name = "assetsort")
    @JsonProperty("assetsort")
    @ApiModelProperty("资产排序")
    private String assetsort;
    /**
     * 资产代码
     */
    @TableField(exist = false)
    @JSONField(name = "assetcode")
    @JsonProperty("assetcode")
    @ApiModelProperty("资产代码")
    private String assetcode;
    /**
     * 资产类型
     */
    @TableField(exist = false)
    @JSONField(name = "assetclassname")
    @JsonProperty("assetclassname")
    @ApiModelProperty("资产类型")
    private String assetclassname;
    /**
     * 规格型号
     */
    @TableField(exist = false)
    @JSONField(name = "eqmodelcode")
    @JsonProperty("eqmodelcode")
    @ApiModelProperty("规格型号")
    private String eqmodelcode;
    /**
     * 入帐时间
     */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "purchdate", format = "yyyy-MM-dd")
    @JsonProperty("purchdate")
    @ApiModelProperty("入帐时间")
    private Timestamp purchdate;
    /**
     * 资产原值
     */
    @TableField(exist = false)
    @JSONField(name = "originalcost")
    @JsonProperty("originalcost")
    @ApiModelProperty("资产原值")
    private String originalcost;
    /**
     * 资产类型
     */
    @TableField(exist = false)
    @JSONField(name = "assetclassid")
    @JsonProperty("assetclassid")
    @ApiModelProperty("资产类型")
    private String assetclassid;
    /**
     * 资产
     */
    @TableField(exist = false)
    @JSONField(name = "emassetname")
    @JsonProperty("emassetname")
    @ApiModelProperty("资产")
    private String emassetname;
    /**
     * 资产
     */
    @TableField(value = "emassetid")
    @JSONField(name = "emassetid")
    @JsonProperty("emassetid")
    @ApiModelProperty("资产")
    private String emassetid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMAsset emasset;



    /**
     * 设置 [盘盈金额]
     */
    public void setAssetinprice(String assetinprice) {
        this.assetinprice = assetinprice;
        this.modify("assetinprice", assetinprice);
    }

    /**
     * 设置 [盘点清查金额]
     */
    public void setAssetcheckprice(String assetcheckprice) {
        this.assetcheckprice = assetcheckprice;
        this.modify("assetcheckprice", assetcheckprice);
    }

    /**
     * 设置 [最新清盘]
     */
    public void setIsnew(Integer isnew) {
        this.isnew = isnew;
        this.modify("isnew", isnew);
    }

    /**
     * 设置 [盘亏金额]
     */
    public void setAssetoutprice(String assetoutprice) {
        this.assetoutprice = assetoutprice;
        this.modify("assetoutprice", assetoutprice);
    }

    /**
     * 设置 [使用部门]
     */
    public void setAssetclearlct(String assetclearlct) {
        this.assetclearlct = assetclearlct;
        this.modify("assetclearlct", assetclearlct);
    }

    /**
     * 设置 [资产清盘记录名称]
     */
    public void setEmassetclearname(String emassetclearname) {
        this.emassetclearname = emassetclearname;
        this.modify("emassetclearname", emassetclearname);
    }

    /**
     * 设置 [盘亏数量]
     */
    public void setAssetoutnum(Integer assetoutnum) {
        this.assetoutnum = assetoutnum;
        this.modify("assetoutnum", assetoutnum);
    }

    /**
     * 设置 [状况]
     */
    public void setAssetclearstate(String assetclearstate) {
        this.assetclearstate = assetclearstate;
        this.modify("assetclearstate", assetclearstate);
    }

    /**
     * 设置 [盘点日期]
     */
    public void setAssetcleardate(Timestamp assetcleardate) {
        this.assetcleardate = assetcleardate;
        this.modify("assetcleardate", assetcleardate);
    }

    /**
     * 格式化日期 [盘点日期]
     */
    public String formatAssetcleardate() {
        if (this.assetcleardate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(assetcleardate);
    }
    /**
     * 设置 [盘盈数量]
     */
    public void setAssetinnum(Integer assetinnum) {
        this.assetinnum = assetinnum;
        this.modify("assetinnum", assetinnum);
    }

    /**
     * 设置 [盘点清查数量]
     */
    public void setAssetchecknum(Integer assetchecknum) {
        this.assetchecknum = assetchecknum;
        this.modify("assetchecknum", assetchecknum);
    }

    /**
     * 设置 [资产]
     */
    public void setEmassetid(String emassetid) {
        this.emassetid = emassetid;
        this.modify("emassetid", emassetid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emassetclearid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


