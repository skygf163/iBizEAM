package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMObjMap;
/**
 * 关系型数据实体[EMObjMap] 查询条件对象
 */
@Slf4j
@Data
public class EMObjMapSearchContext extends QueryWrapperContext<EMObjMap> {

	private String n_emobjmaptype_eq;//[对象关系类型]
	public void setN_emobjmaptype_eq(String n_emobjmaptype_eq) {
        this.n_emobjmaptype_eq = n_emobjmaptype_eq;
        if(!ObjectUtils.isEmpty(this.n_emobjmaptype_eq)){
            this.getSearchCond().eq("emobjmaptype", n_emobjmaptype_eq);
        }
    }
	private String n_emobjmapname_like;//[对象关系名称]
	public void setN_emobjmapname_like(String n_emobjmapname_like) {
        this.n_emobjmapname_like = n_emobjmapname_like;
        if(!ObjectUtils.isEmpty(this.n_emobjmapname_like)){
            this.getSearchCond().like("emobjmapname", n_emobjmapname_like);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_objpname_eq;//[上级对象]
	public void setN_objpname_eq(String n_objpname_eq) {
        this.n_objpname_eq = n_objpname_eq;
        if(!ObjectUtils.isEmpty(this.n_objpname_eq)){
            this.getSearchCond().eq("objpname", n_objpname_eq);
        }
    }
	private String n_objpname_like;//[上级对象]
	public void setN_objpname_like(String n_objpname_like) {
        this.n_objpname_like = n_objpname_like;
        if(!ObjectUtils.isEmpty(this.n_objpname_like)){
            this.getSearchCond().like("objpname", n_objpname_like);
        }
    }
	private String n_objname_eq;//[对象]
	public void setN_objname_eq(String n_objname_eq) {
        this.n_objname_eq = n_objname_eq;
        if(!ObjectUtils.isEmpty(this.n_objname_eq)){
            this.getSearchCond().eq("objname", n_objname_eq);
        }
    }
	private String n_objname_like;//[对象]
	public void setN_objname_like(String n_objname_like) {
        this.n_objname_like = n_objname_like;
        if(!ObjectUtils.isEmpty(this.n_objname_like)){
            this.getSearchCond().like("objname", n_objname_like);
        }
    }
	private String n_objid_eq;//[对象]
	public void setN_objid_eq(String n_objid_eq) {
        this.n_objid_eq = n_objid_eq;
        if(!ObjectUtils.isEmpty(this.n_objid_eq)){
            this.getSearchCond().eq("objid", n_objid_eq);
        }
    }
	private String n_objpid_eq;//[上级对象]
	public void setN_objpid_eq(String n_objpid_eq) {
        this.n_objpid_eq = n_objpid_eq;
        if(!ObjectUtils.isEmpty(this.n_objpid_eq)){
            this.getSearchCond().eq("objpid", n_objpid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emobjmapname", query)
            );
		 }
	}
}



