package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMEQLCTTIRes;
import cn.ibizlab.eam.core.eam_core.filter.EMEQLCTTIResSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMEQLCTTIResService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMEQLCTTIResMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[轮胎位置] 服务对象接口实现
 */
@Slf4j
@Service("EMEQLCTTIResServiceImpl")
public class EMEQLCTTIResServiceImpl extends ServiceImpl<EMEQLCTTIResMapper, EMEQLCTTIRes> implements IEMEQLCTTIResService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEITIResService emeitiresService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_INNERService emwoInnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMServiceService emserviceService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMEQLCTTIRes et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeqlocationid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMEQLCTTIRes> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMEQLCTTIRes et) {
        fillParentData(et);
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emeqlocationid", et.getEmeqlocationid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeqlocationid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMEQLCTTIRes> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMEQLCTTIRes get(String key) {
        EMEQLCTTIRes et = getById(key);
        if(et == null){
            et = new EMEQLCTTIRes();
            et.setEmeqlocationid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMEQLCTTIRes getDraft(EMEQLCTTIRes et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMEQLCTTIRes et) {
        return (!ObjectUtils.isEmpty(et.getEmeqlocationid())) && (!Objects.isNull(this.getById(et.getEmeqlocationid())));
    }
    @Override
    @Transactional
    public boolean save(EMEQLCTTIRes et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMEQLCTTIRes et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMEQLCTTIRes> list) {
        list.forEach(item->fillParentData(item));
        List<EMEQLCTTIRes> create = new ArrayList<>();
        List<EMEQLCTTIRes> update = new ArrayList<>();
        for (EMEQLCTTIRes et : list) {
            if (ObjectUtils.isEmpty(et.getEmeqlocationid()) || ObjectUtils.isEmpty(getById(et.getEmeqlocationid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMEQLCTTIRes> list) {
        list.forEach(item->fillParentData(item));
        List<EMEQLCTTIRes> create = new ArrayList<>();
        List<EMEQLCTTIRes> update = new ArrayList<>();
        for (EMEQLCTTIRes et : list) {
            if (ObjectUtils.isEmpty(et.getEmeqlocationid()) || ObjectUtils.isEmpty(getById(et.getEmeqlocationid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMEQLCTTIRes> selectByEquipid(String emequipid) {
        return baseMapper.selectByEquipid(emequipid);
    }
    @Override
    public void removeByEquipid(String emequipid) {
        this.remove(new QueryWrapper<EMEQLCTTIRes>().eq("equipid",emequipid));
    }

	@Override
    public List<EMEQLCTTIRes> selectByLabserviceid(String emserviceid) {
        return baseMapper.selectByLabserviceid(emserviceid);
    }
    @Override
    public void removeByLabserviceid(String emserviceid) {
        this.remove(new QueryWrapper<EMEQLCTTIRes>().eq("labserviceid",emserviceid));
    }

	@Override
    public List<EMEQLCTTIRes> selectByMserviceid(String emserviceid) {
        return baseMapper.selectByMserviceid(emserviceid);
    }
    @Override
    public void removeByMserviceid(String emserviceid) {
        this.remove(new QueryWrapper<EMEQLCTTIRes>().eq("mserviceid",emserviceid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMEQLCTTIRes> searchDefault(EMEQLCTTIResSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEQLCTTIRes> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEQLCTTIRes>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMEQLCTTIRes et){
        //实体关系[DER1N_EMEQLCTTIRES_EMEQUIP_EQUIPID]
        if(!ObjectUtils.isEmpty(et.getEquipid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEquip equip=et.getEquip();
            if(ObjectUtils.isEmpty(equip)){
                cn.ibizlab.eam.core.eam_core.domain.EMEquip majorEntity=emequipService.get(et.getEquipid());
                et.setEquip(majorEntity);
                equip=majorEntity;
            }
            et.setEquipname(equip.getEquipinfo());
        }
        //实体关系[DER1N_EMEQLCTTIRES_EMSERVICE_LABSERVICEID]
        if(!ObjectUtils.isEmpty(et.getLabserviceid())){
            cn.ibizlab.eam.core.eam_core.domain.EMService labservice=et.getLabservice();
            if(ObjectUtils.isEmpty(labservice)){
                cn.ibizlab.eam.core.eam_core.domain.EMService majorEntity=emserviceService.get(et.getLabserviceid());
                et.setLabservice(majorEntity);
                labservice=majorEntity;
            }
            et.setLabservicename(labservice.getEmservicename());
        }
        //实体关系[DER1N_EMEQLCTTIRES_EMSERVICE_MSERVICEID]
        if(!ObjectUtils.isEmpty(et.getMserviceid())){
            cn.ibizlab.eam.core.eam_core.domain.EMService mservice=et.getMservice();
            if(ObjectUtils.isEmpty(mservice)){
                cn.ibizlab.eam.core.eam_core.domain.EMService majorEntity=emserviceService.get(et.getMserviceid());
                et.setMservice(majorEntity);
                mservice=majorEntity;
            }
            et.setMservicename(mservice.getEmservicename());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMEQLCTTIRes> getEmeqlcttiresByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMEQLCTTIRes> getEmeqlcttiresByEntities(List<EMEQLCTTIRes> entities) {
        List ids =new ArrayList();
        for(EMEQLCTTIRes entity : entities){
            Serializable id=entity.getEmeqlocationid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMEQLCTTIResService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



