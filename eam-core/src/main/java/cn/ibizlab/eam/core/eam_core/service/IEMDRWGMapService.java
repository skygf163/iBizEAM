package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMDRWGMap;
import cn.ibizlab.eam.core.eam_core.filter.EMDRWGMapSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMDRWGMap] 服务对象接口
 */
public interface IEMDRWGMapService extends IService<EMDRWGMap> {

    boolean create(EMDRWGMap et);
    void createBatch(List<EMDRWGMap> list);
    boolean update(EMDRWGMap et);
    void updateBatch(List<EMDRWGMap> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMDRWGMap get(String key);
    EMDRWGMap getDraft(EMDRWGMap et);
    boolean checkKey(EMDRWGMap et);
    boolean save(EMDRWGMap et);
    void saveBatch(List<EMDRWGMap> list);
    Page<EMDRWGMap> searchByEQ(EMDRWGMapSearchContext context);
    Page<EMDRWGMap> searchDefault(EMDRWGMapSearchContext context);
    List<EMDRWGMap> selectByDrwgid(String emdrwgid);
    void removeByDrwgid(String emdrwgid);
    List<EMDRWGMap> selectByRefobjid(String emobjectid);
    void removeByRefobjid(String emobjectid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMDRWGMap> getEmdrwgmapByIds(List<String> ids);
    List<EMDRWGMap> getEmdrwgmapByEntities(List<EMDRWGMap> entities);
}


