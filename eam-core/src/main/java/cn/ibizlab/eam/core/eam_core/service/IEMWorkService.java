package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMWork;
import cn.ibizlab.eam.core.eam_core.filter.EMWorkSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMWork] 服务对象接口
 */
public interface IEMWorkService extends IService<EMWork> {

    boolean create(EMWork et);
    void createBatch(List<EMWork> list);
    boolean update(EMWork et);
    void updateBatch(List<EMWork> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMWork get(String key);
    EMWork getDraft(EMWork et);
    boolean checkKey(EMWork et);
    boolean save(EMWork et);
    void saveBatch(List<EMWork> list);
    Page<EMWork> searchDefault(EMWorkSearchContext context);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMWork> getEmworkByIds(List<String> ids);
    List<EMWork> getEmworkByEntities(List<EMWork> entities);
}


