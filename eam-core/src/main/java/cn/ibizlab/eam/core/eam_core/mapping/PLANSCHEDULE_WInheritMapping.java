

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE_W;
import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface PLANSCHEDULE_WInheritMapping {

    @Mappings({
        @Mapping(source ="planscheduleWid",target = "planscheduleid"),
        @Mapping(source ="planscheduleWname",target = "planschedulename"),
        @Mapping(target ="focusNull",ignore = true),
    })
    PLANSCHEDULE toPlanschedule(PLANSCHEDULE_W minorEntity);

    @Mappings({
        @Mapping(source ="planscheduleid" ,target = "planscheduleWid"),
        @Mapping(source ="planschedulename" ,target = "planscheduleWname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    PLANSCHEDULE_W toPlanscheduleW(PLANSCHEDULE majorEntity);

    List<PLANSCHEDULE> toPlanschedule(List<PLANSCHEDULE_W> minorEntities);

    List<PLANSCHEDULE_W> toPlanscheduleW(List<PLANSCHEDULE> majorEntities);

}


