package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMMonthlyDetail;
import cn.ibizlab.eam.core.eam_core.filter.EMMonthlyDetailSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMMonthlyDetail] 服务对象接口
 */
public interface IEMMonthlyDetailService extends IService<EMMonthlyDetail> {

    boolean create(EMMonthlyDetail et);
    void createBatch(List<EMMonthlyDetail> list);
    boolean update(EMMonthlyDetail et);
    void updateBatch(List<EMMonthlyDetail> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMMonthlyDetail get(String key);
    EMMonthlyDetail getDraft(EMMonthlyDetail et);
    boolean checkKey(EMMonthlyDetail et);
    boolean save(EMMonthlyDetail et);
    void saveBatch(List<EMMonthlyDetail> list);
    Page<EMMonthlyDetail> searchDefault(EMMonthlyDetailSearchContext context);
    List<EMMonthlyDetail> selectByEmmonthlyid(String emmonthlyid);
    void removeByEmmonthlyid(String emmonthlyid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMMonthlyDetail> getEmmonthlydetailByIds(List<String> ids);
    List<EMMonthlyDetail> getEmmonthlydetailByEntities(List<EMMonthlyDetail> entities);
}


