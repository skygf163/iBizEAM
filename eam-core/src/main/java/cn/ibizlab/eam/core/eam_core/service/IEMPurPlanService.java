package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMPurPlan;
import cn.ibizlab.eam.core.eam_core.filter.EMPurPlanSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMPurPlan] 服务对象接口
 */
public interface IEMPurPlanService extends IService<EMPurPlan> {

    boolean create(EMPurPlan et);
    void createBatch(List<EMPurPlan> list);
    boolean update(EMPurPlan et);
    void updateBatch(List<EMPurPlan> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMPurPlan get(String key);
    EMPurPlan getDraft(EMPurPlan et);
    boolean checkKey(EMPurPlan et);
    boolean save(EMPurPlan et);
    void saveBatch(List<EMPurPlan> list);
    Page<EMPurPlan> searchDefault(EMPurPlanSearchContext context);
    List<EMPurPlan> selectByEmbidinquiryid(String embidinquiryid);
    void removeByEmbidinquiryid(String embidinquiryid);
    List<EMPurPlan> selectByItemtypeid(String emitemtypeid);
    void removeByItemtypeid(String emitemtypeid);
    List<EMPurPlan> selectByUnitid(String pfunitid);
    void removeByUnitid(String pfunitid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMPurPlan> getEmpurplanByIds(List<String> ids);
    List<EMPurPlan> getEmpurplanByEntities(List<EMPurPlan> entities);
}


