package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE_M;
import cn.ibizlab.eam.core.eam_core.filter.PLANSCHEDULE_MSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IPLANSCHEDULE_MService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.PLANSCHEDULE_MMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[计划_按月] 服务对象接口实现
 */
@Slf4j
@Service("PLANSCHEDULE_MServiceImpl")
public class PLANSCHEDULE_MServiceImpl extends ServiceImpl<PLANSCHEDULE_MMapper, PLANSCHEDULE_M> implements IPLANSCHEDULE_MService {


    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(PLANSCHEDULE_M et) {
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getPlanscheduleMid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<PLANSCHEDULE_M> list) {
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(PLANSCHEDULE_M et) {
        planscheduleService.update(planschedule_mInheritMapping.toPlanschedule(et));
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("planschedule_mid", et.getPlanscheduleMid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getPlanscheduleMid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<PLANSCHEDULE_M> list) {
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        planscheduleService.remove(key);
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public PLANSCHEDULE_M get(String key) {
        PLANSCHEDULE_M et = getById(key);
        if(et == null){
            et = new PLANSCHEDULE_M();
            et.setPlanscheduleMid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public PLANSCHEDULE_M getDraft(PLANSCHEDULE_M et) {
        return et;
    }

    @Override
    public boolean checkKey(PLANSCHEDULE_M et) {
        return (!ObjectUtils.isEmpty(et.getPlanscheduleMid())) && (!Objects.isNull(this.getById(et.getPlanscheduleMid())));
    }
    @Override
    @Transactional
    public boolean save(PLANSCHEDULE_M et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(PLANSCHEDULE_M et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<PLANSCHEDULE_M> list) {
        List<PLANSCHEDULE_M> create = new ArrayList<>();
        List<PLANSCHEDULE_M> update = new ArrayList<>();
        for (PLANSCHEDULE_M et : list) {
            if (ObjectUtils.isEmpty(et.getPlanscheduleMid()) || ObjectUtils.isEmpty(getById(et.getPlanscheduleMid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<PLANSCHEDULE_M> list) {
        List<PLANSCHEDULE_M> create = new ArrayList<>();
        List<PLANSCHEDULE_M> update = new ArrayList<>();
        for (PLANSCHEDULE_M et : list) {
            if (ObjectUtils.isEmpty(et.getPlanscheduleMid()) || ObjectUtils.isEmpty(getById(et.getPlanscheduleMid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }



    /**
     * 查询集合 数据集
     */
    @Override
    public Page<PLANSCHEDULE_M> searchDefault(PLANSCHEDULE_MSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<PLANSCHEDULE_M> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<PLANSCHEDULE_M>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }






    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.PLANSCHEDULE_MInheritMapping planschedule_mInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IPLANSCHEDULEService planscheduleService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(PLANSCHEDULE_M et){
        if(ObjectUtils.isEmpty(et.getPlanscheduleMid()))
            et.setPlanscheduleMid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE planschedule =planschedule_mInheritMapping.toPlanschedule(et);
        planschedule.set("scheduletype","3");
        planscheduleService.create(planschedule);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<PLANSCHEDULE_M> getPlanscheduleMByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<PLANSCHEDULE_M> getPlanscheduleMByEntities(List<PLANSCHEDULE_M> entities) {
        List ids =new ArrayList();
        for(PLANSCHEDULE_M entity : entities){
            Serializable id=entity.getPlanscheduleMid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IPLANSCHEDULE_MService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



