package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[入库单]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMITEMRIN_BASE", resultMap = "EMItemRInResultMap")
@ApiModel("入库单")
public class EMItemRIn extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 入库单名称
     */
    @DEField(defaultValue = "NAME")
    @TableField(value = "emitemrinname")
    @JSONField(name = "emitemrinname")
    @JsonProperty("emitemrinname")
    @ApiModelProperty("入库单名称")
    private String emitemrinname;
    /**
     * sap传输异常文本
     */
    @TableField(value = "sapreason1")
    @JSONField(name = "sapreason1")
    @JsonProperty("sapreason1")
    @ApiModelProperty("sap传输异常文本")
    private String sapreason1;
    /**
     * 实收数
     */
    @TableField(value = "psum")
    @JSONField(name = "psum")
    @JsonProperty("psum")
    @ApiModelProperty("实收数")
    private Double psum;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @ApiModelProperty("更新人")
    private String updateman;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    @ApiModelProperty("描述")
    private String description;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @ApiModelProperty("建立人")
    private String createman;
    /**
     * 工作流状态
     */
    @TableField(value = "wfstate")
    @JSONField(name = "wfstate")
    @JsonProperty("wfstate")
    @ApiModelProperty("工作流状态")
    private Integer wfstate;
    /**
     * 入库单信息
     */
    @TableField(exist = false)
    @JSONField(name = "itemrininfo")
    @JsonProperty("itemrininfo")
    @ApiModelProperty("入库单信息")
    private String itemrininfo;
    /**
     * 物品金额
     */
    @TableField(value = "amount")
    @JSONField(name = "amount")
    @JsonProperty("amount")
    @ApiModelProperty("物品金额")
    private Double amount;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;
    /**
     * 实收数1
     */
    @TableField(exist = false)
    @JSONField(name = "rin_psum")
    @JsonProperty("rin_psum")
    @ApiModelProperty("实收数1")
    private Double rinPsum;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;
    /**
     * 入库单号
     */
    @DEField(isKeyField = true)
    @TableId(value = "emitemrinid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emitemrinid")
    @JsonProperty("emitemrinid")
    @ApiModelProperty("入库单号")
    private String emitemrinid;
    /**
     * 批次
     */
    @DEField(defaultValue = "NA")
    @TableField(value = "batcode")
    @JSONField(name = "batcode")
    @JsonProperty("batcode")
    @ApiModelProperty("批次")
    private String batcode;
    /**
     * 入库状态
     */
    @DEField(defaultValue = "0")
    @TableField(value = "rinstate")
    @JSONField(name = "rinstate")
    @JsonProperty("rinstate")
    @ApiModelProperty("入库状态")
    private Integer rinstate;
    /**
     * sap传输失败原因
     */
    @TableField(value = "sapreason")
    @JSONField(name = "sapreason")
    @JsonProperty("sapreason")
    @ApiModelProperty("sap传输失败原因")
    private String sapreason;
    /**
     * 含税总金额
     */
    @TableField(exist = false)
    @JSONField(name = "sumall")
    @JsonProperty("sumall")
    @ApiModelProperty("含税总金额")
    private Double sumall;
    /**
     * sap传输状态
     */
    @TableField(value = "sap")
    @JSONField(name = "sap")
    @JsonProperty("sap")
    @ApiModelProperty("sap传输状态")
    private Integer sap;
    /**
     * 工作流实例
     */
    @TableField(value = "wfinstanceid")
    @JSONField(name = "wfinstanceid")
    @JsonProperty("wfinstanceid")
    @ApiModelProperty("工作流实例")
    private String wfinstanceid;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @ApiModelProperty("组织")
    private String orgid;
    /**
     * 流程步骤
     */
    @TableField(value = "wfstep")
    @JSONField(name = "wfstep")
    @JsonProperty("wfstep")
    @ApiModelProperty("流程步骤")
    private String wfstep;
    /**
     * 单价
     */
    @TableField(value = "price")
    @JSONField(name = "price")
    @JsonProperty("price")
    @ApiModelProperty("单价")
    private Double price;
    /**
     * sap税码
     */
    @TableField(value = "sapsm")
    @JSONField(name = "sapsm")
    @JsonProperty("sapsm")
    @ApiModelProperty("sap税码")
    private String sapsm;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;
    /**
     * 入库日期
     */
    @TableField(value = "sdate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "sdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("sdate")
    @ApiModelProperty("入库日期")
    private Timestamp sdate;
    /**
     * sap控制
     */
    @TableField(value = "sapcontrol")
    @JSONField(name = "sapcontrol")
    @JsonProperty("sapcontrol")
    @ApiModelProperty("sap控制")
    private Integer sapcontrol;
    /**
     * 库位
     */
    @TableField(exist = false)
    @JSONField(name = "storepartname")
    @JsonProperty("storepartname")
    @ApiModelProperty("库位")
    private String storepartname;
    /**
     * 物品代码
     */
    @TableField(exist = false)
    @JSONField(name = "itemcode")
    @JsonProperty("itemcode")
    @ApiModelProperty("物品代码")
    private String itemcode;
    /**
     * 班组
     */
    @TableField(exist = false)
    @JSONField(name = "teamid")
    @JsonProperty("teamid")
    @ApiModelProperty("班组")
    private String teamid;
    /**
     * 单位
     */
    @TableField(exist = false)
    @JSONField(name = "unitid")
    @JsonProperty("unitid")
    @ApiModelProperty("单位")
    private String unitid;
    /**
     * 供应商
     */
    @TableField(exist = false)
    @JSONField(name = "labservicename")
    @JsonProperty("labservicename")
    @ApiModelProperty("供应商")
    private String labservicename;
    /**
     * 发票号
     */
    @TableField(exist = false)
    @JSONField(name = "civo")
    @JsonProperty("civo")
    @ApiModelProperty("发票号")
    private String civo;
    /**
     * 单位
     */
    @TableField(exist = false)
    @JSONField(name = "unitname")
    @JsonProperty("unitname")
    @ApiModelProperty("单位")
    private String unitname;
    /**
     * 订单编号
     */
    @TableField(exist = false)
    @JSONField(name = "poid")
    @JsonProperty("poid")
    @ApiModelProperty("订单编号")
    private String poid;
    /**
     * 运杂费
     */
    @TableField(exist = false)
    @JSONField(name = "avgtsfee")
    @JsonProperty("avgtsfee")
    @ApiModelProperty("运杂费")
    private Double avgtsfee;
    /**
     * 物品
     */
    @TableField(exist = false)
    @JSONField(name = "itemname")
    @JsonProperty("itemname")
    @ApiModelProperty("物品")
    private String itemname;
    /**
     * 采购申请
     */
    @TableField(exist = false)
    @JSONField(name = "wplistid")
    @JsonProperty("wplistid")
    @ApiModelProperty("采购申请")
    private String wplistid;
    /**
     * 订单条目
     */
    @TableField(exist = false)
    @JSONField(name = "podetailname")
    @JsonProperty("podetailname")
    @ApiModelProperty("订单条目")
    private String podetailname;
    /**
     * 物品类型
     */
    @TableField(exist = false)
    @JSONField(name = "itemtypename")
    @JsonProperty("itemtypename")
    @ApiModelProperty("物品类型")
    private String itemtypename;
    /**
     * 物品均价
     */
    @TableField(exist = false)
    @JSONField(name = "avgprice")
    @JsonProperty("avgprice")
    @ApiModelProperty("物品均价")
    private Double avgprice;
    /**
     * 服务商
     */
    @TableField(exist = false)
    @JSONField(name = "emservicename")
    @JsonProperty("emservicename")
    @ApiModelProperty("服务商")
    private String emservicename;
    /**
     * 税费
     */
    @TableField(exist = false)
    @JSONField(name = "shf")
    @JsonProperty("shf")
    @ApiModelProperty("税费")
    private Double shf;
    /**
     * 仓库
     */
    @TableField(exist = false)
    @JSONField(name = "storename")
    @JsonProperty("storename")
    @ApiModelProperty("仓库")
    private String storename;
    /**
     * 供应商编号
     */
    @TableField(exist = false)
    @JSONField(name = "labserviceid")
    @JsonProperty("labserviceid")
    @ApiModelProperty("供应商编号")
    private String labserviceid;
    /**
     * 物品大类
     */
    @TableField(exist = false)
    @JSONField(name = "itembtypeid")
    @JsonProperty("itembtypeid")
    @ApiModelProperty("物品大类")
    private String itembtypeid;
    /**
     * 服务商
     */
    @TableField(value = "emserviceid")
    @JSONField(name = "emserviceid")
    @JsonProperty("emserviceid")
    @ApiModelProperty("服务商")
    private String emserviceid;
    /**
     * 订单条目
     */
    @TableField(value = "podetailid")
    @JSONField(name = "podetailid")
    @JsonProperty("podetailid")
    @ApiModelProperty("订单条目")
    private String podetailid;
    /**
     * 仓库
     */
    @TableField(value = "storeid")
    @JSONField(name = "storeid")
    @JsonProperty("storeid")
    @ApiModelProperty("仓库")
    private String storeid;
    /**
     * 物品
     */
    @TableField(value = "itemid")
    @JSONField(name = "itemid")
    @JsonProperty("itemid")
    @ApiModelProperty("物品")
    private String itemid;
    /**
     * 库位
     */
    @TableField(value = "storepartid")
    @JSONField(name = "storepartid")
    @JsonProperty("storepartid")
    @ApiModelProperty("库位")
    private String storepartid;
    /**
     * 采购员
     */
    @TableField(exist = false)
    @JSONField(name = "porempname")
    @JsonProperty("porempname")
    @ApiModelProperty("采购员")
    private String porempname;
    /**
     * 制单人
     */
    @TableField(value = "empid")
    @JSONField(name = "empid")
    @JsonProperty("empid")
    @ApiModelProperty("制单人")
    private String empid;
    /**
     * 制单人
     */
    @TableField(exist = false)
    @JSONField(name = "empname")
    @JsonProperty("empname")
    @ApiModelProperty("制单人")
    private String empname;
    /**
     * 收料人
     */
    @TableField(value = "sempid")
    @JSONField(name = "sempid")
    @JsonProperty("sempid")
    @ApiModelProperty("收料人")
    private String sempid;
    /**
     * 收料人
     */
    @TableField(exist = false)
    @JSONField(name = "sempname")
    @JsonProperty("sempname")
    @ApiModelProperty("收料人")
    private String sempname;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMItem item;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMPODetail podetail;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMService emservice;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMStorePart storepart;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMStore store;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfempid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfempid2;



    /**
     * 设置 [入库单名称]
     */
    public void setEmitemrinname(String emitemrinname) {
        this.emitemrinname = emitemrinname;
        this.modify("emitemrinname", emitemrinname);
    }

    /**
     * 设置 [sap传输异常文本]
     */
    public void setSapreason1(String sapreason1) {
        this.sapreason1 = sapreason1;
        this.modify("sapreason1", sapreason1);
    }

    /**
     * 设置 [实收数]
     */
    public void setPsum(Double psum) {
        this.psum = psum;
        this.modify("psum", psum);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [工作流状态]
     */
    public void setWfstate(Integer wfstate) {
        this.wfstate = wfstate;
        this.modify("wfstate", wfstate);
    }

    /**
     * 设置 [物品金额]
     */
    public void setAmount(Double amount) {
        this.amount = amount;
        this.modify("amount", amount);
    }

    /**
     * 设置 [批次]
     */
    public void setBatcode(String batcode) {
        this.batcode = batcode;
        this.modify("batcode", batcode);
    }

    /**
     * 设置 [入库状态]
     */
    public void setRinstate(Integer rinstate) {
        this.rinstate = rinstate;
        this.modify("rinstate", rinstate);
    }

    /**
     * 设置 [sap传输失败原因]
     */
    public void setSapreason(String sapreason) {
        this.sapreason = sapreason;
        this.modify("sapreason", sapreason);
    }

    /**
     * 设置 [sap传输状态]
     */
    public void setSap(Integer sap) {
        this.sap = sap;
        this.modify("sap", sap);
    }

    /**
     * 设置 [工作流实例]
     */
    public void setWfinstanceid(String wfinstanceid) {
        this.wfinstanceid = wfinstanceid;
        this.modify("wfinstanceid", wfinstanceid);
    }

    /**
     * 设置 [流程步骤]
     */
    public void setWfstep(String wfstep) {
        this.wfstep = wfstep;
        this.modify("wfstep", wfstep);
    }

    /**
     * 设置 [单价]
     */
    public void setPrice(Double price) {
        this.price = price;
        this.modify("price", price);
    }

    /**
     * 设置 [sap税码]
     */
    public void setSapsm(String sapsm) {
        this.sapsm = sapsm;
        this.modify("sapsm", sapsm);
    }

    /**
     * 设置 [入库日期]
     */
    public void setSdate(Timestamp sdate) {
        this.sdate = sdate;
        this.modify("sdate", sdate);
    }

    /**
     * 格式化日期 [入库日期]
     */
    public String formatSdate() {
        if (this.sdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(sdate);
    }
    /**
     * 设置 [sap控制]
     */
    public void setSapcontrol(Integer sapcontrol) {
        this.sapcontrol = sapcontrol;
        this.modify("sapcontrol", sapcontrol);
    }

    /**
     * 设置 [服务商]
     */
    public void setEmserviceid(String emserviceid) {
        this.emserviceid = emserviceid;
        this.modify("emserviceid", emserviceid);
    }

    /**
     * 设置 [订单条目]
     */
    public void setPodetailid(String podetailid) {
        this.podetailid = podetailid;
        this.modify("podetailid", podetailid);
    }

    /**
     * 设置 [仓库]
     */
    public void setStoreid(String storeid) {
        this.storeid = storeid;
        this.modify("storeid", storeid);
    }

    /**
     * 设置 [物品]
     */
    public void setItemid(String itemid) {
        this.itemid = itemid;
        this.modify("itemid", itemid);
    }

    /**
     * 设置 [库位]
     */
    public void setStorepartid(String storepartid) {
        this.storepartid = storepartid;
        this.modify("storepartid", storepartid);
    }

    /**
     * 设置 [制单人]
     */
    public void setEmpid(String empid) {
        this.empid = empid;
        this.modify("empid", empid);
    }

    /**
     * 设置 [收料人]
     */
    public void setSempid(String sempid) {
        this.sempid = sempid;
        this.modify("sempid", sempid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emitemrinid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


