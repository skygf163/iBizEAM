package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEquip;
import cn.ibizlab.eam.core.eam_core.filter.EMEquipSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEquip] 服务对象接口
 */
public interface IEMEquipService extends IService<EMEquip> {

    boolean create(EMEquip et);
    void createBatch(List<EMEquip> list);
    boolean update(EMEquip et);
    void updateBatch(List<EMEquip> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMEquip get(String key);
    EMEquip getDraft(EMEquip et);
    boolean checkKey(EMEquip et);
    boolean save(EMEquip et);
    void saveBatch(List<EMEquip> list);
    Page<EMEquip> searchDefault(EMEquipSearchContext context);
    Page<EMEquip> searchEQTypeTree(EMEquipSearchContext context);
    Page<Map> searchTypeEQNum(EMEquipSearchContext context);
    List<EMEquip> selectByAcclassid(String emacclassid);
    void removeByAcclassid(String emacclassid);
    List<EMEquip> selectByAssetid(String emassetid);
    void removeByAssetid(String emassetid);
    List<EMEquip> selectByEmberthid(String emberthid);
    void removeByEmberthid(String emberthid);
    List<EMEquip> selectByEmbrandid(String embrandid);
    void removeByEmbrandid(String embrandid);
    List<EMEquip> selectByEqlocationid(String emeqlocationid);
    void removeByEqlocationid(String emeqlocationid);
    List<EMEquip> selectByEqtypeid(String emeqtypeid);
    void removeByEqtypeid(String emeqtypeid);
    List<EMEquip> selectByEquippid(String emequipid);
    void removeByEquippid(String emequipid);
    List<EMEquip> selectByEmmachinecategoryid(String emmachinecategoryid);
    void removeByEmmachinecategoryid(String emmachinecategoryid);
    List<EMEquip> selectByEmmachmodelid(String emmachmodelid);
    void removeByEmmachmodelid(String emmachmodelid);
    List<EMEquip> selectByLabserviceid(String emserviceid);
    void removeByLabserviceid(String emserviceid);
    List<EMEquip> selectByMserviceid(String emserviceid);
    void removeByMserviceid(String emserviceid);
    List<EMEquip> selectByRserviceid(String emserviceid);
    void removeByRserviceid(String emserviceid);
    List<EMEquip> selectByContractid(String pfcontractid);
    void removeByContractid(String pfcontractid);
    List<EMEquip> selectByRteamid(String pfteamid);
    void removeByRteamid(String pfteamid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEquip> getEmequipByIds(List<String> ids);
    List<EMEquip> getEmequipByEntities(List<EMEquip> entities);
}


