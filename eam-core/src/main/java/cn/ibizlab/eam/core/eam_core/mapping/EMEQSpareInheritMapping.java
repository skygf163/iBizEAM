

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQSpare;
import cn.ibizlab.eam.core.eam_core.domain.EMObject;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMEQSpareInheritMapping {

    @Mappings({
        @Mapping(source ="emeqspareid",target = "emobjectid"),
        @Mapping(source ="emeqsparename",target = "emobjectname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="description",target = "description"),
        @Mapping(source ="eqsparecode",target = "objectcode"),
        @Mapping(source ="orgid",target = "orgid"),
    })
    EMObject toEmobject(EMEQSpare minorEntity);

    @Mappings({
        @Mapping(source ="emobjectid" ,target = "emeqspareid"),
        @Mapping(source ="emobjectname" ,target = "emeqsparename"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="objectcode",target = "eqsparecode"),
    })
    EMEQSpare toEmeqspare(EMObject majorEntity);

    List<EMObject> toEmobject(List<EMEQSpare> minorEntities);

    List<EMEQSpare> toEmeqspare(List<EMObject> majorEntities);

}


