package cn.ibizlab.eam.core.eam_core.service.logic.impl;

import java.util.Map;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;
import cn.ibizlab.eam.core.eam_core.service.logic.IEMWO_INNERCheckValueLogic;
import cn.ibizlab.eam.core.eam_core.domain.EMWO_INNER;

/**
 * 关系型数据实体[CheckValue] 对象
 */
@Slf4j
@Service
public class EMWO_INNERCheckValueLogicImpl implements IEMWO_INNERCheckValueLogic {

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMWO_INNERService emwo_innerservice;

    public cn.ibizlab.eam.core.eam_core.service.IEMWO_INNERService getEmwo_innerService() {
        return this.emwo_innerservice;
    }


    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMWO_INNERService iBzSysDefaultService;

    public cn.ibizlab.eam.core.eam_core.service.IEMWO_INNERService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    @Override
    public void execute(EMWO_INNER et) {

        KieSession kieSession = null;
        try {
            kieSession = kieContainer.newKieSession();
            kieSession.insert(et); 
            kieSession.setGlobal("emwo_innercheckvaluedefault", et);
            kieSession.setGlobal("emwo_innerservice", emwo_innerservice);
            kieSession.setGlobal("iBzSysEmwo_innerDefaultService", iBzSysDefaultService);
            kieSession.setGlobal("curuser", cn.ibizlab.eam.util.security.AuthenticationUser.getAuthenticationUser());
            kieSession.startProcess("cn.ibizlab.eam.core.eam_core.service.logic.emwo_innercheckvalue");

        } catch (Exception e) {
            throw new RuntimeException("执行[新建检查值]处理逻辑发生异常" + e);
        } finally {
            if(kieSession != null) {
                kieSession.destroy();
            }
        }
    }
}
