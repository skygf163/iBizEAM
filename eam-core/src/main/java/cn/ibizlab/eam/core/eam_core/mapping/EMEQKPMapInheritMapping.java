

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQKPMap;
import cn.ibizlab.eam.core.eam_core.domain.EMObjMap;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMEQKPMapInheritMapping {

    @Mappings({
        @Mapping(source ="emeqkpmapid",target = "emobjmapid"),
        @Mapping(source ="emeqkpmapname",target = "emobjmapname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="description",target = "description"),
        @Mapping(source ="kpid",target = "objid"),
        @Mapping(source ="refobjid",target = "objpid"),
        @Mapping(source ="orgid",target = "orgid"),
    })
    EMObjMap toEmobjmap(EMEQKPMap minorEntity);

    @Mappings({
        @Mapping(source ="emobjmapid" ,target = "emeqkpmapid"),
        @Mapping(source ="emobjmapname" ,target = "emeqkpmapname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="objid",target = "kpid"),
        @Mapping(source ="objpid",target = "refobjid"),
    })
    EMEQKPMap toEmeqkpmap(EMObjMap majorEntity);

    List<EMObjMap> toEmobjmap(List<EMEQKPMap> minorEntities);

    List<EMEQKPMap> toEmeqkpmap(List<EMObjMap> majorEntities);

}


