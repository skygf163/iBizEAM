package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMPO;
/**
 * 关系型数据实体[EMPO] 查询条件对象
 */
@Slf4j
@Data
public class EMPOSearchContext extends QueryWrapperContext<EMPO> {

	private String n_empoid_eq;//[订单号]
	public void setN_empoid_eq(String n_empoid_eq) {
        this.n_empoid_eq = n_empoid_eq;
        if(!ObjectUtils.isEmpty(this.n_empoid_eq)){
            this.getSearchCond().eq("empoid", n_empoid_eq);
        }
    }
	private Integer n_postate_eq;//[订单状态]
	public void setN_postate_eq(Integer n_postate_eq) {
        this.n_postate_eq = n_postate_eq;
        if(!ObjectUtils.isEmpty(this.n_postate_eq)){
            this.getSearchCond().eq("postate", n_postate_eq);
        }
    }
	private String n_emponame_like;//[订单名称]
	public void setN_emponame_like(String n_emponame_like) {
        this.n_emponame_like = n_emponame_like;
        if(!ObjectUtils.isEmpty(this.n_emponame_like)){
            this.getSearchCond().like("emponame", n_emponame_like);
        }
    }
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
	private Timestamp n_createdate_gtandeq;//[建立时间]
	public void setN_createdate_gtandeq(Timestamp n_createdate_gtandeq) {
        this.n_createdate_gtandeq = n_createdate_gtandeq;
        if(!ObjectUtils.isEmpty(this.n_createdate_gtandeq)){
            this.getSearchCond().ge("createdate", n_createdate_gtandeq);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_wfstep_eq;//[流程步骤]
	public void setN_wfstep_eq(String n_wfstep_eq) {
        this.n_wfstep_eq = n_wfstep_eq;
        if(!ObjectUtils.isEmpty(this.n_wfstep_eq)){
            this.getSearchCond().eq("wfstep", n_wfstep_eq);
        }
    }
	private String n_payway_eq;//[付款方式]
	public void setN_payway_eq(String n_payway_eq) {
        this.n_payway_eq = n_payway_eq;
        if(!ObjectUtils.isEmpty(this.n_payway_eq)){
            this.getSearchCond().eq("payway", n_payway_eq);
        }
    }
	private String n_poinfo_like;//[订单信息]
	public void setN_poinfo_like(String n_poinfo_like) {
        this.n_poinfo_like = n_poinfo_like;
        if(!ObjectUtils.isEmpty(this.n_poinfo_like)){
            this.getSearchCond().like("poinfo", n_poinfo_like);
        }
    }
	private String n_labservicename_eq;//[产品供应商]
	public void setN_labservicename_eq(String n_labservicename_eq) {
        this.n_labservicename_eq = n_labservicename_eq;
        if(!ObjectUtils.isEmpty(this.n_labservicename_eq)){
            this.getSearchCond().eq("labservicename", n_labservicename_eq);
        }
    }
	private String n_labservicename_like;//[产品供应商]
	public void setN_labservicename_like(String n_labservicename_like) {
        this.n_labservicename_like = n_labservicename_like;
        if(!ObjectUtils.isEmpty(this.n_labservicename_like)){
            this.getSearchCond().like("labservicename", n_labservicename_like);
        }
    }
	private String n_labserviceid_eq;//[产品供应商]
	public void setN_labserviceid_eq(String n_labserviceid_eq) {
        this.n_labserviceid_eq = n_labserviceid_eq;
        if(!ObjectUtils.isEmpty(this.n_labserviceid_eq)){
            this.getSearchCond().eq("labserviceid", n_labserviceid_eq);
        }
    }
	private String n_zjlempid_eq;//[总经理]
	public void setN_zjlempid_eq(String n_zjlempid_eq) {
        this.n_zjlempid_eq = n_zjlempid_eq;
        if(!ObjectUtils.isEmpty(this.n_zjlempid_eq)){
            this.getSearchCond().eq("zjlempid", n_zjlempid_eq);
        }
    }
	private String n_zjlempname_eq;//[总经理]
	public void setN_zjlempname_eq(String n_zjlempname_eq) {
        this.n_zjlempname_eq = n_zjlempname_eq;
        if(!ObjectUtils.isEmpty(this.n_zjlempname_eq)){
            this.getSearchCond().eq("zjlempname", n_zjlempname_eq);
        }
    }
	private String n_zjlempname_like;//[总经理]
	public void setN_zjlempname_like(String n_zjlempname_like) {
        this.n_zjlempname_like = n_zjlempname_like;
        if(!ObjectUtils.isEmpty(this.n_zjlempname_like)){
            this.getSearchCond().like("zjlempname", n_zjlempname_like);
        }
    }
	private String n_fgempid_eq;//[采购分管副总]
	public void setN_fgempid_eq(String n_fgempid_eq) {
        this.n_fgempid_eq = n_fgempid_eq;
        if(!ObjectUtils.isEmpty(this.n_fgempid_eq)){
            this.getSearchCond().eq("fgempid", n_fgempid_eq);
        }
    }
	private String n_fgempname_eq;//[采购分管副总]
	public void setN_fgempname_eq(String n_fgempname_eq) {
        this.n_fgempname_eq = n_fgempname_eq;
        if(!ObjectUtils.isEmpty(this.n_fgempname_eq)){
            this.getSearchCond().eq("fgempname", n_fgempname_eq);
        }
    }
	private String n_fgempname_like;//[采购分管副总]
	public void setN_fgempname_like(String n_fgempname_like) {
        this.n_fgempname_like = n_fgempname_like;
        if(!ObjectUtils.isEmpty(this.n_fgempname_like)){
            this.getSearchCond().like("fgempname", n_fgempname_like);
        }
    }
	private String n_apprempid_eq;//[批准人]
	public void setN_apprempid_eq(String n_apprempid_eq) {
        this.n_apprempid_eq = n_apprempid_eq;
        if(!ObjectUtils.isEmpty(this.n_apprempid_eq)){
            this.getSearchCond().eq("apprempid", n_apprempid_eq);
        }
    }
	private String n_apprempname_eq;//[批准人]
	public void setN_apprempname_eq(String n_apprempname_eq) {
        this.n_apprempname_eq = n_apprempname_eq;
        if(!ObjectUtils.isEmpty(this.n_apprempname_eq)){
            this.getSearchCond().eq("apprempname", n_apprempname_eq);
        }
    }
	private String n_apprempname_like;//[批准人]
	public void setN_apprempname_like(String n_apprempname_like) {
        this.n_apprempname_like = n_apprempname_like;
        if(!ObjectUtils.isEmpty(this.n_apprempname_like)){
            this.getSearchCond().like("apprempname", n_apprempname_like);
        }
    }
	private String n_rempid_eq;//[采购员]
	public void setN_rempid_eq(String n_rempid_eq) {
        this.n_rempid_eq = n_rempid_eq;
        if(!ObjectUtils.isEmpty(this.n_rempid_eq)){
            this.getSearchCond().eq("rempid", n_rempid_eq);
        }
    }
	private String n_rempname_eq;//[采购员]
	public void setN_rempname_eq(String n_rempname_eq) {
        this.n_rempname_eq = n_rempname_eq;
        if(!ObjectUtils.isEmpty(this.n_rempname_eq)){
            this.getSearchCond().eq("rempname", n_rempname_eq);
        }
    }
	private String n_rempname_like;//[采购员]
	public void setN_rempname_like(String n_rempname_like) {
        this.n_rempname_like = n_rempname_like;
        if(!ObjectUtils.isEmpty(this.n_rempname_like)){
            this.getSearchCond().like("rempname", n_rempname_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emponame", query)
            );
		 }
	}
}



