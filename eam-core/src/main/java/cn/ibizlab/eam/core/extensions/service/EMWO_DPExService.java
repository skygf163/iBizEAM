package cn.ibizlab.eam.core.extensions.service;

import cn.ibizlab.eam.core.eam_core.domain.EMWO;
import cn.ibizlab.eam.core.eam_core.service.impl.EMWO_DPServiceImpl;
import cn.ibizlab.eam.core.util.helper.AddAHhelper;
import cn.ibizlab.eam.core.util.helper.Aops;
import cn.ibizlab.eam.util.dict.StaticDict;
import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.eam.core.eam_core.domain.EMWO_DP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Primary;


/**
 * 实体[点检工单] 自定义服务对象
 */
@Slf4j
@Primary
@Service("EMWO_DPExService")
public class EMWO_DPExService extends EMWO_DPServiceImpl {

    @Autowired
    private AddAHhelper addAHhelper;

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

    /**
     * [Acceptance:验收通过] 行为扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public EMWO_DP acceptance(EMWO_DP et) {
        et = this.get(et.getEmwoDpid());
        EMWO emwo = emwoService.get(et.getWopid());
        if (emwo == null){
            throw new RuntimeException("工单不存在，无法完成操作");
        }
        if (et == null){
            throw new RuntimeException("点检工单不存在，无法完成操作");
        }

        //根据工单类型插入不同的历史记录
        addAHhelper.genRecord(emwo);
        et.setWostate(Integer.valueOf(StaticDict.EMWOSTATE.ITEM_30.getValue()));
        Aops.getSelf(this).update(et);

        emwo.setWostate(Integer.valueOf(StaticDict.EMWOSTATE.ITEM_30.getValue()));
        emwoService.update(emwo);

        return super.acceptance(et);
    }

    @Override
    public boolean create(EMWO_DP et) {
        // 点检人如果为空，默认点检人为责任人
        if (et.getRempid() != null && et.getWpersonid() == null) {
            et.setWpersonid(et.getRempid());
            et.setWpersonname(et.getRempname());
        }
        // 实际点检时间如果为空，默认点检时间为安排日期
        if (et.getWodate() != null && et.getRegionbegindate() == null) {
            et.setRegionbegindate(et.getWodate());
        }
        // 指派抄表人如果为空，默认指派抄表人为责任人
        if (et.getRempid() != null && et.getRecvpersonid() == null) {
            et.setRecvpersonid(et.getRempid());
            et.setRecvpersonname(et.getRempname());
        }
        return super.create(et);
    }
}

