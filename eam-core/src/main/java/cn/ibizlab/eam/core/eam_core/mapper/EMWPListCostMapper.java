package cn.ibizlab.eam.core.eam_core.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.eam.core.eam_core.domain.EMWPListCost;
import cn.ibizlab.eam.core.eam_core.filter.EMWPListCostSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface EMWPListCostMapper extends BaseMapper<EMWPListCost> {

    Page<EMWPListCost> searchCostByItem(IPage page, @Param("srf") EMWPListCostSearchContext context, @Param("ew") Wrapper<EMWPListCost> wrapper);
    Page<EMWPListCost> searchDefault(IPage page, @Param("srf") EMWPListCostSearchContext context, @Param("ew") Wrapper<EMWPListCost> wrapper);
    @Override
    EMWPListCost selectById(Serializable id);
    @Override
    int insert(EMWPListCost entity);
    @Override
    int updateById(@Param(Constants.ENTITY) EMWPListCost entity);
    @Override
    int update(@Param(Constants.ENTITY) EMWPListCost entity, @Param("ew") Wrapper<EMWPListCost> updateWrapper);
    @Override
    int deleteById(Serializable id);
    /**
    * 自定义查询SQL
    * @param sql
    * @return
    */
    @Select("${sql}")
    List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<EMWPListCost> selectByItemid(@Param("emitemid") Serializable emitemid);

    List<EMWPListCost> selectByLabserviceid(@Param("emserviceid") Serializable emserviceid);

    List<EMWPListCost> selectByWplistid(@Param("emwplistid") Serializable emwplistid);

    List<EMWPListCost> selectByUnitid(@Param("pfunitid") Serializable pfunitid);

}
