

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMWO_INNER;
import cn.ibizlab.eam.core.eam_core.domain.EMResRefObj;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMWO_INNERInheritMapping {

    @Mappings({
        @Mapping(source ="emwoInnerid",target = "emresrefobjid"),
        @Mapping(source ="emwoInnername",target = "emresrefobjname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="wopid",target = "resrefobjpid"),
    })
    EMResRefObj toEmresrefobj(EMWO_INNER minorEntity);

    @Mappings({
        @Mapping(source ="emresrefobjid" ,target = "emwoInnerid"),
        @Mapping(source ="emresrefobjname" ,target = "emwoInnername"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="resrefobjpid",target = "wopid"),
    })
    EMWO_INNER toEmwoInner(EMResRefObj majorEntity);

    List<EMResRefObj> toEmresrefobj(List<EMWO_INNER> minorEntities);

    List<EMWO_INNER> toEmwoInner(List<EMResRefObj> majorEntities);

}


