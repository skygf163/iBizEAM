package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMCab;
import cn.ibizlab.eam.core.eam_core.filter.EMCabSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMCabService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMCabMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[货架] 服务对象接口实现
 */
@Slf4j
@Service("EMCabServiceImpl")
public class EMCabServiceImpl extends ServiceImpl<EMCabMapper, EMCab> implements IEMCabService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemService emitemService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMStorePartService emstorepartService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMStoreService emstoreService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMCab et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmcabid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMCab> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMCab et) {
        fillParentData(et);
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emcabid", et.getEmcabid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmcabid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMCab> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMCab get(String key) {
        EMCab et = getById(key);
        if(et == null){
            et = new EMCab();
            et.setEmcabid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMCab getDraft(EMCab et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMCab et) {
        return (!ObjectUtils.isEmpty(et.getEmcabid())) && (!Objects.isNull(this.getById(et.getEmcabid())));
    }
    @Override
    @Transactional
    public boolean save(EMCab et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMCab et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMCab> list) {
        list.forEach(item->fillParentData(item));
        List<EMCab> create = new ArrayList<>();
        List<EMCab> update = new ArrayList<>();
        for (EMCab et : list) {
            if (ObjectUtils.isEmpty(et.getEmcabid()) || ObjectUtils.isEmpty(getById(et.getEmcabid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMCab> list) {
        list.forEach(item->fillParentData(item));
        List<EMCab> create = new ArrayList<>();
        List<EMCab> update = new ArrayList<>();
        for (EMCab et : list) {
            if (ObjectUtils.isEmpty(et.getEmcabid()) || ObjectUtils.isEmpty(getById(et.getEmcabid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMCab> selectByEmstorepartid(String emstorepartid) {
        return baseMapper.selectByEmstorepartid(emstorepartid);
    }
    @Override
    public void removeByEmstorepartid(String emstorepartid) {
        this.remove(new QueryWrapper<EMCab>().eq("emstorepartid",emstorepartid));
    }

	@Override
    public List<EMCab> selectByEmstoreid(String emstoreid) {
        return baseMapper.selectByEmstoreid(emstoreid);
    }
    @Override
    public void removeByEmstoreid(String emstoreid) {
        this.remove(new QueryWrapper<EMCab>().eq("emstoreid",emstoreid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMCab> searchDefault(EMCabSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMCab> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMCab>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMCab et){
        //实体关系[DER1N_EMCAB_EMSTOREPART_EMSTOREPARTID]
        if(!ObjectUtils.isEmpty(et.getEmstorepartid())){
            cn.ibizlab.eam.core.eam_core.domain.EMStorePart emstorepart=et.getEmstorepart();
            if(ObjectUtils.isEmpty(emstorepart)){
                cn.ibizlab.eam.core.eam_core.domain.EMStorePart majorEntity=emstorepartService.get(et.getEmstorepartid());
                et.setEmstorepart(majorEntity);
                emstorepart=majorEntity;
            }
            et.setEmstorepartname(emstorepart.getEmstorepartname());
        }
        //实体关系[DER1N_EMCAB_EMSTORE_EMSTOREID]
        if(!ObjectUtils.isEmpty(et.getEmstoreid())){
            cn.ibizlab.eam.core.eam_core.domain.EMStore emstore=et.getEmstore();
            if(ObjectUtils.isEmpty(emstore)){
                cn.ibizlab.eam.core.eam_core.domain.EMStore majorEntity=emstoreService.get(et.getEmstoreid());
                et.setEmstore(majorEntity);
                emstore=majorEntity;
            }
            et.setEmstorename(emstore.getEmstorename());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMCab> getEmcabByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMCab> getEmcabByEntities(List<EMCab> entities) {
        List ids =new ArrayList();
        for(EMCab entity : entities){
            Serializable id=entity.getEmcabid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMCabService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



