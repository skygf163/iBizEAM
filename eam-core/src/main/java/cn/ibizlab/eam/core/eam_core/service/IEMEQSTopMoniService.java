package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEQSTopMoni;
import cn.ibizlab.eam.core.eam_core.filter.EMEQSTopMoniSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEQSTopMoni] 服务对象接口
 */
public interface IEMEQSTopMoniService extends IService<EMEQSTopMoni> {

    boolean create(EMEQSTopMoni et);
    void createBatch(List<EMEQSTopMoni> list);
    boolean update(EMEQSTopMoni et);
    void updateBatch(List<EMEQSTopMoni> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMEQSTopMoni get(String key);
    EMEQSTopMoni getDraft(EMEQSTopMoni et);
    boolean checkKey(EMEQSTopMoni et);
    boolean save(EMEQSTopMoni et);
    void saveBatch(List<EMEQSTopMoni> list);
    Page<EMEQSTopMoni> searchDefault(EMEQSTopMoniSearchContext context);
    List<EMEQSTopMoni> selectByEmeqstopid(String emeqstopid);
    void removeByEmeqstopid(String emeqstopid);
    List<EMEQSTopMoni> selectByEmeqtypeid(String emeqtypeid);
    void removeByEmeqtypeid(String emeqtypeid);
    List<EMEQSTopMoni> selectByEmequipid(String emequipid);
    void removeByEmequipid(String emequipid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEQSTopMoni> getEmeqstopmoniByIds(List<String> ids);
    List<EMEQSTopMoni> getEmeqstopmoniByEntities(List<EMEQSTopMoni> entities);
}


