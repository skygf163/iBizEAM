package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[机种编号]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMMACHINECATEGORY_BASE", resultMap = "EMMachineCategoryResultMap")
@ApiModel("机种编号")
public class EMMachineCategory extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 流水号
     */
    @DEField(isKeyField = true)
    @TableId(value = "emmachinecategoryid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emmachinecategoryid")
    @JsonProperty("emmachinecategoryid")
    @ApiModelProperty("流水号")
    private String emmachinecategoryid;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;
    /**
     * 机种名称
     */
    @TableField(value = "emmachinecategoryname")
    @JSONField(name = "emmachinecategoryname")
    @JsonProperty("emmachinecategoryname")
    @ApiModelProperty("机种名称")
    private String emmachinecategoryname;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @ApiModelProperty("更新人")
    private String updateman;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @ApiModelProperty("建立人")
    private String createman;
    /**
     * 备注
     */
    @TableField(value = "remarks")
    @JSONField(name = "remarks")
    @JsonProperty("remarks")
    @ApiModelProperty("备注")
    private String remarks;
    /**
     * 机种信息
     */
    @TableField(exist = false)
    @JSONField(name = "jzinfo")
    @JsonProperty("jzinfo")
    @ApiModelProperty("机种信息")
    private String jzinfo;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;
    /**
     * 机种编码
     */
    @TableField(value = "machtypecode")
    @JSONField(name = "machtypecode")
    @JsonProperty("machtypecode")
    @ApiModelProperty("机种编码")
    private String machtypecode;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;



    /**
     * 设置 [机种名称]
     */
    public void setEmmachinecategoryname(String emmachinecategoryname) {
        this.emmachinecategoryname = emmachinecategoryname;
        this.modify("emmachinecategoryname", emmachinecategoryname);
    }

    /**
     * 设置 [备注]
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
        this.modify("remarks", remarks);
    }

    /**
     * 设置 [机种编码]
     */
    public void setMachtypecode(String machtypecode) {
        this.machtypecode = machtypecode;
        this.modify("machtypecode", machtypecode);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emmachinecategoryid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


