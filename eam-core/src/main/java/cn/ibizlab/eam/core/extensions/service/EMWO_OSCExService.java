package cn.ibizlab.eam.core.extensions.service;

import cn.ibizlab.eam.core.eam_core.domain.EMWO;
import cn.ibizlab.eam.core.eam_core.service.impl.EMWO_OSCServiceImpl;
import cn.ibizlab.eam.core.util.helper.AddAHhelper;
import cn.ibizlab.eam.core.util.helper.Aops;
import cn.ibizlab.eam.util.dict.StaticDict;
import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.eam.core.eam_core.domain.EMWO_OSC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Primary;

import java.sql.Timestamp;
import java.util.*;

/**
 * 实体[外委保养工单] 自定义服务对象
 */
@Slf4j
@Primary
@Service("EMWO_OSCExService")
public class EMWO_OSCExService extends EMWO_OSCServiceImpl {

    @Autowired
    private AddAHhelper addAHhelper;

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

    /**
     * [Acceptance:验收通过] 行为扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public EMWO_OSC acceptance(EMWO_OSC et) {

        et = this.get(et.getEmwoOscid());
        EMWO emwo = emwoService.get(et.getEmwoOscid());
        //根据对应的归档信息，插入对应的工单记录
        String strEMEQAHTYPE = emwo.getArchive();

        if (emwo == null){
            throw new RuntimeException("工单不存在，无法完成操作");
        }
        if (et == null){
            throw new RuntimeException("外委工单不存在，无法完成操作");
        }

        //根据工单类型插入不同的历史记录
        addAHhelper.genRecord(emwo) ;

        et.setWostate(Integer.valueOf(StaticDict.EMWOSTATE.ITEM_30.getValue()));
        Aops.getSelf(this).update(et);
        emwo.setWostate(Integer.valueOf(StaticDict.EMWOSTATE.ITEM_30.getValue()));
        emwoService.update(emwo);

        return super.acceptance(et);
    }

    /**
     * [UnAcceptance:验收不通过] 行为扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public EMWO_OSC unAcceptance(EMWO_OSC et) {
        //验证不通过，则非正常关闭
        et = this.get(et.getEmwoOscid());
        EMWO emwo = emwoService.get(et.getEmwoOscid());

        if (emwo==null){
            throw new RuntimeException("工单不存在，无法完成操作");
        }
        if (et==null){
            throw new RuntimeException("外委工单不存在，无法完成操作");
        }
        et.setWostate(Integer.valueOf(StaticDict.EMWOSTATE.ITEM_40.getValue()));
        Aops.getSelf(this).update(et);

        emwo.setWostate(Integer.valueOf(StaticDict.EMWOSTATE.ITEM_40.getValue()));
        emwoService.update(emwo);
        return super.unAcceptance(et);
    }

    @Override
    public boolean create(EMWO_OSC et) {
        Calendar calendar = Calendar.getInstance();
        // 起始时间
        if (et.getRegionbegindate() == null) {
            et.setRegionbegindate(et.getWodate());
        }
        // 结束时间=起始时间+安排工时(activeLengths)
        if (et.getRegionenddate() == null && et.getActivelengths() != null) {
            long activelengths = et.getActivelengths().longValue();
            et.setRegionenddate(new Timestamp(et.getRegionbegindate().getTime() + activelengths * 60 * 60 * 1000));
        }
        // 执行监理人如果为空，设置为责任人
        if (et.getRempid() != null && et.getWpersonid() == null) {
            et.setWpersonid(et.getRempid());
            et.setWpersonname(et.getRempname());
        }
        // 指派监理人如果为空，设置为责任人
        if (et.getRempid() != null && et.getRecvpersonid() == null) {
            et.setRecvpersonid(et.getRempid());
            et.setRecvpersonname(et.getRempname());
        }
        // 制定时间
        if (et.getMdate() == null) {
            et.setMdate(new Timestamp(calendar.getTimeInMillis()));
        }
        // 过期时间=制定时间+10天
        if (et.getMdate() != null && et.getExpiredate() == null) {
            calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) + 10);
            et.setExpiredate(new Timestamp(calendar.getTimeInMillis()));
        }
        return super.create(et);
    }
}

