

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMWO_DP;
import cn.ibizlab.eam.core.eam_core.domain.EMResRefObj;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMWO_DPInheritMapping {

    @Mappings({
        @Mapping(source ="emwoDpid",target = "emresrefobjid"),
        @Mapping(source ="emwoDpname",target = "emresrefobjname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="wopid",target = "resrefobjpid"),
    })
    EMResRefObj toEmresrefobj(EMWO_DP minorEntity);

    @Mappings({
        @Mapping(source ="emresrefobjid" ,target = "emwoDpid"),
        @Mapping(source ="emresrefobjname" ,target = "emwoDpname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="resrefobjpid",target = "wopid"),
    })
    EMWO_DP toEmwoDp(EMResRefObj majorEntity);

    List<EMResRefObj> toEmresrefobj(List<EMWO_DP> minorEntities);

    List<EMWO_DP> toEmwoDp(List<EMResRefObj> majorEntities);

}


