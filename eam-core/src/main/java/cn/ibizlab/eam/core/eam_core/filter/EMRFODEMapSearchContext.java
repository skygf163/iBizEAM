package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMRFODEMap;
/**
 * 关系型数据实体[EMRFODEMap] 查询条件对象
 */
@Slf4j
@Data
public class EMRFODEMapSearchContext extends QueryWrapperContext<EMRFODEMap> {

	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_emrfodemapname_like;//[现象引用名称]
	public void setN_emrfodemapname_like(String n_emrfodemapname_like) {
        this.n_emrfodemapname_like = n_emrfodemapname_like;
        if(!ObjectUtils.isEmpty(this.n_emrfodemapname_like)){
            this.getSearchCond().like("emrfodemapname", n_emrfodemapname_like);
        }
    }
	private String n_rfodename_eq;//[现象]
	public void setN_rfodename_eq(String n_rfodename_eq) {
        this.n_rfodename_eq = n_rfodename_eq;
        if(!ObjectUtils.isEmpty(this.n_rfodename_eq)){
            this.getSearchCond().eq("rfodename", n_rfodename_eq);
        }
    }
	private String n_rfodename_like;//[现象]
	public void setN_rfodename_like(String n_rfodename_like) {
        this.n_rfodename_like = n_rfodename_like;
        if(!ObjectUtils.isEmpty(this.n_rfodename_like)){
            this.getSearchCond().like("rfodename", n_rfodename_like);
        }
    }
	private String n_refobjname_eq;//[现象引用]
	public void setN_refobjname_eq(String n_refobjname_eq) {
        this.n_refobjname_eq = n_refobjname_eq;
        if(!ObjectUtils.isEmpty(this.n_refobjname_eq)){
            this.getSearchCond().eq("refobjname", n_refobjname_eq);
        }
    }
	private String n_refobjname_like;//[现象引用]
	public void setN_refobjname_like(String n_refobjname_like) {
        this.n_refobjname_like = n_refobjname_like;
        if(!ObjectUtils.isEmpty(this.n_refobjname_like)){
            this.getSearchCond().like("refobjname", n_refobjname_like);
        }
    }
	private String n_rfodeid_eq;//[现象]
	public void setN_rfodeid_eq(String n_rfodeid_eq) {
        this.n_rfodeid_eq = n_rfodeid_eq;
        if(!ObjectUtils.isEmpty(this.n_rfodeid_eq)){
            this.getSearchCond().eq("rfodeid", n_rfodeid_eq);
        }
    }
	private String n_refobjid_eq;//[现象引用]
	public void setN_refobjid_eq(String n_refobjid_eq) {
        this.n_refobjid_eq = n_refobjid_eq;
        if(!ObjectUtils.isEmpty(this.n_refobjid_eq)){
            this.getSearchCond().eq("refobjid", n_refobjid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emrfodemapname", query)
            );
		 }
	}
}



