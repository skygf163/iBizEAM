package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMPLANRECORD;
import cn.ibizlab.eam.core.eam_core.filter.EMPLANRECORDSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMPLANRECORD] 服务对象接口
 */
public interface IEMPLANRECORDService extends IService<EMPLANRECORD> {

    boolean create(EMPLANRECORD et);
    void createBatch(List<EMPLANRECORD> list);
    boolean update(EMPLANRECORD et);
    void updateBatch(List<EMPLANRECORD> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMPLANRECORD get(String key);
    EMPLANRECORD getDraft(EMPLANRECORD et);
    boolean checkKey(EMPLANRECORD et);
    boolean save(EMPLANRECORD et);
    void saveBatch(List<EMPLANRECORD> list);
    Page<EMPLANRECORD> searchDefault(EMPLANRECORDSearchContext context);
    List<EMPLANRECORD> selectByEmplanid(String emplanid);
    void removeByEmplanid(String emplanid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMPLANRECORD> getEmplanrecordByIds(List<String> ids);
    List<EMPLANRECORD> getEmplanrecordByEntities(List<EMPLANRECORD> entities);
}


