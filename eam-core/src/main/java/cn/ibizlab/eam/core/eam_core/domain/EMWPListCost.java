package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[询价单]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMWPLISTCOST_BASE", resultMap = "EMWPListCostResultMap")
@ApiModel("询价单")
public class EMWPListCost extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 询价信息
     */
    @TableField(exist = false)
    @JSONField(name = "wplistcostinfo")
    @JsonProperty("wplistcostinfo")
    @ApiModelProperty("询价信息")
    private String wplistcostinfo;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;
    /**
     * 单价
     */
    @TableField(value = "price")
    @JSONField(name = "price")
    @JsonProperty("price")
    @ApiModelProperty("单价")
    private Double price;
    /**
     * 询价人
     */
    @TableField(value = "rempname")
    @JSONField(name = "rempname")
    @JsonProperty("rempname")
    @ApiModelProperty("询价人")
    private String rempname;
    /**
     * 询价人
     */
    @TableField(value = "rempid")
    @JSONField(name = "rempid")
    @JsonProperty("rempid")
    @ApiModelProperty("询价人")
    private String rempid;
    /**
     * 询价时间
     */
    @TableField(value = "adate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "adate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("adate")
    @ApiModelProperty("询价时间")
    private Timestamp adate;
    /**
     * 询价单名称
     */
    @DEField(defaultValue = "NAME")
    @TableField(value = "emwplistcostname")
    @JSONField(name = "emwplistcostname")
    @JsonProperty("emwplistcostname")
    @ApiModelProperty("询价单名称")
    private String emwplistcostname;
    /**
     * 折扣(%)
     */
    @DEField(defaultValue = "100")
    @TableField(value = "discnt")
    @JSONField(name = "discnt")
    @JsonProperty("discnt")
    @ApiModelProperty("折扣(%)")
    private Double discnt;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;
    /**
     * 物品备注
     */
    @TableField(value = "itemdesc")
    @JSONField(name = "itemdesc")
    @JsonProperty("itemdesc")
    @ApiModelProperty("物品备注")
    private String itemdesc;
    /**
     * 单位转换率
     */
    @DEField(defaultValue = "1")
    @TableField(value = "unitrate")
    @JSONField(name = "unitrate")
    @JsonProperty("unitrate")
    @ApiModelProperty("单位转换率")
    private Double unitrate;
    /**
     * 有效期起始
     */
    @TableField(value = "begindate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "begindate", format = "yyyy-MM-dd")
    @JsonProperty("begindate")
    @ApiModelProperty("有效期起始")
    private Timestamp begindate;
    /**
     * 标价
     */
    @TableField(value = "listprice")
    @JSONField(name = "listprice")
    @JsonProperty("listprice")
    @ApiModelProperty("标价")
    private Double listprice;
    /**
     * 物品
     */
    @TableField(exist = false)
    @JSONField(name = "items")
    @JsonProperty("items")
    @ApiModelProperty("物品")
    private String items;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @ApiModelProperty("组织")
    private String orgid;
    /**
     * 价格条件
     */
    @TableField(value = "pricecdt")
    @JSONField(name = "pricecdt")
    @JsonProperty("pricecdt")
    @ApiModelProperty("价格条件")
    private String pricecdt;
    /**
     * 整单位购买
     */
    @DEField(defaultValue = "0")
    @TableField(value = "intunitflag")
    @JSONField(name = "intunitflag")
    @JsonProperty("intunitflag")
    @ApiModelProperty("整单位购买")
    private Integer intunitflag;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    @ApiModelProperty("描述")
    private String description;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @ApiModelProperty("建立人")
    private String createman;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @ApiModelProperty("更新人")
    private String updateman;
    /**
     * 询价单标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emwplistcostid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emwplistcostid")
    @JsonProperty("emwplistcostid")
    @ApiModelProperty("询价单标识")
    private String emwplistcostid;
    /**
     * 采购试算(单价排序)
     */
    @TableField(exist = false)
    @JSONField(name = "wplistcosteval")
    @JsonProperty("wplistcosteval")
    @ApiModelProperty("采购试算(单价排序)")
    private Double wplistcosteval;
    /**
     * 询价单位备注
     */
    @TableField(value = "unitdesc")
    @JSONField(name = "unitdesc")
    @JsonProperty("unitdesc")
    @ApiModelProperty("询价单位备注")
    private String unitdesc;
    /**
     * 有效期截至
     */
    @TableField(value = "enddate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "enddate", format = "yyyy-MM-dd")
    @JsonProperty("enddate")
    @ApiModelProperty("有效期截至")
    private Timestamp enddate;
    /**
     * -
     */
    @TableField(exist = false)
    @JSONField(name = "wplistcostresult")
    @JsonProperty("wplistcostresult")
    @ApiModelProperty("-")
    private String wplistcostresult;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;
    /**
     * 税率
     */
    @TableField(value = "taxrate")
    @JSONField(name = "taxrate")
    @JsonProperty("taxrate")
    @ApiModelProperty("税率")
    private Double taxrate;
    /**
     * 标准单位单价
     */
    @TableField(exist = false)
    @JSONField(name = "sunitprice")
    @JsonProperty("sunitprice")
    @ApiModelProperty("标准单位单价")
    private Double sunitprice;
    /**
     * 标准单位
     */
    @TableField(exist = false)
    @JSONField(name = "sunitid")
    @JsonProperty("sunitid")
    @ApiModelProperty("标准单位")
    private String sunitid;
    /**
     * 询价单位
     */
    @TableField(exist = false)
    @JSONField(name = "unitname")
    @JsonProperty("unitname")
    @ApiModelProperty("询价单位")
    private String unitname;
    /**
     * 标准单位
     */
    @TableField(exist = false)
    @JSONField(name = "sunitname")
    @JsonProperty("sunitname")
    @ApiModelProperty("标准单位")
    private String sunitname;
    /**
     * 物品
     */
    @TableField(exist = false)
    @JSONField(name = "itemname")
    @JsonProperty("itemname")
    @ApiModelProperty("物品")
    private String itemname;
    /**
     * 产品供应商
     */
    @TableField(exist = false)
    @JSONField(name = "labservicename")
    @JsonProperty("labservicename")
    @ApiModelProperty("产品供应商")
    private String labservicename;
    /**
     * 物品均价
     */
    @TableField(exist = false)
    @JSONField(name = "avgprice")
    @JsonProperty("avgprice")
    @ApiModelProperty("物品均价")
    private Double avgprice;
    /**
     * 采购申请号
     */
    @TableField(value = "wplistid")
    @JSONField(name = "wplistid")
    @JsonProperty("wplistid")
    @ApiModelProperty("采购申请号")
    private String wplistid;
    /**
     * 产品供应商
     */
    @TableField(value = "labserviceid")
    @JSONField(name = "labserviceid")
    @JsonProperty("labserviceid")
    @ApiModelProperty("产品供应商")
    private String labserviceid;
    /**
     * 物品
     */
    @TableField(value = "itemid")
    @JSONField(name = "itemid")
    @JsonProperty("itemid")
    @ApiModelProperty("物品")
    private String itemid;
    /**
     * 询价单位
     */
    @TableField(value = "unitid")
    @JSONField(name = "unitid")
    @JsonProperty("unitid")
    @ApiModelProperty("询价单位")
    private String unitid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMItem item;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMService labservice;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMWPList wplist;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFUnit unit;



    /**
     * 设置 [单价]
     */
    public void setPrice(Double price) {
        this.price = price;
        this.modify("price", price);
    }

    /**
     * 设置 [询价人]
     */
    public void setRempname(String rempname) {
        this.rempname = rempname;
        this.modify("rempname", rempname);
    }

    /**
     * 设置 [询价人]
     */
    public void setRempid(String rempid) {
        this.rempid = rempid;
        this.modify("rempid", rempid);
    }

    /**
     * 设置 [询价时间]
     */
    public void setAdate(Timestamp adate) {
        this.adate = adate;
        this.modify("adate", adate);
    }

    /**
     * 格式化日期 [询价时间]
     */
    public String formatAdate() {
        if (this.adate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(adate);
    }
    /**
     * 设置 [询价单名称]
     */
    public void setEmwplistcostname(String emwplistcostname) {
        this.emwplistcostname = emwplistcostname;
        this.modify("emwplistcostname", emwplistcostname);
    }

    /**
     * 设置 [折扣(%)]
     */
    public void setDiscnt(Double discnt) {
        this.discnt = discnt;
        this.modify("discnt", discnt);
    }

    /**
     * 设置 [物品备注]
     */
    public void setItemdesc(String itemdesc) {
        this.itemdesc = itemdesc;
        this.modify("itemdesc", itemdesc);
    }

    /**
     * 设置 [单位转换率]
     */
    public void setUnitrate(Double unitrate) {
        this.unitrate = unitrate;
        this.modify("unitrate", unitrate);
    }

    /**
     * 设置 [有效期起始]
     */
    public void setBegindate(Timestamp begindate) {
        this.begindate = begindate;
        this.modify("begindate", begindate);
    }

    /**
     * 格式化日期 [有效期起始]
     */
    public String formatBegindate() {
        if (this.begindate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(begindate);
    }
    /**
     * 设置 [标价]
     */
    public void setListprice(Double listprice) {
        this.listprice = listprice;
        this.modify("listprice", listprice);
    }

    /**
     * 设置 [价格条件]
     */
    public void setPricecdt(String pricecdt) {
        this.pricecdt = pricecdt;
        this.modify("pricecdt", pricecdt);
    }

    /**
     * 设置 [整单位购买]
     */
    public void setIntunitflag(Integer intunitflag) {
        this.intunitflag = intunitflag;
        this.modify("intunitflag", intunitflag);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [询价单位备注]
     */
    public void setUnitdesc(String unitdesc) {
        this.unitdesc = unitdesc;
        this.modify("unitdesc", unitdesc);
    }

    /**
     * 设置 [有效期截至]
     */
    public void setEnddate(Timestamp enddate) {
        this.enddate = enddate;
        this.modify("enddate", enddate);
    }

    /**
     * 格式化日期 [有效期截至]
     */
    public String formatEnddate() {
        if (this.enddate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(enddate);
    }
    /**
     * 设置 [税率]
     */
    public void setTaxrate(Double taxrate) {
        this.taxrate = taxrate;
        this.modify("taxrate", taxrate);
    }

    /**
     * 设置 [采购申请号]
     */
    public void setWplistid(String wplistid) {
        this.wplistid = wplistid;
        this.modify("wplistid", wplistid);
    }

    /**
     * 设置 [产品供应商]
     */
    public void setLabserviceid(String labserviceid) {
        this.labserviceid = labserviceid;
        this.modify("labserviceid", labserviceid);
    }

    /**
     * 设置 [物品]
     */
    public void setItemid(String itemid) {
        this.itemid = itemid;
        this.modify("itemid", itemid);
    }

    /**
     * 设置 [询价单位]
     */
    public void setUnitid(String unitid) {
        this.unitid = unitid;
        this.modify("unitid", unitid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emwplistcostid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


