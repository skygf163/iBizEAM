

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMOutput;
import cn.ibizlab.eam.core.eam_core.domain.EMObject;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMOutputInheritMapping {

    @Mappings({
        @Mapping(source ="emoutputid",target = "emobjectid"),
        @Mapping(source ="emoutputname",target = "emobjectname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="description",target = "description"),
        @Mapping(source ="outputcode",target = "objectcode"),
        @Mapping(source ="orgid",target = "orgid"),
    })
    EMObject toEmobject(EMOutput minorEntity);

    @Mappings({
        @Mapping(source ="emobjectid" ,target = "emoutputid"),
        @Mapping(source ="emobjectname" ,target = "emoutputname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="objectcode",target = "outputcode"),
    })
    EMOutput toEmoutput(EMObject majorEntity);

    List<EMObject> toEmobject(List<EMOutput> minorEntities);

    List<EMOutput> toEmoutput(List<EMObject> majorEntities);

}


