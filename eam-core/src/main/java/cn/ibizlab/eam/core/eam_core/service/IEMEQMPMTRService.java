package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEQMPMTR;
import cn.ibizlab.eam.core.eam_core.filter.EMEQMPMTRSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEQMPMTR] 服务对象接口
 */
public interface IEMEQMPMTRService extends IService<EMEQMPMTR> {

    boolean create(EMEQMPMTR et);
    void createBatch(List<EMEQMPMTR> list);
    boolean update(EMEQMPMTR et);
    void updateBatch(List<EMEQMPMTR> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMEQMPMTR get(String key);
    EMEQMPMTR getDraft(EMEQMPMTR et);
    boolean checkKey(EMEQMPMTR et);
    boolean save(EMEQMPMTR et);
    void saveBatch(List<EMEQMPMTR> list);
    Page<EMEQMPMTR> searchDefault(EMEQMPMTRSearchContext context);
    List<EMEQMPMTR> selectByMpid(String emeqmpid);
    void removeByMpid(String emeqmpid);
    List<EMEQMPMTR> selectByEquipid(String emequipid);
    void removeByEquipid(String emequipid);
    List<EMEQMPMTR> selectByObjid(String emobjectid);
    void removeByObjid(String emobjectid);
    List<EMEQMPMTR> selectByWoid(String emwoid);
    void removeByWoid(String emwoid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEQMPMTR> getEmeqmpmtrByIds(List<String> ids);
    List<EMEQMPMTR> getEmeqmpmtrByEntities(List<EMEQMPMTR> entities);
}


