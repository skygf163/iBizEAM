DROP FUNCTION IF EXISTS `fn_getsump`;

CREATE FUNCTION `fn_getsump`(
	Prefix FLOAT
) RETURNS int(11)
BEGIN
	#Routine body goes here...

	RETURN case when (case Prefix when 0 then 80 else  round(Prefix) end) >100 then 100 else (case Prefix when 0 then 80 else  round(Prefix) end) end;
END;