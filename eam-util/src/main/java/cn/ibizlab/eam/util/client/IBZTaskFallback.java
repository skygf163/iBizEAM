package cn.ibizlab.eam.util.client;

import cn.ibizlab.eam.util.domain.JobsInfoDTO;
import org.springframework.stereotype.Component;

@Component
public class IBZTaskFallback implements IBZTaskFeignClient {


    @Override
    public JobsInfoDTO startJob(String jobsinfo_id, JobsInfoDTO jobsinfodto) {
        return null;
    }

    @Override
    public Boolean saveJob(JobsInfoDTO jobsinfodto) {
        return null;
    }
}
