import EMEQMonitor_en_US_Base from './emeqmonitor_en_US_base';

function getLocaleResource(){
    const EMEQMonitor_en_US_OwnData = {};
    const targetData = Object.assign(EMEQMonitor_en_US_Base(), EMEQMonitor_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
