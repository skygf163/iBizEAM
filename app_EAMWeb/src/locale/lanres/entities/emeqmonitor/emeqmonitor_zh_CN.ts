import EMEQMonitor_zh_CN_Base from './emeqmonitor_zh_CN_base';

function getLocaleResource(){
    const EMEQMonitor_zh_CN_OwnData = {};
    const targetData = Object.assign(EMEQMonitor_zh_CN_Base(), EMEQMonitor_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;