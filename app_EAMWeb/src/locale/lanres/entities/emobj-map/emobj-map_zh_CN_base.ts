import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("对象关系", null),
		fields: {
			emobjmapid: commonLogic.appcommonhandle("对象关系标识",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			equipid: commonLogic.appcommonhandle("设备编号",null),
			emobjmaptype: commonLogic.appcommonhandle("对象关系类型",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			emobjmapname: commonLogic.appcommonhandle("对象关系名称",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			description: commonLogic.appcommonhandle("描述",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			objptype: commonLogic.appcommonhandle("上级对象类型",null),
			majorequipname: commonLogic.appcommonhandle("主设备",null),
			majorequipid: commonLogic.appcommonhandle("主设备",null),
			objpname: commonLogic.appcommonhandle("上级对象",null),
			objtype: commonLogic.appcommonhandle("对象类型",null),
			objname: commonLogic.appcommonhandle("对象",null),
			objid: commonLogic.appcommonhandle("对象",null),
			objpid: commonLogic.appcommonhandle("上级对象",null),
		},
			views: {
				locationtreeview: {
					caption: commonLogic.appcommonhandle("位置信息",null),
					title: commonLogic.appcommonhandle("位置信息",null),
				},
			},
			location_treeview: {
				nodata:commonLogic.appcommonhandle("",null),
				nodes: {
					root: commonLogic.appcommonhandle("默认根节点",null),
				},
				uiactions: {
				},
			},
			localbyeq_portlet: {
				localbyeq: {
					title: commonLogic.appcommonhandle("设备部件结构", null)
				},
				uiactions: {
				},
			},
		};
		return data;
}
export default getLocaleResourceBase;