import EMItemCS_en_US_Base from './emitem-cs_en_US_base';

function getLocaleResource(){
    const EMItemCS_en_US_OwnData = {};
    const targetData = Object.assign(EMItemCS_en_US_Base(), EMItemCS_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
