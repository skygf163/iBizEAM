import EMENConsum_en_US_Base from './emenconsum_en_US_base';

function getLocaleResource(){
    const EMENConsum_en_US_OwnData = {};
    const targetData = Object.assign(EMENConsum_en_US_Base(), EMENConsum_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
