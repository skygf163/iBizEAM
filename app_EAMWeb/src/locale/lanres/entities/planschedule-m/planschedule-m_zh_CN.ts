import PLANSCHEDULE_M_zh_CN_Base from './planschedule-m_zh_CN_base';

function getLocaleResource(){
    const PLANSCHEDULE_M_zh_CN_OwnData = {};
    const targetData = Object.assign(PLANSCHEDULE_M_zh_CN_Base(), PLANSCHEDULE_M_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;