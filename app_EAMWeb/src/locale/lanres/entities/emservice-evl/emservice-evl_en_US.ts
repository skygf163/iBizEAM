import EMServiceEvl_en_US_Base from './emservice-evl_en_US_base';

function getLocaleResource(){
    const EMServiceEvl_en_US_OwnData = {};
    const targetData = Object.assign(EMServiceEvl_en_US_Base(), EMServiceEvl_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
