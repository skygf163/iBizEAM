import EMEQType_zh_CN_Base from './emeqtype_zh_CN_base';

function getLocaleResource(){
    const EMEQType_zh_CN_OwnData = {};
    const targetData = Object.assign(EMEQType_zh_CN_Base(), EMEQType_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;