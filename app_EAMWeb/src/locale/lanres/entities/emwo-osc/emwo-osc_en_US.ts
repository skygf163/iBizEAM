import EMWO_OSC_en_US_Base from './emwo-osc_en_US_base';

function getLocaleResource(){
    const EMWO_OSC_en_US_OwnData = {};
    const targetData = Object.assign(EMWO_OSC_en_US_Base(), EMWO_OSC_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
