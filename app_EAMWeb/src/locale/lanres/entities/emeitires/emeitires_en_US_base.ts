import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			empid: commonLogic.appcommonhandle("负责人",null),
			eicaminfo: commonLogic.appcommonhandle("轮胎信息",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			description: commonLogic.appcommonhandle("描述",null),
			macaddr: commonLogic.appcommonhandle("地址",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			emeitiresname: commonLogic.appcommonhandle("编码/出场号",null),
			eistate: commonLogic.appcommonhandle("状态",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			disdesc: commonLogic.appcommonhandle("报废原因",null),
			empname: commonLogic.appcommonhandle("负责人",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			emeitiresid: commonLogic.appcommonhandle("轮胎编号",null),
			disdate: commonLogic.appcommonhandle("报废日期",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			eqmodelcode: commonLogic.appcommonhandle("型号",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			equipname: commonLogic.appcommonhandle("设备",null),
			itempusename: commonLogic.appcommonhandle("领料单",null),
			eqlocationname: commonLogic.appcommonhandle("位置",null),
			eqlocationid: commonLogic.appcommonhandle("位置",null),
			equipid: commonLogic.appcommonhandle("设备",null),
			itempuseid: commonLogic.appcommonhandle("领料单",null),
		},
			views: {
				pickupgridview: {
					caption: commonLogic.appcommonhandle("轮胎清单",null),
					title: commonLogic.appcommonhandle("轮胎清单选择表格视图",null),
				},
				pickupview: {
					caption: commonLogic.appcommonhandle("轮胎清单",null),
					title: commonLogic.appcommonhandle("轮胎清单数据选择视图",null),
				},
			},
			main_grid: {
				columns: {
					eicaminfo: commonLogic.appcommonhandle("轮胎信息",null),
					updateman: commonLogic.appcommonhandle("更新人",null),
					updatedate: commonLogic.appcommonhandle("更新时间",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
				},
				uiactions: {
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;