import EMDRWG_en_US_Base from './emdrwg_en_US_base';

function getLocaleResource(){
    const EMDRWG_en_US_OwnData = {};
    const targetData = Object.assign(EMDRWG_en_US_Base(), EMDRWG_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
