import EMMachineCategory_zh_CN_Base from './emmachine-category_zh_CN_base';

function getLocaleResource(){
    const EMMachineCategory_zh_CN_OwnData = {};
    const targetData = Object.assign(EMMachineCategory_zh_CN_Base(), EMMachineCategory_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;