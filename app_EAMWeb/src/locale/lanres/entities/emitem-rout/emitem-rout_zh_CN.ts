import EMItemROut_zh_CN_Base from './emitem-rout_zh_CN_base';

function getLocaleResource(){
    const EMItemROut_zh_CN_OwnData = {};
    const targetData = Object.assign(EMItemROut_zh_CN_Base(), EMItemROut_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;