import EMRFOCA_zh_CN_Base from './emrfoca_zh_CN_base';

function getLocaleResource(){
    const EMRFOCA_zh_CN_OwnData = {};
    const targetData = Object.assign(EMRFOCA_zh_CN_Base(), EMRFOCA_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;