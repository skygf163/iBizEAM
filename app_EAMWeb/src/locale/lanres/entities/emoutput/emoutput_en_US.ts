import EMOutput_en_US_Base from './emoutput_en_US_base';

function getLocaleResource(){
    const EMOutput_en_US_OwnData = {};
    const targetData = Object.assign(EMOutput_en_US_Base(), EMOutput_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
