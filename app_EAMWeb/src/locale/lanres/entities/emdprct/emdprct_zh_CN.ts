import EMDPRCT_zh_CN_Base from './emdprct_zh_CN_base';

function getLocaleResource(){
    const EMDPRCT_zh_CN_OwnData = {};
    const targetData = Object.assign(EMDPRCT_zh_CN_Base(), EMDPRCT_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;