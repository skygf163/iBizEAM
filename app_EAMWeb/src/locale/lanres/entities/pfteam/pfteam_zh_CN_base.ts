import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("班组", null),
		fields: {
			teamcode: commonLogic.appcommonhandle("班组代码",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			pfteamid: commonLogic.appcommonhandle("班组标识",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			teamtypeid: commonLogic.appcommonhandle("班组类型",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			description: commonLogic.appcommonhandle("描述",null),
			teamfn: commonLogic.appcommonhandle("职能",null),
			pfteamname: commonLogic.appcommonhandle("班组名称",null),
			teaminfo: commonLogic.appcommonhandle("班组信息",null),
			teamcapacity: commonLogic.appcommonhandle("能力",null),
		},
			views: {
				gridview: {
					caption: commonLogic.appcommonhandle("班组",null),
					title: commonLogic.appcommonhandle("班组",null),
				},
				editview_editmode: {
					caption: commonLogic.appcommonhandle("班组",null),
					title: commonLogic.appcommonhandle("班组",null),
				},
				pickupgridview: {
					caption: commonLogic.appcommonhandle("班组",null),
					title: commonLogic.appcommonhandle("班组选择表格视图",null),
				},
				pickupview: {
					caption: commonLogic.appcommonhandle("班组",null),
					title: commonLogic.appcommonhandle("班组数据选择视图",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("班组",null),
					title: commonLogic.appcommonhandle("班组",null),
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("PF班组信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("班组标识",null), 
					srfmajortext: commonLogic.appcommonhandle("班组名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					teamcode: commonLogic.appcommonhandle("班组代码",null), 
					pfteamname: commonLogic.appcommonhandle("班组名称",null), 
					teamtypeid: commonLogic.appcommonhandle("班组类型",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					teamfn: commonLogic.appcommonhandle("职能",null), 
					teamcapacity: commonLogic.appcommonhandle("能力",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					pfteamid: commonLogic.appcommonhandle("班组标识",null), 
				},
				uiactions: {
				},
			},
			main3_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("PF班组信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("班组标识",null), 
					srfmajortext: commonLogic.appcommonhandle("班组名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					teamcode: commonLogic.appcommonhandle("班组代码",null), 
					pfteamname: commonLogic.appcommonhandle("班组名称",null), 
					teamtypeid: commonLogic.appcommonhandle("班组类型",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					teamfn: commonLogic.appcommonhandle("职能",null), 
					teamcapacity: commonLogic.appcommonhandle("能力",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					pfteamid: commonLogic.appcommonhandle("班组标识",null), 
				},
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					teamcode: commonLogic.appcommonhandle("班组代码",null),
					pfteamname: commonLogic.appcommonhandle("班组名称",null),
					teamtypeid: commonLogic.appcommonhandle("班组类型",null),
					orgid: commonLogic.appcommonhandle("组织",null),
					teamcapacity: commonLogic.appcommonhandle("能力",null),
					teamfn: commonLogic.appcommonhandle("职能",null),
					description: commonLogic.appcommonhandle("描述",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
				},
				uiactions: {
				},
			},
			editviewtoolbar_toolbar: {
				deuiaction1: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("删除",null),
					tip: commonLogic.appcommonhandle("删除",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("过滤",null),
					tip: commonLogic.appcommonhandle("过滤",null),
				},
			},
			editview_editmodetoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("保存并关闭",null),
					tip: commonLogic.appcommonhandle("保存并关闭",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
		};
		return data;
}
export default getLocaleResourceBase;