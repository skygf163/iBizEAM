import PFTeam_zh_CN_Base from './pfteam_zh_CN_base';

function getLocaleResource(){
    const PFTeam_zh_CN_OwnData = {};
    const targetData = Object.assign(PFTeam_zh_CN_Base(), PFTeam_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;