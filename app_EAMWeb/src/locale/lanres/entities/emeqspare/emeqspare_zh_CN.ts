import EMEQSpare_zh_CN_Base from './emeqspare_zh_CN_base';

function getLocaleResource(){
    const EMEQSpare_zh_CN_OwnData = {};
    const targetData = Object.assign(EMEQSpare_zh_CN_Base(), EMEQSpare_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;