import EMEQSpare_en_US_Base from './emeqspare_en_US_base';

function getLocaleResource(){
    const EMEQSpare_en_US_OwnData = {};
    const targetData = Object.assign(EMEQSpare_en_US_Base(), EMEQSpare_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
