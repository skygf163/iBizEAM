import PLANSCHEDULE_O_en_US_Base from './planschedule-o_en_US_base';

function getLocaleResource(){
    const PLANSCHEDULE_O_en_US_OwnData = {};
    const targetData = Object.assign(PLANSCHEDULE_O_en_US_Base(), PLANSCHEDULE_O_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
