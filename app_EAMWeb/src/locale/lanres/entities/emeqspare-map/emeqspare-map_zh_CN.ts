import EMEQSpareMap_zh_CN_Base from './emeqspare-map_zh_CN_base';

function getLocaleResource(){
    const EMEQSpareMap_zh_CN_OwnData = {};
    const targetData = Object.assign(EMEQSpareMap_zh_CN_Base(), EMEQSpareMap_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;