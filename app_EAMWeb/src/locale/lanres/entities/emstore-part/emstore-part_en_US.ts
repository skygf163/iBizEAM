import EMStorePart_en_US_Base from './emstore-part_en_US_base';

function getLocaleResource(){
    const EMStorePart_en_US_OwnData = {};
    const targetData = Object.assign(EMStorePart_en_US_Base(), EMStorePart_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
