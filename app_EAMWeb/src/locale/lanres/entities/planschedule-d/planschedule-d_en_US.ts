import PLANSCHEDULE_D_en_US_Base from './planschedule-d_en_US_base';

function getLocaleResource(){
    const PLANSCHEDULE_D_en_US_OwnData = {};
    const targetData = Object.assign(PLANSCHEDULE_D_en_US_Base(), PLANSCHEDULE_D_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
