import EMItemPL_en_US_Base from './emitem-pl_en_US_base';

function getLocaleResource(){
    const EMItemPL_en_US_OwnData = {};
    const targetData = Object.assign(EMItemPL_en_US_Base(), EMItemPL_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
