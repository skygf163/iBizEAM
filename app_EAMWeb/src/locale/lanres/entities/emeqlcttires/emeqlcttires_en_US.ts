import EMEQLCTTIRes_en_US_Base from './emeqlcttires_en_US_base';

function getLocaleResource(){
    const EMEQLCTTIRes_en_US_OwnData = {};
    const targetData = Object.assign(EMEQLCTTIRes_en_US_Base(), EMEQLCTTIRes_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
