import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("轮胎位置", null),
		fields: {
			createman: commonLogic.appcommonhandle("建立人",null),
			tiresstate: commonLogic.appcommonhandle("轮胎状态",null),
			description: commonLogic.appcommonhandle("描述",null),
			par: commonLogic.appcommonhandle("使用气压",null),
			amount: commonLogic.appcommonhandle("价格",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			picparams: commonLogic.appcommonhandle("图形8*8=11-88",null),
			valve: commonLogic.appcommonhandle("预警期限(天)",null),
			replacedate: commonLogic.appcommonhandle("更换时间",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			eqmodelcode: commonLogic.appcommonhandle("型号",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			newoldflag: commonLogic.appcommonhandle("新旧标志",null),
			changp: commonLogic.appcommonhandle("厂牌",null),
			systemparam: commonLogic.appcommonhandle("材质层数",null),
			replacereason: commonLogic.appcommonhandle("更换原因",null),
			lctdesc: commonLogic.appcommonhandle("轮胎备注",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			tirestype: commonLogic.appcommonhandle("轮胎车型",null),
			lcttiresinfo: commonLogic.appcommonhandle("轮胎信息",null),
			haveinner: commonLogic.appcommonhandle("有内胎",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			mccode: commonLogic.appcommonhandle("出厂编号",null),
			labservicename: commonLogic.appcommonhandle("供应商",null),
			mservicename: commonLogic.appcommonhandle("制造商",null),
			eqlocationinfo: commonLogic.appcommonhandle("位置信息",null),
			equipname: commonLogic.appcommonhandle("设备",null),
			equipid: commonLogic.appcommonhandle("设备",null),
			emeqlocationid: commonLogic.appcommonhandle("位置标识",null),
			labserviceid: commonLogic.appcommonhandle("供应商",null),
			mserviceid: commonLogic.appcommonhandle("制造商",null),
		},
			views: {
				pickupgridview: {
					caption: commonLogic.appcommonhandle("轮胎位置",null),
					title: commonLogic.appcommonhandle("轮胎位置选择表格视图",null),
				},
				pickupview: {
					caption: commonLogic.appcommonhandle("轮胎位置",null),
					title: commonLogic.appcommonhandle("轮胎位置数据选择视图",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("轮胎位置",null),
					title: commonLogic.appcommonhandle("轮胎位置",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("轮胎位置",null),
					title: commonLogic.appcommonhandle("轮胎位置",null),
				},
			},
			main_form: {
				details: {
					group1: commonLogic.appcommonhandle("轮胎位置基本信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					group2: commonLogic.appcommonhandle("操作信息",null), 
					formpage2: commonLogic.appcommonhandle("其它",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("位置标识",null), 
					srfmajortext: commonLogic.appcommonhandle("位置信息",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					lcttiresinfo: commonLogic.appcommonhandle("轮胎信息",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					emeqlocationid: commonLogic.appcommonhandle("位置标识",null), 
				},
				uiactions: {
				},
			},
			main_grid: {
				columns: {
					lcttiresinfo: commonLogic.appcommonhandle("轮胎信息",null),
					updateman: commonLogic.appcommonhandle("更新人",null),
					updatedate: commonLogic.appcommonhandle("更新时间",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					lcttiresinfo: commonLogic.appcommonhandle("轮胎信息",null),
					equipname: commonLogic.appcommonhandle("设备",null),
					newoldflag: commonLogic.appcommonhandle("新旧标志",null),
					tiresstate: commonLogic.appcommonhandle("轮胎状态",null),
					haveinner: commonLogic.appcommonhandle("有内胎",null),
					eqmodelcode: commonLogic.appcommonhandle("型号",null),
					changp: commonLogic.appcommonhandle("厂牌",null),
					systemparam: commonLogic.appcommonhandle("材质层数",null),
					par: commonLogic.appcommonhandle("使用气压",null),
					amount: commonLogic.appcommonhandle("价格",null),
					mccode: commonLogic.appcommonhandle("出厂编号",null),
					labservicename: commonLogic.appcommonhandle("供应商",null),
					mservicename: commonLogic.appcommonhandle("制造商",null),
					replacereason: commonLogic.appcommonhandle("更换原因",null),
					replacedate: commonLogic.appcommonhandle("更换时间",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
				},
				uiactions: {
				},
			},
			editviewtoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("保存并关闭",null),
					tip: commonLogic.appcommonhandle("保存并关闭",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("删除",null),
					tip: commonLogic.appcommonhandle("删除",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("过滤",null),
					tip: commonLogic.appcommonhandle("过滤",null),
				},
			},
		};
		return data;
}
export default getLocaleResourceBase;