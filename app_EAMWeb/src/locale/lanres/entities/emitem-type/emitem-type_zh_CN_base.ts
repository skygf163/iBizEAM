import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("物品类型", null),
		fields: {
			emitemtypeid: commonLogic.appcommonhandle("物品类型标识",null),
			description: commonLogic.appcommonhandle("描述",null),
			itemtypecode: commonLogic.appcommonhandle("物品类型代码",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			emitemtypename: commonLogic.appcommonhandle("物品类型名称",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			itemtypeinfo: commonLogic.appcommonhandle("物品类型信息",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			itemtypepcode: commonLogic.appcommonhandle("上级类型代码",null),
			itembtypename: commonLogic.appcommonhandle("一级类",null),
			itemmtypename: commonLogic.appcommonhandle("二级类",null),
			itemtypepname: commonLogic.appcommonhandle("上级类型",null),
			itemtypepid: commonLogic.appcommonhandle("上级类型",null),
			itemmtypeid: commonLogic.appcommonhandle("二级类",null),
			itembtypeid: commonLogic.appcommonhandle("一级类",null),
		},
			views: {
				gridexpview: {
					caption: commonLogic.appcommonhandle("物品类型",null),
					title: commonLogic.appcommonhandle("物品类型表格导航视图",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("物品类型",null),
					title: commonLogic.appcommonhandle("物品类型",null),
				},
				infotreeexpview: {
					caption: commonLogic.appcommonhandle("物品",null),
					title: commonLogic.appcommonhandle("物品",null),
				},
				treeexpview: {
					caption: commonLogic.appcommonhandle("物品类型",null),
					title: commonLogic.appcommonhandle("物品类型树导航视图",null),
				},
				editview_itemtype_editmode: {
					caption: commonLogic.appcommonhandle("物品类型",null),
					title: commonLogic.appcommonhandle("物品类型",null),
				},
				pickuptreeview: {
					caption: commonLogic.appcommonhandle("物品类型",null),
					title: commonLogic.appcommonhandle("物品类型选择树视图",null),
				},
				pickupgridview: {
					caption: commonLogic.appcommonhandle("物品类型",null),
					title: commonLogic.appcommonhandle("物品类型选择表格视图",null),
				},
				pickupview: {
					caption: commonLogic.appcommonhandle("物品类型",null),
					title: commonLogic.appcommonhandle("物品类型数据选择视图",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("物品类型",null),
					title: commonLogic.appcommonhandle("物品类型",null),
				},
				itemtreeexpview: {
					caption: commonLogic.appcommonhandle("物品",null),
					title: commonLogic.appcommonhandle("物品",null),
				},
				editview_itemtype: {
					caption: commonLogic.appcommonhandle("物品类型",null),
					title: commonLogic.appcommonhandle("物品类型",null),
				},
				editview_editmode: {
					caption: commonLogic.appcommonhandle("物品类型",null),
					title: commonLogic.appcommonhandle("物品类型",null),
				},
				optionview: {
					caption: commonLogic.appcommonhandle("物品类型",null),
					title: commonLogic.appcommonhandle("物品类型选项操作视图",null),
				},
				treepickupview: {
					caption: commonLogic.appcommonhandle("物品类型",null),
					title: commonLogic.appcommonhandle("物品类型数据选择视图",null),
				},
			},
			main3_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("物品类型信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("物品类型标识",null), 
					srfmajortext: commonLogic.appcommonhandle("物品类型名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					itemtypecode: commonLogic.appcommonhandle("物品类型代码",null), 
					emitemtypename: commonLogic.appcommonhandle("物品类型名称",null), 
					itemtypepname: commonLogic.appcommonhandle("上级类型",null), 
					itembtypename: commonLogic.appcommonhandle("一级类",null), 
					itemmtypename: commonLogic.appcommonhandle("二级类",null), 
					itemmtypeid: commonLogic.appcommonhandle("二级类",null), 
					itemtypepid: commonLogic.appcommonhandle("上级类型",null), 
					emitemtypeid: commonLogic.appcommonhandle("物品类型标识",null), 
					itembtypeid: commonLogic.appcommonhandle("一级类",null), 
				},
				uiactions: {
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("物品类型信息",null), 
					grouppanel8: commonLogic.appcommonhandle("操作信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("物品类型标识",null), 
					srfmajortext: commonLogic.appcommonhandle("物品类型名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					itemtypecode: commonLogic.appcommonhandle("物品类型代码",null), 
					emitemtypename: commonLogic.appcommonhandle("物品类型名称",null), 
					itemtypepname: commonLogic.appcommonhandle("上级类型",null), 
					itembtypename: commonLogic.appcommonhandle("一级类",null), 
					itemmtypename: commonLogic.appcommonhandle("二级类",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					emitemtypeid: commonLogic.appcommonhandle("物品类型标识",null), 
				},
				uiactions: {
				},
			},
			main_grid: {
				columns: {
					itemtypecode: commonLogic.appcommonhandle("物品类型代码",null),
					emitemtypename: commonLogic.appcommonhandle("物品类型名称",null),
					itemtypepcode: commonLogic.appcommonhandle("上级类型代码",null),
					itemtypepname: commonLogic.appcommonhandle("上级类型",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			pickup_grid: {
				columns: {
					itemtypecode: commonLogic.appcommonhandle("物品类型代码",null),
					emitemtypename: commonLogic.appcommonhandle("物品类型名称",null),
					itemtypepname: commonLogic.appcommonhandle("上级类型",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			nav_grid: {
				columns: {
					itemtypecode: commonLogic.appcommonhandle("物品类型代码",null),
					emitemtypename: commonLogic.appcommonhandle("物品类型名称",null),
					itemtypepname: commonLogic.appcommonhandle("上级类型",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
					n_emitemtypename_like: commonLogic.appcommonhandle("物品类型名称(文本包含(%))",null), 
					n_itemtypepid_eq: commonLogic.appcommonhandle("上级类型(等于(=))",null), 
				},
				uiactions: {
				},
			},
			editview_editmodetoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("保存",null),
					tip: commonLogic.appcommonhandle("保存",null),
				},
				deuiaction1: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("删除",null),
					tip: commonLogic.appcommonhandle("删除",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("过滤",null),
					tip: commonLogic.appcommonhandle("过滤",null),
				},
			},
			editview_itemtypetoolbar_toolbar: {
				deuiaction1: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
			},
			editview_itemtype_editmodetoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("保存并关闭",null),
					tip: commonLogic.appcommonhandle("保存并关闭",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			editviewtoolbar_toolbar: {
				deuiaction1: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
			},
			itemtypetree_treeview: {
				nodata:commonLogic.appcommonhandle("",null),
				nodes: {
					subitem: commonLogic.appcommonhandle("下属物品",null),
					root: commonLogic.appcommonhandle("默认根节点",null),
					itemtype: commonLogic.appcommonhandle("物品类型",null),
					subtype: commonLogic.appcommonhandle("下属类型",null),
				},
				uiactions: {
				},
			},
			info_treeview: {
				nodata:commonLogic.appcommonhandle("",null),
				nodes: {
					itemtype: commonLogic.appcommonhandle("全部物品类型",null),
					root: commonLogic.appcommonhandle("默认根节点",null),
				},
				uiactions: {
					emitemtype_new: commonLogic.appcommonhandle("添加类型",null),
				},
			},
			itemtype_treeview: {
				nodata:commonLogic.appcommonhandle("",null),
				nodes: {
					root: commonLogic.appcommonhandle("默认根节点",null),
					allitem: commonLogic.appcommonhandle("全部物品",null),
				},
				uiactions: {
				},
			},
		};
		return data;
}
export default getLocaleResourceBase;