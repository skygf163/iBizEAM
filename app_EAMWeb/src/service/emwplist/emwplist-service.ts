import { Http } from '@/utils';
import { Util } from '@/utils';
import EMWPListServiceBase from './emwplist-service-base';


/**
 * 采购申请服务对象
 *
 * @export
 * @class EMWPListService
 * @extends {EMWPListServiceBase}
 */
export default class EMWPListService extends EMWPListServiceBase {

    /**
     * Creates an instance of  EMWPListService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWPListService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}