import { Http } from '@/utils';
import { Util } from '@/utils';
import EMEQMaintanceServiceBase from './emeqmaintance-service-base';


/**
 * 抢修记录服务对象
 *
 * @export
 * @class EMEQMaintanceService
 * @extends {EMEQMaintanceServiceBase}
 */
export default class EMEQMaintanceService extends EMEQMaintanceServiceBase {

    /**
     * Creates an instance of  EMEQMaintanceService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQMaintanceService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}