import { Http } from '@/utils';
import { Util } from '@/utils';
import EMEQKeepServiceBase from './emeqkeep-service-base';


/**
 * 维护保养服务对象
 *
 * @export
 * @class EMEQKeepService
 * @extends {EMEQKeepServiceBase}
 */
export default class EMEQKeepService extends EMEQKeepServiceBase {

    /**
     * Creates an instance of  EMEQKeepService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQKeepService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}