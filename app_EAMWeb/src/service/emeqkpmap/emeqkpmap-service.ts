import { Http } from '@/utils';
import { Util } from '@/utils';
import EMEQKPMapServiceBase from './emeqkpmap-service-base';


/**
 * 设备关键点关系服务对象
 *
 * @export
 * @class EMEQKPMapService
 * @extends {EMEQKPMapServiceBase}
 */
export default class EMEQKPMapService extends EMEQKPMapServiceBase {

    /**
     * Creates an instance of  EMEQKPMapService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQKPMapService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}