import { Http } from '@/utils';
import { Util } from '@/utils';
import EMRFODETypeServiceBase from './emrfodetype-service-base';


/**
 * 现象分类服务对象
 *
 * @export
 * @class EMRFODETypeService
 * @extends {EMRFODETypeServiceBase}
 */
export default class EMRFODETypeService extends EMRFODETypeServiceBase {

    /**
     * Creates an instance of  EMRFODETypeService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMRFODETypeService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}