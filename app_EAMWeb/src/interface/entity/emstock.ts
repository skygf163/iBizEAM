/**
 * 库存
 *
 * @export
 * @interface EMStock
 */
export interface EMStock {

    /**
     * 批次
     *
     * @returns {*}
     * @memberof EMStock
     */
    batcode?: any;

    /**
     * 库存标识
     *
     * @returns {*}
     * @memberof EMStock
     */
    emstockid?: any;

    /**
     * 库存货架管理
     *
     * @returns {*}
     * @memberof EMStock
     */
    storepartgl?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMStock
     */
    updatedate?: any;

    /**
     * 库存信息
     *
     * @returns {*}
     * @memberof EMStock
     */
    stockinfo?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMStock
     */
    createman?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMStock
     */
    orgid?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMStock
     */
    enable?: any;

    /**
     * 库存名称
     *
     * @returns {*}
     * @memberof EMStock
     */
    emstockname?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMStock
     */
    createdate?: any;

    /**
     * 库存金额
     *
     * @returns {*}
     * @memberof EMStock
     */
    amount?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMStock
     */
    description?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMStock
     */
    updateman?: any;

    /**
     * 库存数量
     *
     * @returns {*}
     * @memberof EMStock
     */
    stockcnt?: any;

    /**
     * 库位
     *
     * @returns {*}
     * @memberof EMStock
     */
    storepartname?: any;

    /**
     * 物品一级类型
     *
     * @returns {*}
     * @memberof EMStock
     */
    itembtypeid?: any;

    /**
     * 仓库
     *
     * @returns {*}
     * @memberof EMStock
     */
    storename?: any;

    /**
     * 平均价
     *
     * @returns {*}
     * @memberof EMStock
     */
    price?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMStock
     */
    itemname?: any;

    /**
     * 物品一级类型
     *
     * @returns {*}
     * @memberof EMStock
     */
    itembtypename?: any;

    /**
     * 库位
     *
     * @returns {*}
     * @memberof EMStock
     */
    storepartid?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMStock
     */
    itemid?: any;

    /**
     * 仓库
     *
     * @returns {*}
     * @memberof EMStock
     */
    storeid?: any;
}