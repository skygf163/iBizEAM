/**
 * 维修记录
 *
 * @export
 * @interface EMEQCheck
 */
export interface EMEQCheck {

    /**
     * 起始时间
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    regionbegindate?: any;

    /**
     * 检修日期
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    activedate?: any;

    /**
     * 详细内容
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    content?: any;

    /**
     * 材料费(￥)
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    mfee?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    enable?: any;

    /**
     * 责任人
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    rempid?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    updateman?: any;

    /**
     * 预算(￥)
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    prefee?: any;

    /**
     * 检修结果
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    activeadesc?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    orgid?: any;

    /**
     * 结束时间
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    regionenddate?: any;

    /**
     * 检修记录标识
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    emeqcheckid?: any;

    /**
     * 人工费(￥)
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    pfee?: any;

    /**
     * 检修记录名称
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    emeqcheckname?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    createman?: any;

    /**
     * 持续时间(H)
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    activelengths?: any;

    /**
     * 责任人
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    rempname?: any;

    /**
     * 检修原因
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    activebdesc?: any;

    /**
     * 服务费(￥)
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    sfee?: any;

    /**
     * 检修记录
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    activedesc?: any;

    /**
     * 停运时间(分)
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    eqstoplength?: any;

    /**
     * 责任部门
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    rdeptid?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    updatedate?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    description?: any;

    /**
     * 责任部门
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    rdeptname?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    createdate?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    objname?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    rservicename?: any;

    /**
     * 方案
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    rfoacname?: any;

    /**
     * 责任班组
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    rteamname?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    acclassname?: any;

    /**
     * 工单
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    woname?: any;

    /**
     * 设备分组
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    equipgroup?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    equipname?: any;

    /**
     * 原因
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    rfocaname?: any;

    /**
     * 模式
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    rfomoname?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    rfodename?: any;

    /**
     * 方案
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    rfoacid?: any;

    /**
     * 责任班组
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    rteamid?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    objid?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    acclassid?: any;

    /**
     * 模式
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    rfomoid?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    equipid?: any;

    /**
     * 原因
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    rfocaid?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    rserviceid?: any;

    /**
     * 工单
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    woid?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMEQCheck
     */
    rfodeid?: any;
}