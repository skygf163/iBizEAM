/**
 * 抢修记录
 *
 * @export
 * @interface EMEQMaintance
 */
export interface EMEQMaintance {

    /**
     * 详细内容
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    content?: any;

    /**
     * 持续时间(H)
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    activelengths?: any;

    /**
     * 维修日期
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    activedate?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    orgid?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    enable?: any;

    /**
     * 维修记录名称
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    emeqmaintancename?: any;

    /**
     * 责任部门
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    rdeptid?: any;

    /**
     * 责任人
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    rempid?: any;

    /**
     * 图片
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    pic1?: any;

    /**
     * 故障分析
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    activebdesc?: any;

    /**
     * 预算(￥)
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    prefee?: any;

    /**
     * 结束时间
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    regionenddate?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    description?: any;

    /**
     * 维修记录
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    activedesc?: any;

    /**
     * 起始时间
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    regionbegindate?: any;

    /**
     * 维修记录标识
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    emeqmaintanceid?: any;

    /**
     * 停运时间(分)
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    eqstoplength?: any;

    /**
     * 服务费(￥)
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    sfee?: any;

    /**
     * 图片
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    pic5?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    updatedate?: any;

    /**
     * 人工费(￥)
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    pfee?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    createman?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    updateman?: any;

    /**
     * 责任部门
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    rdeptname?: any;

    /**
     * 图片
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    pic4?: any;

    /**
     * 材料费(￥)
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    mfee?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    createdate?: any;

    /**
     * 图片
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    pic?: any;

    /**
     * 图片
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    pic3?: any;

    /**
     * 图片
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    pic2?: any;

    /**
     * 维修结果
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    activeadesc?: any;

    /**
     * 责任人
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    rempname?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    rservicename?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    acclassname?: any;

    /**
     * 方案
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    rfoacname?: any;

    /**
     * 模式
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    rfomoname?: any;

    /**
     * 原因
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    rfocaname?: any;

    /**
     * 工单
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    woname?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    equipname?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    objname?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    rfodename?: any;

    /**
     * 责任班组
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    rteamname?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    rfodeid?: any;

    /**
     * 工单
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    woid?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    rserviceid?: any;

    /**
     * 责任班组
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    rteamid?: any;

    /**
     * 原因
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    rfocaid?: any;

    /**
     * 模式
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    rfomoid?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    equipid?: any;

    /**
     * 方案
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    rfoacid?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    objid?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMEQMaintance
     */
    acclassid?: any;
}