/**
 * 关键点记录
 *
 * @export
 * @interface EMEQKPRCD
 */
export interface EMEQKPRCD {

    /**
     * 采集时间
     *
     * @returns {*}
     * @memberof EMEQKPRCD
     */
    edate?: any;

    /**
     * 上次采集时间
     *
     * @returns {*}
     * @memberof EMEQKPRCD
     */
    bdate?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMEQKPRCD
     */
    orgid?: any;

    /**
     * 关键点记录名称
     *
     * @returns {*}
     * @memberof EMEQKPRCD
     */
    emeqkprcdname?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMEQKPRCD
     */
    updateman?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMEQKPRCD
     */
    createdate?: any;

    /**
     * 数值
     *
     * @returns {*}
     * @memberof EMEQKPRCD
     */
    nval?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMEQKPRCD
     */
    updatedate?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMEQKPRCD
     */
    createman?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMEQKPRCD
     */
    enable?: any;

    /**
     * 值
     *
     * @returns {*}
     * @memberof EMEQKPRCD
     */
    val?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMEQKPRCD
     */
    description?: any;

    /**
     * 关键点记录标识
     *
     * @returns {*}
     * @memberof EMEQKPRCD
     */
    emeqkprcdid?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMEQKPRCD
     */
    objname?: any;

    /**
     * 关键点
     *
     * @returns {*}
     * @memberof EMEQKPRCD
     */
    kpname?: any;

    /**
     * 工单
     *
     * @returns {*}
     * @memberof EMEQKPRCD
     */
    woname?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEQKPRCD
     */
    equipname?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMEQKPRCD
     */
    objid?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEQKPRCD
     */
    equipid?: any;

    /**
     * 工单
     *
     * @returns {*}
     * @memberof EMEQKPRCD
     */
    woid?: any;

    /**
     * 关键点
     *
     * @returns {*}
     * @memberof EMEQKPRCD
     */
    kpid?: any;
}