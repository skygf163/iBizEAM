/**
 * 能源
 *
 * @export
 * @interface EMEN
 */
export interface EMEN {

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMEN
     */
    createdate?: any;

    /**
     * 能源备注
     *
     * @returns {*}
     * @memberof EMEN
     */
    energydesc?: any;

    /**
     * 能源标识
     *
     * @returns {*}
     * @memberof EMEN
     */
    emenid?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMEN
     */
    description?: any;

    /**
     * 能源代码
     *
     * @returns {*}
     * @memberof EMEN
     */
    energycode?: any;

    /**
     * 能源类型
     *
     * @returns {*}
     * @memberof EMEN
     */
    energytypeid?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMEN
     */
    updateman?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMEN
     */
    createman?: any;

    /**
     * 倍率
     *
     * @returns {*}
     * @memberof EMEN
     */
    vrate?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMEN
     */
    enable?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMEN
     */
    orgid?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMEN
     */
    updatedate?: any;

    /**
     * 能源单价
     *
     * @returns {*}
     * @memberof EMEN
     */
    price?: any;

    /**
     * 能源名称
     *
     * @returns {*}
     * @memberof EMEN
     */
    emenname?: any;

    /**
     * 能源信息
     *
     * @returns {*}
     * @memberof EMEN
     */
    energyinfo?: any;

    /**
     * 单位
     *
     * @returns {*}
     * @memberof EMEN
     */
    unitname?: any;

    /**
     * 物品二级类
     *
     * @returns {*}
     * @memberof EMEN
     */
    itemmtypeid?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMEN
     */
    itemname?: any;

    /**
     * 物品库存单价
     *
     * @returns {*}
     * @memberof EMEN
     */
    itemprice?: any;

    /**
     * 物品二级类
     *
     * @returns {*}
     * @memberof EMEN
     */
    itemmtypename?: any;

    /**
     * 物品类型
     *
     * @returns {*}
     * @memberof EMEN
     */
    itemtypeid?: any;

    /**
     * 物品大类
     *
     * @returns {*}
     * @memberof EMEN
     */
    itembtypeid?: any;

    /**
     * 物品大类
     *
     * @returns {*}
     * @memberof EMEN
     */
    itembtypename?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMEN
     */
    itemid?: any;
}