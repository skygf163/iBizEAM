/**
 * 人事关系
 *
 * @export
 * @interface PFEmpPostMap
 */
export interface PFEmpPostMap {

    /**
     * 职员
     *
     * @returns {*}
     * @memberof PFEmpPostMap
     */
    empname?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof PFEmpPostMap
     */
    createdate?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof PFEmpPostMap
     */
    updatedate?: any;

    /**
     * 人事关系标识
     *
     * @returns {*}
     * @memberof PFEmpPostMap
     */
    pfemppostmapid?: any;

    /**
     * 部门
     *
     * @returns {*}
     * @memberof PFEmpPostMap
     */
    deptid?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof PFEmpPostMap
     */
    createman?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof PFEmpPostMap
     */
    description?: any;

    /**
     * 部门
     *
     * @returns {*}
     * @memberof PFEmpPostMap
     */
    deptname?: any;

    /**
     * 职员
     *
     * @returns {*}
     * @memberof PFEmpPostMap
     */
    empid?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof PFEmpPostMap
     */
    updateman?: any;

    /**
     * 排序值
     *
     * @returns {*}
     * @memberof PFEmpPostMap
     */
    orderflag?: any;

    /**
     * 人事关系名称
     *
     * @returns {*}
     * @memberof PFEmpPostMap
     */
    pfemppostmapname?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof PFEmpPostMap
     */
    enable?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof PFEmpPostMap
     */
    orgid?: any;

    /**
     * 班组
     *
     * @returns {*}
     * @memberof PFEmpPostMap
     */
    teamname?: any;

    /**
     * 岗位
     *
     * @returns {*}
     * @memberof PFEmpPostMap
     */
    postname?: any;

    /**
     * 班组
     *
     * @returns {*}
     * @memberof PFEmpPostMap
     */
    teamid?: any;

    /**
     * 岗位
     *
     * @returns {*}
     * @memberof PFEmpPostMap
     */
    postid?: any;
}