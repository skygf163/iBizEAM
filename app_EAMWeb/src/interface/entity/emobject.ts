/**
 * 对象
 *
 * @export
 * @interface EMObject
 */
export interface EMObject {

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMObject
     */
    updatedate?: any;

    /**
     * 对象信息
     *
     * @returns {*}
     * @memberof EMObject
     */
    objectinfo?: any;

    /**
     * 对象类型
     *
     * @returns {*}
     * @memberof EMObject
     */
    emobjecttype?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMObject
     */
    updateman?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMObject
     */
    createman?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMObject
     */
    enable?: any;

    /**
     * 对象标识
     *
     * @returns {*}
     * @memberof EMObject
     */
    emobjectid?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMObject
     */
    orgid?: any;

    /**
     * 对象代码
     *
     * @returns {*}
     * @memberof EMObject
     */
    objectcode?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMObject
     */
    createdate?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMObject
     */
    description?: any;

    /**
     * 对象名称
     *
     * @returns {*}
     * @memberof EMObject
     */
    emobjectname?: any;

    /**
     * 主设备
     *
     * @returns {*}
     * @memberof EMObject
     */
    majorequipname?: any;

    /**
     * 主设备
     *
     * @returns {*}
     * @memberof EMObject
     */
    majorequipid?: any;
}