/**
 * 计划条件
 *
 * @export
 * @interface EMPlanCDT
 */
export interface EMPlanCDT {

    /**
     * 计划条件标识
     *
     * @returns {*}
     * @memberof EMPlanCDT
     */
    emplancdtid?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMPlanCDT
     */
    createman?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMPlanCDT
     */
    createdate?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMPlanCDT
     */
    enable?: any;

    /**
     * 临界值
     *
     * @returns {*}
     * @memberof EMPlanCDT
     */
    triggerval?: any;

    /**
     * 测点值类型
     *
     * @returns {*}
     * @memberof EMPlanCDT
     */
    dpvaltype?: any;

    /**
     * 上次触发值
     *
     * @returns {*}
     * @memberof EMPlanCDT
     */
    lastval?: any;

    /**
     * 预警值
     *
     * @returns {*}
     * @memberof EMPlanCDT
     */
    nowval?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMPlanCDT
     */
    description?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMPlanCDT
     */
    orgid?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMPlanCDT
     */
    updatedate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMPlanCDT
     */
    updateman?: any;

    /**
     * 触发操作
     *
     * @returns {*}
     * @memberof EMPlanCDT
     */
    triggerdp?: any;

    /**
     * 计划条件名称
     *
     * @returns {*}
     * @memberof EMPlanCDT
     */
    emplancdtname?: any;

    /**
     * 测点类型
     *
     * @returns {*}
     * @memberof EMPlanCDT
     */
    dptype?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMPlanCDT
     */
    objname?: any;

    /**
     * 测点
     *
     * @returns {*}
     * @memberof EMPlanCDT
     */
    dpname?: any;

    /**
     * 计划
     *
     * @returns {*}
     * @memberof EMPlanCDT
     */
    planname?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMPlanCDT
     */
    equipname?: any;

    /**
     * 测点
     *
     * @returns {*}
     * @memberof EMPlanCDT
     */
    dpid?: any;

    /**
     * 计划
     *
     * @returns {*}
     * @memberof EMPlanCDT
     */
    planid?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMPlanCDT
     */
    equipid?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMPlanCDT
     */
    objid?: any;
}