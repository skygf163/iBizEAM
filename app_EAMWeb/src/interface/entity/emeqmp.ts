/**
 * 设备仪表
 *
 * @export
 * @interface EMEQMP
 */
export interface EMEQMP {

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMEQMP
     */
    updateman?: any;

    /**
     * 设备仪表代码
     *
     * @returns {*}
     * @memberof EMEQMP
     */
    mpcode?: any;

    /**
     * 设备仪表名称
     *
     * @returns {*}
     * @memberof EMEQMP
     */
    emeqmpname?: any;

    /**
     * 仪表备注
     *
     * @returns {*}
     * @memberof EMEQMP
     */
    mpdesc?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMEQMP
     */
    createman?: any;

    /**
     * 设备仪表信息
     *
     * @returns {*}
     * @memberof EMEQMP
     */
    mpinfo?: any;

    /**
     * 设备仪表标识
     *
     * @returns {*}
     * @memberof EMEQMP
     */
    emeqmpid?: any;

    /**
     * 正常参考值
     *
     * @returns {*}
     * @memberof EMEQMP
     */
    normalrefval?: any;

    /**
     * 设备仪表类型
     *
     * @returns {*}
     * @memberof EMEQMP
     */
    mptypeid?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMEQMP
     */
    createdate?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMEQMP
     */
    updatedate?: any;

    /**
     * 仪表范围
     *
     * @returns {*}
     * @memberof EMEQMP
     */
    mpscope?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMEQMP
     */
    orgid?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMEQMP
     */
    description?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMEQMP
     */
    enable?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMEQMP
     */
    objname?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEQMP
     */
    equipname?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMEQMP
     */
    objid?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEQMP
     */
    equipid?: any;
}