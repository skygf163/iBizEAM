/**
 * 现象分类
 *
 * @export
 * @interface EMRFODEType
 */
export interface EMRFODEType {

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMRFODEType
     */
    updateman?: any;

    /**
     * 现象分类标识
     *
     * @returns {*}
     * @memberof EMRFODEType
     */
    emrfodetypeid?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMRFODEType
     */
    createdate?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMRFODEType
     */
    orgid?: any;

    /**
     * 现象分类名称
     *
     * @returns {*}
     * @memberof EMRFODEType
     */
    emrfodetypename?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMRFODEType
     */
    description?: any;

    /**
     * 现象分类代码
     *
     * @returns {*}
     * @memberof EMRFODEType
     */
    rfodetypecode?: any;

    /**
     * 现象分类信息
     *
     * @returns {*}
     * @memberof EMRFODEType
     */
    rfodetypeinfo?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMRFODEType
     */
    updatedate?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMRFODEType
     */
    createman?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMRFODEType
     */
    enable?: any;
}