import EMBerthAuthServiceBase from './emberth-auth-service-base';


/**
 * 泊位权限服务对象
 *
 * @export
 * @class EMBerthAuthService
 * @extends {EMBerthAuthServiceBase}
 */
export default class EMBerthAuthService extends EMBerthAuthServiceBase {

    /**
     * Creates an instance of  EMBerthAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMBerthAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}