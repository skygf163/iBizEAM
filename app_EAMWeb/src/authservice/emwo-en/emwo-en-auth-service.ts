import EMWO_ENAuthServiceBase from './emwo-en-auth-service-base';


/**
 * 能耗登记工单权限服务对象
 *
 * @export
 * @class EMWO_ENAuthService
 * @extends {EMWO_ENAuthServiceBase}
 */
export default class EMWO_ENAuthService extends EMWO_ENAuthServiceBase {

    /**
     * Creates an instance of  EMWO_ENAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWO_ENAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}