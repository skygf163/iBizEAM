import EMEQKPMapAuthServiceBase from './emeqkpmap-auth-service-base';


/**
 * 设备关键点关系权限服务对象
 *
 * @export
 * @class EMEQKPMapAuthService
 * @extends {EMEQKPMapAuthServiceBase}
 */
export default class EMEQKPMapAuthService extends EMEQKPMapAuthServiceBase {

    /**
     * Creates an instance of  EMEQKPMapAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQKPMapAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}