import PLANSCHEDULE_DAuthServiceBase from './planschedule-d-auth-service-base';


/**
 * 计划_按天权限服务对象
 *
 * @export
 * @class PLANSCHEDULE_DAuthService
 * @extends {PLANSCHEDULE_DAuthServiceBase}
 */
export default class PLANSCHEDULE_DAuthService extends PLANSCHEDULE_DAuthServiceBase {

    /**
     * Creates an instance of  PLANSCHEDULE_DAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  PLANSCHEDULE_DAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}