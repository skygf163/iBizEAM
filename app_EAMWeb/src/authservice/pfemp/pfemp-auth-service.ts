import PFEmpAuthServiceBase from './pfemp-auth-service-base';


/**
 * 职员权限服务对象
 *
 * @export
 * @class PFEmpAuthService
 * @extends {PFEmpAuthServiceBase}
 */
export default class PFEmpAuthService extends PFEmpAuthServiceBase {

    /**
     * Creates an instance of  PFEmpAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  PFEmpAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}