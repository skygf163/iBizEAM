import PLANSCHEDULE_WAuthServiceBase from './planschedule-w-auth-service-base';


/**
 * 计划_按周权限服务对象
 *
 * @export
 * @class PLANSCHEDULE_WAuthService
 * @extends {PLANSCHEDULE_WAuthServiceBase}
 */
export default class PLANSCHEDULE_WAuthService extends PLANSCHEDULE_WAuthServiceBase {

    /**
     * Creates an instance of  PLANSCHEDULE_WAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  PLANSCHEDULE_WAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}