import EMItemROutAuthServiceBase from './emitem-rout-auth-service-base';


/**
 * 退货单权限服务对象
 *
 * @export
 * @class EMItemROutAuthService
 * @extends {EMItemROutAuthServiceBase}
 */
export default class EMItemROutAuthService extends EMItemROutAuthServiceBase {

    /**
     * Creates an instance of  EMItemROutAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemROutAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}