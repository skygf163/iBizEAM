import EMMachineCategoryAuthServiceBase from './emmachine-category-auth-service-base';


/**
 * 机种编号权限服务对象
 *
 * @export
 * @class EMMachineCategoryAuthService
 * @extends {EMMachineCategoryAuthServiceBase}
 */
export default class EMMachineCategoryAuthService extends EMMachineCategoryAuthServiceBase {

    /**
     * Creates an instance of  EMMachineCategoryAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMMachineCategoryAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}