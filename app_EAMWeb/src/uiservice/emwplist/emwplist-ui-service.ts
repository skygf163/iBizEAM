import EMWPListUIServiceBase from './emwplist-ui-service-base';

/**
 * 采购申请UI服务对象
 *
 * @export
 * @class EMWPListUIService
 */
export default class EMWPListUIService extends EMWPListUIServiceBase {

    /**
     * Creates an instance of  EMWPListUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWPListUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}