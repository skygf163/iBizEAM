import EMApplyUIServiceBase from './emapply-ui-service-base';

/**
 * 外委申请UI服务对象
 *
 * @export
 * @class EMApplyUIService
 */
export default class EMApplyUIService extends EMApplyUIServiceBase {

    /**
     * Creates an instance of  EMApplyUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMApplyUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}