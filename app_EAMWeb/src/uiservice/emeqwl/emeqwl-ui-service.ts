import EMEQWLUIServiceBase from './emeqwl-ui-service-base';

/**
 * 设备运行日志UI服务对象
 *
 * @export
 * @class EMEQWLUIService
 */
export default class EMEQWLUIService extends EMEQWLUIServiceBase {

    /**
     * Creates an instance of  EMEQWLUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQWLUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}