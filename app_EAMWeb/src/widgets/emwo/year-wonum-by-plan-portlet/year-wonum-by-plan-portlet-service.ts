import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * YearWONumByPlan 部件服务对象
 *
 * @export
 * @class YearWONumByPlanService
 */
export default class YearWONumByPlanService extends ControlService {
}
