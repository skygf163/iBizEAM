/**
 * DrTree 部件模型
 *
 * @export
 * @class DrTreeModel
 */
export default class DrTreeModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof DrTreeModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'bpersonid',
      },
      {
        name: 'createman',
      },
      {
        name: 'content',
      },
      {
        name: 'rempid',
      },
      {
        name: 'drwgcode',
      },
      {
        name: 'createdate',
      },
      {
        name: 'updateman',
      },
      {
        name: 'rempname',
      },
      {
        name: 'efilecontent',
      },
      {
        name: 'description',
      },
      {
        name: 'drwgtype',
      },
      {
        name: 'enable',
      },
      {
        name: 'lct',
      },
      {
        name: 'emdrwgname',
      },
      {
        name: 'drwgstate',
      },
      {
        name: 'orgid',
      },
      {
        name: 'emdrwg',
        prop: 'emdrwgid',
      },
      {
        name: 'drwginfo',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'bpersonname',
      },
      {
        name: 'deptid',
      },
    ]
  }


}