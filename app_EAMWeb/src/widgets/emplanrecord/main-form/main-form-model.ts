/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof MainModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emplanrecordid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emplanrecordname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'emplanname',
        prop: 'emplanname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'triggerdate',
        prop: 'triggerdate',
        dataType: 'DATETIME',
      },
      {
        name: 'istrigger',
        prop: 'istrigger',
        dataType: 'YESNO',
      },
      {
        name: 'triggerca',
        prop: 'triggerca',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'triggerre',
        prop: 'triggerre',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'remark',
        prop: 'remark',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'emplanid',
        prop: 'emplanid',
        dataType: 'PICKUP',
      },
      {
        name: 'emplanrecordid',
        prop: 'emplanrecordid',
        dataType: 'GUID',
      },
      {
        name: 'emplanrecord',
        prop: 'emplanrecordid',
        dataType: 'FONTKEY',
      },
    ]
  }

}