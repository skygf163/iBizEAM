/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'wplistcostresult',
          prop: 'wplistcostresult',
          dataType: 'TEXT',
        },
        {
          name: 'itemname',
          prop: 'itemname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'labservicename',
          prop: 'labservicename',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'wplistcosteval',
          prop: 'wplistcosteval',
          dataType: 'FLOAT',
        },
        {
          name: 'price',
          prop: 'price',
          dataType: 'FLOAT',
        },
        {
          name: 'taxrate',
          prop: 'taxrate',
          dataType: 'FLOAT',
        },
        {
          name: 'unitname',
          prop: 'unitname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'unitrate',
          prop: 'unitrate',
          dataType: 'FLOAT',
        },
        {
          name: 'intunitflag',
          prop: 'intunitflag',
          dataType: 'YESNO',
        },
        {
          name: 'listprice',
          prop: 'listprice',
          dataType: 'FLOAT',
        },
        {
          name: 'discnt',
          prop: 'discnt',
          dataType: 'FLOAT',
        },
        {
          name: 'unitdesc',
          prop: 'unitdesc',
          dataType: 'TEXT',
        },
        {
          name: 'pricecdt',
          prop: 'pricecdt',
          dataType: 'LONGTEXT_1000',
        },
        {
          name: 'rempname',
          prop: 'rempname',
          dataType: 'TEXT',
        },
        {
          name: 'adate',
          prop: 'adate',
          dataType: 'DATETIME',
        },
        {
          name: 'begindate',
          prop: 'begindate',
          dataType: 'DATE',
        },
        {
          name: 'enddate',
          prop: 'enddate',
          dataType: 'DATE',
        },
        {
          name: 'itemdesc',
          prop: 'itemdesc',
          dataType: 'LONGTEXT_1000',
        },
        {
          name: 'labserviceid',
          prop: 'labserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'itemid',
          prop: 'itemid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'emwplistcostname',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'emwplistcostid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'emwplistcostid',
          dataType: 'GUID',
        },
        {
          name: 'unitid',
          prop: 'unitid',
          dataType: 'PICKUP',
        },
        {
          name: 'wplistid',
          prop: 'wplistid',
          dataType: 'PICKUP',
        },
        {
          name: 'emwplistcost',
          prop: 'emwplistcostid',
        },
        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}