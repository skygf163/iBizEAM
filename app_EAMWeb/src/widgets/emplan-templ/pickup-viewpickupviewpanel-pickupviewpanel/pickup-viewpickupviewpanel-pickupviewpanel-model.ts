/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'orgid',
      },
      {
        name: 'plantemplinfo',
      },
      {
        name: 'mpersonname',
      },
      {
        name: 'emplantemplname',
      },
      {
        name: 'rempname',
      },
      {
        name: 'prefee',
      },
      {
        name: 'activelengths',
      },
      {
        name: 'updateman',
      },
      {
        name: 'plandesc',
      },
      {
        name: 'mpersonid',
      },
      {
        name: 'plantype',
      },
      {
        name: 'mdate',
      },
      {
        name: 'recvpersonid',
      },
      {
        name: 'eqstoplength',
      },
      {
        name: 'emplantempl',
        prop: 'emplantemplid',
      },
      {
        name: 'mtflag',
      },
      {
        name: 'content',
      },
      {
        name: 'emwotype',
      },
      {
        name: 'plancvl',
      },
      {
        name: 'archive',
      },
      {
        name: 'enable',
      },
      {
        name: 'createman',
      },
      {
        name: 'rdeptname',
      },
      {
        name: 'description',
      },
      {
        name: 'rempid',
      },
      {
        name: 'rdeptid',
      },
      {
        name: 'recvpersonname',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'createdate',
      },
      {
        name: 'planstate',
      },
      {
        name: 'rservicename',
      },
      {
        name: 'rteamname',
      },
      {
        name: 'acclassname',
      },
      {
        name: 'acclassid',
      },
      {
        name: 'rserviceid',
      },
      {
        name: 'rteamid',
      },
    ]
  }


}