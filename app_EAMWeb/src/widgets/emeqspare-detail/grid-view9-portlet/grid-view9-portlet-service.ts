import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * GridView9 部件服务对象
 *
 * @export
 * @class GridView9Service
 */
export default class GridView9Service extends ControlService {
}
