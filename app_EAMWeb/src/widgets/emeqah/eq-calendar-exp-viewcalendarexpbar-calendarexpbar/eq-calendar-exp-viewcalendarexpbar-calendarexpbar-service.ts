import { Http } from '@/utils';
import { Util, Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import EMEQAHService from '@/service/emeqah/emeqah-service';
import EqCalendarExpViewcalendarexpbarModel from './eq-calendar-exp-viewcalendarexpbar-calendarexpbar-model';


/**
 * EqCalendarExpViewcalendarexpbar 部件服务对象
 *
 * @export
 * @class EqCalendarExpViewcalendarexpbarService
 */
export default class EqCalendarExpViewcalendarexpbarService extends ControlService {

    /**
     * 活动历史服务对象
     *
     * @type {EMEQAHService}
     * @memberof EqCalendarExpViewcalendarexpbarService
     */
    public appEntityService: EMEQAHService = new EMEQAHService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof EqCalendarExpViewcalendarexpbarService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of EqCalendarExpViewcalendarexpbarService.
     * 
     * @param {*} [opts={}]
     * @memberof EqCalendarExpViewcalendarexpbarService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new EqCalendarExpViewcalendarexpbarModel();
    }

}