/**
 * Main4 部件模型
 *
 * @export
 * @class Main4Model
 */
export default class Main4Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main4Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emwo_innerid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emwo_innername',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'equipname',
        prop: 'equipname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'objname',
        prop: 'objname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'emwo_innername',
        prop: 'emwo_innername',
        dataType: 'TEXT',
      },
      {
        name: 'emwo_innerid',
        prop: 'emwo_innerid',
        dataType: 'GUID',
      },
      {
        name: 'wogroup',
        prop: 'wogroup',
        dataType: 'SSCODELIST',
      },
      {
        name: 'activelengths',
        prop: 'activelengths',
        dataType: 'FLOAT',
      },
      {
        name: 'wotype',
        prop: 'wotype',
        dataType: 'SSCODELIST',
      },
      {
        name: 'emeqlcttiresname',
        prop: 'emeqlcttiresname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'closeflag',
        prop: 'closeflag',
        dataType: 'YESNO',
      },
      {
        name: 'emeqlctgssname',
        prop: 'emeqlctgssname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'emeitiresname',
        prop: 'emeitiresname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'priority',
        prop: 'priority',
        dataType: 'SSCODELIST',
      },
      {
        name: 'yxcb',
        prop: 'yxcb',
        dataType: 'TEXT',
      },
      {
        name: 'archive',
        prop: 'archive',
        dataType: 'SMCODELIST',
      },
      {
        name: 'wodesc',
        prop: 'wodesc',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'wodate',
        prop: 'wodate',
        dataType: 'DATETIME',
      },
      {
        name: 'regionbegindate',
        prop: 'regionbegindate',
        dataType: 'DATETIME',
      },
      {
        name: 'regionenddate',
        prop: 'regionenddate',
        dataType: 'DATETIME',
      },
      {
        name: 'eqstoplength',
        prop: 'eqstoplength',
        dataType: 'FLOAT',
      },
      {
        name: 'waitmodi',
        prop: 'waitmodi',
        dataType: 'FLOAT',
      },
      {
        name: 'waitbuy',
        prop: 'waitbuy',
        dataType: 'FLOAT',
      },
      {
        name: 'worklength',
        prop: 'worklength',
        dataType: 'FLOAT',
      },
      {
        name: 'wresult',
        prop: 'wresult',
        dataType: 'TEXT',
      },
      {
        name: 'mfee',
        prop: 'mfee',
        dataType: 'FLOAT',
      },
      {
        name: 'cplanflag',
        prop: 'cplanflag',
        dataType: 'YESNO',
      },
      {
        name: 'rempid',
        prop: 'rempid',
        dataType: 'PICKUP',
      },
      {
        name: 'rempname',
        prop: 'rempname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'recvpersonid',
        prop: 'recvpersonid',
        dataType: 'PICKUP',
      },
      {
        name: 'recvpersonname',
        prop: 'recvpersonname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'wpersonid',
        prop: 'wpersonid',
        dataType: 'PICKUP',
      },
      {
        name: 'wpersonname',
        prop: 'wpersonname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rdeptname',
        prop: 'rdeptname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rteamname',
        prop: 'rteamname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'mpersonid',
        prop: 'mpersonid',
        dataType: 'PICKUP',
      },
      {
        name: 'mpersonname',
        prop: 'mpersonname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'emwo_inner',
        prop: 'emwo_innerid',
        dataType: 'FONTKEY',
      },
    ]
  }

}