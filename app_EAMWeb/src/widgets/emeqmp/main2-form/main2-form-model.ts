/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main2Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emeqmpid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emeqmpname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'mpcode',
        prop: 'mpcode',
        dataType: 'TEXT',
      },
      {
        name: 'emeqmpname',
        prop: 'emeqmpname',
        dataType: 'TEXT',
      },
      {
        name: 'equipname',
        prop: 'equipname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'objname',
        prop: 'objname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'mptypeid',
        prop: 'mptypeid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'normalrefval',
        prop: 'normalrefval',
        dataType: 'TEXT',
      },
      {
        name: 'mpscope',
        prop: 'mpscope',
        dataType: 'TEXT',
      },
      {
        name: 'mpdesc',
        prop: 'mpdesc',
        dataType: 'TEXT',
      },
      {
        name: 'orgid',
        prop: 'orgid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'objid',
        prop: 'objid',
        dataType: 'PICKUP',
      },
      {
        name: 'equipid',
        prop: 'equipid',
        dataType: 'PICKUP',
      },
      {
        name: 'emeqmpid',
        prop: 'emeqmpid',
        dataType: 'GUID',
      },
      {
        name: 'emeqmp',
        prop: 'emeqmpid',
        dataType: 'FONTKEY',
      },
    ]
  }

}