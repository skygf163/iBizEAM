import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * File 部件服务对象
 *
 * @export
 * @class FileService
 */
export default class FileService extends ControlService {
}
