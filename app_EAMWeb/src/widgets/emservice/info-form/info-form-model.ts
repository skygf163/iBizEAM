/**
 * Info 部件模型
 *
 * @export
 * @class InfoModel
 */
export default class InfoModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof InfoModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emserviceid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emservicename',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'servicecode',
        prop: 'servicecode',
        dataType: 'TEXT',
      },
      {
        name: 'emservicename',
        prop: 'emservicename',
        dataType: 'TEXT',
      },
      {
        name: 'range',
        prop: 'range',
        dataType: 'NSCODELIST',
      },
      {
        name: 'servicegroup',
        prop: 'servicegroup',
        dataType: 'NMCODELIST',
      },
      {
        name: 'labservicelevelid',
        prop: 'labservicelevelid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'labservicetypeid',
        prop: 'labservicetypeid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'sums',
        prop: 'sums',
        dataType: 'INT',
      },
      {
        name: 'servicestate',
        prop: 'servicestate',
        dataType: 'SSCODELIST',
      },
      {
        name: 'emserviceid',
        prop: 'emserviceid',
        dataType: 'GUID',
      },
      {
        name: 'emservice',
        prop: 'emserviceid',
        dataType: 'FONTKEY',
      },
    ]
  }

}