import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, PanelControlBase } from '@/studio-core';
import EMPlanDetailService from '@/service/emplan-detail/emplan-detail-service';
import ByPlanService from './by-plan-panel-service';
import EMPlanDetailUIService from '@/uiservice/emplan-detail/emplan-detail-ui-service';
import { PanelDetailModel,PanelRawitemModel,PanelTabPanelModel,PanelTabPageModel,PanelFieldModel,PanelContainerModel,PanelControlModel,PanelUserControlModel,PanelButtonModel } from '@/model/panel-detail';
import ByPlanModel from './by-plan-panel-model';
import CodeListService from "@service/app/codelist-service";

/**
 * dashboard_sysportlet4_list_itempanel部件基类
 *
 * @export
 * @class PanelControlBase
 * @extends {ByPlanPanelBase}
 */
export class ByPlanPanelBase extends PanelControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof ByPlanPanelBase
     */
    protected controlType: string = 'PANEL';

    /**
     * 建构部件服务对象
     *
     * @type {ByPlanService}
     * @memberof ByPlanPanelBase
     */
    public service: ByPlanService = new ByPlanService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMPlanDetailService}
     * @memberof ByPlanPanelBase
     */
    public appEntityService: EMPlanDetailService = new EMPlanDetailService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof ByPlanPanelBase
     */
    protected appDeName: string = 'emplandetail';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof ByPlanPanelBase
     */
    protected appDeLogicName: string = '计划步骤';

    /**
     * 界面UI服务对象
     *
     * @type {EMPlanDetailUIService}
     * @memberof ByPlanBase
     */  
    public appUIService: EMPlanDetailUIService = new EMPlanDetailUIService(this.$store);


    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof ByPlan
     */
    public detailsModel: any = {
        orderflag: new PanelFieldModel({ caption: '', itemType: 'FIELD',visible: true, disabled: false, name: 'orderflag', panel: this })
,
        emplandetailname: new PanelFieldModel({ caption: '', itemType: 'FIELD',visible: true, disabled: false, name: 'emplandetailname', panel: this })
,
        equipname: new PanelFieldModel({ caption: '', itemType: 'FIELD',visible: true, disabled: false, name: 'equipname', panel: this })
,
        objname: new PanelFieldModel({ caption: '', itemType: 'FIELD',visible: true, disabled: false, name: 'objname', panel: this })
,
        recvpersonname: new PanelFieldModel({ caption: '', itemType: 'FIELD',visible: true, disabled: false, name: 'recvpersonname', panel: this })
,
        container1: new PanelContainerModel({ caption: '', itemType: 'CONTAINER',visible: true, disabled: false, name: 'container1', panel: this })
,
    };

    /**
     * 面板逻辑
     *
     * @public
     * @param {{ name: string, newVal: any, oldVal: any }} { name, newVal, oldVal }
     * @memberof ByPlan
     */
    public panelLogic({ name, newVal, oldVal }: { name: string, newVal: any, oldVal: any }): void {
                






    }

    /**
     * 数据模型对象
     *
     * @type {ByPlanModel}
     * @memberof ByPlan
     */
    public dataModel: ByPlanModel = new ByPlanModel();

    /**
     * 界面行为标识数组
     *
     * @type {Array<any>}
     * @memberof ByPlan
     */
    public actionList:Array<any> = [];

    /**
     * 界面行为
     *
     * @param {*} row
     * @param {*} tag
     * @param {*} $event
     * @memberof ByPlan
     */
    public uiAction(row: any, tag: any, $event: any) {
    }
}