import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import EMItemService from '@/service/emitem/emitem-service';
import InfoService from './info-form-service';
import EMItemUIService from '@/uiservice/emitem/emitem-ui-service';
import {
    FormButtonModel,
    FormPageModel,
    FormItemModel,
    FormDRUIPartModel,
    FormPartModel,
    FormGroupPanelModel,
    FormIFrameModel,
    FormRowItemModel,
    FormTabPageModel,
    FormTabPanelModel,
    FormUserControlModel,
} from '@/model/form-detail';

/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {InfoEditFormBase}
 */
export class InfoEditFormBase extends EditFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof InfoEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {InfoService}
     * @memberof InfoEditFormBase
     */
    public service: InfoService = new InfoService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMItemService}
     * @memberof InfoEditFormBase
     */
    public appEntityService: EMItemService = new EMItemService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof InfoEditFormBase
     */
    protected appDeName: string = 'emitem';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof InfoEditFormBase
     */
    protected appDeLogicName: string = '物品';

    /**
     * 界面UI服务对象
     *
     * @type {EMItemUIService}
     * @memberof InfoBase
     */  
    public appUIService: EMItemUIService = new EMItemUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof InfoEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        itemcode: null,
        emitemname: null,
        itemtypename: null,
        itemgroup: null,
        dens: null,
        itemmodelcode: null,
        itemserialcode: null,
        acclassname: null,
        isassetflag: null,
        checkmethod: null,
        itemdesc: null,
        isnew: null,
        emitemid: null,
        emitem: null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof InfoEditFormBase
     */
    public majorMessageField: string = 'emitemname';

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof InfoEditFormBase
     */
    public rules(): any{
        return {
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof InfoBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof InfoEditFormBase
     */
    public detailsModel: any = {
        grouppanel2: new FormGroupPanelModel({ caption: '物品信息', detailType: 'GROUPPANEL', name: 'grouppanel2', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emitem.info_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({
    caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srforikey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfkey: new FormItemModel({
    caption: '物品标识', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srfmajortext: new FormItemModel({
    caption: '物品名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srftempmode: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfuf: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfdeid: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfsourcekey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        itemcode: new FormItemModel({
    caption: '物品代码', detailType: 'FORMITEM', name: 'itemcode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 1,
}),

        emitemname: new FormItemModel({
    caption: '物品名称', detailType: 'FORMITEM', name: 'emitemname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        itemtypename: new FormItemModel({
    caption: '物品类型', detailType: 'FORMITEM', name: 'itemtypename', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        itemgroup: new FormItemModel({
    caption: '物品分组', detailType: 'FORMITEM', name: 'itemgroup', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        dens: new FormItemModel({
    caption: '密度', detailType: 'FORMITEM', name: 'dens', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        itemmodelcode: new FormItemModel({
    caption: '产品型号', detailType: 'FORMITEM', name: 'itemmodelcode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        itemserialcode: new FormItemModel({
    caption: '产品系列号', detailType: 'FORMITEM', name: 'itemserialcode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        acclassname: new FormItemModel({
    caption: '总帐科目', detailType: 'FORMITEM', name: 'acclassname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        isassetflag: new FormItemModel({
    caption: '按资产', detailType: 'FORMITEM', name: 'isassetflag', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        checkmethod: new FormItemModel({
    caption: '验收方法', detailType: 'FORMITEM', name: 'checkmethod', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        itemdesc: new FormItemModel({
    caption: '物品备注', detailType: 'FORMITEM', name: 'itemdesc', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        isnew: new FormItemModel({
    caption: '物品新旧标识', detailType: 'FORMITEM', name: 'isnew', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        emitemid: new FormItemModel({
    caption: '物品标识', detailType: 'FORMITEM', name: 'emitemid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

    };

    /**
     * 新建默认值
     * @memberof InfoEditFormBase
     */
    public createDefault() {                    
        if (this.data.hasOwnProperty('itemgroup')) {
            this.data['itemgroup'] = '1';
        }
        if (this.data.hasOwnProperty('isnew')) {
            this.data['isnew'] = '1';
        }
    }

    /**
     * 面板数据变化处理事件
     * @param {any} item 当前数据
     * @param {any} $event 面板事件数据
     *
     * @memberof InfoBase
     */
    public onPanelDataChange(item:any,$event:any) {
        Object.assign(item, $event, {rowDataState:'update'});
    }
}