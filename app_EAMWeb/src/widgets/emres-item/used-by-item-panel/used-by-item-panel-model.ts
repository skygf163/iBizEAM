/**
 * UsedByItem 部件模型
 *
 * @export
 * @class UsedByItemModel
 */
export default class UsedByItemModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof UsedByItemModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'bdate',
        prop: 'bdate'
      },
      {
        name: 'equipname',
        prop: 'equipname'
      },
      {
        name: 'amount',
        prop: 'amount'
      },
      {
        name: 'snum',
        prop: 'snum'
      }
    ]
  }
}