import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * UsedByItem 部件服务对象
 *
 * @export
 * @class UsedByItemService
 */
export default class UsedByItemService extends ControlService {
}