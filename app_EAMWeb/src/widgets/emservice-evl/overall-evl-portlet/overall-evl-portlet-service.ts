import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * OverallEVL 部件服务对象
 *
 * @export
 * @class OverallEVLService
 */
export default class OverallEVLService extends ControlService {
}
