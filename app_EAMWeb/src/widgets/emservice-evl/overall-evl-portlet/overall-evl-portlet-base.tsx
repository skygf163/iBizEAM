import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, MainControlBase } from '@/studio-core';
import EMServiceEvlService from '@/service/emservice-evl/emservice-evl-service';
import OverallEVLService from './overall-evl-portlet-service';
import EMServiceEvlUIService from '@/uiservice/emservice-evl/emservice-evl-ui-service';
import { Environment } from '@/environments/environment';
import UIService from '@/uiservice/ui-service';

/**
 * dashboard_sysportlet1部件基类
 *
 * @export
 * @class MainControlBase
 * @extends {OverallEVLPortletBase}
 */
export class OverallEVLPortletBase extends MainControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof OverallEVLPortletBase
     */
    protected controlType: string = 'PORTLET';

    /**
     * 建构部件服务对象
     *
     * @type {OverallEVLService}
     * @memberof OverallEVLPortletBase
     */
    public service: OverallEVLService = new OverallEVLService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMServiceEvlService}
     * @memberof OverallEVLPortletBase
     */
    public appEntityService: EMServiceEvlService = new EMServiceEvlService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof OverallEVLPortletBase
     */
    protected appDeName: string = 'emserviceevl';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof OverallEVLPortletBase
     */
    protected appDeLogicName: string = '服务商评估';

    /**
     * 界面UI服务对象
     *
     * @type {EMServiceEvlUIService}
     * @memberof OverallEVLBase
     */  
    public appUIService: EMServiceEvlUIService = new EMServiceEvlUIService(this.$store);


    /**
     * 长度
     *
     * @type {number}
     * @memberof OverallEVL
     */
    @Prop() public height?: number;

    /**
     * 宽度
     *
     * @type {number}
     * @memberof OverallEVL
     */
    @Prop() public width?: number;

    /**
     * 门户部件类型
     *
     * @type {number}
     * @memberof OverallEVLBase
     */
    public portletType: string = 'chart';

    /**
     * 界面行为模型数据
     *
     * @memberof OverallEVLBase
     */
    public uiactionModel: any = {
    }



    /**
     * 是否自适应大小
     *
     * @returns {boolean}
     * @memberof OverallEVLBase
     */
    @Prop({default: false})public isAdaptiveSize!: boolean;

    /**
     * 获取多项数据
     *
     * @returns {any[]}
     * @memberof OverallEVLBase
     */
    public getDatas(): any[] {
        return [];
    }

    /**
     * 获取单项树
     *
     * @returns {*}
     * @memberof OverallEVLBase
     */
    public getData(): any {
        return {};
    }

    /**
     * 获取高度
     *
     * @returns {any[]}
     * @memberof OverallEVLBase
     */
    get getHeight(): any{
        if(!this.$util.isEmpty(this.height) && !this.$util.isNumberNaN(this.height)){
            if(this.height == 0){
                return 'auto';
            } else {
                return this.height+'px';
            }
        } else {
            return '500px';
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof OverallEVLBase
     */
    public created() {
        this.afterCreated();
    }

    /**
     * 执行created后的逻辑
     *
     *  @memberof OverallEVLBase
     */    
    public afterCreated(){
        if (this.viewState) {
            this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }) => {
                if(Object.is(tag, "all-portlet") && Object.is(action,'loadmodel')){
                   this.calcUIActionAuthState(data);
                }
                if (!Object.is(tag, this.name)) {
                    return;
                }
                const refs: any = this.$refs;
                Object.keys(refs).forEach((_name: string) => {
                    this.viewState.next({ tag: _name, action: action, data: data });
                });
            });
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof OverallEVLBase
     */
    public destroyed() {
        this.afterDestroy();
    }

    /**
     * 执行destroyed后的逻辑
     *
     * @memberof OverallEVLBase
     */
    public afterDestroy() {
        if (this.viewStateEvent) {
            this.viewStateEvent.unsubscribe();
        }
    }

    /**
     * 计算界面行为权限
     *
     * @memberof OverallEVLBase
     */
    public calcUIActionAuthState(data:any = {}) {
        //  如果是操作栏，不计算权限
        if(this.portletType && Object.is('actionbar', this.portletType)) {
            return;
        }
        let _this: any = this;
        let uiservice: any = _this.appUIService ? _this.appUIService : new UIService(_this.$store);
        if(_this.uiactionModel){
            ViewTool.calcActionItemAuthState(data,_this.uiactionModel,uiservice);
        }
    }


    /**
     * 刷新
     *
     * @memberof OverallEVLBase
     */
    public refresh(args?: any) {
      this.viewState.next({ tag: 'dashboard_sysportlet1_chart', action: 'refresh', data: args });
    }

}
